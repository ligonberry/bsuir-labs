class Compiler
  class << self
    def compile_node(node)
      handlers = {
        'block' => method(:compile_node),
        'statement' => method(:compile_node),
        'simple_statement' => method(:compile_node),
        'compound_statement' => method(:compile_node),
        'class_statement' => method(:compile_class_statement),
        'function_statement' => method(:compile_function_statement),
        'for_statement' => method(:compile_for_statement),
        'while_statement' => method(:compile_while_statement),
        'if_statement' => method(:compile_if_statement),
        'return_statement' => method(:compile_return_statement),
        'assignment_statement' => method(:compile_assignment_statement),
        'tail' => method(:compile_node),
        'attribute_loading' => method(:compile_attribute_loading),
        'function_call' => method(:compile_function_call),
        'subscription' => method(:compile_subscription),
        'import_statement' => method(:compile_import_statement),
        'expression_statement' => method(:compile_expression_statement),
        'expression' => method(:compile_or_test),
        'expression_list' => method(:compile_node),
        'and_test' => method(:compile_and_test),
        'not_test' => method(:compile_not_test),
        'comparison' => method(:compile_comparison),
        'comp_op' => method(:compile_comp_op),
        'arithmetic_expression' => method(:compile_arithmetic_expression),
        'term' => method(:compile_term),
        'factor' => method(:compile_factor),
        'power' => method(:compile_power),
        'atom_expression' => method(:compile_node),
        'list_creation' => method(:compile_list_creation),
        'brackets_expression' => method(:compile_brackets_expression),
        'atom' => method(:compile_atom),
        'EMPTY' => method(:ignore_node),
        'NEWLINE' => method(:ignore_node),
        'RETURN' => method(:ignore_node),
        'COMMA' => method(:ignore_node),
        'EQUAL' => method(:ignore_node)
      }
      bytecode = []
      if node.is_a?(Array)
        node.each do |sub_node|
          bytecode += compile_node(sub_node)
        end
      else
        node_name, node = node.to_a[0]
        bytecode = handlers[node_name].call(node)
      end
      return bytecode
    end

    def generate_command(name, argument)
      return [name, argument]
    end

    def ignore_node(node)
      return []
    end

    def open_node(node)
      return node.to_a[0]
    end

    def compile(ast)
      return compile_node(ast)
    end

    def compile_function_statement(node)
      function_name = open_node(node[1])[1]
      function_arguments = get_arguments(node[3])
      function_code = {
        'args' => function_arguments,
        'code' => compile_node(node[7])
      }
      bytecode = [generate_command('LOAD_CODE', function_code)]
      bytecode.push(generate_command('LOAD_STRING', function_name))
      bytecode.push(generate_command('MAKE_FUNCTION', nil))
      bytecode.push(generate_command('STORE_NAME', function_name))
      return bytecode
    end

    def compile_class_statement(node)
      bytecode = [generate_command('LOAD_BUILD_CLASS', nil)]
      bases = []
      if node[2].is_a?(Array)
        bases = get_arguments(node[2][1])
      end
      class_code = {
        'args' => bases,
        'code' => compile_node(node[5])
      }
      bytecode += [generate_command('LOAD_CODE', class_code)]
      class_name = open_node(node[1])[1]
      bytecode += [generate_command('LOAD_STRING', class_name)]
      bytecode.push(generate_command('MAKE_FUNCTION', nil))
      bytecode.push(generate_command('CALL_FUNCTION', 1))
      bytecode.push(generate_command('STORE_NAME', class_name))
      return bytecode
    end

    def compile_while_statement(node)
      bytecode = compile_node(node[1])
      while_block = compile_node(node[4])
      while_block.push(generate_command(
        'JUMP',
        -(while_block.size + bytecode.size + 1)
      ))
      bytecode.push(generate_command(
        'JUMP_IF_FALSE',
        while_block.size + 1
      ))
      bytecode += while_block
      return bytecode
    end

    def compile_for_statement(node)
      for_arguments = get_arguments(node[1])
      for_expression = compile_node(node[3])
      for_expression.push(generate_command('GET_ITER', nil))
      for_block = []
      if for_arguments.size > 1
        for_block.push(generate_command(
          'UNPACK_SEQUENCE',
          for_arguments.size
        ))
      end
      for_arguments.each do |argument|
        for_block.push(generate_command('STORE_NAME', argument))
      end
      for_block += compile_node(node[6])
      for_block.push(generate_command(
        'JUMP',
        -(for_block.size + for_arguments.size)
      ))
      for_expression.push(generate_command('FOR_ITER', for_block.size + 1))
      for_block.push(generate_command('POP', nil))
      return for_expression + for_block
    end


    def get_arguments(node)
      arguments = []
      arguments_node = open_node(node)[1]
      if arguments_node.is_a?(Array)
        arguments.push(open_node(arguments_node[0])[1])
        if arguments_node[1].is_a?(Array)
          arguments_node[1].each do |argument|
            arguments.push(open_node(argument[-1])[1])
          end
        end
      end
      return arguments
    end

    def compile_if_statement(node)
      if_bytecode = compile_if(node)

      else_bytecode = []
      if node[6].is_a?(Array)
        else_bytecode += compile_node(node[6][3])
      end

      elif_blocks = []
      if node[5].is_a?(Array)
        node[5].each do |elif_statement|
          elif_blocks.push(compile_if(elif_statement))
        end
      end

      jump_len = else_bytecode.size + 1
      elif_bytecode = []
      elif_blocks.reverse.each do |elif_block|
        elif_block.push(generate_command('JUMP', jump_len))
        elif_bytecode = elif_block + elif_bytecode
        jump_len += elif_block.size
      end

      if_bytecode.push(generate_command('JUMP', jump_len))
      return if_bytecode + elif_bytecode + else_bytecode
    end

    def compile_if(node)
      if_test = compile_node(node[1])
      if_block = compile_node(node[4])
      jump_command = [generate_command('JUMP_IF_FALSE', if_block.size + 2)]
      return if_test + jump_command + if_block
    end

    def compile_return_statement(node)
      bytecode = compile_node(node[1])
      bytecode.push(generate_command('RETURN', nil))
      return bytecode
    end

    def compile_import_statement(node)
      module_name = open_node(node[1])[1]
      bytecode = [generate_command('IMPORT_NAME', module_name)]
      bytecode.push(generate_command('STORE_NAME', module_name))
      return bytecode
    end

    def compile_expression_statement(node)
      bytecode = compile_node(node[0])
      bytecode.push(generate_command('POP', nil))
      return bytecode
    end

    def compile_assignment_statement(node)
      bytecode = compile_node(node[2])
      bytecode.push(generate_command('LOAD_NAME', open_node(node[0])[1]))
      bytecode += compile_node(node[1])
      bytecode[-1][0] = {
        'LOAD_NAME' => 'STORE_NAME',
        'LOAD_ATTRIBUTE' => 'STORE_ATTRIBUTE',
        'SUBSCRIPT' => 'STORE_SUBSCRIPT',
      }[bytecode[-1][0]]
      return bytecode
    end


    def compile_attribute_loading(node)
      return [generate_command('LOAD_ATTRIBUTE', open_node(node[1])[1])]
    end

    def compile_function_call(node)
      node_name, value = open_node(node[1])
      if value.is_a?(Array)
        args_count = 1
        args_count += value[1].size if value[1].is_a?(Array)
      else
        args_count = 0
      end
      bytecode = compile_node(node[1])
      bytecode.push(generate_command('CALL_FUNCTION', args_count))
      return bytecode
    end

    def compile_subscription(node)
      bytecode = compile_node(node[1])
      bytecode.push(generate_command('SUBSCRIPT', nil))
      return bytecode
    end

    def compile_or_test(node)
      bytecode = compile_node(node[0])
      if node[1].is_a?(Array)
        and_bytecode = compile_node(node[1][1])
        bytecode.push(generate_command(
          'JUMP_IF_TRUE',
          and_bytecode.size + 1
        ))
        bytecode += and_bytecode
      end
      return bytecode
    end

    def compile_and_test(node)
      bytecode = compile_node(node[0])
      if node[1].is_a?(Array)
        not_bytecode = compile_node(node[1][1])
        bytecode.push(generate_command(
          'JUMP_IF_FALSE',
          not_bytecode.size + 1
        ))
        bytecode += not_bytecode
      end
      return bytecode
    end


    def compile_not_test(node)
      if node.size == 2
        bytecode = compile_node(node[1])
        bytecode.push(generate_command('NOT', nil))
      else
        bytecode = compile_node(node)
      end
      return bytecode
    end

    def compile_comparison(node)
      bytecode = compile_node(node[0])
      if node[1].is_a?(Array)
        node[1].each do |comp_op, arithmetic_expression|
          bytecode += compile_node(arithmetic_expression)
          bytecode += compile_node(comp_op)
        end
      end
      return bytecode
    end

    def compile_comp_op(node)
      name, value = open_node(node)
      return [generate_command('COMPARE', value)]
    end

    def compile_arithmetic_expression(node)
      bytecode = compile_node(node[0])
      if node[1].is_a?(Array)
        node[1].each do |operation_node, term|
          operation = {
            'PLUS' => 'ADD',
            'MINUS' => 'SUBTRACT'
          }[open_node(operation_node)[0]]
          bytecode += compile_node(term)
          bytecode.push(generate_command(operation, nil))
        end
      end
      return bytecode
    end

    def compile_term(node)
      bytecode = compile_node(node[0])
      if node[1].is_a?(Array)
        node[1].each do |operation_node, factor|
          operation = {
            'STAR' => 'MULTIPLY',
            'SLASH' => 'DIVIDE',
            'PERCENT' => 'MODULO'
          }[open_node(operation_node)[0]]
          bytecode += compile_node(factor)
          bytecode.push(generate_command(operation, nil))
        end
      end
      return bytecode
    end

    def compile_factor(node)
      if node.size == 2
        bytecode = compile_node(node[1])
        if open_node(node[0])[0] == 'MINUS'
          bytecode.push(generate_command('NEGATIVE', nil))
        end
      else
        bytecode = compile_node(node)
      end
      return bytecode
    end

    def compile_power(node)
      bytecode = compile_node(node[0])
      if node[1].is_a?(Array)
        bytecode += compile_node(node[1][1])
        bytecode.push(generate_command('POWER', nil))
      end
      return bytecode
    end


    def compile_atom_expression(node)
      return compile_node(node[0])
    end

    def compile_list_creation(node)
      node_name, value = open_node(node[1])
      if value.is_a?(Array)
        args_count = 1
        args_count += value[1].size if value[1].is_a?(Array)
      else
        args_count = 0
      end
      bytecode = compile_node(node[1])
      bytecode.push(generate_command('BUILD_LIST', args_count))
      return bytecode
    end

    def compile_brackets_expression(node)
      return compile_node(node[1])
    end

    def compile_atom(node)
      name, value = open_node(node)
      return [generate_command('LOAD_'+name, value)]
    end
  end
end
