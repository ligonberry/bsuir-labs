def bubble_sort(arr):
    a = arr
    for i in range(len(a), 0, -1):
        for j in range(1, i):
            if a[j - 1] > a[j]:
                tmp = a[j - 1]
                a[j - 1] = a[j]
                a[j] = tmp
    return a


arr = [50, 100, 10, 40, 10, 50, 80, 40, 30, 120, 410]
print("TEST 4")
print(arr)
print(bubble_sort(arr))
