def repair_line(tokens, index)
  line_number = tokens[index][2]
  start, finish = index, index + 1
  line_tokens = []
  while (tokens[start][2] == line_number && start >= 0)
    line_tokens.unshift(tokens[start])
    start -= 1
  end
  while (tokens[finish][2] == line_number && finish < tokens.size)
    line_tokens.push(tokens[finish])
    finish += 1
  end
  words = []
  line_tokens.each_with_index do |token, i|
    words.push(token[0])
  end
  words[index - start - 1] = "->(#{words[index - start - 1]})"

  return '    ' * (line_tokens[0][3]-1) + words.join(' ')
end

def parse(tokens)
  node, index = block(tokens, 0, 0)
  unless node
    error = "Error: Invalid syntax in line #{tokens[index][2]} at #{tokens[index][3]}:\n#{repair_line(tokens, index)}"
    return error
  end
  return node
end

def statement(tokens, index)
  return check_nonterminal(
    tokens, index, 'statement',
    [ method(:simple_statement), method(:compound_statement) ].freeze
  )
end

def compound_statement(tokens, index)
  return check_nonterminal(
    tokens, index, 'compound_statement',
    [ method(:function_statement),
      method(:if_statement),
      method(:for_statement),
      method(:while_statement),
      method(:class_statement) ].freeze
  )
end

def simple_statement(tokens, index)
  return check_nonterminal(
    tokens, index, 'simple_statement',
    [ method(:expression_statement),
      method(:assignment_statement),
      method(:import_statement),
      method(:return_statement),
      'NEWLINE' ].freeze
  )
end

def optional_production(tokens, index, production)
  production = production['optional']
  node, new_index = check_production(tokens, index, production)
  unless node
    return generate_node('EMPTY', nil), index
  end
  return node, new_index
end

def any_count_production(tokens, index, production)
  new_index = index
  nodes = []
  production = production['any_count']
  loop do
    node, next_index = check_production(tokens, new_index, production)
    break unless node
    new_index = next_index
    nodes.push(node)
  end
  unless nodes
    return generate_node('EMPTY', nil), index
  end
  return nodes, new_index
end

def any_production(tokens, index, productions)
  best_index = index
  new_index = index
  productions.each do |production|
    node, new_index = check_production(tokens, new_index, production)
    if node
      return node, new_index
    elsif new_index > best_index
      best_index = new_index
    end
    new_index = index
  end
  return nil, best_index
end

def all_productions(tokens, index, productions)
  new_index = index
  nodes = []
  productions.each do |production|
    node, new_index = check_production(tokens, new_index, production)
    if node
      nodes.push(node)
    else
      return nil, new_index
    end
  end
  return nodes, new_index
end

def check_production(tokens, index, production)
  if production.is_a?(Array) && production.frozen?
    node, index = any_production(tokens, index, production)
  elsif production.is_a?(Array) && !production.frozen?
    node, index = all_productions(tokens, index, production)
  elsif production.is_a?(Hash)
    if production.include?('any_count')
      node, index = any_count_production(tokens, index, production)
    elsif production.include?('optional')
      node, index = optional_production(tokens, index, production)
    end
  elsif production.respond_to? :call
    node, index = production.call(tokens, index)
  elsif production.is_a?(String)
    node, index = check_terminal(tokens, index, production)
  end
  return node, index
end

def generate_node(name, value)
  return {
    "#{name}" => value
  }
end

def check_terminal(tokens, index, name)
  if tokens[index][1] == name
    return generate_node(name, tokens[index][0]), index + 1
  end
  return nil, index
end

def check_nonterminal(tokens, index, name, production)
  node, new_index = check_production(
    tokens, index, production
  )
  if node
    return generate_node(name, node), new_index
  end
  return nil, new_index
end

def function_statement(tokens, index)
  depth = tokens[index][3]
  return check_nonterminal(
    tokens, index, 'function_statement',
    [ 'DEF', 'NAME', 'LPAR',
      { 'optional' => method(:arguments_list) }, 'RPAR', 'COLON', 'NEWLINE',
      lambda { |x, y| block(x, y, depth) } ]
  )
end

def if_statement(tokens, index)
  depth = tokens[index][3]
  return check_nonterminal(
    tokens, index, 'if_statement',
    [ 'IF', method(:expression), 'COLON', 'NEWLINE',
      lambda { |x, y| block(x, y, depth) },
      { 'any_count' => [
          'ELIF', method(:expression), 'COLON', 'NEWLINE',
          lambda { |x, y| block(x, y, depth) },
        ]},
      { 'optional' => [
          'ELSE', 'COLON', 'NEWLINE',
          lambda { |x, y| block(x, y, depth) },
        ]},
    ]
  )
end

def for_statement(tokens, index)
  depth = tokens[index][3]
  return check_nonterminal(
    tokens, index, 'for_statement',
    [ 'FOR', method(:arguments_list), 'IN', method(:expression), 'COLON', 'NEWLINE',
      lambda { |x, y| block(x, y, depth) } ]
  )
end

def while_statement(tokens, index)
  depth = tokens[index][3]
  return check_nonterminal(
    tokens, index, 'while_statement',
    [ 'WHILE', method(:expression), 'COLON', 'NEWLINE',
      lambda { |x, y| block(x, y, depth) } ]
  )
end

def class_statement(tokens, index)
  depth = tokens[index][3]
  return check_nonterminal(
    tokens, index, 'class_statement',
    [ 'CLASS', 'NAME',
      {'optional' => [ 'LPAR', method(:arguments_list), 'RPAR'] },
      'COLON', 'NEWLINE',
      lambda { |x, y| block(x, y, depth) } ]
  )
end

def block(tokens, index, parent_depth=0)
  new_index = index
  nodes = []
  while tokens[new_index][1] == 'NEWLINE'
    new_index += 1
    if new_index >= tokens.size
      return nil, index
    end
  end
  depth = tokens[new_index][3]
  if depth <= parent_depth
    return nil, index
  end
  while new_index < tokens.size && (
    depth == tokens[new_index][3] ||
    tokens[new_index][1] == 'NEWLINE'
  )
    if tokens[new_index][1] == 'NEWLINE'
      new_index += 1
    else
      node, new_index = statement(tokens, new_index)
      if node
        nodes.push(node)
      else
        return nil, new_index
      end
    end
  end
  return generate_node('block', nodes), new_index
end

def import_statement(tokens, index)
  return check_nonterminal(
    tokens, index, 'import_statement',
    ['IMPORT', 'NAME', 'NEWLINE']
  )
end

def arguments_list(tokens, index)
  return check_nonterminal(
    tokens, index, 'arguments_list',
    {'optional' => [ 'NAME', {'any_count' => ['COMMA', 'NAME'] }]}
  )
end

def expression_statement(tokens, index)
  return check_nonterminal(
    tokens, index, 'expression_statement',
    [method(:expression), 'NEWLINE']
  )
end

def assignment_statement(tokens, index)
  return check_nonterminal(
    tokens, index, 'assignment_statement',
    [ 'NAME', [ method(:assignment_tail), 'EQUAL' ].freeze, method(:expression), 'NEWLINE']
  )
end

def assignment_tail(tokens, index)
  return check_nonterminal(
    tokens, index, 'tail',
    [ [ [method(:attribute_loading), method(:subscription)].freeze, 'EQUAL'  ],
      [ [method(:attribute_loading), method(:subscription), method(:function_call)].freeze,
         method(:assignment_tail) ] ].freeze
  )
end

def return_statement(tokens, index)
  return check_nonterminal(
    tokens, index, 'return_statement',
    [ 'RETURN', method(:expression), 'NEWLINE']
  )
end

def expression(tokens, index)
  return check_nonterminal(
    tokens, index, 'expression',
    [ method(:and_test), {'optional'=> ['OR', method(:and_test)]} ]
  )
end

def and_test(tokens, index)
  return check_nonterminal(
    tokens, index, 'and_test',
    [ method(:not_test), {'optional' => ['AND', method(:not_test)]} ]
  )
end

def not_test(tokens, index)
  return check_nonterminal(
    tokens, index, 'not_test',
    [ ['NOT', method(:not_test)], method(:comparison) ].freeze
  )
end

def comparison(tokens, index)
  return check_nonterminal(
    tokens, index, 'comparison',
    [ method(:arithmetic_expression),
      {'any_count' => [method(:comp_op), method(:arithmetic_expression)]} ]
  )
end

def comp_op(tokens, index)
  return check_nonterminal(
    tokens, index, 'comp_op',
    [ 'LESS', 'LESSEQUAL', 'GREATER', 'GREATEREQUAL',
      'EQEQUAL', 'NOTEQUAL', 'IN', ['NOT', 'IN'],
      'IS', ['IS', 'NOT'] ].freeze
  )
end

def arithmetic_expression(tokens, index)
  return check_nonterminal(
    tokens, index, 'arithmetic_expression',
    [ method(:term), {'any_count' => [['PLUS', 'MINUS'].freeze, method(:term)]}]
  )
end

def expression_list(tokens, index)
  return check_nonterminal(
    tokens, index, 'expression_list',
    [ method(:expression), { 'any_count' => ['COMMA', method(:expression)] }]
  )
end

def term(tokens, index)
  return check_nonterminal(
    tokens, index, 'term',
    [ method(:factor), {'any_count' => [['STAR', 'SLASH', 'PERCENT'].freeze, method(:factor)]}]
  )
end

def factor(tokens, index)
  return check_nonterminal(
    tokens, index, 'factor',
    [[['PLUS', 'MINUS'].freeze, method(:factor)], method(:power)].freeze
  )
end

def power(tokens, index)
  return check_nonterminal(
    tokens, index, 'power',
    [ method(:atom_expression), {'optional' => ['DOUBLESTAR', method(:factor)]}]
  )
end

def atom_expression(tokens, index)
  return check_nonterminal(
    tokens, index, 'atom_expression',
    [[
      method(:brackets_expression),
      method(:list_creation),
      method(:atom)
    ].freeze, {'optional' => method(:tail) }]
  )
end

def brackets_expression(tokens, index)
  return check_nonterminal(
    tokens, index, 'brackets_expression',
    ['LPAR', method(:expression), 'RPAR']
  )
end

def tail(tokens, index)
  return check_nonterminal(
    tokens, index, 'tail',
    [[
      method(:attribute_loading),
      method(:function_call),
      method(:subscription)
    ].freeze,
    {'optional' => method(:tail)}  ]
  )
end

def attribute_loading(tokens, index)
  return check_nonterminal(
    tokens, index, 'attribute_loading',
    [ 'DOT', 'NAME']
  )
end

def function_call(tokens, index)
  return check_nonterminal(
    tokens, index, 'function_call',
    ['LPAR', {'optional' => method(:expression_list)}, 'RPAR']
  )
end

def subscription(tokens, index)
  return check_nonterminal(
    tokens, index, 'subscription',
    [ 'LSQB', method(:expression), 'RSQB']
  )
end

def list_creation(tokens, index)
  return check_nonterminal(
    tokens, index, 'list_creation',
    ['LSQB', {'optional' method(:expression_list)}, 'RSQB']
  )
end

def atom(tokens, index)
  return check_nonterminal(
    tokens, index, 'atom',
    [ 'NAME', 'FLOAT', 'INT', 'STRING' ].freeze
  )
end

if __FILE__ == $0
  f = File.open("1.py")
  code = f.read
  tokens = tokenize(code)
  parsing = parse(tokens[0])
  puts parsing
  f.close()
end
