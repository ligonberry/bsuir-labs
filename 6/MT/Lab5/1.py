def newton(function, df, x0):
    delta = abs(0 - function(x0))
    while delta > 1e-5:
        x0 = x0 - function(x0) / df(x0)
        delta = abs(0 - function(x0))
    return x0


def f1(x):
    return 3 * x**2 - 2


def f(x):
    return x**3 - 2*x - 5


def bisection(function, a, b):
    start = a
    end = b
    if function(a) == 0:
        return a
    elif function(b) == 0:
        return b
    elif function(a) * function(b) > 0:
        return
    else:
        mid = (start + end) / 2
        while abs(start - mid) > 10**-7:
            if function(mid) == 0:
                return mid
            elif function(mid) * function(start) < 0:
                end = mid
            else:
                start = mid
            mid = (start + end) / 2

        return mid


if __name__ == "__main__":
    print "Root of the equation x**3 - 2*x - 5 = 0 by newton method is", newton(f, f1, 0.5)
    print "Root of the equation x**3 - 2*x - 5 = 0 by bisection method is", bisection(f, 0.5, 100)
