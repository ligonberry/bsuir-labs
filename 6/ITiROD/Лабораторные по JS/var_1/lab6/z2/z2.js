countPlace();

function countPlace(){
    var inputMessageFloors = 'Введите число этажей';
    var inputMessageBlock = 'Введите число подъездов';
    var inputMessageFlats = 'Введите число квартир на этаже';
    var inputMessageFlatNumber = 'Введите номер искомой квартиры';
    
    
    var floors = parseInt(prompt(inputMessageFloors, 'Этажи'));
    if (isNaN(floors) || floors <= 0){
        alert('Некорректное значение этажей');
        return;
    }
        
    var blocks = parseInt(prompt(inputMessageBlock, 'Подъезды'));
    if (isNaN(blocks) || blocks <= 0){
        alert('Некорректное значение подъездов');
        return;
    }
    var flats = parseInt(prompt(inputMessageFlats, 'Квартиры'));
    if (isNaN(flats)|| flats <= 0){
        alert('Некорректное значение квартир');
        return;
    }
    var flatNumber = parseInt(prompt(inputMessageFlatNumber, 'Номер Квартиры'));
    if (isNaN(flatNumber) || flatNumber <= 0){
        alert('Некорректное значение номера квартиры');
        return;
    }
    
    var currentBlock = Math.ceil(flatNumber / (flats * floors));

    if (currentBlock > blocks){
        alert('Такого номера квартиры не существует');
        return;
    }

    var currentFloor = Math.ceil((flatNumber - (currentBlock-1) * flats * floors) / flats);
    
    alert('Подъезд: ' + currentBlock + '\nЭтаж: ' + currentFloor);
    
}