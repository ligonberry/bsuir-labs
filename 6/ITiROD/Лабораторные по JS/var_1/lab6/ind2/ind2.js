findStrings();

function findStrings(){
    var messageInputString = 'Введите строку';
    var inputString = prompt(messageInputString, 'Строка');

    if (inputString.length > 80){
        alert('Строка должна быть меньше 80 символов');
        return;
    }

    var regEx = /(-)?(\d+)/g;
    var matches =  inputString.match(regEx);

    if (matches === null){
        alert('Целых чисел не найдено');
        return;
    }
    
    var result = '';
    for (var i=0; i<matches.length; i++){
        result += '\n' + matches[i] ;
    }
    alert('Найденные значения: ' + result);
    return;
}