function func(x) {
  return (Math.exp(x) - Math.exp(-x))/2;
};

var f = [];
function factorial(n) {
  if (n == 0 || n == 1) {
    return 1;
  }
  if (f[n] > 0) {
    return f[n];
  }
  return f[n] = factorial(n-1) * n;
};

function seriesFunc(x, n) {
  var result = 0;
  for (var i = 0; i < n; i += 1) {
    result += Math.pow(x, i * 2 + 1) / factorial(i * 2 + 1);
  }
  return result;
};

function init() {
  var nStr = prompt('Введите количество членов ряда (n):');
  var n = parseInt(nStr, 10) || 1;
  
  var numStr = prompt('Введите количество чилел, от которых необходимо посчитать функцию:');
  var nums = parseInt(numStr, 10) || 1;
  
  for (var i = 0; i < nums; i += 1) {
    var xStr = prompt('Введите x от 0.1 до 1 ('+ i + 1 +' значение)');
    var x = parseFloat(xStr, 10) || 0.1;
    if (x < 0.1) {
      x = 0.1;
    } else if (x > 1) {
      x = 1;
    }
  
    console.log('Вычисление через ряд: ', seriesFunc(x, n));
    console.log('Вычисление через функцию: ', func(x, n));
  }
};

init();
