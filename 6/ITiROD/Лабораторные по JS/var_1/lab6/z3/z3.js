fibonachhi();

function fibonachhi(){
    var i = parseInt(prompt('Введите номер числа Фибоначи', 'i='));
    if (isNaN(i) || i<=0){
        alert('Неправильное значение i');
        return;
    }

    if (i<3){
        alert('Результат: 1');
        return;
    }

    var first = 1;
    var next = 1;
    var k = 2;
    while (k != i){
        var temp = next;
        next += first;
        first = temp;
        k+=1;
    }

    alert('Результат: ' + next);
    return;
}