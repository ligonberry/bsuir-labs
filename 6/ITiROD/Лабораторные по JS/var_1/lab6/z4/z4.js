dayOfWeek();

function dayOfWeek(){
    var inputDay = 'Введите день';
    var inputMonth= 'Введите месяц';
    var inputYear = 'Введите год';    
    
    var day = parseInt(prompt(inputDay, 'День'));
    if (isNaN(day) || day <= 0 || day > 31){
        alert('Некорректное значение дня');
        return;
    }
    var month = parseInt(prompt(inputMonth, 'Месяц'));
    if (isNaN(month) || month <= 0 || month > 12){
        alert('Некорректное значение месяца');
        return;
    }
    var year = parseInt(prompt(inputYear, 'Год'));
    if (isNaN(year) || year <= 0){
        alert('Некорректное значение года');
        return;
    }
    
    var date = new Date(year + '-' + month + '-' + day) 
    if (date === undefined){
        alert('Некорректное значение введенных данных');
        return;
    }

    var dayOfWeek = '';
    switch(date.getDay()){
        case 0: dayOfWeek = 'Воскресенье'; break;
        case 1: dayOfWeek = 'Понедельник'; break;
        case 2: dayOfWeek = 'Вторник'; break;
        case 3: dayOfWeek = 'Среда'; break;
        case 4: dayOfWeek = 'Четверг'; break;
        case 5: dayOfWeek = 'Пятница'; break;
        default: dayOfWeek = 'Cуббота'; break;
    }
    alert('День недели: ' + dayOfWeek);
}