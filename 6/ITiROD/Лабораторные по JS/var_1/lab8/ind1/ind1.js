function getColumn(matr, column) {
    var result = [];
    for (let i = 0; i < matr.length; i += 1) {
        result.push(matr[i][column]);
    }
    return result;
};

function getPoints(matrix) {
    var result = [];

    for (var i = 0; i < matrix.length; i += 1) {
        var maxRowEl = Math.max.apply(null, matrix[i]);
        var minRowEl = Math.min.apply(null, matrix[i]);

        for (var j = 0; j < matrix[i].length; j += 1) {
            var el = matrix[i][j];
            if (el !== maxRowEl && el !== minRowEl) 
                continue;

            var column = getColumn(matrix, j);
            var maxColEl = Math.max.apply(null, column);
            var minColEl = Math.min.apply(null, column);

            if (
                (el === maxRowEl && el === minColEl) ||
                (el === minRowEl && el === maxColEl)
            ) {
                result.push(el);
            }
        }
    }

    return result;
}

var initMatrix = [
    [1, 2, 7, 11],
    [5, 7, 8, 9],
    [2, 7, 9, 12],
];

console.log(getPoints(initMatrix));
