console.log(range(5,2));
console.log(range(5,2, -1));
console.log(range(-17,-20, -1));
console.log(range(-17,-20));

function range(start, end, step = 1){
    var min = Math.min(start, end);
    var max = Math.max(start, end);
    var result = [];
    
    for (var i=min; i<=max; i+=Math.abs(step))
        result.push(i);

    return step > 0 ? result : result.reverse();
}