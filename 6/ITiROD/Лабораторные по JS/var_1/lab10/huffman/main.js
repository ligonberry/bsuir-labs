class Tree {

    constructor(text) {
        this.root =  this.buidTree(this.getFrequency(text));
    }

    encode() {
        let result = {};
        const chars = this.root.value;

        for (let i = 0; i < chars.length; i++) {
            result[chars[i]] = '';
            let temp = this.root;

            while (temp.value !== chars[i]) {
                if (temp.left.value.indexOf(chars[i]) !== -1) {
                    result[chars[i]] += '0';
                    temp = temp.left;
                }
                else {
                    result[chars[i]] += '1';
                    temp = temp.right;
                }
            }
        }
        return result;
    }

    buidTree(elementsFrequency) {
        let nodes = this.toNodesArray(elementsFrequency);
        while (nodes.length > 1) {
            const index = this.smalestSumIndex(nodes);
            let temp = new Node(nodes[index-1], 
                                nodes[index],
                                nodes[index-1].value + nodes[index].value,
                                nodes[index-1].frequency + nodes[index].frequency);
            nodes.splice(index-1, 2, temp);
        }

        return nodes[0];
    }

    toNodesArray(array) {
        let result = [];
        array.forEach(element => {
            result.push(new Node(null, null, element[0], element[1]))
        })
        return result;
    }

    smalestSumIndex(nodes) {
        let index = nodes.length-1;
        for (let i = nodes.length - 2 ; i > 0; i--){
            if (nodes[index].frequency + nodes[index-1].frequency > nodes[i].frequency + nodes[i-1].frequency) {
                index = i;
            }
            else {
                break;
            }
        }
        return index;
    }

    getFrequency(text) {
        let dict = {};

        for (let i = 0; i < text.length; i++) {
            if (text[i] in dict) {
                dict[text[i]]++;
            }
            else {
                dict[text[i]] = 1;
            }
        };

        let result = [];
        Object.keys(dict).forEach(element => {
            result.push([element, dict[element]]);
        });

        return result.sort(this.sortByValue).sort(this.sortByFrequency);
    }

    sortByValue(a, b) {
        if (a[0] < b[0]) {
                return -1;
        }
        if (a[0] > b[0]) {
            return 1;
        }   
        return 0;
    }

    sortByFrequency(a, b){
        if (a[1] < b[1]) {
            return 1;
        }
        if (a[1] > b[1]) {
            return -1;
        }   
        return 0;
    }

}

class Node {

    constructor(left, right, value, frequency) {
        this.left = left;
        this.right = right;
        this.value = value;
        this.frequency = frequency;
    }

}

let tree = new Tree('дддддггггггввввввбббббббааааааааааааааа');
console.log(tree.encode());
//ccbbbaaaaaa
