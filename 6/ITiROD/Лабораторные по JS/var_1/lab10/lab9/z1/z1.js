class Vector {    
    constructor(x, y, z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    plus(vector){
        let sum = new Vector(0, 0, 0);
        sum.x = vector.x + this.x;
        sum.y = vector.y + this.y;
        sum.z = vector.z + this.z;
        return sum;
    }

    scalar(vector){
        return vector.x * this.x + vector.y * this.y + vector.z * this.z;
    }

    get length() {
        return Math.pow(Math.pow(this.x, 3) + Math.pow(this.y, 3) + Math.pow(this.z, 3), 1/3);
    }

    toString(){
        return "{x:" + this.x + ", y:" + this.y + ", z:" + this.z + "}";
    }

    valueOf(){
        return this.length;
    }
};

let vector1 = new Vector(1, 1, 1);
let vector2 = new Vector(1, 2, 3);

console.log(vector1.toString());
console.log(vector2.valueOf());
console.log(vector1.plus(vector2));
console.log(vector1.scalar(vector2));