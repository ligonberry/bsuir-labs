class Task {
    constructor(){
        this.name = "";
        this.description = "";
        this.start = undefined;
        this.end = undefined;
        this.tasks = [];
    }
};

class InProgressTask extends Task {
    constructor(){
        super();
        this.percent = 0;
        this.isComplite = false;
    }
};

const task = new Task();
const inProgressTask = new InProgressTask();

if (!(task.hasOwnProperty("percent")))
    console.log("No property percent in Task class");
if (inProgressTask.hasOwnProperty("percent"))
    console.log("Percent in InProgressTask class");
if (inProgressTask.hasOwnProperty("percent"))
    console.log("Name in InProgressTask class");