class Disk {
    constructor(){
        this.type = "";
        this.name = "";
        this.author = "";
        this.price = 0.0;
        this.note = "";
    }
}

class List {
    constructor(){
        this.disks = [];
    }

    addDisk(disk){
        this.disks.push(disk);
    }

    sortByName(){
        let res = [];
        for (let i = 0; i < this.disks.length; i++){
            if (this.disks[i].type in res){
                res[this.disks[i].type].push(this.disks[i]);
            }
            else {
                res[this.disks[i].type] = [this.disks[i]]
            }
        }
        for (let prop in res) {
            res[prop].sort(function (a, b) {
                if (a.name > b.name) {
                  return 1;
                }
                if (a.name < b.name) {
                  return -1;
                }
                return 0;
              });;
        }
        return res;
    }

    searchByAuthor(author){
        var res = [];
        for (var i = 0; i < this.disks.length; i++){
            if (this.disks[i].author == author)
                res.push(this.disks[i]);
        }
        return res;
    }

    searchByName(name){
        let res = [];
        for (let i = 0; i < this.disks.length; i++){
            if (this.disks[i].name == name)
                res.push(this.disks[i]);
        }
        return res;
    }

    searchByType(type){
        let res = [];
        for (let i = 0; i < this.disks.length; i++){
            if (this.disks[i].type == type)
                res.push(this.disks[i]);
        }
        return res;
    }
}

let list = new List();

for(let i = 0; i < 5; i++){
    let disk = new Disk();
    disk.name = prompt("Введите название произведения");
    disk.type = prompt("Введите тип записи");
    disk.author = prompt("Введите автора");
    list.addDisk(disk);
}

console.log(list.searchByType(prompt("Введите тип для поиска")));
console.log(list.searchByAuthor(prompt("Введите автора для поиска")));
console.log(list.searchByName(prompt("Введите имя для поиска")));
console.log(list.sortByName());
