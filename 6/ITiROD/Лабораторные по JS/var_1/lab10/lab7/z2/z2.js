function numberCheck(func){
    return function (numberVariable){
        if(typeof(numberVariable) !== 'number')
            return 'Переменная не число';
        return func(...arguments);
    }
}

function typeCheck(func, type){
    return function (){
        for (let i = 0; i < arguments.length; i++){
            if (typeof(arguments[i]) !== type)
                return 'Переменная не типа ' + type;
        }
        return func(...arguments);
    }
}

function sum(){
    if (arguments.length == 1){
        return arguments[0];
    }

    let sum = 0;
    for (let i = 0; i < arguments.length; i++)
        sum += arguments[i];
    return sum;
}

const numberCheckFunction = numberCheck(sum);
console.log(numberCheckFunction('grtgtrg'));

const typeCheckFunction = typeCheck(sum, 'number');
console.log(typeCheckFunction(1, 1, 1));