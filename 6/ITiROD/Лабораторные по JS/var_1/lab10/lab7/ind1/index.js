const n = 1000;

const function1 = (x) => {
  return Math.sqrt(x*x + 0.6) / (1.4 + Math.sqrt(0.8*x*x + 1.3));
};

const function2 = (x) => {
  return Math.cos(x*x + 0.6) / (0.7 + Math.sin(0.8*x + 1));
};

const function3 = (x) => {
  return 1 / Math.sqrt(2 + 0.5*x*x);
};

const function4 = (x) => {
  return Math.sin(2*x) / (x*x);
};

const integate = (fn, a, b) => {
  let result = 0;

  for (let i = 0; i < n; i += 1) {
    const center = (b - a) * (2*i + 1) / (2*n) + a;
    const square = (b - a) * fn(center) / n;
    result += square;
  }

  return result;
};

console.log(integate(function1, 1.3, 2.5));
console.log(integate(function2, 0.4, 0.8));
console.log(integate(function3, 0.4, 1.2));
console.log(integate(function4, 0.8, 1.2));
