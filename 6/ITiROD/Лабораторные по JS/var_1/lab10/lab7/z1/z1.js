console.log(isRectangle(0,0,0,1,1,1,1,0));

const isRectangle = (x1, y1, x2, y2, x3, y3, x4, y4) => {
    const a = vectorLength(x1, y1, x2, y2);
    const b = vectorLength(x2, y2, x3, y3);
    const c = vectorLength(x3, y3, x4, y4);

    const d1 = vectorLength(x1, y1, x3, y3);
    const d2 = vectorLength(x2, y2, x4, y4);

    const d1Sides = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
    const d2Sides = Math.sqrt(Math.pow(b, 2) + Math.pow(c,2));

    return (d1 === d1Sides && d2 === d2Sides) ? true : false;
}

const vectorLength = (startX, startY, endX, endY) => {
    return Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));
}