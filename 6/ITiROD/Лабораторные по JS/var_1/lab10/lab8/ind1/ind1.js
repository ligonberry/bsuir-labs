const getColumn = (matr, column) => {
    const result = [];
    for (let i = 0; i < matr.length; i += 1) {
        result.push(matr[i][column]);
    }

    return result;
};

const getPoints = (matrix) => {
    const result = [];

    for (let i = 0; i < matrix.length; i += 1) {
        const maxRowEl = Math.max(...matrix[i]);
        const minRowEl = Math.min(...matrix[i]);

        for (let j = 0; j < matrix[i].length; j += 1) {
            const el = matrix[i][j];
            if (el !== maxRowEl && el !== minRowEl
            ) {
                continue;
            }

            const column = getColumn(matrix, j);

            const maxColEl = Math.max(...column);
            const minColEl = Math.min(...column);

            if (
                (el === maxRowEl && el === minColEl) ||
                (el === minRowEl && el === maxColEl)
            ) {
                result.push(el);
            }
        }
    }

    return result;
}

const initMatrix = [
    [1, 2, 7, 11],
    [5, 7, 8, 9],
    [2, 7, 9, 12],
];

console.log(getPoints(initMatrix));
