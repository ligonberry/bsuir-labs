console.log(range(5,2));
console.log(range(5,2, -1));
console.log(range(-17,-20, -1));
console.log(range(-17,-20));

function range(start, end, step = 1){
    const min = Math.min(start, end);
    const max = Math.max(start, end);
    let result = [];
    
    for (let i = min; i <= max; i += Math.abs(step))
        result.push(i);

    return step > 0 ? result : result.reverse();
}