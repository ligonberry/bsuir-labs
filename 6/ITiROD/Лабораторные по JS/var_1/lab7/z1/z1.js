console.log(isRectangle(0,0,0,1,1,1,1,0));

function isRectangle(x1, y1, x2, y2, x3, y3, x4, y4){
    var a = vectorLength(x1, y1, x2, y2);
    var b = vectorLength(x2, y2, x3, y3);
    var c = vectorLength(x3, y3, x4, y4);

    var d1 = vectorLength(x1, y1, x3, y3);
    var d2 = vectorLength(x2, y2, x4, y4);

    var d1Sides = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
    var d2Sides = Math.sqrt(Math.pow(b, 2) + Math.pow(c,2));

    if (d1 === d1Sides && d2 === d2Sides){
        return true;
    }
    return false;
}

function vectorLength(startX, startY, endX, endY){
    return Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));
}
