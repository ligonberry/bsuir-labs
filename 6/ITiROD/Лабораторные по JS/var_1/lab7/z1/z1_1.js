var rectPoints = [
  [1, 1],
  [5, 1],
  [5, -1],
  [1, -1],
];

function isInRectangle(){  
  
  var point = [1.1, 0.5];
  var epsilon = 1e-10;
  var angle = 0;

  for (var i = 0; i < rectPoints.length; i += 1) {
    var p1 = rectPoints[i];
    var p2 = rectPoints[(i + 1) % rectPoints.length];
  
    var a1 =  point[1] - p1[1];
    var b1 = p1[0] - point[0];
  
    var a2 = point[1] - p2[1];
    var b2 = p2[0] - point[0];
  
    var cos = (a1*a2 + b1*b2) / (Math.sqrt(a1*a1 + b1*b1) * Math.sqrt(a2*a2 + b2*b2));
    var ang = Math.acos(cos);
  
    angle += ang;
  }
  
  if (2 * Math.PI - epsilon < angle && angle < 2 * Math.PI + epsilon)
    return 'точка внутри прямоугольника';
  return 'точка снаружи прямоугольника';
  
}

console.log(isInRectangle());