function numberCheck(func){
    return function f(numberVariable){
        if(typeof(numberVariable) !== 'number')
            return 'Переменная не число';
        return func.apply(null, arguments);
    }
}

function typeCheck(func, type){
    return function f(){
        for (var i = 0; i < arguments.length; i++){
            if (typeof(arguments[i]) !== type)
                return 'Переменная не типа ' + type;
        }
        return func.apply(null, arguments);
    }
}

function sum(){
    if (arguments.length == 1){
        return arguments[0];
    }

    var sum = 0;
    for (var i = 0; i < arguments.length; i++)
        sum += arguments[i];
    return sum;
}

var numberCheckFunction = numberCheck(sum);
console.log(numberCheckFunction('grtgtrg'));

var typeCheckFunction = typeCheck(sum, 'number');
console.log(typeCheckFunction(1, 1, 1));