var n = 1000;

function function1(x) {
  return Math.sqrt(x*x + 0.6) / (1.4 + Math.sqrt(0.8*x*x + 1.3));
};

function function2(x) {
  return Math.cos(x*x + 0.6) / (0.7 + Math.sin(0.8*x + 1));
};

function function3(x) {
  return 1 / Math.sqrt(2 + 0.5*x*x);
};

function function4 (x) {
  return Math.sin(2*x) / (x*x);
};

function integate (fn, a, b) {
  var result = 0;

  for (var i = 0; i < n; i += 1) {
    var center = (b - a) * (2*i + 1) / (2*n) + a;
    var square = (b - a) * fn(center) / n;
    result += square;
  }

  return result;
};

console.log(integate(function1, 1.3, 2.5));
console.log(integate(function2, 0.4, 0.8));
console.log(integate(function3, 0.4, 1.2));
console.log(integate(function4, 0.8, 1.2));
