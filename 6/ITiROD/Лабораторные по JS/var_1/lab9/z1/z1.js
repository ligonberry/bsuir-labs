var Vector = {
    x: 0,
    y: 0,
    z: 0,
    plus: function(vector){
        sum = Object.create(Vector)
        sum.x = vector.x + this.x;
        sum.y = vector.y + this.y;
        sum.z = vector.z + this.z;
        return sum;
    },
    scalar: function(vector){
        return vector.x * this.x + vector.y * this.y + vector.z * this.z;
    },
    get length() {
        return Math.pow(Math.pow(this.x, 3) + Math.pow(this.y, 3) + Math.pow(this.z, 3), 1/3);
    },
    toString: function(){
        return "{x:" + this.x + ", y:" + this.y + ", z:" + this.z + "}";
    },
    valueOf: function(){
        return this.length;
    }
};

var vector1 = Object.create(Vector);
var vector2 = Object.create(Vector);

vector1.x = 1;
vector1.y = 1;
vector1.z = 1;
console.log(vector1.toString());

vector2.x = 1;
vector2.y = 2;
vector2.z = 3;
console.log(vector2.valueOf());

console.log(vector1.plus(vector2));
console.log(vector1.scalar(vector2));