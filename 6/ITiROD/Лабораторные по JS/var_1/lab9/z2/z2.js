var Task = {
    name: "",
    description: "",
    start: undefined,
    end: undefined,
    tasks: []
};

var InProgressTask = {
    percent: 0,
    isComplite: false
};

InProgressTask.__proto__  = Task;

if (!("percent" in Task))
    console.log("No property percent in Task class");
if ("percent" in InProgressTask)
    console.log("Percent in InProgressTask class");
if ("name" in InProgressTask)
    console.log("Name in InProgressTask class");