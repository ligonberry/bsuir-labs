var Disk = {
    type: "",
    name: "",
    author: "",
    price: 0.0,
    note: ""
}

var List = {
    disks: [],
    addDisk: function(disk){
        this.disks.push(disk);
    },
    sortByName: function(){
        var res = [];
        for (var i = 0; i < this.disks.length; i++){
            if (this.disks[i].type in res){
                res[this.disks[i].type].push(this.disks[i]);
            }
            else {
                res[this.disks[i].type] = [this.disks[i]]
            }
        }
        for (var prop in res) {
            res[prop].sort(function (a, b) {
                if (a.name > b.name) {
                  return 1;
                }
                if (a.name < b.name) {
                  return -1;
                }
                return 0;
              });;
        }
        return (res);
    },
    searchByAuthor: function(author){
        var res = [];
        for (var i = 0; i < this.disks.length; i++){
            if (this.disks[i].author == author)
                res.push(this.disks[i]);
        }
        return res
    },
    searchByName: function(name){
        var res = [];
        for (var i = 0; i < this.disks.length; i++){
            if (this.disks[i].name == name)
                res.push(this.disks[i]);
        }
        return res
    },
    searchByType: function(type){
        var res = [];
        for (var i = 0; i < this.disks.length; i++){
            if (this.disks[i].type == type)
                res.push(this.disks[i]);
        }
        return res
    }
}

var list = Object.create(List);

for(var i=0; i<5; i++){
    var disk = Object.create(Disk);
    disk.name = prompt("Введите название произведения");
    disk.type = prompt("Введите тип записи");
    disk.author = prompt("Введите автора");
    list.addDisk(disk);
}

console.log(list.searchByType(prompt("Введите тип для поиска")));
console.log(list.searchByAuthor(prompt("Введите автора для поиска")));
console.log(list.searchByName(prompt("Введите имя для поиска")));
console.log(list.sortByName());
