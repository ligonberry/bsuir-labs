/* Реализуйте на ECMAScript 2015 алгоритм сжатия информации по Хаффману */


class Node {
    constructor(left, right, elem, frequency) {
        this.left_parent = left;
        this.right_parent = right;
        this.elem = elem;
        this.frequency = frequency;
    }

}


class HuffmanTree {
    constructor(text) {
        this.huffmanTree =  this.createHuffmanTree(this.getCharFrequency(text));
    }

    static sortByElement(a, b) {
        if (a[0] < b[0]) {
            return -1;
        } else if (a[0] > b[0]) {
            return 1;
        }
        return 0;
    }

    getCharFrequency(str) {
        let dict = {};

        for (let i = 0; i < str.length; i++) {
            if (str[i] in dict) {
                dict[str[i]] += 1;
            } else {
                dict[str[i]] = 1;
            }
        }

        let result = [];
        Object.keys(dict).forEach(element => {
            result.push([element, dict[element]]);
        });

        return result.sort(HuffmanTree.sortByElement);
    }

    getPrepareNodes(array) {
        let result = [];
        array.forEach(element => {
            result.push(new Node(null, null, element[0], element[1]));
        });
        return result;
    }

    static _isMin(node, node_prev){
        return (node.frequency  >  node_prev.frequency)
    }


    static getIndexWithMinFrequency(nodes) {
        const len = nodes.length;
        let index =  len - 1;
        for (let i = len - 2 ; i > 0; i--){
            const node_prev = nodes[i-1];
            const node = nodes[index];
            if (HuffmanTree._isMin(node, node_prev)) {
                index = i;
            } else {
                break;
            }
        }
        return index;
    }


    createHuffmanTree(array) {
        let nodes = this.getPrepareNodes(array);

        while (nodes.length > 1) {
            const index = HuffmanTree.getIndexWithMinFrequency(nodes);

            let newNode = new Node(nodes[index-1],
                                   nodes[index],
                                  nodes[index-1].elem + nodes[index].elem,
                                  nodes[index-1].frequency + nodes[index].frequency);

            nodes.splice(index-1, 2, newNode);
        }

        return nodes[0];
    }

    static _isLeftParent(temp, char) {
        return temp.left_parent.elem.indexOf(char) !== -1
    }

    solve() {
        let result = {};
        const chars = this.huffmanTree.elem;

        for (let i = 0, len = chars.length; i < len; i++) {
            const char = chars[i];
            result[char] = '';

            let temp = this.huffmanTree;
            while (temp.elem !== char) {
                if (HuffmanTree._isLeftParent(temp, char)) {
                    result[char] += '0';
                    temp = temp.left_parent;
                }
                else {
                    result[char] += '1';
                    temp = temp.right_parent;
                }
            }
        }
        return result;
    }

}


main = () => {
    const example = 'дддддггггггввввввбббббббааааааааааааааа';
    const rawText = prompt('Введите текст, который хотите сжать с помощью алгоритма Хаффмана');
    const text = rawText || example;
    let tree = new HuffmanTree(text);
    console.log(tree.solve());
};

main();
