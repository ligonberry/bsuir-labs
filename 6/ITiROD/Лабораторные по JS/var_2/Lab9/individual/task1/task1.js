/* Разработать программу «Биржа труда». Имеется список фирм с вакансиями.
   Каждый пункт списка содержит: название фирмы, наименование специальности, должность, оклад, количество дней отпуска,
   требования к нанимаемому (наличие высшего образования (да/нет), возрастной диапазон (min/max)).
   Также имеется список кандидатов, каждый пункт которого содержит: ФИО кандидата, возраст, специальность,
   наличие высшего образования (да/нет), желаемую должность, минимальный оклад.

   Требуется:
       1. для каждого кандидата подобрать список возможных вакансий;
       2. выдать список дефицитных вакансий (кандидаты отсутствуют);
       3. выдать список фирм, для которых не подобран ни один кандидат.

   В каждом варианте задания реализовать следующие функции класса: инициализации данных класса, добавления, удаления,
   корректировки, сортировки  и просмотра записей. */


class Vacancy {
    static getAge(){
        let _age = Math.trunc(Math.random()*100);
        const _age1 = _age < 18 ? 18: _age;

        _age = Math.trunc(Math.random()*100);
        const _age2 = _age < 18 ? 18: _age;
        return [_age1, _age2]
    }

    constructor(){
        this.firm = 'firm' + Math.trunc(Math.random()*10);
        this.specialtyName = 'spec' + Math.trunc(Math.random()*10);
        this.position = 'pos' + Math.trunc(Math.random()*10);
        this.salary = Math.trunc((Math.random()*1000));
        this.vacationDays = Math.trunc(Math.random()*50);
        this.higherEducation = Math.trunc(Math.random()*10) % 2 === 0 ? 'yes' : 'no';

        this.minAge = 18;
        this.maxAge = 100;
    }

}


class Candidate {
    constructor(){
        this.name = 'Name' + Math.trunc(Math.random()*10);
        this.surname = 'Surname' + Math.trunc(Math.random()*10);
        this.age = Math.trunc((Math.random()*100));

        this.specialtyName = 'spec' + Math.trunc(Math.random()*10);
        this.higherEducation = Math.trunc(Math.random()*10) % 2 === 0 ? 'yes' : 'no';
        this.position= 'pos' + Math.trunc(Math.random()*10);
        this.salary = Math.trunc((Math.random()*1000));
    }
}


class LaborExchange {
    constructor(){
        this.vacancies = []
    }

    addVacancy(vacancy) {
        this.vacancies.push(vacancy);
    }

    deleteVacancy(vacancy) {
        this.vacancies.remove(vacancy);
    }

    updateVacancy(vacancy, newVacancy){
        let index = this.vacancies.indexOf(vacancy);
        if (index !== -1) {
            this.vacancies[index] =newVacancy;
        }
    }

    sortVacanciesBySalary(){
        return this.vacancies.sort(function(a, b) {
            if (a.salary > b.salary){
                return -1;
            }
            else if (b.salary > a.salary){
                return 1;
            }
            return 0;
        })
    }

    showVacancies(){
        console.log(this.vacancies)
    }

    getVacanciesCount(){
        return this.vacancies.length
    }

    static _isMatches(vacancy, candidate){
        return vacancy.higherEducation === candidate.higherEducation &&
            vacancy.maxAge > candidate.age && vacancy.minAge < candidate.age &&
            vacancy.salary > candidate.salary && vacancy.position === candidate.position &&
            vacancy.specialtyName === candidate.specialtyName;

    }

    getVacancyForCandidate(candidate){
        let results = new LaborExchange();
        const vacancyCount = this.vacancies.length;
        for (let i=0; i < vacancyCount; i++) {
            vacancy = this.vacancies[i];
             if (LaborExchange._isMatches(vacancy, candidate)) {
                results.addVacancy(vacancy)
            }
        }

        if (results.getVacanciesCount()) {
            console.log('Список вакансий для кандидата ', candidate);
            console.log(results);
        }
    }


    getVacancy(candidates, isDeficit){
        let results = new LaborExchange();

        const vacancyCount = this.vacancies.length;

        for (let j=0; j < vacancyCount; j++) {
            let isDeficit_ = isDeficit;
            vacancy = this.vacancies[j];

            for (let i=0; i < candidates.length; i++) {
                let candidate = candidates[i];
                if (LaborExchange._isMatches(vacancy, candidate)) {
                    isDeficit_ = !isDeficit;
                    break;
                }
            }

            if (isDeficit_) {
                results.addVacancy(vacancy);
            }
        }
        return results
    }


    static unique(deficit, closed) {
        const obj = {};

        const deficitVacancyCount = deficit.vacancies.length;
        const closedVacancyCount = closed.vacancies.length;

        for (let i = 0; i < deficitVacancyCount; i++) {
            let isDeficitForFirm = true;
            const firm = deficit.vacancies[i].firm;

            for (let j=0; j < closedVacancyCount; j++){
                if (firm === closed.vacancies[j].firm) {
                    isDeficitForFirm = false;
                    break;
                }
            }

            if (isDeficitForFirm)
                obj[firm] = true;
        }

        return Object.keys(obj);
    };


    getFirmNameWithDeficitVacancy(candidates){
        let deficitVacancy = this.getVacancy(candidates, true);
        const closedVacancy = this.getVacancy(candidates, false);
        return LaborExchange.unique(deficitVacancy, closedVacancy);
    }

}


let laborExchange = new LaborExchange();

for(let i = 0; i < 600; i++){
    let vacancy = new Vacancy();
    laborExchange.addVacancy(vacancy)
}


let vacancy = new Vacancy();
laborExchange.addVacancy(vacancy);

let newVacancy = new Vacancy();
newVacancy.firm = 'FIRM';
laborExchange.updateVacancy(vacancy, newVacancy);
//console.log(laborExchange.sortVacanciesBySalary());

let candidates = [];

for(let i = 0; i < 100; i++){
    let candidate = new Candidate();
    candidates.push(candidate);
    laborExchange.getVacancyForCandidate(candidate);
}


console.log('Дефицитные вакансии: ', laborExchange.getVacancy(candidates, true));
console.log('Список фирм, для которых не подобран ни один кандидат', laborExchange.getFirmNameWithDeficitVacancy(candidates));




