/* Задача имеет название, описание, дату начала и дату окончания.
   Любая задача может иметь набор дочерних подзадач. Создайте класс для представления задачи.

   Выполняемая задача – наследник задачи с дополнительным свойствами: процент выполнения (число)
   и флагом задача завершена. Реализуйте данное наследование.
 */


class Task {
    constructor(){
        this.name = "";
        this.description = "";
        this.start = Date();
        this.end = Date();
        this.tasks = [];
    }
}

class InProgressTask extends Task {
    constructor(){
        super();
        this.percent = 0;
        this.isComplete = false;
    }
}


const isHaveProperty = (object, property) => {
    if (object.hasOwnProperty(property)) {
        console.log(`В классе ${object.constructor.name} есть свойство ${property}`);
        return
    }
    console.log(`В классе ${object.constructor.name} нет свойства ${property}`);
};


const main = () => {
    const task = new Task();
    const inProgressTask = new InProgressTask();

    isHaveProperty(task, "isComplete");
    isHaveProperty(inProgressTask, "isComplete");
    isHaveProperty(inProgressTask, "name");
};


main();

