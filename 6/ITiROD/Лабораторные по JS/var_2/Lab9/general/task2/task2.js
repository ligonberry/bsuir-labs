/* Создайте класс Vector для представления вектора в трехмерном пространстве (свойства для координат x, y, z).
   Добавьте в прототип Vector два метода plus() и scalar() для вычисления суммы двух векторов и
   скалярного произведения двух векторов. Добавьте в прототип свойство только для чтения length,
   подсчитывающее длину вектора.
   Переопределите в классе Vector методы toString() и valueOf(). Протестируйте созданный класс. */

class Vector {
    constructor(x, y, z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    plus(vector){
        let vector_sum = new Vector(0, 0, 0);
        vector_sum.x = vector.x + this.x;
        vector_sum.y = vector.y + this.y;
        vector_sum.z = vector.z + this.z;
        return vector_sum;
    };

    scalar(vector){
        return vector.x * this.x + vector.y * this.y + vector.z * this.z;
    }

    get length() {
        return Math.pow(Math.pow(this.x, 3) + Math.pow(this.y, 3) + Math.pow(this.z, 3), 1/3);
    }

    toString(){
        return `Vector { x: ${this.x}, y: ${this.y}, z: ${this.y} }`;
    }

    valueOf(){
        return this.length;
    }
}


const main = () => {
    const vector1 = new Vector(0, 0, 0);
    const vector2 = new Vector(-1, 2, 0);

    console.log(`Строковое представление вектора = ${vector1.toString()}`);
    console.log(`Длинна вектора ${vector1.toString()} = ${vector1.valueOf()}`);
    console.log(`Результат сложения векторов ${vector1.toString()} + ${vector2.toString()} = ${vector1.plus(vector2)}`);
    console.log(`Результат скалярного произведения векторов ${vector1.toString()} x ${vector2.toString()} = ${vector1.scalar(vector2)}`);
};

main();

