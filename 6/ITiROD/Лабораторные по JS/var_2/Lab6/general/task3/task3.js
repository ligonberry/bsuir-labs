/*  3. Вычисление и вывод i-го числа Фибоначчи, где параметр i вводится пользователем.
    При вводе некорректных данных предусмотреть генерацию исключительной ситуации. */

const getPromptString = (indicator) => {
    return prompt(`Введите ${indicator} `);
};

const UserException = (message) => {
    // noinspection JSUnusedGlobalSymbols
    this.message = message;
    // noinspection JSUnusedGlobalSymbols
    this.name = "UserTask2Exception";
};

const getAlertNotInteger = (indicator) => {
    throw new UserException(`Введенный параметр '${indicator}' - не  целое число.`)
};


const getAlertNotPositive = (indicator) => {
    throw new UserException(`Введенный параметр '${indicator}' не может быть отрицательным.`)
};


const getCheckedInputNumber = (indicator) => {
    const input =  getPromptString(indicator);
    const number  = parseInt(input);

    if (Number.isNaN(number)) {
        getAlertNotInteger(indicator);
        getCheckedInputNumber(indicator);
    } else if (number < 0) {
        getAlertNotPositive(indicator);
        getCheckedInputNumber(indicator);
    }
    return number
};

// https://en.wikipedia.org/wiki/Fibonacci_number
const getFibonacciByBineFormula = (n) => {
    let index = Math.pow(5, 0.5);

    let left = (1 + index) / 2;
    let right = (1 - index) / 2;

    return Math.round((Math.pow(left, n) - Math.pow(right, n)) / index);
};

const main = () => {
    const number = getCheckedInputNumber('i (номер числа Фибоначчи)');
    const fibonacci_number = getFibonacciByBineFormula(number);
    alert(fibonacci_number)
};

main();
