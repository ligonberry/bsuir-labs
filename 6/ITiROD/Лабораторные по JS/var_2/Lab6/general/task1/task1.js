/* 1. В бесконечном цикле делается запрос на ввод двух чисел (два отдельных prompt).
   Числа сравниваются и выводится одна из трёх фраз: «числа равны», «первое число меньше»,
   «второе число меньше». Если пользователь ввёл не число, выводится фраза «первый ввод – не число»
   (или «второй ввод – не число»), и выполнение скрипта прекращается. */

const getPromptString = (order) => {
    return prompt(`Введите ${order}-ое число (целое или с плавающей точкой) `);
};


const getAlertString = (order) => {
    return alert(`${order}-ый ввод - не число`);
};

const getCheckedInputNumber = (order) => {

    const input =  getPromptString(order);
    const number  = parseFloat(input);

    if (Number.isNaN(number)){
        getAlertString(order);
        throw 'Ввод - не число'
    }
    return number
};

const compareNumbers = (number_1, number_2) => {
    if (number_1 > number_2) {
        alert('Второе число меньше');
    } else if (number_2 > number_1) {
        alert('Первое число меньше');
    } else {
        alert('Числа равны');
    }
};

const main = () => {
    // noinspection InfiniteLoopJS
    for (;;) {
        const number_1 = getCheckedInputNumber('1');
        const number_2 = getCheckedInputNumber('2');

        compareNumbers(number_1, number_2)
    }
};

main();
