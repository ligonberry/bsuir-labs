/*  2. Многоквартирный дом характеризуется следующими тремя показателями: этажность, число подъездов,
    количество квартир на этаже. Скрипт запрашивает эти показатели и номер квартиры. Выводится номер подъезда,
    в котором находится указанная квартира.
    При вводе некорректных данных предусмотреть генерацию исключительных ситуаций. */


const getPromptString = (indicator) => {
    return prompt(`Введите ${indicator} `);
};

const UserException = (message) => {
    // noinspection JSUnusedGlobalSymbols
    this.message = message;
    // noinspection JSUnusedGlobalSymbols
    this.name = "UserTask2Exception";
};

const getAlertNotInteger = (indicator) => {
    throw new UserException(`Введенный параметр '${indicator}' - не целое число.`)
};


const getAlertNotPositive = (indicator) => {
    throw new UserException(`Введенный параметр '${indicator}' не может быть отрицательным.`)
};


const getCheckedInputNumber = (indicator) => {
    const input =  getPromptString(indicator);
    const number  = parseInt(input);

    if (Number.isNaN(number)) {
        getAlertNotInteger(indicator);
        getCheckedInputNumber(indicator);
    } else if (number <= 0) {
        getAlertNotPositive(indicator);
        getCheckedInputNumber(indicator);
    }
    return number
};

const getAllEntranceApartmentCount = (countOfFloors, countOfApartments) => {
    return countOfFloors * countOfApartments;
};


const getEntrancesNumber = (allEntranceApartmentCount, apartmentNumber, countOfEntrances) => {
    const entrancesNumber = Math.trunc(apartmentNumber / allEntranceApartmentCount) + 1;
    if (entrancesNumber > countOfEntrances) {
        throw new UserException(`Квартира с номером '${apartmentNumber} не может находится в доме
                            с заданными параметрами.`)
    }
    return entrancesNumber;
};


const main = () => {

    const countOfFloors = getCheckedInputNumber('этажность');
    const countOfEntrances = getCheckedInputNumber('число подъездов');
    const countOfApartments = getCheckedInputNumber('количество квартир на этаже');
    const apartmentNumber = getCheckedInputNumber('номер квартиры');

    const allEntranceApartmentCount = getAllEntranceApartmentCount(countOfFloors, countOfApartments);
    const entrancesNumber = getEntrancesNumber(allEntranceApartmentCount, apartmentNumber, countOfEntrances);
    alert(`Квартира с номером ${apartmentNumber} находится в подъезде №${entrancesNumber}`);

};

main();
