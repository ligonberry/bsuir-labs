/* 1 января 2015 года – это был четверг. Скрипт запрашивает номер месяца (1..12) и число в этом месяце (1..31).
   Выведите имя дня недели. */

const isInputDayValid = (day) => {
    if (isNaN(day) || day <= 0 || day > 31){
        alert(`Введенный параметр ${day} некорректнен`);
        return false
    }
    return true
};


const isInputMonthValid = (month) => {
    if (isNaN(month) || month <= 0 || month > 12){
        alert(`Введенный параметр ${month} некорректнен`);
        return false
    }
    return true
};


const isInputYearValid = (year) => {
    if (isNaN(year) || year <= 0){
        alert(`Введенный параметр ${year} некорректнен`);
        return false
    }
    return true
};


const isInputDateValid = (date) => {
    if (date === undefined){
        alert(`Введенный параметр ${date} некорректнен`);
        return false
    }
    return true
};


const getValidInputData = () => {
    const inputDay = 'Введите день';
    const inputMonth = 'Введите месяц';
    const inputYear = 'Введите год';

    const day = parseInt(prompt(inputDay));
    if (!isInputDayValid(day)){
        return
    }

    const month = parseInt(prompt(inputMonth));
    if (!isInputMonthValid(month)){
        return
    }

    const year = parseInt(prompt(inputYear));
    if (!isInputYearValid(year)){
        return
    }

    const date = new Date(year + '-' + month + '-' + day);
    if (isInputDateValid(date)) {
        return date;
    }
};

const main = () => {
    const date = getValidInputData();

    let dayOfWeek = '';
    switch(date.getDay()){
        case 0: dayOfWeek = 'Воскресенье'; break;
        case 1: dayOfWeek = 'Понедельник'; break;
        case 2: dayOfWeek = 'Вторник'; break;
        case 3: dayOfWeek = 'Среда'; break;
        case 4: dayOfWeek = 'Четверг'; break;
        case 5: dayOfWeek = 'Пятница'; break;
        default: dayOfWeek = 'Cуббота'; break;
    }
    alert('День недели: ' + dayOfWeek);
};

main();
