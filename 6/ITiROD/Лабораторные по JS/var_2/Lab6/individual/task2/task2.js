/* 2. Дана строка, состоящая из слов, разделенных пробелами.
      Вывести на экран порядковый номер слова минимальной длины и количество символов в этом слове. */


const main = () => {
    const str = ('Because many word pairs English homophones (words that sound alike), ' +
        'some mistakes that people make when speaking aren’t noticeable others. For example, ' +
        'were tell you that today its cold outside dog lost it’s collar');

    const wordArray = str.split(" ");
    const wordArraySort = wordArray.slice();
    const minWord = wordArraySort.sort((a, b) => a.length - b.length)[0];
    alert(`Порядковый номер слова минимальной длинны - ${wordArray.indexOf(minWord)}. Количество символов в нем - ${minWord.length}`)

};

main();
