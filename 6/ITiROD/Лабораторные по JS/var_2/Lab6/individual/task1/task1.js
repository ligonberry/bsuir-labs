/* 1. Необходимо разложить функцию Y(x) из своего варианта в ряд S(x),
   а затем с помощью полученного ряда найти значение функции и сравнить его со значением,
   вычисленным с помощью стандартных функций. Программа должна:

    - сначала запросить у пользователя количество членов ряда (n);
    - затем запросить у пользователя количество чисел, от которых он хочет посчитать функцию;
    - затем пользователь вводит по одному числу (x от 0.1 до 1),
      программа считает значение функции с помощью ряда и с помощью стандартных функций и выводит оба значения.	 */

const getPromptString = (indicator) => {
    return prompt(`Введите ${indicator} `);
};

const UserException = (message) => {
    // noinspection JSUnusedGlobalSymbols
    this.message = message;
    // noinspection JSUnusedGlobalSymbols
    this.name = 'UserTask1Exception';
};

const getAlertNotNumber = (indicator) => {
    throw new UserException(`Введенный параметр '${indicator}' - не число.`)
};


const getAlertNotPositive = (indicator) => {
    throw new UserException(`Введенный параметр '${indicator}' не может быть отрицательным.`)
};

const getCheckedInputNumber = (indicator) => {
    const input =  getPromptString(indicator);
    const number  = parseFloat(input);

    if (!Number.isFinite(number)) {
        getAlertNotNumber(indicator);
        getCheckedInputNumber(indicator);
    } else if (number < 0) {
        getAlertNotPositive(indicator);
        getCheckedInputNumber(indicator);
    }
    return number;
};


const factorial = (n) => {
    let val = 1;
    for (let i = 2; i <= n; i++) {
        val = val * i;
    }
    return val;
};

const getSeriesResult = (x, n) => {
    let result = 0;
    for (let i = 0; i < n; i += 1) {
        result += (Math.pow(i+1, 2) * Math.pow(x, i+1)) / factorial(2*(i+1) + 1);
    }
    return result;
};

const getFunctionResult = (x) => {
    let sqrt_x = Math.sqrt(x);
    return 0.25 * ((x + 1) * Math.sinh(sqrt_x) / sqrt_x - Math.cosh(sqrt_x));
};


const main = () => {
    const n = getCheckedInputNumber('количество членов ряда');
    const nums = getCheckedInputNumber('количество числел, от которых вы хотите посчитать функцию');

    for (let i = 0; i < nums; i += 1) {
        let x = getCheckedInputNumber(`x от 0.1 до 1 (${i + 1} значение)`);
        console.log('Вычисление через ряд: ', getSeriesResult(x, n));
        console.log('Вычисление через функцию: ', getFunctionResult(x, n));
    }
};

main();
