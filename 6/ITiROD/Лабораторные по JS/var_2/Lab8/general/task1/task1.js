/* Напишите функцию range(), принимающую два аргумента, начало и конец диапазона, и возвращающую массив,
   который содержит все числа из диапазона, включая начальное и конечное.
   Третий необязательный аргумент функции range() – шаг для построения массива.
   Убедитесь, что функция range() работает с отрицательным шагом: например,
   вызов range(5, 2, -1) возвращает [5, 4, 3, 2]. */


const range = (start, end, step=1) => {
    const result = [];

    const min = Math.min(start, end);
    const max = Math.max(start, end);

    for (let i=min; i<=max; i+=Math.abs(step))
        result.push(i);

    if (step > 0) {
        return result;
    }
    return result.reverse();

};

const main = () => {
    console.log(range(5,2));
    console.log(range(5,2, -1));

    console.log('\n');
    console.log(range(-1,-10));
    console.log(range(-1,-10, -1));

};

main();
