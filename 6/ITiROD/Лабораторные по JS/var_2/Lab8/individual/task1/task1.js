/* Создать функцию createMatrix(), принимающую количество строк и количество столбцов матрицы и
   возвращающее матрицу (массив массивов), заполненную случайными числами в диапазоне от 0 до 100.
   Написать функцию, выполняющую операции в соответствием с индивидуальным заданием.

   Самостоятельно реализуйте любой алгоритм сортировки массива (без использования библиотечных функций).
   Желательно, чтобы ваша функция сортировки могла принимать компаратор для сравнения элементов
   (как это делает стандартный метод sort()).

   Дана действительная квадратная матрица порядка N. Найти сумму и произведение элементов,
   расположенных в заштрихованной части матрицы, см. рисунок «б». */


const getVector = (n) => {
    let vector = [];
    for (let i = 0; i < n; i++) {
        vector[i] = Math.floor(Math.random() * 100);
    }
    return vector
};


const createMatrix = (n, m) => {
    let matrix = [];
    for (let j=0; j < m; j++) {
        matrix[j] = getVector(n);

    }
    //console.log(matrix);
    return matrix
};

const compare = (a1, a2) => a1 - a2;

const sort = (arr, cmp=compare) => {
    for (let i = 0; i < arr.length; i++) {
        for (let j = i; j > 0; j--) {
            if (cmp(arr[j], arr[j - 1]) < 0) {
                [arr[j], arr[j - 1]] = [arr[j - 1], arr[j]];
            }
        }
    }
    return arr;
};


const getSumAndProduct = (n) => {
    let result_product = 1;
    let result_sum = 0;

    const martix = createMatrix(n, n);
    console.log('Матрица:');
    console.log(martix);

    const len = Math.floor(n / 2);

    for (let j=0; j<=n; j++) {
        for (let i=0; i<=n; i++) {
            if (i <= j && j < len) {
                //console.log(martix[j][i]);
                result_sum += martix[j][i];
                result_product *= martix[j][i];
            }

            if (i < (n - j) && j >= len) {
                //console.log(martix[j][i]);
                result_sum += martix[j][i];
                result_product *= martix[j][i];
            }

            if (n-i-1 <= j && i < n && j < len) {
                //console.log(martix[j][i], i, j);
                result_sum += martix[j][i];
                result_product *= martix[j][i];
            }

            if (n-i-1 < n-j && i < n && j >= len) {
                //console.log(martix[j][i], i, j);
                result_sum += martix[j][i];
                result_product *= martix[j][i];
            }
        }
    }

    const center_elem = martix[len][len];
    result_sum = result_sum - center_elem;
    result_product = result_product / center_elem;

    return {result_sum, result_product}
};

const main = () => {
    console.log('Сгенерированная матрица =', createMatrix(2, 3));
    console.log();

    const vector = getVector(5);
    console.log('Исходный массив:');
    console.log(vector);
    console.log();
    console.log('Отсортированный массив:');
    console.log(sort(vector));
    console.log();

    const result = getSumAndProduct(5);
    console.log('Сумма элементов: ', result.result_sum);
    console.log('Произведение элементов: ', result.result_product);
};

main();

