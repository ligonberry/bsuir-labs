/* Декоратор функции получает на вход существующую функцию и возвращает новую функцию, изменяющую
   (дополняющую) поведение исходной.
   2. Создать декоратор для функции с произвольным количеством параметров,
      дополняющий переданную функцию проверкой всех аргумента на указанный тип.*/

const isAllArgumentsBelongToType = (func, type) => {
    return function f(){
        for (let i = 0; i < arguments.length; i++){
            if (typeof(arguments[i]) !== type)
                return `Переменная ${arguments[i]} не типа ${type}`;
        }
        return func.apply(this, arguments);
    }
};


const main = () => {
    const typeCheckFunction = isAllArgumentsBelongToType(Math.min, "number");
    console.log('TEST 1');
    console.log(typeCheckFunction('some string'));
    console.log('TEST 2');
    console.log(typeCheckFunction(1, 10, -1, 3.4, -5.67));
};

main();
