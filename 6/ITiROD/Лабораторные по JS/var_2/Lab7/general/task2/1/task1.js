/* Декоратор функции получает на вход существующую функцию и возвращает новую функцию, изменяющую
   (дополняющую) поведение исходной.
   1. Создать декоратор для функции с одним параметром, дополняющий переданную функцию проверкой
      аргумента на тип number. */

const isNumberType = (func) => {
    return function f(){
        if(typeof(arguments[0]) !== 'number')
            return `Переменная '${arguments[0]}' - не число`;
        return func.apply(this, arguments);
    }
};


const main = () => {
    const numberCheckFunction = isNumberType(Math.sqrt);
    console.log('TEST 1');
    console.log(numberCheckFunction('some string'));

    console.log('TEST 2');
    console.log(numberCheckFunction(4));
};

main();
