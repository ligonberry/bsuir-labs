/* На декартовой плоскости прямоугольник задаётся четырьмя точками – своими вершинами (у каждой точки две числовые координаты).
   Вершины перечисляются последовательно, в порядке обхода по часовой стрелке, начиная с произвольной вершины.
    1.	Написать функцию, проверяющую, образуют ли заданные восемь чисел вершины некоего прямоугольника.  */

const getPromptString = (indicator) => {
    return prompt(`Введите ${indicator} `);
};


const UserException = (message) => {
    // noinspection JSUnusedGlobalSymbols
    this.message = message;
    // noinspection JSUnusedGlobalSymbols
    this.name = "UserTask2Exception";
};


const getAlertNotNumber = (indicator) => {
    throw new UserException(`Введенный параметр '${indicator}' - не число.`)
};


const getCheckedInputNumber = (indicator) => {
    const input =  getPromptString(indicator);
    const number  = parseInt(input);

    if (Number.isNaN(number)) {
        getAlertNotNumber(indicator);
    }
    return number
};


const getVectorLength = (startX, startY, endX, endY) => {
    return Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));
};


const isRectangle = (x1, y1, x2, y2, x3, y3, x4, y4) => {
    const a = getVectorLength(x1, y1, x2, y2);
    const b = getVectorLength(x2, y2, x3, y3);
    const c = getVectorLength(x3, y3, x4, y4);

    const d1 = getVectorLength(x1, y1, x3, y3);
    const d2 = getVectorLength(x2, y2, x4, y4);

    const d1Sides = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
    const d2Sides = Math.sqrt(Math.pow(b, 2) + Math.pow(c, 2));

    return d1 === d1Sides && d2 === d2Sides;

};


const main = () => {
    /*
    const x1 = getCheckedInputNumber('x1');
    const y1 = getCheckedInputNumber('y1');

    const x2 = getCheckedInputNumber('x2');
    const y2 = getCheckedInputNumber('y2');

    const x3 = getCheckedInputNumber('x3');
    const y3 = getCheckedInputNumber('y3');

    const x4 = getCheckedInputNumber('x4');
    const y4 = getCheckedInputNumber('y4');

    console.log(isRectangle(x1, y1, x2, y2, x3, y3, x4, y4))
    */
    console.log("TEST 1");
    console.log(isRectangle(0,0,0,1,1,1,1,0));

    console.log("TEST 2");
    console.log(isRectangle(1,1,2,2,3,3,5,4));
};

main();


