/* На декартовой плоскости прямоугольник задаётся четырьмя точками – своими вершинами (у каждой точки две числовые координаты).
   Вершины перечисляются последовательно, в порядке обхода по часовой стрелке, начиная с произвольной вершины.
    2.	Написать функцию, проверяющую принадлежность указанной точки заданному прямоугольнику. */

// Правильно объявленный прямоугольник
const RECTANGLE_POINTS = [[0, 0], [0, 1], [1, 1], [1, 0]];


const getPromptString = (indicator) => {
    return prompt(`Введите ${indicator} `);
};


const UserException = (message) => {
    // noinspection JSUnusedGlobalSymbols
    this.message = message;
    // noinspection JSUnusedGlobalSymbols
    this.name = "UserTask2Exception";
};


const getAlertNotNumber = (indicator) => {
    throw new UserException(`Введенный параметр '${indicator}' - не число.`)
};


const getCheckedInputNumber = (indicator) => {
    const input =  getPromptString(indicator);
    const number  = parseFloat(input);

    if (Number.isNaN(number)) {
        getAlertNotNumber(indicator);
    }
    return number;
};


const isInRectangle = (point) => {
    if (point[0] >= RECTANGLE_POINTS[0][0] && point[0] <= RECTANGLE_POINTS[2][0] &&
        point[1] >= RECTANGLE_POINTS[0][1] && point[1] <= RECTANGLE_POINTS[2][1]) {
        return `Точка ${point} находится внутри прямоугольника ${RECTANGLE_POINTS}`
    }
    return `Точка ${point} находится вне прямоугольника ${RECTANGLE_POINTS}`;
};


const main = () => {
    const x = getCheckedInputNumber("x");
    const y = getCheckedInputNumber("y");
    const point = [x, y];
    console.log(isInRectangle(point));
};


main();



