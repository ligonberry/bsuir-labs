/* Найти наибольшее количество предложений текста, в которых есть одинаковые слова. */

const unique = (arr) => {
    const obj = {};

    for (let i = 0; i < arr.length; i++) {
        const str = arr[i];
        obj[str] = true;
    }

    return Object.keys(obj);
};


const getMaxCount = (sentencesArray) => {
    let result = 0;
    for (let i=0; i < sentencesArray.length; i++) {
        const wordArray = sentencesArray[i].split(' ');

        const raw_len = wordArray.length;
        const unique_len = unique(wordArray).length;

        if (raw_len !== unique_len) {
            result += 1
        }
    }
    return result
};

const main = () => {
    const str = ('Because many many word pairs word English homophones.' +
        'Some mistakes that people make when speaking aren’t noticeable others.' +
        'For example example were tell you that today its cold outside dog lost it’s collar.');

    const sentencesArray = str.split('.');
    console.log(getMaxCount(sentencesArray))

};

main();
