/* Вычислить значение интеграла. Обеспечить выбор пользователем значения требуемой точности,
   начальной и конечной точки, численного метода. */


const function1 = (x) => (Math.sqrt(1.5 * Math.pow(x, 2) + 2.3)) / (3 + Math.sqrt(0.3 * x + 1));


const function2 = (x) => 1 / (Math.sqrt(3 * Math.pow(x, 2) + 1));


const function3 = (x) => Math.cos(0.3*x + 0.5) / (1.8 + Math.sin(Math.pow(x, 2) + 0.8));


const function4 = (x) => Math.log(Math.pow(x, 2) + 2) / (x + 1);


const getSumResult = (fn, a, h, n) => {
    let result_sum = 0;
    for (let i=1; i < n; i++) {
        result_sum += 2 * fn(a + h*i)
    }
    return result_sum
};


const trapezoidalRuleAlgorithm = (fn, a, b, n) => {
    const h = (b - a) / n;

    const result_f = fn(a) + fn(b);
    const result_sum = getSumResult(fn, a, h, n);

    return (h / 2) * (result_f + result_sum)
};


const rectangleRuleAlgorithm = (fn, a, b) => {
    const result_f = fn((a + b) / 2);
    return (b - a) * result_f
};


const main = () => {
    const n = 1000;

    console.log(`Результат для первого интеграла (метод прямоугольников) = ${rectangleRuleAlgorithm(function1, 0.8, 2.4)}`);
    console.log(`Результат для первого интеграла (метод трапеций) = ${trapezoidalRuleAlgorithm(function1, 0.8, 2.4, n)}`);
    console.log('\n');

    console.log(`Результат для второго интеграла (метод прямоугольников) = ${rectangleRuleAlgorithm(function2, 1.4, 2.2)}`);
    console.log(`Результат для второго интеграла (метод трапеций) = ${trapezoidalRuleAlgorithm(function2, 1.4, 2.2, n)}`);
    console.log('\n');

    console.log(`Результат для третьего интеграла (метод прямоугольников) = ${rectangleRuleAlgorithm(function3, 0.3, 1.1)}`);
    console.log(`Результат для третьего интеграла (метод трапеций) = ${trapezoidalRuleAlgorithm(function3, 0.3, 1.1, n)}`);
    console.log('\n');

    console.log(`Результат для четвертого интеграла (метод прямоугольников) = ${rectangleRuleAlgorithm(function4, 1.4, 2.2)}`);
    console.log(`Результат для четвертого интеграла (метод трапеций) = ${trapezoidalRuleAlgorithm(function4, 1.4, 2.2, n)}`);
};

main();
