import time
from string import ascii_uppercase

from caesarcipher import CaesarCipher


class MyCaesarCipher:

    def __init__(self,
                 base_shift=3,
                 encrypt_path='encrypt.txt',
                 decrypt_path='decrypt.txt',
                 alphabet=ascii_uppercase):
        self.alphabet = alphabet
        self._alphabet_len = len(self.alphabet)
        self.shift = base_shift
        self.encrypt_path = encrypt_path
        self.decrypt_path = decrypt_path

    def _get_shifted_encrypt_char(self, char):
        return self.alphabet[(self.alphabet.index(char) + self.shift)
                             % self._alphabet_len]

    def _encrypt(self, text):
        encrypted = ""
        for char in text:
            if char not in self.alphabet:
                encrypted += char
                continue
            encrypted += self._get_shifted_encrypt_char(char)
        return encrypted

    def encrypt_file(self):
        with open(self.encrypt_path, 'r') as file:
            text = file.read()
            return self._encrypt(text)

    def _get_shifted_decrypt_char(self, char):
        return self.alphabet[(self.alphabet.index(char) - self.shift)
                             % self._alphabet_len]

    def _decrypt(self, text):
        decrypted = ""
        for char in text:
            if char not in self.alphabet:
                decrypted += char
                continue
            decrypted += self._get_shifted_decrypt_char(char)
        return decrypted

    def decrypt_file(self):
        with open(self.decrypt_path, 'r') as file:
            text = file.read()
            return self._decrypt(text)


def read_file(path):
    with open(path, 'r') as file:
        text = file.read()
        return text


def test_encrypt():
    text = read_file('encrypt.txt')

    t11 = time.time()
    cipher_ = CaesarCipher(text, offset=3)
    cipher_encode_ = cipher_.encoded
    t12 = time.time()

    t21 = time.time()
    cipher = MyCaesarCipher()
    cipher_encode = cipher.encrypt_file()
    t22 = time.time()

    assert cipher_encode_ == cipher_encode
    print('Standard library ', t12 - t11)
    print('My function ', t22 - t21)


def test_decrypt():
    text = read_file('decrypt.txt')

    t11 = time.time()
    cipher_ = CaesarCipher(text, offset=3)
    cipher_decode_ = cipher_.decoded
    t12 = time.time()

    t21 = time.time()
    cipher = MyCaesarCipher()
    cipher_decode = cipher.decrypt_file()
    t22 = time.time()

    assert cipher_decode_ == cipher_decode
    print('Standard library ', t12 - t11)
    print('My function ', t22 - t21)


if __name__ == '__main__':
    print('Encode algorithm')
    test_encrypt()
    print()
    print('Decode algorithm')
    test_decrypt()
