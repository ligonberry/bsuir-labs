import json
import string
import random
import datetime

from des import decrypt, encrypt
from response_ import Response

SERVERS = {}


def get_random_id(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class AuthServer:

    def __init__(self):
        self._db = {'ligonberry': ['ligonkey']}
        self._access_key = get_random_id()  # Kc_tgs
        self._tgs_key = get_random_id()  # Kas_tgs
        self._server_tgs_key = get_random_id()  # Ktgs_ss

    # noinspection PyBroadException
    def authenticate_request(self, login):
        try:
            key = self._db[login][0]  # Kc
            access_key = self._access_key  # Kc_tgs
            tgs_key = self._tgs_key  # Kas_tgs

            server_id = 1  # tgs
            access_date = datetime.datetime.now().timestamp()  # t1
            access_period = 60 * 1200  # p1

            tgt = json.dumps([login,
                              server_id,
                              access_date,
                              access_period,
                              access_key])

            data = json.dumps([encrypt(tgs_key, tgt), access_key])

            return Response(Response.OK, encrypt(key, data))

        except BaseException:
            return Response(Response.BAD_RESPONSE, None)

    def access_request(self, packed_data):
        data = json.loads(packed_data)

        tgt = json.loads(decrypt(self._tgs_key, data[0]))
        auth = json.loads(decrypt(tgt[4], data[1]))  # Aut1
        server_id = data[2]  # ID

        if auth[0] != tgt[0]:
            print('ДОСТУП ЗАПРЕЩЕН!')
            return Response(Response.BAD_RESPONSE, None)

        if auth[1] > tgt[2] + tgt[3]:
            print('ВРЕМЯ СЕССИИ ИСТЕКЛО!')
            return Response(Response.BAD_RESPONSE, None)

        server_tgs_key = self._server_tgs_key  # Ktgs_ss
        SERVERS[server_id].user_access = server_tgs_key
        new_access_key_to_server = get_random_id()  # Kc_ss
        access_key = self._access_key  # Kc_tgs

        login = auth[0]  # c
        ss = tgt[1]
        stamp = datetime.datetime.now().timestamp()  # t3
        period = 2 * 60 * 1000  # p2

        tgs = json.dumps([login, ss, stamp, period, new_access_key_to_server])
        response_data = json.dumps([encrypt(server_tgs_key, tgs), new_access_key_to_server])

        return Response(Response.OK, encrypt(access_key, response_data))
