class Response:
    BAD_RESPONSE = 501
    NOT_AUTH = 400
    OK = 0

    def __init__(self, status, payload):
        self.status = status
        self.payload = payload
