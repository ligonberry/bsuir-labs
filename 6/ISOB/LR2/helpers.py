import constants


def nsplit(s, n):  # 'Разбивает последовательность на len(s) кусков длинны n'
    return [s[k:k + n] for k in range(0, len(s), n)]


def permut(block, table):
    return [block[x - 1] for x in table]


def expand(block, table):
    return [block[x - 1] for x in table]


def xor(t1, t2):
    return [x ^ y for x, y in zip(t1, t2)]


def shift(g, d, n):
    return g[n:] + g[:n], d[n:] + d[:n]


def binvalue(val, bitsize):  # Return the binary value as a string of the given size
    binval = bin(val)[2:] if isinstance(val, int) else bin(ord(val))[2:]
    if len(binval) > bitsize:
        raise TypeError("Двоичное значение больше ожидаемого размера")

    while len(binval) < bitsize:
        binval = "0" + binval

    return binval


def substitute(d_e):
    subblocks = nsplit(d_e, 6)
    result = []
    for i in range(len(subblocks)):
        block = subblocks[i]
        row = int(str(block[0]) + str(block[5]), 2)
        column = int(''.join([str(x) for x in block[1:][:-1]]), 2)
        val = constants.S_BOX[i][row][column]
        binary = binvalue(val, 4)
        result += [int(x) for x in binary]

    return result


def convert_to_bit_arr(text):
    array = []
    for char in text:
        binval = binvalue(char, 8)
        array.extend([int(x) for x in list(binval)])
    return array


def convert_to_string(array):
    res = ''.join([chr(int(y, 2)) for y in [''.join([str(x) for x in bytes_]) for bytes_ in nsplit(array, 8)]])
    return res