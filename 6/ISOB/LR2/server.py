import json

from auth_server import get_random_id
from des import decrypt, encrypt
from response_ import Response


class Server:
    def __init__(self):
        self.id = get_random_id()
        self.user_access = None  # Ktgs_ss
        self.key_to_server = None  # Kc_ss
        self.auth_data = None   # Aut2

    def _process_request(self, data):
        key_to_server = self.key_to_server  # Kc_ss
        stamp = self.auth_data[1] + 1  # t4 + 1
        response = 'RESPONSE'
        if data == 'request/':
            return Response(Response.OK, encrypt(key_to_server, json.dumps([response, stamp])))
        else:
            return Response(Response.BAD_RESPONSE, None)

    def request(self, packed_data, login):
        data = json.loads(packed_data)
        tgs = json.loads(decrypt(self.user_access, data[0]))
        self.key_to_server = tgs[4]  # Kc_ss
        self.auth_data = json.loads(decrypt(self.key_to_server, data[1]))  # Aut2
        if self.auth_data[0] != login:
            print('НЕПРАВИЛЬНЫЙ ЛОГИН!')
            return Response(Response.BAD_RESPONSE, None)

        return self._process_request(self.auth_data[2])
