import json
import datetime

from auth_server import AuthServer, SERVERS
from des import decrypt, encrypt
from response_ import Response
from server import Server

DELIMITER = '-' * 150


class Client:

    def __init__(self, auth_key, login):
        self.auth_key = auth_key  # Kc
        self.login = login
        self.access_key = None  # Kc_tgs
        self.tgt = None
        self.key_to_server = None  # Kc_ss
        self.tgs = None

    def authenticate(self, auth_server):
        print('Начальные данные')
        print(f'c: {self.login}, Kc: {self.auth_key}')
        print(DELIMITER)
        print()

        print(f'ШАГ 1. Отправка идентификатора: C -> AS: (c: {self.login})')
        # 'Клиент C посылает серверу аутентификации AS свой идентификатор c
        # (идентификатор передается открытым текстом).'
        print(DELIMITER)
        print()

        response = auth_server.authenticate_request(self.login)

        if response.status == Response.BAD_RESPONSE:
            print('НЕ АВТОРИЗОВАН!')
            return

        print('ШАГ 2. Получение билета от сервера аутентификации: AS->C: (('
              'TGT) KAS_TGS, KC_TGS)')
        # На этом шаге сервер аутентификации AS, проверив, что клиент C
        # имеется в его базе, возвращает ему билет для доступа к серверу
        # выдачи разрешений и ключ для взаимодействия с сервером выдачи
        # разрешений.
        data = json.loads(decrypt(self.auth_key, response.payload))
        self.tgt = data[0]
        print(f'TGT (билет на доступ к серверу выдачи разрешений): {self.tgt}')
        self.access_key = data[1]
        print(f'Kc_tgs (ключ, выдаваемый C для доступа к серверу выдачи '
              f'разрешений TGS): {self.access_key}')
        print(DELIMITER)
        print()

    def send_access(self, auth_server, server):
        auth_1 = json.dumps([self.login, datetime.datetime.now().timestamp()])
        data = json.dumps([self.tgt, encrypt(self.access_key, auth_1),
                           server.id])

        print('ШАГ 3. Обращение к серверу выдачи разрешений ТGS: C->TGS (('
              'TGT) KAS_TGS, (Aut1) KC_TGS, (ID))')
        # Клиент C на этот раз обращается к серверу выдачи разрешений ТGS.
        # Он пересылает полученный от AS билет, зашифрованный на ключе KAS_TGS,
        # и аутентификационный блок, содержащий идентификатор c и метку времени,
        # показывающую, когда была сформирована посылка.
        response = auth_server.access_request(data)
        if response.status == Response.BAD_RESPONSE:
            print('Неправильный запрос')
            return
        print(DELIMITER)
        print()

        print('ШАГ 4. TGS посылает клиенту C ключ шифрования и билет: TGS->C: '
              '((TGS) KTGS_SS, KC_SS) KC_TGS')
        response_data = json.loads(decrypt(self.access_key, response.payload))
        self.tgs = response_data[0]
        print(f'TGS (расшифрован с помощью Ktgs_ss): {self.tgs}')
        self.key_to_server = response_data[1]  # Kc_ss
        print(f'Kc_ss: {self.key_to_server}')
        print(DELIMITER)
        print()

    def send_request(self, server, request):
        auth_2 = json.dumps([self.login, datetime.datetime.now().timestamp(),
                             request])
        data = json.dumps([self.tgs, encrypt(self.key_to_server, auth_2)])

        print('ШАГ 5. C посылает билет, полученный от сервера выдачи '
              'разрешений, и свой аутентификационный блок серверу SS')
        response = server.request(data, self.login)

        if response.status == Response.BAD_RESPONSE:
            print('Неправильный запрос')
            return

        response_data = json.loads(decrypt(self.key_to_server,
                                           response.payload))
        if response_data[1] != json.loads(auth_2)[1] + 1:
            print('Сервер не отвечает')
            return
        print(DELIMITER)
        print()

        print('ШАГ 6. SS должен доказать C свою подлинность.')
        print(f'Расшифровка: {response_data[0]}')


if __name__ == '__main__':
    try:
        client = Client('ligonkey', 'ligonberry')
        server_1 = Server()
        SERVERS[server_1.id] = server_1
        auth_server_1 = AuthServer()

        client.authenticate(auth_server_1)
        client.send_access(auth_server_1, server_1)

        client.send_request(server_1, 'request/')

    except ValueError:
        print('Failed to connect')
