import constants
import helpers as hlp


def generate_keys(key):
    # 'Генерируем последовательность ключей для работы алгоритма DES'
    keys = []
    key = hlp.convert_to_bit_arr(key)
    key = hlp.permut(key, constants.CP_1)
    d, c = hlp.nsplit(key, 28)
    for i in range(16):
        d, c = hlp.shift(d, c, constants.SHIFT[i])
        keys.append(hlp.permut(d + c, constants.CP_2))
    return keys


def run(key, text, action=constants.ENCRYPT):

    # 'Проверка на допустимость заданного ключа'
    if len(key) < 8:
        raise TypeError("Ключ должен быть 56-битным!")
    elif len(key) > 8:
        return key[:8]

    # 'Проверка на допустимость заданного блока текста'
    if action == constants.ENCRYPT:
        text = add_padding(text)
    elif len(text) % 8 != 0:
        raise TypeError("Размер данных должен быть кратным 8")

    # 'Подготовительная фаза алгоритма DES'
    keys = generate_keys(key)  # 'Генерируем последовательность ключей для работы алгоритма DES'
    text_blocks = hlp.nsplit(text, 8)  # 'Разбиваем данные на кусочки длинны 8 байт'
    result = []  # 'Объявляем массив для битового представления конечного результата'

    # 'Основная фаза алгоритма DES'
    for block in text_blocks:
        block = hlp.convert_to_bit_arr(block)  # 'Конвертируем последовательности символов в массив битов'
        block = hlp.permut(block, constants.PI)  # 'Забеливание (переупорядочиваются в соответствии со стандартной таблицей)
        l, r = hlp.nsplit(block, 32)
        for i in range(16):  # '16 раундов одной и той же функции, которая использует операции сдвига и подстановки'
            d_e = hlp.expand(r, constants.E)
            if action == constants.ENCRYPT:  # 'Операции сдвига алгоритма DES'
                tmp = hlp.xor(keys[i], d_e)
            else:
                tmp = hlp.xor(keys[15 - i], d_e)
            tmp = hlp.substitute(tmp)  # 'Операция подстановки'
            tmp = hlp.permut(tmp, constants.P)  # 'Операция переупорядочивания'
            tmp = hlp.xor(l, tmp)
            l = r
            r = tmp
        # 'Добавление промежуточного результата в массив битового представления конечного результата'
        result += hlp.permut(r + l, constants.PI_1)

    # 'Конвертация из битового представления в текстовое'
    final_res = hlp.convert_to_string(result)

    if action == constants.DECRYPT:
        return remove_padding(final_res)
    else:
        return final_res


def add_padding(text):
    pad_len = 8 - (len(text) % 8)
    text += pad_len * chr(pad_len)
    return text


def remove_padding(data):
    pad_len = ord(data[-1])
    return data[:-pad_len]


def encrypt(key, text):
    return run(key, text)


def decrypt(key, text):
    return run(key, text, constants.DECRYPT)

