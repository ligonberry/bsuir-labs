#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <zconf.h>

int main() {
    // Объявление используемых переменных и структур данных
    int currentSocket, newSocket;
    u_int16_t port = 8000;
    struct sockaddr_in currentAddress, destinationAddress;
    char buffer[100];

    currentSocket = socket(AF_INET, SOCK_STREAM, 0);
    puts("1. Создание соединения...\n");

    // 2. Конфигурация соединения.
    puts("2.1 Конфигурация соединения...\n");
    memset(&currentAddress, 0, sizeof(struct sockaddr_in));
    currentAddress.sin_family = AF_INET;
    currentAddress.sin_port = htons(port);
    bind(currentSocket, (struct sockaddr *) &currentAddress, sizeof(struct sockaddr_in));
    printf("2.2 Соединение установлено с портом %d. \n", port);
    puts("");

    // 3. Начинаем слушать все соединения в бесконечном цикле
    listen(currentSocket, 5);
    puts("3. Готов к установлению соединения и принятию сообщений.\n");

    unsigned int destinationLen = sizeof(destinationAddress);
    for(;;) {
        // 4. Принятие соединения
        puts("====================================");
        puts("4. Принятие сообщений...\n");
        newSocket = accept(currentSocket, (struct sockaddr *) &destinationAddress, &destinationLen);


        if (fork() == 0) // Если процесс для обработки переданных пакетов данных создан успешно, то
        {
            close(currentSocket);

            // 5. Чтение переданных пакетов данных
            memset(buffer, 0, sizeof(buffer));
            puts("5. Чтение переданных пакетов данных.\n");
            (int) read(newSocket, buffer, 400);
            printf("Приняты пакеты данных: \n%s\n", buffer);

            // 6. Закрытие соединения
            puts("6. Закрытие соединения\n");
            close(newSocket);
            return 0;
        }
        else
            close(newSocket);  // Закрыть соединение, если не возможно создать процесс для обработки переданных пакетов
    }
}