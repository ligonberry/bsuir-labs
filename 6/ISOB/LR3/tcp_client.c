#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <zconf.h>
#include <arpa/inet.h>

int main() {
    puts("1. Создание соединения...");
    int socketConnection = socket(AF_INET, SOCK_STREAM, 0);

    // 1. Подготовка к передаче данных.
    struct sockaddr_in destination;
    char *destinationIP = "192.168.43.115";
    u_int16_t port = 8000;

    // 2. Конфигурация соединения.
    puts("2. Конфигурация соединения...");
    memset(&destination, 0, sizeof(struct sockaddr_in));
    destination.sin_family = AF_INET;
    destination.sin_addr.s_addr = inet_addr(destinationIP);
    destination.sin_port = htons(port);

    // 3. Соединение с сервером
    connect(socketConnection, (struct sockaddr *) &destination, sizeof(struct sockaddr_in));
    puts("3. Соединение успешно установлено.");
    puts("IP адрес - 192.168.43.115");
    puts("Порт - 8000");

    // 4. Данные, которые будут отправлены на сервер
    puts("4. Сбор пакетов данных, отправляемых на сервер.");
    char *package1 = "# Первый пакет данных\n";
    char *package2 = "# Второй пакет данных\n";
    char *package3 = "# Третий пакет данных\n";
    char *package4 = "# Четвертый пакет данных\n";
    char *package5 = "# Пятый пакет данных\n";

    puts("5. Отправка пакетов данных на сервер.");
    write(socketConnection, package1, strlen(package1));
    write(socketConnection, package2, strlen(package2));
    write(socketConnection, package3, strlen(package3));
    write(socketConnection, package4, strlen(package4));
    write(socketConnection, package5, strlen(package5));

    // Закрытие соединение.
    close(socketConnection);
    puts("6. Закрытие соединения.");

    return 0;
}