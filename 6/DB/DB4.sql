-- noinspection SqlResolveForFile

CREATE TYPE Diff_Tables AS TABLE OF VARCHAR2(30);
CREATE TYPE Diff_Functions AS TABLE OF VARCHAR2(30);
CREATE TYPE Diff_Procedures AS TABLE OF VARCHAR2(30);

CREATE TYPE Diff AS OBJECT
(
  tables     Diff_Tables,
  functions  Diff_Functions,
  procedures Diff_Procedures,
  script     VARCHAR2(1000)
);


CREATE OR REPLACE FUNCTION compare_schemas(dev_schema_name VARCHAR2, prod_schema_name VARCHAR2) RETURN Diff
IS
  TYPE TABLE_INFO_RECORD IS RECORD (object_id NUMBER, object_name VARCHAR2(30));
  TYPE TABLE_INFO IS TABLE OF TABLE_INFO_RECORD;

  TYPE CONSTRAINT_INFO_RECORD IS RECORD (object_id NUMBER, column_name VARCHAR2(4000), src_table_name VARCHAR2(30), ref_table_name VARCHAR2(30), constraint_name VARCHAR2(30));
  TYPE CONSTRAINT_INFO IS TABLE OF CONSTRAINT_INFO_RECORD;

  TYPE FUNCTION_INFO_RECORD IS RECORD (object_name VARCHAR2(30), object_type VARCHAR2(30));
  TYPE FUNCTION_INFO IS TABLE OF FUNCTION_INFO_RECORD;

  dev_but_not_prod TABLE_INFO;
  prod_but_not_dev TABLE_INFO;
  dev_and_prod TABLE_INFO;
  dev_constraints CONSTRAINT_INFO;
  constraints_prod_but_not_dev CONSTRAINT_INFO;

  functions_prod_but_not_dev FUNCTION_INFO;
  functions_dev_but_not_prod FUNCTION_INFO;
  functions_dev_and_prod FUNCTION_INFO;

  script VARCHAR2(1000) := '';

  diff_tables Diff_Tables;
  diff_functions Diff_Functions;
  diff_procedures Diff_Procedures;

BEGIN
  SELECT * BULK COLLECT INTO dev_but_not_prod
  FROM (SELECT object_id, object_name
        FROM ALL_OBJECTS
        WHERE object_type = 'TABLE'
          AND owner = dev_schema_name
        MINUS
        SELECT object_id, object_name
        FROM ALL_OBJECTS
        WHERE object_type = 'TABLE'
          AND owner = prod_schema_name);

  SELECT object_id,
         cols.column_name           column_name,
         src_constraints.table_name src_table_name,
         ref_constraints.table_name ref_table_name
         BULK COLLECT INTO dev_constraints
  FROM ALL_OBJECTS
         LEFT JOIN ALL_CONS_COLUMNS cols
                   ON cols.table_name = object_name
         LEFT JOIN ALL_CONSTRAINTS src_constraints
                   ON src_constraints.constraint_name = cols.constraint_name
         LEFT JOIN ALL_CONSTRAINTS ref_constraints
                   ON ref_constraints.constraint_name = src_constraints.r_constraint_name
  WHERE object_type = 'TABLE'
    AND src_constraints.constraint_type = 'R'
    AND owner = dev_schema_name;


  SELECT * BULK COLLECT INTO dev_and_prod
  FROM (SELECT object_id, object_name
        FROM ALL_OBJECTS
        WHERE object_type = 'TABLE'
          AND owner = dev_schema_name
        INTERSECT
        SELECT object_id, object_name
        FROM ALL_OBJECTS
        WHERE object_type = 'TABLE'
          AND owner = prod_schema_name);

  IF dev_and_prod.COUNT > 0 THEN
    DECLARE
      count_dev          NUMBER;
      count_prod         NUMBER;
      count_dev_and_prod NUMBER;
    BEGIN
      FOR i IN dev_and_prod.FIRST..dev_and_prod.LAST
        LOOP

          SELECT COUNT(*) INTO count_dev_and_prod
          FROM (SELECT *, COUNT(*) INTO count_dev
                FROM ALL_OBJECTS
                       LEFT JOIN ALL_CONS_COLUMNS cols
                                 ON cols.table_name = object_name
                       LEFT JOIN ALL_CONSTRAINTS src_constraints
                                 ON src_constraints.constraint_name = cols.constraint_name
                       LEFT JOIN ALL_CONSTRAINTS ref_constraints
                                 ON ref_constraints.constraint_name =
                                    src_constraints.r_constraint_name
                WHERE object_type = 'TABLE'
                  AND object_name = dev_and_prod(i).object_name
                  AND src_constraints.constraint_type = 'R'
                  AND owner = dev_schema_name) dev
                 INNER JOIN
               (SELECT *, COUNT(*) INTO count_prod
                FROM ALL_OBJECTS
                       LEFT JOIN ALL_CONS_COLUMNS cols
                                 ON cols.table_name = object_name
                       LEFT JOIN ALL_CONSTRAINTS src_constraints
                                 ON src_constraints.constraint_name = cols.constraint_name
                       LEFT JOIN ALL_CONSTRAINTS ref_constraints
                                 ON ref_constraints.constraint_name =
                                    src_constraints.r_constraint_name
                WHERE object_type = 'TABLE'
                  AND object_name = dev_and_prod(i).object_name
                  AND src_constraints.constraint_type = 'R'
                  AND owner = prod_schema_name) prod
               ON dev.src_table_name = prod.src_table_name AND
                  dev.ref_table_name = prod.ref_table_name AND dev.column_name = prod.column_name;

          IF count_dev_and_prod = count_dev = count_prod THEN

            SELECT COUNT(*) INTO count_dev_and_prod
            FROM (
                   SELECT *, COUNT(*) INTO count_dev
                   FROM ALL_TAB_COLUMNS
                   WHERE table_name = dev_and_prod(i).object_name
                     AND owner = dev_schema_name) dev
                   INNER JOIN
                 (
                   SELECT *, COUNT(*) INTO count_prod
                   FROM ALL_TAB_COLUMNS
                   WHERE table_name = dev_and_prod(i).object_name
                     AND owner = prod_schema_name) prod
                 ON dev.column_name = prod.column_name AND dev.data_type = prod.data_type;

            IF count_dev_and_prod = count_dev = count_prod THEN
              CONTINUE;
            END IF;
          END IF;

          dev_but_not_prod(dev_but_not_prod.COUNT + 1) := dev_and_prod(i);

        END LOOP;
    END;
  END IF;

  IF dev_but_not_prod.COUNT > 1 THEN
    FOR i IN 2..dev_but_not_prod.LAST
      LOOP
        FOR j in dev_constraints.FIRST..dev_constraints.LAST
          LOOP
            IF dev_constraints(j).ref_table_name = dev_but_not_prod(i).object_name AND
               dev_constraints(j).src_table_name = dev_but_not_prod(i - 1).object_name THEN
              DECLARE
                temp TABLE_INFO_RECORD;
              BEGIN
                temp := dev_but_not_prod(i);
                dev_but_not_prod(i) := dev_but_not_prod(i - 1);
                dev_but_not_prod(i - 1) := temp;
              END;
            END IF;
          END LOOP;
      END LOOP;

    FOR i IN 2..dev_but_not_prod.LAST
      LOOP
        DECLARE
          count NUMBER := 0;
        BEGIN
          FOR j in dev_constraints.FIRST..dev_constraints.LAST
            LOOP
              IF dev_constraints(j).ref_table_name = dev_but_not_prod(i).object_name AND
                 dev_constraints(j).src_table_name = dev_but_not_prod(i - 1).object_name THEN
                RAISE_APPLICATION_ERROR(-20001, 'Loopback!');
                EXIT;
              END IF;
            END LOOP;
        END;
      END LOOP;
  END IF;

  FOR i IN dev_but_not_prod.FIRST..dev_but_not_prod.LAST
    LOOP
      diff_tables(diff_tables.COUNT + 1) := dev_but_not_prod.object_name;

      script := script || replace(
          DBMS_METADATA.get_ddl('TABLE', dev_but_not_prod(i).object_name, dev_schema_name),
          dev_schema_name, prod_schema_name);
    END LOOP;

  SELECT * BULK COLLECT INTO prod_but_not_dev
  FROM (SELECT object_id, object_name
        FROM ALL_OBJECTS
        WHERE object_type = 'TABLE'
          AND owner = prod_schema_name
        MINUS
        SELECT object_id, object_name
        FROM ALL_OBJECTS
        WHERE object_type = 'TABLE'
          AND owner = dev_schema_name);

  FOR i IN 1..prod_but_not_dev.COUNT
    LOOP
      SELECT constraint_name BULK COLLECT INTO constraints_prod_but_not_dev
      FROM ALL_CONSTRAINTS
      WHERE constraint_name = r_constraint_name
        AND constraint_type = 'R'
        AND table_name = prod_but_not_dev(i).object_name
        AND owner = prod_schema_name;

      FOR j IN 1..constraints_prod_but_not_dev.COUNT
        LOOP
          script := script || 'ALTER TABLE ' || prod_but_not_dev(i).object_name ||
                    ' DROP CONSTRAINT ' || constraints_prod_but_not_dev(j).constraint_name || ';';
        END LOOP;
      script := script || 'DROP TABLE ' || prod_but_not_dev(i).object_name || ';';
    END LOOP;

  SELECT * BULK COLLECT INTO functions_dev_but_not_prod
  FROM (SELECT object_name,
               object_type
        FROM ALL_OBJECTS
        WHERE object_type = 'FUNCTION'
           OR object_type = 'PROCEDURE' AND owner = dev_schema_name
        MINUS
        SELECT object_name,
               object_type
        FROM ALL_OBJECTS
        WHERE object_type = 'FUNCTION'
           OR object_type = 'PROCEDURE' AND owner = prod_schema_name);


  SELECT * BULK COLLECT INTO functions_dev_and_prod
  FROM (SELECT object_name,
               object_type
        FROM ALL_OBJECTS
        WHERE object_type = 'FUNCTION'
           OR object_type = 'PROCEDURE' AND owner = dev_schema_name
        INTERSECT
        SELECT object_name,
               object_type
        FROM ALL_OBJECTS
        WHERE object_type = 'FUNCTION'
           OR object_type = 'PROCEDURE' AND owner = prod_schema_name);

  DECLARE
    count_dev          NUMBER;
    count_prod         NUMBER;
    count_dev_and_prod NUMBER;
  BEGIN
    FOR i IN functions_dev_and_prod.FIRST..functions_dev_and_prod.LAST
      LOOP
        SELECT COUNT(*) INTO count_dev_and_prod
        FROM (SELECT text, COUNT(*) INTO count_dev
              FROM ALL_SOURCE
              WHERE "type" = 'FUNCTION'
                 OR "type" = 'PROCEDURE' AND name = functions_dev_and_prod(i).object_name AND
                    owner = dev_schema_name
              ORDER BY line) dev
               INNER JOIN
             (SELECT text, COUNT(*) INTO count_prod
              FROM ALL_SOURCE
              WHERE "type" = 'FUNCTION'
                 OR "type" = 'PROCEDURE' AND name = functions_dev_and_prod(i).object_name AND
                    owner = prod_schema_name
              ORDER BY line) prod
             ON dev.text = prod.text;

        IF count_dev_and_prod = count_dev = count_prod THEN
          CONTINUE;
        END IF;

        functions_dev_but_not_prod(functions_dev_but_not_prod.COUNT + 1) :=
            functions_dev_and_prod(i);
      END LOOP;
  END;

  FOR i IN functions_dev_but_not_prod.FIRST..functions_dev_but_not_prod.LAST
    LOOP
      IF (functions_dev_but_not_prod(i).object_type = 'FUNCTION') THEN
        diff_functions(diff_functions.COUNT + 1) := functions_dev_but_not_prod(i).object_name;
      ELSE
        diff_procedures(diff_procedures.COUNT + 1) := functions_dev_but_not_prod(i).object_name;
      END IF;

      script := script || replace(
          DBMS_METADATA.get_ddl(functions_dev_but_not_prod(i).object_type,
                                functions_dev_but_not_prod(i).object_name, dev_schema_name),
          dev_schema_name,
          prod_schema_name);
    END LOOP;

  SELECT * BULK COLLECT INTO functions_prod_but_not_dev
  FROM (SELECT object_name,
               object_type
        FROM ALL_OBJECTS
        WHERE object_type = 'FUNCTION'
           OR object_type = 'PROCEDURE' AND owner = prod_schema_name
        MINUS
        SELECT object_name,
               object_type
        FROM ALL_OBJECTS
        WHERE object_type = 'FUNCTION'
           OR object_type = 'PROCEDURE' AND owner = dev_schema_name);

  FOR i in functions_prod_but_not_dev.FIRST..functions_prod_but_not_dev.LAST
    LOOP
      script := script || 'DROP ' || functions_prod_but_not_dev(i).object_type || ' ' ||
                functions_prod_but_not_dev(i).object_name || ';';
    END LOOP;

  RETURN Diff(diff_tables, diff_functions, diff_procedures, script);

END;

BEGIN
  SELECT * FROM TABLE (compare_schemas('dev', 'prod'));
END;