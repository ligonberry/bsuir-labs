-- noinspection SqlResolveForFile 
-- ( DBMS_OUTPUT.PUT_LINE )


-- --------------------------------------------------------------------------- --
-- 1. Создайте таблицу MyTable(id number, val number)                          --
-- --------------------------------------------------------------------------- --
CREATE TABLE MyTable
(
	id NUMBER,
	val NUMBER
);


-- --------------------------------------------------------------------------- --
-- 2. Напишите анонимный блок, который записывает в таблицу MyTable 10 000     --
-- целых случайных записей                                                     --
-- --------------------------------------------------------------------------- --
BEGIN
  FOR i IN 1 .. 10000
    LOOP
      INSERT INTO MYTABLE (ID, VAL) values (i, TRUNC(DBMS_RANDOM.value(1, 10000)));
    END LOOP;
  COMMIT;
END;


-- --------------------------------------------------------------------------- --
-- 3. Напишите функцию, которая выводит TRUE если четных значений val в таблице -
-- MyTable больше, FALSE если больше нечетных значений и EQUAL если количество --
-- четных и нечетных равно                                                     --
-- --------------------------------------------------------------------------- --
CREATE OR REPLACE FUNCTION oddOrEven RETURN VARCHAR2
IS
  even NUMBER := 0;
  odd  NUMBER := 0;
BEGIN
  SELECT COUNT(VAL) INTO even FROM MYTABLE WHERE MOD(VAL, 2) = 0;
  SELECT COUNT(VAL) INTO odd FROM MYTABLE WHERE MOD(VAL, 2) != 0;
  IF even > odd THEN
    RETURN 'TRUE';
  ELSIF even < odd THEN
    RETURN 'FALSE';
  ELSE
    RETURN 'EQUAL';
  END IF;
END;

-- --------------------------------------------------------------------------- --
--4. Напишите функцию, которая по введенному значению ID, сгенерирует и        --
-- выведет в консоль текстовое значение команды insert для вставки             --
-- указанной строки                                                            --
-- --------------------------------------------------------------------------- --
CREATE OR REPLACE FUNCTION generateInsertCode(id_ NUMBER) RETURN VARCHAR2
IS
  val_ NUMBER;
BEGIN
  SELECT VAL INTO val FROM MYTABLE WHERE ID=id_;
  DBMS_OUTPUT.PUT_LINE('You should write such code: ');
  RETURN 'INSERT INTO MYTABLE values (' || id_ || ', ' || val_ || ');';
END;


-- --------------------------------------------------------------------------- --
-- 5. Написать процедуры реализующие DML операции (INSERT, UPDATE, DELETE)     --
-- для указанной таблицы                                                       --
-- --------------------------------------------------------------------------- --
CREATE OR REPLACE PROCEDURE insertIntoMyTable(id_ NUMBER, val_ NUMBER)
IS
BEGIN
  INSERT INTO MYTABLE (ID, VAL) values (id_, val_);
END;

CREATE OR REPLACE PROCEDURE updateMyTable(id_ NUMBER, val_ NUMBER)
IS
BEGIN
  UPDATE MYTABLE
    SET VAL = val_
  WHERE ID = id_;
END;

CREATE OR REPLACE PROCEDURE deleteFromMyTable(id_ NUMBER DEFAULT NULL, val_ NUMBER DEFAULT NULL)
IS
BEGIN
  IF id_ IS NOT NULL and val_ IS NOT NULL THEN
    DBMS_OUTPUT.PUT_LINE('ID and VAL');
    DELETE FROM MYTABLE WHERE ID = id_ and VAL = val_;
  ELSIF id_ IS NOT NULL THEN
    DBMS_OUTPUT.PUT_LINE('Only ID');
    DELETE FROM MYTABLE WHERE ID = id_;
  ELSIF val_ IS NOT NULL THEN
    DBMS_OUTPUT.PUT_LINE('ONLY VAL');
    DELETE FROM MYTABLE WHERE VAL = val_;
  else
  DELETE FROM MYTABLE ;
  END IF;
END;


BEGIN
  deleteFromMyTable(val_=>4);
END;