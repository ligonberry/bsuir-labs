-- noinspection SqlSignatureForFile
-- noinspection SqlResolveForFile

-------------------------------------------------------------------------------------------------
-- 1. Реализовать таблицу, в которой будут хранится метрики производительности базы данных     --
-- наименование, опасное значение, критическое значение, какие-либо дополнительные поля,       --
-- необходимые для реализации. Необходимо обеспечить возможность добавлять и удалять метрики   --
-------------------------------------------------------------------------------------------------
CREATE TABLE Metrics (
		id 							NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
		name 						VARCHAR2(100),
		danger_value 		NUMBER,
		critical_value 	NUMBER,

		query_str       CLOB
);

CREATE OR REPLACE PROCEDURE addMetric(name IN VARCHAR2, danger_value IN NUMBER, critical_value IN NUMBER)
IS
BEGIN
	INSERT INTO Metrics (name, danger_value, critical_value) VALUES (name, danger_value, critical_value);
END;


CREATE OR REPLACE PROCEDURE deleteMetric(name IN VARCHAR2)
IS
BEGIN
	DELETE FROM Metrics WHERE Metrics.name = name;
END;

-------------------------------------------------------------------------------------------------
-- 2. Написать Oracle задание с заданной периодичностью проверяющую метрики производительности --
-- в случае достижения опасного значения отправляющее email извещение, в случае достижения     --
-- критического значения кроме отправки email, в отдельную таблицу в виде json-файлов записать --
-- информацию об  активных сессиях, выполняющихся запросах                                     --
-- (!!! Текущее значение метрик необходимо брать из представлений производительности           --
-- словаря данных Oracle)                                                                      --
-------------------------------------------------------------------------------------------------
CREATE TABLE Log_JSON (
		id 				NUMBER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
		json_data CLOB,
		CONSTRAINT json_documents_json CHECK (json_data IS JSON)
);

CREATE OR REPLACE PROCEDURE checkMetrics
	IS
		val NUMBER := 0;
		CURSOR c_Metrics IS SELECT * FROM Metrics;

	PROCEDURE writeLog(log_msg IN CLOB)
	IS
	BEGIN
		INSERT INTO Log_JSON (json_data) VALUES (log_msg);
		COMMIT;
	END;

	PROCEDURE logSession
	IS
	  message CLOB;
	BEGIN
	  SELECT json_objectagg(*) INTO message
				FROM v$session "session",v$sqlarea area
				WHERE area.hash_value = "session".sql_hash_value
					and "session".schemaname != 'SYS'
					and "session".status = 'ACTIVE';
		writeLog(message);
	END;

	PROCEDURE sendEmail(msg IN VARCHAR2)
	IS
	BEGIN
	APEX_MAIL.SEND(
	  p_to => 'admin@gmail.com',
	  p_from => 'admin@gmail.com',
		p_subj => 'Alert!',
	  p_body => msg);
	APEX_MAIL.PUSH_QUEUE;
	EXCEPTION
		WHEN OTHERS THEN
			writeLog('{"Message": "Error sending email"}');
			NULL;
	END;
	BEGIN
		FOR metric IN c_Metrics LOOP

		  EXECUTE IMMEDIATE metric.query_str INTO val;

			--IF metric.name = 'sysstat' THEN
				--SELECT (a.value / b.value) INTO val FROM V$SYSSTAT a, V$SYSSTAT b
				--WHERE a.name = 'parse time cpu' AND b.name = 'CPU used by this session';

			--ELSIF metric.name = 'pgastat' THEN
					--SELECT value INTO val FROM V$PGASTAT
					--WHERE name = 'total PGA allocated';
			--END IF;



			IF val >= metric.critical_value THEN
					writeLog('{"Message": "Critical value"}');
					logSession;
					sendEmail('Critical value');

			ELSIF val >= metric.danger_value THEN
					writeLog('{"Message": "Danger value"}');
					sendEmail('Danger value');
			END IF;

		END LOOP;
END;


BEGIN
	DBMS_SCHEDULER.CREATE_JOB(job_name => 'job_metrics',
														job_type => 'STORED_PROCEDURE',
														job_action => 'checkMetrics',
														start_date => SYSTIMESTAMP,
														end_date => SYSTIMESTAMP + 1,
														repeat_interval => 'FREQ=MINUTELY;INTERVAL=1',
														enabled => TRUE,
														auto_drop => FALSE,
														comments => 'No comments.');
END;


BEGIN
	addMetric('sysstat', 1, 1);
	addMetric('pgastat', 0, 0);
END;


BEGIN
	DBMS_SCHEDULER.STOP_JOB('job_metrics, sys.dw_jobs');
END;


-------------------------------------------------------------------------------------------------
-- 3.1 Разработать процедуру, которая вносит в очередь следующую информацию: ID-сессии под     --
-- которой данные добавляются в очередь, и случайное число.                                    --
-------------------------------------------------------------------------------------------------

CREATE TABLE Queues(
	session_id    NUMBER,
	random_number NUMBER
);


BEGIN
		dbms_aqadm.create_queue_table(
			queue_table          => 'Queues',
			queue_payload_type   => 'RAW'
		);


		dbms_aqadm.create_queue(
			queue_name          => 'Queue',
			queue_table         => 'Queues'
		);


		dbms_aqadm.start_queue(
			queue_name          => 'Queue'
		);
END;

-------------------------------------------------------------------------------------------------
-- 3.2 Разработать процедуру которая забирает данные из очереди и добавляет данные в таблицу   --
-- следующей структуры                                                                         --
-------------------------------------------------------------------------------------------------


CREATE TABLE Result(
	SourceSID NUMBER,
	DestSID	  NUMBER,
	SourceNum NUMBER,
	DestNum	  NUMBER,
	ResNum	  NUMBER
);


CREATE OR REPLACE PROCEDURE enqueue 
  IS
  	message Result%ROWTYPE;
	BEGIN 
		SELECT SID INTO message.SourceSID FROM v$session;
		SELECT dbms_random.random INTO message.SourceNum FROM dual;
	
		dbms_aq.enqueue(
			queue_name => 'Queue',
					enqueue_options      => NULL,
					message_properties   => NULL,
					payload              => message,
					msgid                => NULL
		);
	END;


CREATE OR REPLACE PROCEDURE dequeue 
  IS
		message       Result%ROWTYPE;
		dest_sid      NUMBER;
		random_num    NUMBER;
		res_num       NUMBER;
		  
	BEGIN 
		dbms_aq.dequeue(
			queue_name => 'Queue',
					dequeue_options    => NULL,
					message_properties => NULL,
					payload            => message,
					msgid              => NULL
		);

	SELECT SID INTO dest_sid FROM v$session;
	SELECT dbms_random.random INTO random_num FROM DUAL;
	res_num := (SourceNum + DestNum) * (SourceNum + DestNum);

	INSERT INTO Result (SourceSID, DestSID, SourceNum, DestNum, ResNum) VALUES (message.SourceSID,
	                                                                            dest_sid,
	                                                                            message.SourceNum,
	                                                                            random_num,
	                                                                            res_num);
	END;