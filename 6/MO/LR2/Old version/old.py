import random
import numpy as np
from math import inf

from lab1 import inverse_matrix


class SimplexMethodSolver:
    def __init__(self, A, c, initial_plan, J):
        self.A = A
        self.c = c
        self.initial_plan = initial_plan
        self.x = initial_plan
        self.J = J

    def _get_non_basis_deltas(self, deltas):
        non_basis_deltas = []
        for index, delta in enumerate(deltas):
            if index not in self.J:
                non_basis_deltas.append((index, delta))
        return non_basis_deltas

    @staticmethod
    def _is_optimum(non_basis_deltas):
        return all(delta >= 0 for index, delta in non_basis_deltas)

    @staticmethod
    def _get_new_delta(non_basis_deltas):
        return random.choice(
                    [(index, delta) for index, delta in non_basis_deltas if
                     delta < 0])

    def _get_tetas(self, z):
        tetas = []
        for index, value in zip(self.J, z):
            if value <= 0:
                tetas.append(inf)
            else:
                tetas.append(float(self.x[index]) / value)
        return tetas

    @staticmethod
    def _is_not_limited(tetas):
        return all(t == inf for t in tetas)

    def solve(self):
        basis_matrix = self.A[:, self.J]
        inverse_basis_matrix = np.linalg.inv(basis_matrix)

        while True:
            basis_cost = self.c[self.J]
            u = basis_cost @ inverse_basis_matrix
            deltas = u @ self.A - self.c

            non_basis_deltas = self._get_non_basis_deltas(deltas)

            if self._is_optimum(non_basis_deltas):
                return self.x, self.J

            else:
                delta = self._get_new_delta(non_basis_deltas)
                J0 = delta[0]
                z = inverse_basis_matrix @ self.A[:, J0]
                tetas = self._get_tetas(z)
                if self._is_not_limited(tetas):
                    print(
                        "Cost function feasible plans aren't limited at the top"
                    )
                else:
                    min_teta = min(tetas)
                    min_teta_index = tetas.index(min_teta)
                    self.J[min_teta_index] = J0
                    new_plan = []
                    for index, value in enumerate(self.x):
                        if index in self.J:
                            if index == J0:
                                new_plan.append(min_teta)
                            else:
                                new_plan.append(
                                    value - min_teta * z[self.J.index(index)])
                        else:
                            new_plan.append(0)
                    self.x = new_plan

                    inverse_basis_matrix = inverse_matrix(inverse_basis_matrix,
                                                          self.A[:, J0],
                                                          tetas.index(min_teta))


if __name__ == '__main__':
    p = SimplexMethodSolver(
        A=np.array([[1, 2, 1, 0, 0, 0],
                    [2, 1, 0, 1, 0, 0],
                    [1, 0, 0, 0, 1, 0],
                    [0, 1, 0, 0, 0, 1]]),
        c=np.array([20, 26, 0, 0, 0, 1]),
        initial_plan=np.array([0, 0, 10, 11, 5, 4]),
        J=[2, 3, 4, 5]
    )
    print(p.solve())

