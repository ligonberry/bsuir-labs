import numpy as np
import sympy as sp
import pulp as lp


def run(c_matrix, d_matrix, a_array, x_cap_array, x_wave_array):
    x_symbols = [sp.Symbol('x' + str(i)) for i in range(len(c_matrix[0]))]

    spm_x = sp.Matrix(x_symbols)
    spm_c = sp.Matrix(c_matrix[0])
    spm_d = sp.Matrix(d_matrix[0])
    f_x = 0.5 * spm_x.T * spm_d * spm_x + spm_c.T * spm_x
    subs_x_wave = {}
    for i, x in enumerate(x_symbols):
        subs_x_wave[x] = x_wave_array[i]
    gradient_f_x = [sp.diff(f_x, x).evalf(subs=subs_x_wave)[0] for x in
                    x_symbols]
    print('Gradient f(x):', gradient_f_x)

    g_x_array = []
    for i in range(len(a_array)):
        spm_c = sp.Matrix(c_matrix[i + 1])
        spm_d = sp.Matrix(d_matrix[i + 1])
        spm_a = sp.Matrix([a_array[i]])
        g_x = 0.5 * spm_x.T * spm_d * spm_x + spm_c.T * spm_x + spm_a
        g_x_array.append(g_x)

    new_limitations_indexes = []
    for i, g_x in enumerate(g_x_array):
        if int(g_x.evalf(subs=subs_x_wave)[0] * 10000) / 10000 == 0:
            new_limitations_indexes.append(i)

    gradient_g_x_array = []
    for index in new_limitations_indexes:
        gradient_g_x = [sp.diff(g_x_array[index], x).evalf(subs=subs_x_wave)[0]
                        for x in x_symbols]
        gradient_g_x_array.append(gradient_g_x)
        print('Gradient g(x):', gradient_g_x)

    lp_problem = lp.LpProblem('lab7', lp.LpMinimize)
    lp_x_array = []
    for i in range(len(x_symbols)):
        if x_wave_array[i] == 0:
            lp_x_array.append(lp.LpVariable('var' + str(i), 0, 1))
        else:
            # why -1 is here when None is expected?
            lp_x_array.append(lp.LpVariable('var' + str(i), -1, 1))
    lp_goal = gradient_f_x[0] * lp_x_array[0]
    for i in range(1, len(lp_x_array)):
        lp_goal += gradient_f_x[i] * lp_x_array[i]
    lp_problem += lp_goal
    for gradient_g_x in gradient_g_x_array:
        lp_constraint = gradient_g_x[0] * lp_x_array[0]
        for i in range(1, len(lp_x_array)):
            lp_constraint += gradient_g_x[i] * lp_x_array[i]
        lp_constraint = lp_constraint <= 0
        lp_problem += lp_constraint

    lp_problem.solve()

    if lp_problem.status != 1:
        print("Can't be solved. Status:", lp.LpStatus[lp_problem.status])

    objective_value = lp.value(lp_problem.objective)
    print('Objective value:', objective_value)
    lp_solution = [v.varValue for v in lp_problem.variables()]
    print('Solution:', lp_solution)

    if objective_value == 0:
        return list(x_wave_array)

    a = np.array(gradient_f_x).dot(lp_solution)
    b = (np.array(x_cap_array) - np.array(x_wave_array)).dot(gradient_f_x)
    alpha = -a / 2 / b if b > 0 else 1
    print('Alpha:', alpha)

    t = 1
    f_x_wave = f_x[0].evalf(subs=subs_x_wave)

    while True:
        print('T:', t)
        x_bird_array = np.array(x_wave_array) + t * np.array(
            lp_solution) + alpha * t * (np.array(x_cap_array) - np.array(
                            x_wave_array))
        subs_x_bird = {}
        for i, x in enumerate(x_symbols):
            subs_x_bird[x] = x_bird_array[i]
        f_x_bird = f_x[0].evalf(subs=subs_x_bird)

        minimize = f_x_bird < f_x_wave
        match_constraint = True
        for g_x in g_x_array:
            if g_x[0].evalf(subs=subs_x_bird) > 0:
                match_constraint = False
                break
        match_limitations = True
        for x_bird in x_bird_array:
            if x_bird < 0:
                match_limitations = False
        if minimize and match_constraint and match_limitations:
            return list(x_bird_array)

        t /= 2


def get_data_2():
    c_matrix = [[-1, -1, -1, -1, -2, 0, -2, -3], [0, 60, 80, 0, 0, 0, 40, 0],
                [2, 0, 3, 0, 2, 0, 3, 0], [0, 0, 80, 0, 0, 0, 0, 0],
                [0, -2, 1, 2, 0, 0, -2, 1], [-4, -2, 6, 0, 4, -2, 60, 2]]
    d_matrix_0 = [[2, 1, 0, 4, 0, 3, 0, 0], [0, 4, 0, 3, 1, 1, 3, 2],
                  [1, 3, 0, 5, 0, 4, 0, 4]]
    d_matrix_0 = np.array(d_matrix_0).transpose().dot(d_matrix_0).tolist()
    d_matrix_1 = [[0, 0, 0.5, 2.5, 1, 0, -2.5, -2],
                  [0.5, 0.5, -0.5, 0, 0.5, -0.5, -0.5, -0.5],
                  [0.5, 0.5, 0.5, 0, 0.5, 1, 2.5, 4]]
    d_matrix_1 = np.array(d_matrix_1).transpose().dot(d_matrix_1).tolist()
    d_matrix_2 = [[1, 2, -1.5, 3, -2.5, 0, -1, -0.5],
                  [-1.5, -0.5, -1, 2.5, 3.5, 3, -1.5, -0.5],
                  [1.5, 2.5, 1, 1, 2.5, 1.5, 3, 0]]
    d_matrix_2 = np.array(d_matrix_2).transpose().dot(d_matrix_2).tolist()
    d_matrix_3 = [[0.75, 0.5, -1, 0.25, 0.25, 0, 0.25, 0.75],
                  [-1, 1, 1, 0.75, 0.75, 0.5, 1, -0.75],
                  [0.5, -0.25, 0.5, 0.75, 0.5, 1.25, -0.75, -0.25]]
    d_matrix_3 = np.array(d_matrix_3).transpose().dot(d_matrix_3).tolist()
    d_matrix_4 = [[1.5, -1.5, -1.5, 2, 1.5, 0, 0.5, -1.5],
                  [-0.5, -2.5, -0.5, -1, -2.5, 2.5, 1, 2],
                  [-2.5, 1, -2, -1.5, -2.5, 0.5, 2.5, -2.5]]
    d_matrix_4 = np.array(d_matrix_4).transpose().dot(d_matrix_4).tolist()
    d_matrix_5 = [[1, 0.25, -0.5, 1.25, 1.25, -0.5, 0.25, -0.75],
                  [-1, -0.75, -0.75, 0.5, -0.25, 1.25, 0.25, -0.5],
                  [0, 0.75, 0.5, -0.5, -1, 1, -1, 1]]
    d_matrix_5 = np.array(d_matrix_5).transpose().dot(d_matrix_5).tolist()
    d_matrix = [d_matrix_0, d_matrix_1, d_matrix_2, d_matrix_3, d_matrix_4,
                d_matrix_5]
    a_array = [-51.75, -436.75, -33.7813, -303.375, -41.75]
    x_cap_array = [0, 0, 0, 0, 0, 0, 0, 0]
    x_wave_array = [1, 0, 0, 2, 4, 2, 0, 0]
    return c_matrix, d_matrix, a_array, x_cap_array, x_wave_array


def get_data():
    c_matrix = [[-3, -3], [1, -1], [-1, 1]]
    d_matrix = [[[2, 1], [1, 2]], [[1, 0], [0, 1]], [[1, 0.5], [0.5, 1]]]
    a_array = [-1, -1.5]
    x_cap_array = [0, 0]
    x_wave_array = [0, 1]
    return c_matrix, d_matrix, a_array, x_cap_array, x_wave_array


data = get_data_2()
answer = run(*data)
if answer is not None:
    print('Answer:', answer)
