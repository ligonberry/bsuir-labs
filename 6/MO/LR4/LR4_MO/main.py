from math import inf

import numpy as np


def inverse_matrix(Ar, x, i):
    l = Ar @ x
    if l[i] == 0:
        raise ValueError("Matrix is irreversible")
    else:
        l1 = l.copy()
        l1[i] = -1
        l2 = (-1.0 / l[i]) * l1
        Q = np.eye(len(x))
        Q[:, i] = l2
        return Q @ Ar


class DualMethodSolver:
    def __init__(self, A, b, c, basis_plan, Jb, d_bottom, d_up):
        self.A = A
        self.b = b
        self.c = c
        self.m, self.n = A.shape
        self.y = basis_plan
        self.Jb = Jb
        self.co_plan = self.y @ self.A - self.c

        self.d_bottom = d_bottom
        self.d_up = d_up

    # region STEP FIRST
    def _get_kappa(self, B):
        return B @ self.b

    def _first_step(self, B):
        """Найдем базисные компоненты псевдоплана каппа, соотв. базису Jb. """
        return self._get_kappa(B)

    # endregion

    # region STEP TWO
    def _show_optimum_plan(self, optimum_pseudoplan):
        print(f'Оптимальный план двойственной задачи = {self.y}, '
              f'базисное множество = {self.Jb} '
              f'Опртимальный псевдоплан = {optimum_pseudoplan}')

    def _second_step(self, pseudoplan):
        """Если все компоненты вектора каппа > 0 (для j принадлеж. Jb) =>
           STOP (оптимальный базисный план найден).
           Иначен - переходим к Шагу 3"""
        optimum_pseudoplan = []
        for j in range(self.n):
            if j in self.Jb:
                j_b = self.Jb.index(j)
                optimum_pseudoplan.append(pseudoplan[j_b])
            else:
                optimum_pseudoplan.append(0)

        self._show_optimum_plan(optimum_pseudoplan)
        return optimum_pseudoplan, self.y, self.Jb

    # endregion

    # region STEP THREE
    @staticmethod
    def _get_j_k(pseudoplan):
        """ Находим базисный индекс j_k ∈ J Б , для которого kappa[j_k] < 0 """
        for j_k, kappa_jk in enumerate(pseudoplan):
            if kappa_jk < 0:
                return j_k

    @staticmethod
    def _get_B_k(k, B):
        """ B_k - k-я строка матрицы B """
        return B[k, :]

    def _get_A_j(self, j):
        """ A_j — j-ый столбец матрицы условий A """
        return self.A[:, j]

    def _get_mu(self, B_k):
        """ Находим числа μ_j = B_k @ A_j """
        mu = []

        for j in range(self.n):
            A_j = self._get_A_j(j)
            # print('B_k @ A_j', B_k, A_j, B_k @ A_j)
            mu.append(B_k @ A_j)
        return np.array(mu)

    def _third_step(self, B, pseudoplan):
        """Среди Jb выберем индекс jk, для которого каппа_jk < 0.
           Представим m-вектор дельта_y и числа мю_j (j принадлеж. Jн).
           по правилам: * дельта_у = e_k * B
                        * мю_j = дельта_у * A'j (j принадлеж. Jн)"""
        j_k = self._get_j_k(pseudoplan)
        B_k = self._get_B_k(j_k, B)
        mu = self._get_mu(B_k)
        return mu

    # endregion

    # region STEP FOUR
    def _get_Jn(self):
        return list(set(range(self.n)) - set(self.Jb))

    def _fourth_step(self, mu):
        """Найдем σ-s по формуле: σ_j = c_j - A'j * y / μ_j."""
        Jn = self._get_Jn()

        sigmas = []
        for j in Jn:
            if mu[j] < 0:
                sigma = (self.c[j] - self.y @ self.A[:, j]) / mu[j]
                sigmas.append(sigma)
            else:
                sigmas.append(inf)

        return sigmas

    # endregion

    # region STEP FIVE
    def _seven_step(self, pseudoplan, j_0):
        """ Строим новое базисное множество J'_Б = (J_Б - j k ) ∪ j 0 """
        j_k = self._get_j_k(pseudoplan)
        self.Jb[j_k] = j_0

    # endregion

    # region EIGHT STEP
    def _eight_step(self, pseudoplan, B, sigma_0):
        """ Строим небазисные компоненты нового базисного коплана y"""
        j_k = self._get_j_k(pseudoplan)
        B_k = self._get_B_k(j_k, B)
        self.y = self.y + sigma_0 * B_k

    # endregion

    # region NINE SIX
    def _nine_step(self, B, j_0):
        """Вычислим B', обратную матрице А"""
        col_i = self.Jb.index(j_0)
        B = inverse_matrix(B, self.A[:, j_0], col_i)
        return B

    # endregion

    def solve(self):
        A = self.A[:, self.Jb]
        B = np.linalg.inv(A)
        # print('B: ', B)

        while True:
            pseudoplan = self._first_step(B)

            deltas = pseudoplan @ self.A - self.c
            # print('deltas', deltas)

            # сформируем множества
            Jn = self._get_Jn()

            Jn_plus = []
            Jn_minus = []
            for j in Jn:
                if deltas[j] >= 0:
                    Jn_plus.append(j)
                else:
                    Jn_minus.append(j)
            # print('J', Jn_plus, Jn_minus)

            # найдем вектор каппа (Мы должны сюда перейти, вынести все, что выше из цикла?)
            cappa = [0]*self.n
            for j in range(self.n):
                if j in Jn_minus:
                    cappa[j] = self.d_bottom[j]
                if j in Jn_plus:
                    cappa[j] = self.d_up[j]

            cappa = np.array(cappa)
            cappa = cappa[Jn]
            cappa = cappa.tolist()

            for j in range(self.n):
                if j in self.Jb:
                    sum_j = []
                    for i in range(len(Jn)):
                        sum_j.append(self.A[:, i] * cappa[i])
                    # print('sum_j', sum_j)
                    temp = B @ (self.b - sum_j)
                    print('temp', temp)
                    cappa.append(temp)

            # print('cappa', cappa)




            if np.all(pseudoplan >= 0):
                return self._second_step(pseudoplan)

            else:
                mu = self._third_step(B, pseudoplan)

                sigmas = self._fourth_step(mu)

                # Выберем в качестве σ_0 наименьший элемент
                sigma_0 = min(sigmas)

                # Если σ_0 = ∞, то STOP: ограничения прямой задачи несовместны. В
                # противном случае перейдем к шагу 6.
                if sigma_0 == inf:
                    print('Ограничения исходной задачи несовместны. '
                          'Двойственная задача не ограничена снизу на множестве '
                          'ее планов')
                    return

                # Найдем такой небазисный индекс j_0 ∈ J_n , что σ_0 = σ_j0 .
                Jn = self._get_Jn()
                i = sigmas.index(sigma_0)  # Индекс, который у σ_0 в массиве σ-s
                j_0 = Jn[i]

                # Строим новое базисное множество
                # J'_Б = (J_Б \ j k ) ∪ j 0 = {j_1 , . . . , j_k−1 , j_0 , j_k+1 , . . . , j_m }.
                self._seven_step(pseudoplan, j_0)

                # Строим небазисные компоненты нового базисного коплана y
                self._eight_step(pseudoplan, B, sigma_0)

                # Строим матрицу B = (A_b) ^ (-1), где A_b = (A_j , j ∈ J_b ),
                # по тем же правилам, что и в прямом симплекс-методе
                B = self._nine_step(B, j_0)


def test_1():
    print('TEST 1')
    dp = DualMethodSolver(
        A=np.array([
            [1, -1, 3, -2],
            [1, -5, 11, -6]
        ]),
        b=np.array([1, 9]),
        c=np.array([1, 1, -2, -3]),
        basis_plan=np.array([1.5, -0.5]),
        Jb=[0, 1],
        d_bottom=[1, 1, 1, 1],
        d_up=[100, 100, 100, 100]
    )

    dp.solve()
    print()


def test_2():
    print('TEST 2')
    dp = DualMethodSolver(
        A=np.array([
            [-2, -1, 1, -7, 0, 0, 0, 2],
            [4, 2, 1, 0, 1, 5, -1, -5],
            [1, 1, 0, -1, 0, 3, -1, 1]
        ]),
        b=np.array([-2, 4, 3]),
        c=np.array([2, 2, 1, -10, 1, 4, -2, -3]),
        basis_plan=np.array([1, 1, 1]),
        Jb=[1, 4, 6]
    )

    dp.solve()
    print()


if __name__ == '__main__':
    test_1()
