from math import inf

import numpy as np

from lab1 import inverse_matrix


class DualMethodSolver:
    def __init__(self, A, b, c, basis_plan, Jb):
        self.A = A
        self.b = b
        self.c = c
        self.m, self.n = A.shape
        self.y = basis_plan
        self.Jb = Jb
        self.co_plan = self.y @ self.A - self.c

    # region STEP FIRST
    def _get_pseudoplan(self, B):
        return B @ self.b

    def _first_step(self, B):
        """Найдем базисные компоненты псевдоплана кси, соотв. базису Jb"""
        return self._get_pseudoplan(B)

    # endregion

    # region STEP TWO
    def _second_step(self, pseudoplan):
        """Если все компоненты вектора каппа > 0 (для j принадлеж. Jb) =>
           STOP (оптимальный базисный план найден).
           Иначен - переходим к Шагу 3"""
        optimum_pseudoplan = []
        for j in range(self.n):
            if j in self.Jb:
                j_b = self.Jb.index(j)
                optimum_pseudoplan.append(pseudoplan[j_b])
            else:
                optimum_pseudoplan.append(0)
        print(f'Оптимальный план двойственной задачи = {self.y}, '
              f'базисное множество = {self.Jb} '
              f'Опртимальный псевдоплан = {optimum_pseudoplan}')
        return optimum_pseudoplan, self.y, self.Jb

    # endregion

    # region STEP THREE
    @staticmethod
    def _get_s_arr(pseudoplan):
        s_arr = []
        for j, x_j in enumerate(pseudoplan):
            if x_j < 0:
                s_arr.append(j)
        return s_arr

    def _get_min_s(self, pseudoplan):
        return min(self._get_s_arr(pseudoplan))

    def create_e_vector(self, s):
        e = np.zeros(self.m)
        e[s] = 1
        return e

    @staticmethod
    def _get_delta_y(s, B):
        return B[s, :]

    def _get_mu(self, delta_y):
        mu = []

        for j in range(self.n):
            mu.append(delta_y @ self.A[:, j])
        return np.array(mu)

    def _third_step(self, B, pseudoplan):
        """Среди Jb выберем индекс js, для которого кси_js < 0.
           Представим m-вектор дельта_y и числа мю_j (j принадлеж. Jн).
           по правилам дельта_у = e_s * B
                       мю_j = дельта_у * A'j (j принадлеж. Jн)"""
        s = self._get_min_s(pseudoplan)
        delta_y = self._get_delta_y(s, B)
        mu = self._get_mu(delta_y)
        return mu

    # endregion

    # region STEP FOUR
    def _fourth_step(self, mu):
        """Найдем дельта_0 по формуле:
                дельта_0 = min(c_j - A'j * y) / мю_j."""
        Jn = set(range(self.n)) - set(self.Jb)

        delta_arr = []
        for j in Jn:
            if mu[j] < 0:
                delta = (self.c[j] - self.y @ self.A[:, j]) / mu[j]
                delta_arr.append(delta)
            else:
                delta_arr.append(inf)

        return delta_arr, list(Jn)

    # endregion

    # region STEP FIVE
    def _five_step(self, pseudoplan, B, delta_0, j_0):
        """Построим новый базисный двойственный план y' и
           соответствующий ему Jb:
           y' = y + delta_0 * delta_y
           Jb = (Jb - js) + j0"""
        s = self._get_min_s(pseudoplan)
        delta_y = self._get_delta_y(s, B)
        self.y = self.y + delta_0 * delta_y
        self.Jb[s] = j_0

    # endregion

    # region STEP SIX
    def _six_step(self, B, j_0):
        """Вычислим B', обратную матрице А"""
        i = self.Jb.index(j_0)
        B = inverse_matrix(B, self.A[:, j_0], i)
        _ = self.A[:, self.Jb]
        return B

    # endregion

    def solve(self):
        A = self.A[:, self.Jb]
        B = np.linalg.inv(A)

        while True:
            pseudoplan = self._first_step(B)

            if np.all(pseudoplan >= 0):
                return self._second_step(pseudoplan)

            else:
                mu = self._third_step(B, pseudoplan)

                if np.all(mu >= 0):
                    print('Ограничения исходной задачи несовместны. '
                          'Двойственная задача не ограничена снизу на множестве'
                          ' ее планов')
                    return

                else:
                    delta_arr, Jn = self._fourth_step(mu)

                    # Выберем в качестве delta_0 наименьший элемент
                    # И найдем его индекс j_0.
                    delta_0 = min(delta_arr)
                    j_0 = Jn[delta_arr.index(delta_0)]

                    self._five_step(pseudoplan, B, delta_0, j_0)

                    B = self._six_step(B, j_0)


def test_1():
    print('TEST 1')
    dp = DualMethodSolver(
        A=np.array([
            [1, -1, 3, -2],
            [1, -5, 11, -6]
        ]),
        b=np.array([1, 9]),
        c=np.array([1, 1, -2, -3]),
        basis_plan=np.array([1.5, -0.5]),
        Jb=[0, 1]
    )

    dp.solve()
    print()


def test_2():
    print('TEST 2')
    A_b = np.array([[1, 3],
                   [1, 11]])
    c_b = np.array([1, 4])
    y_b = c_b @ np.linalg.inv(A_b)

    dp = DualMethodSolver(
        A=np.array([
            [1, -1, 3],
            [1, -5, 11]
        ]),
        b=np.array([0, 3]),
        c=np.array([1, 1, 4]),
        basis_plan=y_b,
        Jb=[0, 1]
    )

    dp.solve()
    print()


def test_3():
    print('TEST 3')
    A_b = np.array([[1, 1, 0],
                   [0, 1, 1],
                   [1, 0, 1]])
    c_b = np.array([1, 1, 1])
    y_b = c_b @ np.linalg.inv(A_b)

    dp = DualMethodSolver(
        A=np.array([
            [1, 1, 0, -4, 2],
            [0, 1, 1, 1, -2],
            [1, 0, 1, 3, -2]
        ]),
        b=np.array([-2, -2, -2]),
        c=np.array([1, 1, 1, 0, 1]),
        basis_plan=y_b,
        Jb=[0, 1, 2]
    )

    dp.solve()
    print()


def test_4():
    print('TEST 4')
    A_b = np.array([[1, 8, -1],
                    [0, 1, -7],
                    [0, 0, 1]])
    c_b = np.array([3, 2, 1])
    y_b = c_b @ np.linalg.inv(A_b)

    dp = DualMethodSolver(
        A=np.array([
            [1, 8, -1, 10, 14, 24],
            [0, 1, -7, 8, -5, 3],
            [0, 0, 1, -1, 1, 0]
        ]),
        b=np.array([4, 28, -4]),
        c=np.array([3, 2, 1, 1, -5, -10]),
        basis_plan=y_b,
        Jb=[0, 1, 2]
    )

    dp.solve()
    print()


if __name__ == '__main__':
    test_1()
    test_2()
    test_3()
    test_4()
