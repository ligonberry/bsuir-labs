from itertools import product

import numpy as np


# noinspection PyPep8Naming
class TransportProblemSolver:
    HR = '=' * 100

    @staticmethod
    def _validate_data(a, b):
        if np.any(a < 0) or np.any(b < 0):
            raise ValueError("Значения количества продукции у производителя и "
                             "продукции, необходимой потребителю не могут быть "
                             "отрицательными.")

    def _make_task_closed(self):
        """ Необходимо привести открытую транспортную задачу к закрытому
        (замкнутому) виду, добавляем столбец (строку) с нулевыми стоимостями.
        """
        sum_a = sum(self.a)
        sum_b = sum(self.b)
        print('sum: ', sum_a, sum_b)
        if sum_a < sum_b:
            self.a = np.append(self.a, [sum_b - sum_a])
            self.c = np.vstack((self.c, np.zeros(self.m)))
        else:
            self.b = np.append(self.b, [sum_a - sum_b])
            self.c = np.column_stack((self.c, np.zeros(self.n)))

    def is_closed_task(self, a, b):
        if sum(a) != sum(b):
            # Необходимо привести открытую транспортную задачу к закрытому
            # (замкнутому) виду.
            self._make_task_closed()

    @staticmethod
    def _is_values_not_zero(a, b):
        if np.any(a != 0) or np.any(b != 0):
            return True
        return False

    @staticmethod
    def _is_all_values_zero(a, b):
        if np.all(a == 0) and np.all(b == 0):
            return True
        return False

    @staticmethod
    def _insert_basis_plan_item(x, Jb, payload, i, j):
        x[i, j] = payload
        Jb.append((i, j))

    def _set_initial_basis_plan(self, x, Jb):
        self.Jb = Jb
        self.x = x

    def get_initial_basis_plan(self):
        """ Метод получения базисного плана - метод северо-западного угла."""
        x = np.zeros([self.n, self.m])
        Jb = []

        # Копируем массивы, чтобы не повредить их в результате наших манипуляций
        a_copy = self.a.copy()
        b_copy = self.b.copy()

        # Указатели для метода северо-западного угла
        i = 0
        j = 0

        while any(a_copy) > 0 and any(b_copy) > 0:

            # Находим, что меньше: сколько продавец может дать
            # (=> покупатель не удовлетворится), или же сколько покупатель может
            # купить (=> у продавца еще останется товар)
            a, b = a_copy[i], b_copy[j]
            payload = min(a, b)

            TransportProblemSolver._insert_basis_plan_item(x, Jb, payload, i, j)

            if a < b:
                # Продавец все продал. Покупатель не удовлетворен.
                # Ему нужно еще {ПОТРЕБНОСТЬ - payload} продукции
                a_copy[i] = 0
                b_copy[j] -= payload
                i += 1
            else:
                # Покупатель все купил, у продавца остались излишки.
                # Продавец может продать {ОСТАТКИ - payload} продукции
                b_copy[j] = 0
                a_copy[i] -= payload
                j += 1

        self._set_initial_basis_plan(x, Jb)
        print('Начальный базисный план перевозок, '
              f'полученный методом северо-западного угла: \n{self.x}.\n'
              f'Множество базисных позиций для этого плана: {self.Jb}')

    def __init__(self, suppliers, consumers, cost_matrix):

        TransportProblemSolver._validate_data(suppliers, consumers)

        self.a = suppliers
        self.b = consumers
        self.c = cost_matrix

        self.i = 0

        self.x = None  # Базисный план перевозок
        self.Jb = []  # Множество базисных позиций

        self.u = None
        self.v = None

        self.cycle = None

        self.is_closed_task(suppliers, consumers)
        self.get_initial_basis_plan()

        self.solve()

    n = property(lambda self: len(self.a))
    m = property(lambda self: len(self.b))

    @staticmethod
    def _is_neighbors(Jb_elem, result):
        for elem in result:
            if Jb_elem[0] == elem[0] or Jb_elem[1] == elem[1]:
                return True
        return False

    def _get_ordered_Jb_chain(self):
        """ Получает цепь Jb. Цепью (простой, элементарной), соединяющей клетку
            (i_1, j_1) с клеткой (i_k, j_k) называют послеовательность вида
            {(i_1, j_1), (i_1, j_2), (i_2, j_2), ..., (i_k, j_k)} в которой
            каждые соседние две клетки лежат в одной строке или в одном столбце,
            но ни в одной строке, ни в одном столбце нет трех последовательных
            клеток. """
        sorted_Jb = sorted(self.Jb[:])

        start = sorted_Jb.pop(0)
        result = [start]

        anchor = start
        while sorted_Jb:
            for Jb_elem in sorted_Jb:
                if TransportProblemSolver._is_neighbors(Jb_elem, result):
                    anchor = Jb_elem
                    break
            result.append(anchor)
            sorted_Jb.remove(anchor)
        return result

    @staticmethod
    def _show_potentials(u, v):
        print("Значения потенциалов на данной итерации: ")
        print("U: ", u)
        print("V: ", v)
        print(TransportProblemSolver.HR)

    def _set_potentials(self, u, v):
        self.u = u
        self.v = v

    def count_potentials(self):
        """ Некоторой произвольной строке (в нашем случае ` u[Jb_chain[0][0]]`
            припишем потенциал 0. Уравнение потенциалов u_i + v_j = c_ij
            позволяет однозначно найти остальные потенциалы. """
        u = np.full(self.n, np.nan)
        v = np.full(self.m, np.nan)

        Jb_chain = self._get_ordered_Jb_chain()

        u[Jb_chain[0][0]] = 0
        for i, j in Jb_chain:
            if np.isnan(u[i]) and not np.isnan(v[j]):
                u[i] = self.c[i, j] - v[j]
            elif not np.isnan(u[i]) and np.isnan(v[j]):
                v[j] = self.c[i, j] - u[i]
            elif not np.isnan(u[i]) and not np.isnan(v[j]):
                continue

        TransportProblemSolver._show_potentials(u, v)
        self._set_potentials(u, v)

    def _validate_delta(self, delta):
        if delta[1] not in self.Jb:
            return True
        return False

    def is_optimal(self):
        deltas = []
        for (i, j) in product(range(self.n), range(self.m)):
            fake_potential = self.u[i] + self.v[j]
            deltas.append((self.c[i, j] - fake_potential, (i, j)))

        return all(delta[0] >= 0 for delta in deltas
                   if self._validate_delta(delta))

    def _show_optimal_base_plan(self):
        print('Оптимальный базисный план: ')
        print(self.x)
        print(f'Множество базисных позиций оптимального плана: {self.Jb}')

    def _show_all_deltas(self, deltas):
        print('Разницы между стоимостями перевозки `c[i, j]` и '
              '`потенциалами` для небазисных клеток: ')
        print(np.array([d[0] for d in deltas]).reshape(self.n, self.m))
        print(TransportProblemSolver.HR)

    def _get_not_basic_deltas(self, deltas):
        not_basic_deltas = []
        for delta in deltas:
            if delta[1] not in self.Jb and not np.isnan(delta[0]):
                not_basic_deltas.append(delta)
        return not_basic_deltas

    def _get_max(self, deltas):
        not_basic_deltas = self._get_not_basic_deltas(deltas)
        return min(not_basic_deltas, key=lambda x: x[0])

    @staticmethod
    def _show_max_delta(max_delta):
        print(f'Максимальная разница = {max_delta[0]}. '
              f'Индексы = {max_delta[1]}')
        print(TransportProblemSolver.HR)

    def get_max_delta(self):
        deltas = []
        for i, j in product(range(self.n), range(self.m)):
            potential = self.u[i] + self.v[j]
            deltas.append((self.c[i, j] - potential, (i, j)))

        self._show_all_deltas(deltas)
        max_delta = self._get_max(deltas)
        TransportProblemSolver._show_max_delta(max_delta)

        return max_delta

    def _is_neighbor(self, neighbor):
        if neighbor[0] > self.n or neighbor[1] > self.m:
            # Сосед точки находится за пределами сетки m на n
            return False
        if neighbor[0] < 0 or neighbor[1] < 0:
            # Сосед точки имеет отрицательные координаты
            return False
        return True

    def _walk_to_closest(self, point, points, delta):

        neighbor = (point[0] + delta[0], point[1] + delta[1])

        # Проверяем существует ли реально сосед точки `point`
        if not self._is_neighbor(neighbor):
            return None

        while neighbor not in points:
            neighbor = (neighbor[0] + delta[0], neighbor[1] + delta[1])

            if not self._is_neighbor(neighbor):
                return None

        return neighbor

    def _get_neighbors(self, points, anchor):
        """ Необходимо получить ближайшие к заданной клетке точки
            (ее соседей по всем направлениям). """
        neighbors = []  # Массив соседей для нашей точки (`anchor`)
        # Направления для поиска соседей нашей точки
        directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        for direction in directions:
            neighbors.append(self._walk_to_closest(anchor, points, direction))
        return neighbors

    def _walk(self, path, step, anchor):
        # Копируем массив, чтобы не повредить его в результате наших манипуляций
        path_copy = path[:]
        path_copy.append(step)

        points = []
        for point in self.Jb:
            if point[0] == step[0] or point[1] == step[1]:
                points.append(point)

        # У точки может не быть соседей сбоку или по вертикали =>
        # отфильтровываем их
        existing_neighbors = []
        for neighbor in self._get_neighbors(points, step):
            if neighbor:
                existing_neighbors.append(neighbor)

        for point in existing_neighbors:
            if point == anchor and len(path_copy) > 3:
                return path_copy
            elif point not in path_copy:
                # Ищем всех соседей для новой клетки, которой нет в цепочке
                res = self._walk(path_copy, point, anchor)
                if res:
                    return res

    def get_cycle(self, cell):
        """ Построить цикл, начиная с заданной клетки `cell`."""
        return self._walk([], cell, cell)

    def _is_has_horizontal_neighbors(self, point):
        right_neighbor = self._walk_to_closest(point, self.cycle, (0, 1))
        left_neighbor = self._walk_to_closest(point, self.cycle, (0, -1))
        return right_neighbor and left_neighbor

    def _is_has_vertical_neighbors(self, point):
        upper_neighbor = self._walk_to_closest(point, self.cycle, (1, 0))
        lower_neighbor = self._walk_to_closest(point, self.cycle, (-1, 0))
        return upper_neighbor and lower_neighbor

    def _is_corner(self, point):
        """ Проверить является ли точка `point` угловой. """
        return not (self._is_has_vertical_neighbors(point) or
                    self._is_has_horizontal_neighbors(point))

    def _get_corner_points(self):
        """ Получить точки, которые являются угловыми в постоенном цикле. """
        corner_points = []
        for point in self.cycle:
            if self._is_corner(point):
                corner_points.append(point)
        return corner_points

    @staticmethod
    def _get_negative_points(cornered_points):
        negative_points = []
        for index, point in enumerate(cornered_points):
            if index % 2 == 1:
                negative_points.append(point)
        return negative_points

    def solve(self):
        while True:
            # ШАГ 1
            self.count_potentials()

            # ШАГ 3
            if self.is_optimal():
                return self._show_optimal_base_plan()

            else:
                # ШАГ 4
                delta = self.get_max_delta()
                self.Jb = self.Jb + [delta[1]]

                # ШАГ 5
                # Пытаемся построить цикл начиная с заданной клетки
                # (клетки с максимальной разницей c[i, j] и `потенциала`)
                self.cycle = self.get_cycle(delta[1])
                print('Клетки цикла ', self.cycle)
                print(TransportProblemSolver.HR)

                # ШАГ 6
                # Строим новый базисный план
                cornered_points = self._get_corner_points()
                negative_points = TransportProblemSolver._get_negative_points(
                    cornered_points)

                teta_index = min(negative_points, key=lambda x: self.x[x])
                teta = self.x[teta_index]
                print('teta и ее индексы ', teta, teta_index)
                print(TransportProblemSolver.HR)

                # Положительные элементы увеличиваются на 1,
                # а отрицательные уменьшаются на 1
                for index, cell in enumerate(cornered_points):
                    if index % 2 == 0:
                        self.x[cell] += teta
                    else:
                        self.x[cell] -= teta

                print('Новый базисный план ')
                print(self.x)
                print(TransportProblemSolver.HR)
                # ШАГ 7
                # Новому плану перевозок приписываем новый базис
                self.Jb.remove(teta_index)
                print()


def test_0():
    TransportProblemSolver(
        suppliers=np.array([0, 0, 0]),
        consumers=np.array([0, 0, 0]),

        cost_matrix=np.array([
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]
        ])
    )


def test_1():
    TransportProblemSolver(
        suppliers=np.array([70, 60, 30]),
        consumers=np.array([30, 90, 100]),

        cost_matrix=np.array([
            [8, 3, 1],
            [4, 7, 4],
            [5, 2, 6]
        ])
    )


def test_2():
    TransportProblemSolver(
        suppliers=np.array([20, 20, 20]),
        consumers=np.array([20, 20, 20]),

        cost_matrix=np.array([
            [1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1]
        ])
    )


def test_3():
    TransportProblemSolver(
        suppliers=np.array([20, 11, 18, 27]),
        consumers=np.array([11, 4, 10, 12, 8, 9, 10, 4]),

        cost_matrix=np.array([
            [-3, 6, 7, 12, 6, -3, 2, 16],
            [4, 3, 7, 10, 0, 1, -3, 7],
            [19, 3, 2, 7, 3, 7, 8, 15],
            [1, 4, -7, -3, 9, 13, 17, 22]
        ])
    )


if __name__ == '__main__':
    # test_1()
    # test_2()
    test_3()
