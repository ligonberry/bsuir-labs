import random
import time

import numpy as np


def inverse_matrix(Ar, x, i):
    l = Ar @ x
    if l[i] == 0:
        raise ValueError("Matrix is irreversible")
    else:
        l1 = l.copy()
        l1[i] = -1
        l2 = (-1.0 / l[i]) * l1
        Q = np.eye(len(x))
        Q[:, i] = l2
        return Q @ Ar


def solve_by_linalg(A, x, i):
    Al = A.copy()
    Al[:, i] = x
    return np.linalg.inv(Al)


def setup_1():
    i = 1  # new column index
    A = np.array([
        [1, 0, 5],
        [2, 1, 6],
        [3, 4, 0],
    ])
    x = np.array([2, 2, 2])
    return i, A, x


def test_1():
    i, A, x = setup_1()
    result_by_linalg = solve_by_linalg(A, x, i)
    Ar = np.linalg.inv(A)
    result = inverse_matrix(Ar, x, i)
    assert result_by_linalg.all() == result.all()


def setup_2():
    bound = 10**4
    n = random.randint(0, bound)
    i = random.randint(0, n)
    A = np.array(np.random.randint(bound, size=(n, n)))
    x = np.random.randint(bound, size=(1, n))[0]
    return i, A, x


def test_2():
    i, A, x = setup_2()

    t_before_linalg = time.time()
    result_by_linalg = solve_by_linalg(A, x, i)
    t_after_linalg = time.time()

    t_before = time.time()
    Ar = np.linalg.inv(A)
    result = inverse_matrix(Ar, x, i)
    t_after = time.time()

    assert result_by_linalg.all() == result.all()

    print('np.linalg function time ', t_after_linalg - t_before_linalg)
    print('custom function time ', t_after - t_before)


if __name__ == '__main__':
    test_1()
    test_2()
