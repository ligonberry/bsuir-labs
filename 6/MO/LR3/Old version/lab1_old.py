import numpy as np

def inverse_matrix(Ar, x, i):
    l = Ar @ x
    if l[i] == 0:
        raise ValueError("Matrix is irreversible")
    else:
        l1 = l.copy()
        l1[i] = -1
        l2 = (-1.0 / l[i]) * l1
        Q = np.eye(len(x))
        Q[:, i] = l2
        return Q @ Ar
