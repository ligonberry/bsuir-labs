from random import choice

import numpy as np

from lab2_old import SimplexMethodSolver


def get_auxiliary_task_solution(extended, b, m, J_fake):
    extended.extend(A=np.eye(m), c=-np.ones(m), initial_plan=b, J=J_fake)
    return extended.solve()


def get_J(basis_index, J_fake):
    J_intersection = set(basis_index) & set(J_fake)  # для случая 3)
    J_union = set(basis_index) | set(J_fake)  # для случая 2)
    return J_intersection, J_union


def create_e_vector(basis_index, intersection_index, m):
    e = np.zeros(m)
    e[basis_index.index(intersection_index)] = 1
    return e


def get_alpha_elem(extended, e, basis_index, J_b, J_nb):
    basis_matrix = extended.A[J_b, :][:, basis_index]
    inverse_basis_matrix = np.linalg.inv(basis_matrix)
    return e @ inverse_basis_matrix @ extended.A[J_b, :][:, J_nb].T


def case_1(basic_plan, basic_index, n):
    if np.any(basic_plan[n:]):
        print(f"""Заданные ограничения несовместны => нет допустимых базисных 
                  планов ({basic_plan}, {basic_index})""")
        return True


def case_3a(basic_index, j_0, j_k):
    column_index = basic_index.index(j_k)
    basic_index[column_index] = j_0


def case_3b(basic_index, j_k, J_b, n):
    print(basic_index, j_k, J_b)
    linearly_dependent_index = j_k - n
    del J_b[linearly_dependent_index]
    basic_index.remove(j_k)


def prepare_simplex_method(A, b):
    n_ = 0
    m, n = A.shape
    extended = SimplexMethodSolver(A=A, c=np.zeros(n), initial_plan=np.zeros(n),
                                   J=[])
    J_all = range(n + m)
    J_fake = range(n, n + m)
    J_b = list(range(m))

    basic_plan, basic_index = get_auxiliary_task_solution(extended, b, m,
                                                          J_fake)

    if case_1(basic_plan, basic_index, n):
        return

    else:
        J_intersection, J_union = get_J(basic_index, J_fake)

        while J_intersection:
            j_k = choice(list(J_intersection))
            j_0 = choice(list(set(J_all) - set(basic_index)))

            e = create_e_vector(basic_index, j_k, m - n_)
            alpha = get_alpha_elem(extended, e, basic_index, J_b,
                                   j_0)
            print(j_k, j_0)

            if alpha:
                case_3a(basic_index, j_0, j_k)
            else:
                n_ += 1
                case_3b(basic_index, j_k, J_b, n)

            J_intersection, J_union = get_J(basic_index, J_fake)

        return basic_plan[:n], basic_index


def inverse(A, b):
    for i, value in enumerate(b):
        if value < 0:
            A[i] = [-elem for elem in A[i]]
    return A


def test_1():
    c = np.array([1, 1, 0, 0, 0])
    A = np.array([[-1, 1, 1, 0, 0],
                  [1, 0, 0, 1, 0],
                  [0, 1, 0, 0, 1]])
    b = [1, 3, 2]
    A = inverse(A, b)
    initial_plan = np.array([0, 0, 1, 3, 2])
    J = [2, 3, 4]

    result_by_lab2 = SimplexMethodSolver(A=A, c=c, initial_plan=initial_plan,
                                         J=J).solve()

    x, J_b = prepare_simplex_method(A, b)

    result = SimplexMethodSolver(A=A, c=c, initial_plan=x, J=J_b).solve()

    print('Тест №1')
    print(f'Результат полученный только с помощью основной фазы симпликс-метода'
          f'== {result_by_lab2}')
    print(f'Результат полученный только с симпликс-метода == {result}')
    print()


def test_2():
    c = np.array([1, -3, -5, -1])
    A = np.array([[1, 4, 4, 1],
                  [1, 7, 8, 2]])
    b = [5, 9]
    A = inverse(A, b)
    initial_plan = np.array([1, 0, 1, 0])
    J = [0, 2]

    result_by_lab2 = SimplexMethodSolver(A=A, c=c, initial_plan=initial_plan,
                                         J=J).solve()

    x, J_b = prepare_simplex_method(A, b)

    result = SimplexMethodSolver(A=A, c=c, initial_plan=x, J=J_b).solve()

    print('Тест №2')
    print(
        f'Результат полученный только с помощью основной фазы симпликс-метода '
        f'== {result_by_lab2}')
    print(f'Результат полученный только с симпликс-метода == {result}')
    print()


def test_3():
    c = np.array([1, 1, 1, 1])
    A = np.array([[1, 3, 1, 2],
                  [2, 0, -1, 1]])
    b = [5, 1]
    A = inverse(A, b)
    initial_plan = np.array([0, 1, 0, 1])
    J = [1, 3]

    result_by_lab2 = SimplexMethodSolver(A=A, c=c, initial_plan=initial_plan,
                                         J=J).solve()

    x, J_b = prepare_simplex_method(A, b)

    result = SimplexMethodSolver(A=A, c=c, initial_plan=x, J=J_b).solve()

    print('Тест №3')
    print(
        f'Результат полученный только с помощью основной фазы симпликс-метода '
        f'== {result_by_lab2}')
    print(f'Результат полученный только с симпликс-метода == {result}')
    print()


def test_4():
    c = np.array([-2, 1, 1, -1, 4, 1])
    A = np.array([[3, 1, 2, 6, 9, 3],
                  [1, 2, -1, 2, 3, 1]])
    b = [15, 5]
    A = inverse(A, b)
    initial_plan = np.array([1, 0, 0, 0, 0, 4])
    J = [0, 5]

    result_by_lab2 = SimplexMethodSolver(A=A, c=c, initial_plan=initial_plan,
                                         J=J).solve()

    x, J_b = prepare_simplex_method(A, b)

    result = SimplexMethodSolver(A=A, c=c, initial_plan=x, J=J_b).solve()

    print('Тест №4')
    print(
        f'Результат полученный только с помощью основной фазы симпликс-метода '
        f'== {result_by_lab2}')
    print(f'Результат полученный только с симпликс-метода == {result}')
    print()


def test_5():
    A = np.array([[1, 1, 1],
                  [2, 2, 2]])
    b = [0, 0]

    x, J_b = prepare_simplex_method(A, b)
    print('Тест №5')
    print(f'Начальный базисный план {x} с базисными индексами {J_b}')
    print()


def test_6():
    A = np.array([[1, 1, 1],
                  [2, 2, 2],
                  [3, 3, 3]])
    b = [0, 0, 0]

    x, J_b = prepare_simplex_method(A, b)
    print('Тест №6')
    print(f'Начальный базисный план {x} с базисными индексами {J_b}')
    print()


def test_7():
    A = np.array([0, 1, 1, 1, 0, -8, 1, 5,
                  0, -1, 0, -7.5, 0, 0, 0, 2.0,
                  0, 2, 1, 0, -1, 3, -1.4, 0,
                  1, 1, 1, 1, 0, 3, 1, 1], float).reshape(4, 8)
    b = [15, -45, 1.8, 19]
    c = np.array([-6, -9, -5, 2, -6, 0, 1, 3])

    x, J_b = prepare_simplex_method(A, b)
    print(x, J_b)

    result = SimplexMethodSolver(A=A, c=c, initial_plan=x, J=J_b).solve()
    print(result)


if __name__ == '__main__':
    #test_1()
    # test_2()
    # test_3()
    # test_4()
    test_5()
    # test_6()
    # test_7()
