import click

from voice_cli.permission_utils import (pass_current_user_id)
from voice_cli.cli_utils.cli_voice import cli_voice
from voice_cli.presenter_utils import user2str
from voice_cli.error_handling import error_handler_wrapper
from voice_cli.config_utils import (change_current_user,
                                    get_default_config_path)
from voice.models import User


@cli_voice.group("auth")
def cli_auth():
    """Provide functional to relogin and show current user."""

@cli_auth.command("relogin")
@click.argument("username", type=str)
@click.pass_obj
@error_handler_wrapper
def relogin(ctx, username):
    """
    Change current user in config ot 'username'.
    If user doesn't exist, create him.

    \b
    Example of usage:
        voice auth relogin anton 
    """
    voice = ctx.voice

    users = voice.user_service.select_by_logins([username])
    if len(users) == 0:
        user = User(login=username)
        user_id = voice.user_service.add(user)
        print(
            "User '{}' does not exist.\n"
            "So, it will be created with id {}.".format(
                username,
                user_id,
            )
        )


    config_path = get_default_config_path()
    change_current_user(config_path, username)


@cli_auth.command("current_user")
@click.option("-v", "--verbose", count=True, default=2,
              help="Verbose of print. Must be in range [1, 3]. Default: 2.")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def print_current_user(ctx, user_id, verbose):
    """
    Print current logining user.

    Usage:
        voice auth current_user -vvv
    """

    print("Current user:")
    user = ctx.voice.user_service.select([user_id])[0]
    print(user2str(user, verbose=2))

