import click

from voice_cli.permission_utils import (
    pass_current_user_id
)
from voice_cli.cli_utils.cli_voice import cli_voice
from voice_cli.presenter_utils import fake2str
from voice_cli.error_handling import error_handler_wrapper
from voice.models import (
    Fake
)
from pprint import pprint


@cli_voice.group("fake")
def cli_fake():
    """Name to work with fake's entities"""


@cli_fake.command("add")
@click.option("-n", "--nickname", type=str, default=None,
              help="Nickanme of created fake.")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def add(ctx, user_id, nickname):
    """Create fake from current fake of user."""
    voice = ctx.voice

    fake = Fake(
        nickname=nickname,
    )

    fake_id = voice.fake_service.add(fake)


@cli_fake.command("show")
@click.argument("fake_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def show(ctx, user_id, fake_id):
    """Show all information about fake"""
    voice = ctx.voice
    print("fake_id:", fake_id)

    fakes = voice.fake_service.select([fake_id])
    print("fakes_len:", len(fakes))
    if len(fakes) == 0:
        print("Object with this id is not exists")
        return 0

    fake = fakes[0]
    string = fake2str(fake, verbose=2)
    print(string)


@cli_fake.command("list")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def list(ctx, user_id):
    """Show all fakes connected with current user"""
    voice = ctx.voice

    fakes = voice.fake_service.select_by_owner(user_id)
    if len(fakes) == 0:
        print("Objects connected with this fake don't exist")
        return 0

    for fake in fakes:
        string = fake2str(fake, verbose=1)
        print(string)


@cli_fake.command("update")
@click.option("-i", "--fake_id", type=int, default=None,
             help="id of fake to update")
@click.option("-n", "--nickname", type=str, default=None,
              help="new nickname of fake")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def update(ctx, user_id, fake_id, nickname):
    """Update fake with passed id"""
    if fake_id is None or nickname is None:
        msg = "fake_id and nickname must be passed"
        raise click.BadParameter(message=msg)

    voice = ctx.voice

    fakes = voice.fake_service.select([fake_id])
    print("fakes_len:", len(fakes))
    if len(fakes) == 0:
        print("Object with this id is not exists")
        return 0

    fake = fakes[0]
    fake.nickname = nickname
    voice.fake_service.update(fake)


@cli_fake.command("remove")
@click.argument("fake_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def remove(ctx, user_id, fake_id):
    """Remove fake with passed id"""
    voice = ctx.voice

    fakes = voice.fake_service.select([fake_id])
    print("fakes_len:", len(fakes))
    if len(fakes) == 0:
        print("Object with this id is not exists")
        return 0

    fake = fakes[0]
    voice.fake_service.remove([fake.id])

