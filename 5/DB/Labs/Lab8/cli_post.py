import click

from voice_cli.permission_utils import (
    pass_current_user_id
)
from voice_cli.cli_utils.cli_voice import cli_voice
from voice_cli.presenter_utils import post2str
from voice_cli.error_handling import error_handler_wrapper
from voice.models import (
    Post
)


@cli_voice.group("post")
def cli_post():
    """Name to work with post's entities"""


@cli_post.command("add")
@click.option("-t", "--title", type=str, default=None,
              help="Title of created post.")
@click.option("-c", "--content", type=str, default=None,
              help="Content of comment.")
@click.option("-ci", "--category_id", type=int, default=None,
              help="Category with post will be conntected")
@click.option("-bi", "--blog_id", type=int, default=None,
              help="Blog with post will be connected")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def add(ctx, user_id, title, content, category_id, blog_id):
    """Create post from current fake of user."""
    voice = ctx.voice

    post = Post(
        title=title,
        content=content,
    )

    post_id = voice.post_service.add(post)


@cli_post.command("show")
@click.argument("post_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def show(ctx, user_id, post_id):
    """Show all information about post"""
    voice = ctx.voice
    print("post_id:", post_id)

    posts = voice.post_service.select([post_id])
    print("posts_len:", len(posts))
    if len(posts) == 0:
        print("Object with this id is not exists")
        return 0

    post = posts[0]
    string = post2str(post, verbose=2)
    print(string)


@cli_post.command("list")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def list(ctx, user_id):
    """Show all posts connected with current fake"""
    voice = ctx.voice

    posts = voice.post_service.select_by_owner(user_id)
    if len(posts) == 0:
        print("Objects connected with this fake don't exist")
        return 0

    for post in posts:
        string = post2str(post, verbose=1)
        print(string)


@cli_post.command("update")
@click.option("-i", "--post_id", type=int, default=None,
             help="id of post to update")
@click.option("-t", "--title", type=str, default=None,
              help="new title of post")
@click.option("-c", "--content", type=str, default=None,
              help="Content of comment.")
@click.option("-ci", "--category_id", type=int, default=None,
              help="Category with post will be conntected")
@click.option("-bi", "--blog_id", type=int, default=None,
              help="Blog with post will be connected")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def update(ctx, user_id, post_id, title, content, category_id, blog_id):
    """Update post with passed id"""
    if post_id is None:
        msg = "post_id must be passed"
        raise click.BadParameter(message=msg)

    voice = ctx.voice

    posts = voice.post_service.select([post_id])
    print("posts_len:", len(posts))
    if len(posts) == 0:
        print("Object with this id is not exists")
        return 0

    post = posts[0]
    if title is not None:
        post.title = title
    if content is not None:
        post.content = content
    voice.post_service.update(post)


@cli_post.command("remove")
@click.argument("post_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def remove(ctx, user_id, post_id):
    """Remove post with passed id"""
    voice = ctx.voice

    posts = voice.post_service.select([post_id])
    print("posts_len:", len(posts))
    if len(posts) == 0:
        print("Object with this id is not exists")
        return 0

    post = posts[0]
    voice.post_service.remove([post.id])

