from .cli_authentication import cli_auth
from voice_cli.cli_utils.cli_blog import cli_blog
from voice_cli.cli_utils.cli_post import cli_post
from voice_cli.cli_utils.cli_fake import cli_fake
from voice_cli.cli_utils.cli_tag import cli_tag
from voice_cli.cli_utils.cli_category import cli_category
from voice_cli.cli_utils.cli_comment import cli_comment
from voice_cli.cli_utils.cli_debug import cli_debug

