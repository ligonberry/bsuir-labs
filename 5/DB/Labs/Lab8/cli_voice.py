import os
import click
import logging

from voice import Voice
from voice_cli.error_handling import error_handler_wrapper
from voice_cli.config_utils import (get_default_config_path,
                                      get_settings_from_config)
from collections import namedtuple

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


def check_config_existence():
    config_path = get_default_config_path()
    if not os.path.exists(config_path):
        raise Exception(
            "Config not found. May be config has been removed..."
            " If you have no ideas. Try reinstall Voice."
        )


@click.group("voice", context_settings=CONTEXT_SETTINGS,
             no_args_is_help=True)
@click.pass_context
@error_handler_wrapper
def cli_voice(ctx):
    print("hello 0")
    check_config_existence()
    config_path = get_default_config_path()
    settings = get_settings_from_config(config_path)

    voice = Voice(settings.connection_string)

    ctx.obj = namedtuple("items", ["voice", "settings"])
    ctx.obj.voice = voice
    ctx.obj.settings = settings

    #logging_file = os.path.expanduser(settings.logging_file)
    #logging.basicConfig(
    #    filename=logging_file,
    #    level=settings.logging_level
    #)
