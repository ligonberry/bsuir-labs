import click

from voice_cli.permission_utils import (
    pass_current_user_id
)
from voice_cli.cli_utils.cli_voice import cli_voice
from voice_cli.presenter_utils import category2str
from voice_cli.error_handling import error_handler_wrapper
from voice.models import (
    Category
)
from pprint import pprint


@cli_voice.group("category")
def cli_category():
    """Name to work with category's entities"""


@cli_category.command("add")
@click.option("-n", "--name", type=str, default=None,
              help="Name of created category.")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def add(ctx, user_id, name):
    """Create category from current fake of user."""
    voice = ctx.voice

    category = Category(
        name=name,
    )

    category_id = voice.category_service.add(category)


@cli_category.command("show")
@click.argument("category_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def show(ctx, user_id, category_id):
    """Show all information about category"""
    voice = ctx.voice
    print("category_id:", category_id)

    categorys = voice.category_service.select([category_id])
    print("categorys_len:", len(categorys))
    if len(categorys) == 0:
        print("Object with this id is not exists")
        return 0

    category = categorys[0]
    string = category2str(category, verbose=2)
    print(string)


@cli_category.command("list")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def list(ctx, user_id):
    """Show all categorys connected with current fake"""
    voice = ctx.voice

    categorys = voice.category_service.select_by_owner(user_id)
    if len(categorys) == 0:
        print("Objects connected with this fake don't exist")
        return 0

    for category in categorys:
        string = category2str(category, verbose=1)
        print(string)


@cli_category.command("update")
@click.option("-i", "--category_id", type=int, default=None,
             help="id of category to update")
@click.option("-n", "--name", type=str, default=None,
              help="new name of category")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def update(ctx, user_id, category_id, name):
    """Update category with passed id"""
    if category_id is None or name is None:
        msg = "category_id and name must be passed"
        raise click.BadParameter(message=msg)

    voice = ctx.voice

    categorys = voice.category_service.select([category_id])
    print("categorys_len:", len(categorys))
    if len(categorys) == 0:
        print("Object with this id is not exists")
        return 0

    category = categorys[0]
    category.name = name
    voice.category_service.update(category)


@cli_category.command("remove")
@click.argument("category_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def remove(ctx, user_id, category_id):
    """Remove category with passed id"""
    voice = ctx.voice

    categorys = voice.category_service.select([category_id])
    print("categorys_len:", len(categorys))
    if len(categorys) == 0:
        print("Object with this id is not exists")
        return 0

    category = categorys[0]
    voice.category_service.remove([category.id])

