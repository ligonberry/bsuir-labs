import click

from voice_cli.cli_utils.cli_voice import cli_voice
from voice_cli.error_handling import error_handler_wrapper


@cli_voice.group("debug")
def cli_debug():
    """Functional for debuging"""


@cli_debug.command("drop")
@click.pass_obj
@error_handler_wrapper
def add(ctx):
    """Create blog from current fake of user."""
    voice = ctx.voice

    voice.drop_all_tables()
