import click

from voice_cli.permission_utils import (
    pass_current_user_id
)
from voice_cli.cli_utils.cli_voice import cli_voice
from voice_cli.presenter_utils import tag2str
from voice_cli.error_handling import error_handler_wrapper
from voice.models import (
    Tag
)
from pprint import pprint


@cli_voice.group("tag")
def cli_tag():
    """Name to work with tag's entities"""


@cli_tag.command("add")
@click.option("-n", "--name", type=str, default=None,
              help="Name of created tag.")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def add(ctx, user_id, name):
    """Create tag from current fake of user."""
    voice = ctx.voice

    tag = Tag(
        name=name,
    )

    tag_id = voice.tag_service.add(tag)


@cli_tag.command("show")
@click.argument("tag_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def show(ctx, user_id, tag_id):
    """Show all information about tag"""
    voice = ctx.voice
    print("tag_id:", tag_id)

    tags = voice.tag_service.select([tag_id])
    print("tags_len:", len(tags))
    if len(tags) == 0:
        print("Object with this id is not exists")
        return 0

    tag = tags[0]
    string = tag2str(tag, verbose=2)
    print(string)


@cli_tag.command("list")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def list(ctx, user_id):
    """Show all tags connected with current fake"""
    voice = ctx.voice

    tags = voice.tag_service.select_by_owner(user_id)
    if len(tags) == 0:
        print("Objects connected with this fake don't exist")
        return 0

    for tag in tags:
        string = tag2str(tag, verbose=1)
        print(string)


@cli_tag.command("update")
@click.option("-i", "--tag_id", type=int, default=None,
             help="id of tag to update")
@click.option("-n", "--name", type=str, default=None,
              help="new name of tag")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def update(ctx, user_id, tag_id, name):
    """Update tag with passed id"""
    if tag_id is None or name is None:
        msg = "tag_id and name must be passed"
        raise click.BadParameter(message=msg)

    voice = ctx.voice

    tags = voice.tag_service.select([tag_id])
    print("tags_len:", len(tags))
    if len(tags) == 0:
        print("Object with this id is not exists")
        return 0

    tag = tags[0]
    tag.name = name
    voice.tag_service.update(tag)


@cli_tag.command("remove")
@click.argument("tag_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def remove(ctx, user_id, tag_id):
    """Remove tag with passed id"""
    voice = ctx.voice

    tags = voice.tag_service.select([tag_id])
    print("tags_len:", len(tags))
    if len(tags) == 0:
        print("Object with this id is not exists")
        return 0

    tag = tags[0]
    voice.tag_service.remove([tag.id])

