import click

from voice_cli.permission_utils import (
    pass_current_user_id
)
from voice_cli.cli_utils.cli_voice import cli_voice
from voice_cli.presenter_utils import comment2str
from voice_cli.error_handling import error_handler_wrapper
from voice.models import (
    Comment
)


@cli_voice.group("comment")
def cli_comment():
    """Name to work with comment's entities"""


@cli_comment.command("add")
@click.option("-c", "--content", type=str, default=None,
              help="Content of comment.")
@click.option("-pi", "--post_id", type=int, default=None,
              help="Post with comment will be connected")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def add(ctx, user_id, content, post_id):
    """Create comment from current fake of user."""
    voice = ctx.voice

    comment = Comment(
        content=content,
    )

    comment_id = voice.comment_service.add(comment)


@cli_comment.command("show")
@click.argument("comment_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def show(ctx, user_id, comment_id):
    """Show all information about comment"""
    voice = ctx.voice
    print("comment_id:", comment_id)

    comments = voice.comment_service.select([comment_id])
    print("comments_len:", len(comments))
    if len(comments) == 0:
        print("Object with this id is not exists")
        return 0

    comment = comments[0]
    string = comment2str(comment, verbose=2)
    print(string)


@cli_comment.command("list")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def list(ctx, user_id):
    """Show all comments connected with current fake"""
    voice = ctx.voice

    comments = voice.comment_service.select_by_owner(user_id)
    if len(comments) == 0:
        print("Objects connected with this fake don't exist")
        return 0

    for comment in comments:
        string = comment2str(comment, verbose=1)
        print(string)


@cli_comment.command("update")
@click.option("-i", "--comment_id", type=int, default=None,
             help="id of comment to update")
@click.option("-c", "--content", type=str, default=None,
              help="Content of comment.")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def update(ctx, user_id, comment_id, content):
    """Update comment with passed id"""
    if comment_id is None:
        msg = "comment_id must be passed"
        raise click.BadParameter(message=msg)

    voice = ctx.voice

    comments = voice.comment_service.select([comment_id])
    print("comments_len:", len(comments))
    if len(comments) == 0:
        print("Object with this id is not exists")
        return 0

    comment = comments[0]
    if content is not None:
        comment.content = content
    voice.comment_service.update(comment)


@cli_comment.command("remove")
@click.argument("comment_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def remove(ctx, user_id, comment_id):
    """Remove comment with passed id"""
    voice = ctx.voice

    comments = voice.comment_service.select([comment_id])
    print("comments_len:", len(comments))
    if len(comments) == 0:
        print("Object with this id is not exists")
        return 0

    comment = comments[0]
    voice.comment_service.remove([comment.id])

