import click

from voice_cli.permission_utils import pass_current_user_id
from voice_cli.cli_utils.cli_voice import cli_voice
from voice_cli.converter_utils import validate_ids_list
from voice_cli.presenter_utils import user_to_str
from voice_cli.error_handling import error_handler_wrapper
from voice_cli.parser_utils import OptionEatAll


@cli_voice.group("user")
def cli_user():
    """Provide functional to work with user's entities."""


# region adding
@cli_user.command("add")
@click.argument("name", type=str, metavar="<name>")
@click.option("--email", type=str,
              help="Email of created user.")
@click.pass_obj
@error_handler_wrapper
def add(ctx, name, email):
    """Create and add user with passed params to database.

    \b
    Example of usage:
        voice user add Anton --email thescir@yandex.ru
    """
    voice = ctx.voice

    user_id = voice.add_user(
        name=name,
        email=email,
    )

    print("User {} has been created with id {}".format(name, user_id))
# endregion


# region editing
@cli_user.command("edit")
@click.option("-e", "--email", metavar="<text>",
              help="New email of user.")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def edit(ctx, user_id, name, email):
    """Edit current user with forwarded params.

    \b
    Example of usage:
        voice user edit -e cheshir@yandex.ru
    """
    voice = ctx.voice

    voice.edit_user(
        user_id,
        name=name,
        email=email,
    )
# endregion


# region showing
@cli_user.command("show")
@click.option("--ids", cls=OptionEatAll,
              callback=validate_ids_list,
              metavar="<list of ids>",
              help="Include user with id from list.")
@click.option("-v", "--verbose", count=True, default=2,
              help="Verbose of print. Must be in range [1, 3]. Default: 2.")
@click.pass_obj
@error_handler_wrapper
def show(ctx, ids, verbose):
    """Print users meeting the conditions.

    Result will be conjunctions(through options) of
    disjunctions(through id of ids-options).

    \b
    Example of usage:
        voice user show --ids 1 3 -vvv
    """
    voice = ctx.voice

    users = voice.get_users(
        user_ids=ids,
    )

    for user in users:
        user_string = user_to_str(
            user=user,
            date_format=ctx.settings.output_date_format,
            verbose=verbose
        )
        print(user_string)
# endregion


# region trusting
@cli_user.group("trust")
def cli_user_trust():
    """Provide functionals to build trust between users."""


@cli_user_trust.command("add")
@click.argument("username", type=str, nargs=1,
                metavar="<username>")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def add_connection(ctx, user_id, username):
    """Add trust from current user to user_ids.

    \b
    Example of usage:
        voice user trust add 1 2 3
    """
    voice = ctx.voice

    users = voice.get_users(names=[username])
    if len(users) == 0:
        print("User with name '{}' doesn't exist".format(username))
        return

    voice.add_trust(
        dominant_id=users[0].id,
        submissive_id=user_id,
    )


@cli_user_trust.command("remove")
@click.argument("username", type=str, nargs=1,
                metavar="<username>")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def remove_connection(ctx, user_id, username):
    """Remove trust from current user to user_ids.

    \b
    Example of usage:
        voice user trust add username
    """
    voice = ctx.voice

    users = voice.get_users(names=[username])
    if len(users) == 0:
        print("User with name '{}' doesn't exist".format(username))
        return

    voice.remove_trust(
        dominant_id=users[0].id,
        submissive_id=user_id,
    )
# endregion
