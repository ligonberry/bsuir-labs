import click

from voice_cli.permission_utils import (
    pass_current_user_id
)
from voice_cli.cli_utils.cli_voice import cli_voice
from voice_cli.presenter_utils import blog2str
from voice_cli.error_handling import error_handler_wrapper
from voice.models import (
    Blog
)
from pprint import pprint


@cli_voice.group("blog")
def cli_blog():
    """Name to work with blog's entities"""


@cli_blog.command("add")
@click.option("-t", "--title", type=str, default=None,
              help="Title of created blog.")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def add(ctx, user_id, title):
    """Create blog from current fake of user."""
    voice = ctx.voice

    blog = Blog(
        title=title,
    )

    blog_id = voice.blog_service.add(blog)


@cli_blog.command("show")
@click.argument("blog_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def show(ctx, user_id, blog_id):
    """Show all information about blog"""
    voice = ctx.voice
    print("blog_id:", blog_id)

    blogs = voice.blog_service.select([blog_id])
    print("blogs_len:", len(blogs))
    if len(blogs) == 0:
        print("Object with this id is not exists")
        return 0

    blog = blogs[0]
    string = blog2str(blog, verbose=2)
    print(string)


@cli_blog.command("list")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def list(ctx, user_id):
    """Show all blogs connected with current fake"""
    voice = ctx.voice

    blogs = voice.blog_service.select_by_owner(user_id)
    if len(blogs) == 0:
        print("Objects connected with this fake don't exist")
        return 0

    for blog in blogs:
        string = blog2str(blog, verbose=1)
        print(string)


@cli_blog.command("update")
@click.option("-i", "--blog_id", type=int, default=None,
             help="id of blog to update")
@click.option("-t", "--title", type=str, default=None,
              help="new title of blog")
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def update(ctx, user_id, blog_id, title):
    """Update blog with passed id"""
    if blog_id is None or title is None:
        msg = "blog_id and title must be passed"
        raise click.BadParameter(message=msg)

    voice = ctx.voice

    blogs = voice.blog_service.select([blog_id])
    print("blogs_len:", len(blogs))
    if len(blogs) == 0:
        print("Object with this id is not exists")
        return 0

    blog = blogs[0]
    blog.title = title
    voice.blog_service.update(blog)


@cli_blog.command("remove")
@click.argument("blog_id", type=int)
@click.pass_obj
@pass_current_user_id
@error_handler_wrapper
def remove(ctx, user_id, blog_id):
    """Remove blog with passed id"""
    voice = ctx.voice

    blogs = voice.blog_service.select([blog_id])
    print("blogs_len:", len(blogs))
    if len(blogs) == 0:
        print("Object with this id is not exists")
        return 0

    blog = blogs[0]
    voice.blog_service.remove([blog.id])

