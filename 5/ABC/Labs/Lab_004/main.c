#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main (int argc, char *argv[])
{
    srand(time(NULL));
    int NRA, NCA, NCB, res;
    printf("Enter number of rows in matrix A ");
    if (scanf("%d", &NRA) != 1 || NRA <= 0){
        printf("Error input!");
        return 0;
    }
    printf("Enter number of columns in matrix A ");

    if (scanf("%d", &NCA) != 1 || NCA <= 0) {
        printf("Error input!");
        return 0;
    }
    printf("Enter number of columns in matrix B ");

    if (scanf("%d", &NCB) != 1 && NCB <= 0) {
        printf("Error input!");
        return 0;
    }

    int	tid, nthreads, i, j, k, chunk;
    double	a[NRA][NCA],           /* matrix A to be multiplied */
        b[NCA][NCB],           /* matrix B to be multiplied */
        c[NRA][NCB];           /* result matrix C */

    chunk = 10;                    /* set loop iteration chunk size */
    printf("\n");
    printf("\033[1;33mThread 1\033[0m starting matrix multiply...\n");
    printf("\033[1;32mThread 1\033[0m starting matrix multiply...\n");
    printf("\033[1;31mThread 1\033[0m starting matrix multiply...\n");
    printf("\033[1;35mThread 1\033[0m starting matrix multiply...\n");
/*** Spawn a parallel region explicitly scoping all variables ***/
#pragma omp parallel shared(a,b,c,nthreads,chunk) private(tid,i,j,k)
  {
  tid = omp_get_thread_num();
  if (tid == 0)
    {
    nthreads = omp_get_num_threads();
    }
  /*** Initialize matrices ***/
  #pragma omp for schedule (static, chunk)
  for (i=0; i<NRA; i++)
    for (j=0; j<NCA; j++)
      a[i][j]= i+j;
  #pragma omp for schedule (static, chunk)
  for (i=0; i<NCA; i++)
    for (j=0; j<NCB; j++)
      b[i][j]= i*j;
  #pragma omp for schedule (static, chunk)
  for (i=0; i<NRA; i++)
    for (j=0; j<NCB; j++)
      c[i][j]= 0;

  /*** Do matrix multiply sharing iterations on outer loop ***/
  /*** Display who does which iterations for demonstration purposes ***/
  #pragma omp for schedule (static, chunk)
  for (i=0; i<NRA; i++)
    {
    //printf("Thread=%d did row=%d\n",tid,i);
    for(j=0; j<NCB; j++)
      for (k=0; k<NCA; k++)
        c[i][j] += a[i][k] * b[k][j];
    }
  }   /*** End of parallel region ***/

/*** Print results ***/
printf("******************************************************\n");
printf("Result Matrix:\n");
for (i=0; i<NRA; i++)
  {
  for (j=0; j<NCB; j++) {
      int col_choice = rand() % 4;
      if (col_choice == 0)
          printf("\033[1;33m%6.2f    \033[0m", c[i][j]);
      if (col_choice == 1)
          printf("\033[1;32m%6.2f    \033[0m", c[i][j]);
      if (col_choice == 2)
          printf("\033[1;31m%6.2f    \033[0m", c[i][j]);
      if (col_choice == 3)
          printf("\033[1;35m%6.2f    \033[0m", c[i][j]);
  }
      printf("\n");
  }
printf("******************************************************\n");
printf ("Done.\n");

}