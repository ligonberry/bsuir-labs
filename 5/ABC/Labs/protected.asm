org 0
use16

jmp main

;========================================
;; Данные
;========================================
save_ss dw ?
save_sp dw ?
save_ds dw ?
save_es dw ?
save_fs dw ?
save_gs dw ?
;========================================
;; Код 16бит
;========================================
main:

    mov ax,3
    int 10h

    mov [save_ss], ss
    mov [save_ds], ds
    mov [save_es], es
    mov [save_fs], fs
    mov [save_gs], gs
    mov [save_sp], sp

    call    disable_interrupts

    lgdt [GDTR]  
    
    call set_PE

    

    jmp fword 8:protected_mode_begining;


;========================================
;; Функции
;========================================

; Устанавливает флаг PE
set_PE:
    mov     eax, cr0 ; прочитать регистр CR0
    or      al, 1    ; установить бит PE,
    mov     cr0, eax ; с этого момента мы в защищенном режиме
    ret

; Запрещает маскируемые и немаскируемые прерывания
disable_interrupts:
    cli ;запрещаем прерывания
    mov al,8Fh;запрещаем NMI
    out 70h,al
    in al,71h
    ret

putstr:
    lodsb
    or al,al
    jz  putstrd
    mov ah,0x0E
    mov bx,0x0007
    int 0x10
    jmp putstr
putstrd:
    retn



    
;========================================
;; Код 32бит
;========================================


use32

protected_mode_begining:
       
    mov eax,10h ;здесь пихаем селекторы
    mov es,ax 
    mov ds,ax
    mov fs,ax
    mov ss,ax
    mov ax,18h
    mov gs,ax  

    mov  edi, 0xA8000  
    mov  esi, hi_string  
    cld
    
    loo:                     
        lodsb                   
        test al, al  
        jz   exit       
        stosb              
        mov  al, 7              
        stosb
        jmp  loo

exit:
    jmp fword 32:protected_mode_exiting


;========================================
; Код 16бит
;========================================

use16
protected_mode_exiting:
	;jmp $
    mov eax, cr0
    and al, 0xFE
    mov cr0, eax

    jmp 0x1000:real_mode

hi_string db 'Hello from PM', 0


real_mode:
    

    mov ss,[save_ss]
    mov sp,[save_sp]
    mov ds,[save_ds]
    mov es,[save_es]
    mov fs,[save_fs]
    mov gs,[save_gs]

    in         al,70h              ; индексный порт CMOS
    and        al,07Fh             ; сброс бита 7 отменяет блокирование NMI
    out        70h,al
    sti

    jmp $


;========================================
;; Таблица дескрипторов
;========================================
align   8
;GDT:
 ;   dq 0 ;пустой
  ;  db 0FFh,0FFh,0,0,1,9Ah,0CFh,0 ;код
   ; db 0FFh,0FFh,0,0,1,92h,0CFh,0;данные
   ; db 0FFh,0FFh,0,80h,0Bh,92h,40h,0 ;видеосегмент
   ; dd 0FFh,0FFh,0,0,1,9Ah,0,0; ; 5-й дескриптор (код для перехода в R-Mode)
    ;dd 0FFh,0FFh,0,0,1,92h,0,0; ; 6-й дескриптор (стек и данные для перехода в R-Mode)

gdt32:
    dd 0x0, 0x0

    dw 0xFFFF ; segment length, bits 0-15
    dw 0x0    ; segment base, bits 0-15
    db 0x1    ; segment base, bits 16-23
    db 0x9A   ; flags (8 bits)
    db 0x40   ; flags (4 bits) + segment length, bits 16-19
    db 0x0    ; segment base, bits 24-31

    dw 0xFFFF
    dw 0x0
    db 0x1
    db 0x92
    db 0xCF ; db 0x40
    db 0x0

    dw 0xFFFF
    dw 0x8000
    db 0x0B
    db 0x92
    db 0x40
    db 0x0

    dw 0xFFFF
    dw 0x0
    db 0x1
    db 0x9A
    db 0x0
    db 0x0

    dw 0xFFFF
    dw 0x0
    db 0x1
    db 0x92
    db 0x0
    db 0x0


label GDT_SIZE at $-gdt32
GDTR:
    dw GDT_SIZE-1
    dd gdt32+10000h