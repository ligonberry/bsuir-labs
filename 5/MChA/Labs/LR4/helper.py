import random
import heapq
import itertools


def calculate_error(data_old, data):
    solution_old = data_old.solution
    solution = data.solution

    _show_maximum_deviation(solution_old, solution)


def get_chain(matrix):
    return itertools.chain(*matrix)


def calculate_maximum_deviation(res_matrix_old, res_matrix):
    solution_old = list(get_chain(res_matrix_old))
    solution = list(get_chain(res_matrix))

    return _show_maximum_deviation(solution_old, solution)


def _show_maximum_deviation(solution_old, solution):
    largest_error = heapq.nlargest(1, (((abs(a - b)), a, b) for a, b in zip(solution_old, solution[::2])))
    deviation = format(largest_error[0][0], '.8f')

    return deviation

