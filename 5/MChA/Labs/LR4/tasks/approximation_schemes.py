import numpy as np
from scipy import linalg

from sympy.abc import x


def explicit_first_approximation_way(matrix, n, h, g_2):
    for t in range(1, n + 1):
        matrix[t, n] = matrix[t, n - 1] + g_2 * h


def explicit_second_approximation_way(matrix, n, h, tau, g_2):
    rho = tau / (h ** 2)
    dummy_layer = [0] * (n + 1)
    for t in range(1, n + 1):
        dummy_layer[t] = matrix[t, n - 1] + 2 * g_2 * h
        matrix[t, n] = (matrix[t - 1, n] +
                        rho * (dummy_layer[t] -
                               2 * matrix[t - 1, n] +
                               matrix[t - 1, n - 1]
                               )
                        )


def explicit_third_approximation_way(matrix, n, h, g_2):
    for t in range(1, n + 1):
        matrix[t, n] = (4 / 3 * matrix[t, n - 1] -
                        1 / 3 * matrix[t, n - 2] +
                        2 * h * g_2 / 3
                        )


def implicit_first_approximation_way(matrix, t, n, h, g_2):
    return matrix[t, n - 1] + g_2 * h


def implicit_second_approximation_way(matrix, t, n, h, tau, g_2):
    rho = tau / (h ** 2)
    dummy_node = matrix[t, n - 1] + 2 * g_2 * h
    matrix[t, n] = (matrix[t - 1, n] +
                    rho * (dummy_node -
                           2 * matrix[t - 1, n] +
                           matrix[t - 1, n - 1]
                           )
                    )
    return matrix[t, n]


def implicit_third_approximation_way(matrix, t, n, h, g_2):
    matrix[t, n] = (4 / 3 * matrix[t, n - 1] -
                    1 / 3 * matrix[t, n - 2] +
                    2 * h * g_2 / 3
                    )
    return matrix[t, n]


def solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions, y_boundary_conditions,
                                        init_condition, n, tau, flag=1):
    f, k = coefficients_before_y

    a, b = boundary_conditions
    h = (b - a) / n
    rho = tau / h ** 2

    matrix = np.zeros((n + 1, n + 1))

    g_1, g_2 = y_boundary_conditions

    for i in range(n + 1):  # On the zero layer for t = 0.
        x_value = a + h * i
        if x_value <= b:
            matrix[0, i] = init_condition.subs(x, x_value)

    for t in range(1, n + 1):
        matrix[t, 0] = g_1  # the leftmost
        matrix[t, n] = g_2

    for t in range(1, n + 1):  # The calculation by the "explicit scheme".
        for x_ in range(1, n):
            matrix[t, x_] = (matrix[t - 1, x_] +
                             rho * (matrix[t - 1, x_ + 1] -
                                    2 * matrix[t - 1, x_] +
                                    matrix[t - 1, x_ - 1]
                                    ) + tau * f
                             )

    # The rightmost:
    if flag == 1:
        explicit_first_approximation_way(matrix, n, h, g_2)
    elif flag == 2:
        explicit_second_approximation_way(matrix, n, h, tau, g_2)
    elif flag == 3:
        explicit_third_approximation_way(matrix, n, h, g_2)

    return matrix


def solve_implicit_approximation_scheme(boundary_conditions, y_boundary_conditions, init_condition, n, tau, flag=1):
    a, b = boundary_conditions

    matrix = np.zeros((n + 1, n + 1))  # initialize the U value matrix

    h = (b - a) / n
    rho = tau / (h ** 2)  # parameter rho

    g_1, g_2 = y_boundary_conditions

    for i in range(n + 1):  # On the zero layer for t = 0.
        x_value = a + h * i
        if x_value <= b:
            matrix[0, i] = init_condition.subs(x, x_value)

    A = np.zeros((n + 1, n + 1))
    for i in range(1, n):
        A[i, i - 1] = -rho
        A[i, i] = 1 + 2 * rho
        A[i, i + 1] = -rho
    A[0, 0] = 1.
    A[n, n] = 1.

    solution = []
    for t in range(n):
        rhs = matrix[t]
        rhs[0] = g_1

        # The rightmost:
        if flag == 1 and t > 0:
            rhs[n] = implicit_first_approximation_way(matrix, t, n, h, g_2)
        elif flag == 2 and t > 0:
            rhs[n] = implicit_second_approximation_way(matrix, t, n, h, tau, g_2)
        elif flag == 3 and t > 0:
            rhs[n] = implicit_third_approximation_way(matrix, t, n, h, g_2)

        # solve the matrix equation Ac = rhs  , c = rhs/A, .solve(rhs)
        temperatures = linalg.solve(A, rhs)

        solution.append(list(temperatures))
        matrix[t + 1] = temperatures

    return solution
