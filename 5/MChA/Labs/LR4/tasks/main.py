""" Лабораторная работа 4. Вариант 4.1.11

Задание:
--------
1. Использовать явную и неявную разностную схему с различными вариантами аппроксимации граничных значений.
2. Шаг tau выбрать из условия устойчивости: tau <= 0.5 * (h^2 / k)
3. Изобразить графики зависимости приближенного решения от х при t = 0, 2*tau, 4*tau, ..., T.

du      d^2 (u)
-- = k --------- + f(x, t), a < x < b, 0 < t < T,
dt       dx^2

u(a, x) = g1(x),

du
---(b, x) = g2(x)
dx

U(x, 0) = ф(x)  a <= x <= b;


Исходные данные:
---------------
a = 0, g1 = 0.5
b = 1, g2 = 0.5 - ГУ
k(x, t) = 1
T = 0.05
ф(x) = | x - 0.5 | - начальное
f(x) = 0
"""

import matplotlib
import numpy as np
from sympy.abc import x

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from tasks.approximation_schemes import solve_explicit_approximation_scheme, solve_implicit_approximation_scheme
from tasks.table import create_table


def main():
    a = 0
    b = 1
    boundary_conditions = a, b

    g_1 = 0.5
    g_2 = 0.5
    y_boundary_conditions = g_1, g_2

    n = 52
    n_old = 26

    f = 0
    k = 1
    coefficients_before_y = f, k

    # EXPLICIT
    init_condition = abs(x - 0.5)
    h = (b - a) / n_old
    tau_old = (h ** 2 / 100 * k)
    explicit_res_matrix_1_old = solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions,
                                                                    y_boundary_conditions,
                                                                    init_condition, n_old, tau_old)

    tau = tau_old / 4
    explicit_res_matrix_1 = solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions,
                                                                y_boundary_conditions,
                                                                init_condition, n, tau)
    print('First approximation way for explicit scheme')

    create_table(explicit_res_matrix_1_old,
                 explicit_res_matrix_1,
                 coefficients_before_y,
                 boundary_conditions,
                 y_boundary_conditions,
                 init_condition, n_old, tau_old)

    explicit_res_matrix_2_old = solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions,
                                                                    y_boundary_conditions,
                                                                    init_condition, n_old, tau_old, flag=2)

    explicit_res_matrix_2 = solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions,
                                                                y_boundary_conditions,
                                                                init_condition, n, tau, flag=2)

    print('Second approximation way for explicit scheme')
    create_table(explicit_res_matrix_2_old,
                 explicit_res_matrix_2,
                 coefficients_before_y,
                 boundary_conditions,
                 y_boundary_conditions,
                 init_condition, n_old, tau_old)

    explicit_res_matrix_3_old = solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions,
                                                                    y_boundary_conditions,
                                                                    init_condition, n_old, tau_old, flag=3)

    explicit_res_matrix_3 = solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions,
                                                                y_boundary_conditions,
                                                                init_condition, n, tau, flag=3)

    print('Third approximation way for explicit scheme')
    create_table(explicit_res_matrix_3_old,
                 explicit_res_matrix_3,
                 coefficients_before_y,
                 boundary_conditions,
                 y_boundary_conditions,
                 init_condition, n_old, tau_old)

    # IMPLICIT
    implicit_res_matrix_1_old = solve_implicit_approximation_scheme(boundary_conditions,
                                                                    y_boundary_conditions,
                                                                    init_condition, n_old, tau_old)

    implicit_res_matrix_1 = solve_implicit_approximation_scheme(boundary_conditions,
                                                                y_boundary_conditions,
                                                                init_condition, n, tau)

    print('First approximation way for implicit scheme')
    create_table(implicit_res_matrix_1_old,
                 implicit_res_matrix_1,
                 coefficients_before_y,
                 boundary_conditions,
                 y_boundary_conditions,
                 init_condition, n_old, tau_old)

    implicit_res_matrix_2_old = solve_implicit_approximation_scheme(boundary_conditions,
                                                                    y_boundary_conditions,
                                                                    init_condition, n_old, tau_old, flag=2)

    implicit_res_matrix_2 = solve_implicit_approximation_scheme(boundary_conditions,
                                                                y_boundary_conditions,
                                                                init_condition, n, tau, flag=2)

    print('Second approximation way for implicit scheme')
    create_table(implicit_res_matrix_2_old,
                 implicit_res_matrix_2,
                 coefficients_before_y,
                 boundary_conditions,
                 y_boundary_conditions,
                 init_condition, n_old, tau_old)

    implicit_res_matrix_3_old = solve_implicit_approximation_scheme(boundary_conditions,
                                                                    y_boundary_conditions,
                                                                    init_condition, n_old, tau_old, flag=3)

    implicit_res_matrix_3 = solve_implicit_approximation_scheme(boundary_conditions,
                                                                y_boundary_conditions,
                                                                init_condition, n, tau, flag=3)

    print('Third approximation way for implicit scheme')
    create_table(implicit_res_matrix_3_old,
                 implicit_res_matrix_3,
                 coefficients_before_y,
                 boundary_conditions,
                 y_boundary_conditions,
                 init_condition, n_old, tau_old)

    x_points = np.linspace(a, b, n + 1)
    for i in range(0, 60, 10):
        explicit_temperatures_1 = explicit_res_matrix_1[i]
        plt.plot(x_points, explicit_temperatures_1, 'g-', label='explicit, 1')
        explicit_temperatures_2 = explicit_res_matrix_2[i]
        plt.plot(x_points, explicit_temperatures_2, 'b--', label='explicit, 2')
        explicit_temperatures_3 = explicit_res_matrix_3[i]
        plt.plot(x_points, explicit_temperatures_3, 'r--', label='explicit, neuman')
        implicit_temperatures_1 = implicit_res_matrix_1[i]
        plt.plot(x_points, implicit_temperatures_1, 'y-', label='implicit, 1')
        implicit_temperatures_2 = implicit_res_matrix_2[i]
        plt.plot(x_points, implicit_temperatures_2, 'm--', label='implicit, 2')
        implicit_temperatures_neuman = implicit_res_matrix_3[i]
        plt.plot(x_points, implicit_temperatures_neuman, 'c--', label='implicit, neuman')
        plt.title(f't = {i}')
        plt.legend()
        plt.grid()
        plt.show()


if __name__ == '__main__':
    main()
