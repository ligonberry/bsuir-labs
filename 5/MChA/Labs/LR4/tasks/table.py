from prettytable import PrettyTable
from tasks.approximation_schemes import solve_explicit_approximation_scheme, solve_implicit_approximation_scheme
from helper import calculate_maximum_deviation


def find_explicit_solutions(results, coefficients_before_y,
                            boundary_conditions,
                            y_boundary_conditions,
                            init_condition,
                            n, tau,
                            calculation_count=1):
    for i in range(calculation_count):
        result = solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions,
                                                     y_boundary_conditions,
                                                     init_condition, n * 2,
                                                     tau / 4)
        results.append(result)

    result_4 = solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions,
                                                   y_boundary_conditions,
                                                   init_condition, n * 4,
                                                   tau / 16)
    results.append(result_4)
    return results


def find_implicit_solutions(results, coefficients_before_y,
                            boundary_conditions,
                            y_boundary_conditions,
                            init_condition,
                            n, tau,
                            calculation_count=1):
    for i in range(calculation_count):
        result = solve_implicit_approximation_scheme(coefficients_before_y, boundary_conditions,
                                                     y_boundary_conditions,
                                                     init_condition, n * 2,
                                                     tau / 4)
        results.append(result)

    result_4 = solve_implicit_approximation_scheme(coefficients_before_y, boundary_conditions,
                                                   y_boundary_conditions,
                                                   init_condition, n * 4,
                                                   tau / 16)
    results.append(result_4)
    return results


def find_standard_deviation(result, n, t):
    return sum(result[t]) / n


def create_table(result_0, result_1,
                 coefficients_before_y,
                 boundary_conditions,
                 y_boundary_conditions,
                 init_condition,
                 n, tau,
                 calculation_count=1,
                 flag=1):
    table = PrettyTable(["i", "N", "tau", "s_t1 (t1 = 5)", "s_t2 (t2 = 20) ", "maximum deviation"])
    a, b = boundary_conditions
    results = [result_0, result_1]

    if flag == 1:
        results = find_explicit_solutions(results, coefficients_before_y, boundary_conditions, y_boundary_conditions,
                                          init_condition, n * 2, tau / 4)
    if flag == 2:
        results = find_implicit_solutions(results, coefficients_before_y, boundary_conditions, y_boundary_conditions,
                                          init_condition, n * 2, tau / 4)

    for i in range(len(results) - 1):
        h = (b - a) / n
        tau = (h ** 2 / 2)
        t1 = 5
        s_t1 = format(find_standard_deviation(results[i], n, t1), '.8f')
        t2 = 20
        s_t2 = format(find_standard_deviation(results[i], n, t2), '.8f')
        max_t1 = calculate_maximum_deviation(results[i], results[i + 1])
        tau_str = format(tau, '.8f')
        table.add_row([i, n, tau_str, s_t1, s_t2, max_t1])
        n *= 2
        tau /= 4

    print(table)
    print()
