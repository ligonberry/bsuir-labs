import numpy as np
import sympy


def _TDMA(c, b, a, f):
    n = len(b)
    y = [b[0]]
    P = [-c[0] / b[0]]
    Q = [f[0] / b[0]]

    for i in range(1, n - 1):
        y.append(a[i - 1] * P[i - 1] + b[i])
        P.append(-c[i] / y[i])
        Q.append((f[i] - a[i - 1] * Q[i - 1]) / y[i])

    y.append(a[-1] * P[-1] + b[-1])
    Q.append((f[-1] - a[-1] * Q[-1]) / y[-1])
    x = [0] * n
    x[-1] = Q[-1]
    for i in range(n - 1, 0, -1):
        x[i - 1] = P[i - 1] * x[i] + Q[i - 1]
    return x


def TDMAsolver(lin_system):
    c, b, a, d = [], [], [], []
    cs = sympy.Poly(lin_system[0]).coeffs()
    if len(cs) < 3:
        cs.append(0)
    d.append(-cs[2])
    c.append(cs[1])
    b.append(cs[0])

    for i in range(1, len(lin_system) - 1):
        cs = sympy.Poly(lin_system[i]).coeffs()
        d.append(-cs[3])
        c.append(cs[2])
        b.append(cs[1])
        a.append(cs[0])

    cs = sympy.Poly(lin_system[-1]).coeffs()
    if len(cs) < 3:
        cs.append(0)
    d.append(-cs[2])
    b.append(cs[1])
    a.append(cs[0])
    return _TDMA(c, b, a, d)


def main():
    A = np.array([[10, 2, 0, 0], [3, 10, 4, 0], [0, 1, 7, 5], [0, 0, 3, 4]], dtype=float)

    a = np.array([3., 1, 3])
    b = np.array([10., 10., 7., 4.])
    c = np.array([2., 4., 5.])
    d = np.array([3, 4, 5, 6.])

    print(_TDMA(c, b, a, d))

    print(np.linalg.solve(A, d))

    assert all(_TDMA(c, b, a, d)) == all(np.linalg.solve(A, d))


if __name__ == '__main__':
    main()