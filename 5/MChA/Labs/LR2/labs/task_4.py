""" Лабораторная работа 2.
Задание:
Методом конечных разностей найти приближенное решение краевой задачи
с тремя верными значащими цифрами. Решение системы разностных уравнений
найти используя метод прогонки.
Использовать разностную схему второго порядка точности.

Исходные данные:
a = 0, b = 1.7, c = 0.925,
k(x) (при a<x<c) = 1.8
k(x) (при c<x<b) = 0.4
q(x) (при a<x<c) = 7.0
q(x) (при c<x<b) = 12
f(x) = 8*x / (2 + x**3)
"""
from collections import namedtuple

import matplotlib.pyplot as plt
import numpy
import sympy

from tdma import TDMAsolver
from helper import check_convergence_points, replace_x, set_boundary_values

x = sympy.Symbol('x')


def create_boundary_equation(y, coefficients_before_y, h):
    yk_p1, yk, yk_m1 = y
    f_x = 8*x / (2 + x**3)
    k_x, q_x = coefficients_before_y
    func = - k_x * (yk_p1 - 2 * yk + yk_m1) / h ** 2
    func += (q_x * yk)
    func -= f_x
    return func


def left_border_equation(y, h):
    k = 1.8
    y0, y1, y2 = y
    func = -k * (-y2 + 4 * y1 - 3 * y0) / (2 * h) + 0.5 * y0
    return func


def right_border_equation(y, h):
    k = 0.4
    yn, yn_m1, yn_m2 = y
    func = k * (3 * yn - 4 * yn_m1 + yn_m2) / (2 * h) + 0.5 * yn
    return func


def create_system_of_equations(bounds, start_y_number, coefficients_before_y, coefficients, h):
    bound_left, bound_medium, bound_right = bounds
    k_x, q_x = coefficients_before_y
    system = [0]
    for i in range(1, start_y_number - 1):
        if bound_left + i * h < bound_right:
            k_, q_ = k_x[0], q_x[0]
        else:
            k_, q_ = k_x[1], q_x[1]

        y = coefficients[i-1], coefficients[i], coefficients[i+1]
        coefficients_before_y_ = k_, q_
        system.append(create_boundary_equation(y, coefficients_before_y_, h).evalf())
    return system


def add_boundary_values(system, coefficients, h):
    y_1 = coefficients[0], coefficients[1], coefficients[2]
    system[0] = left_border_equation(y_1, h)
    y_2 = coefficients[-1], coefficients[-2], coefficients[-3]
    system.append(right_border_equation(y_2, h))
    return system


def pretty_printer(solutions):
    n = len(solutions)
    for i, y in zip(range(n), solutions):
        print(f'y({i}) = {y}')


def method_of_difference_approximations(bounds, coefficients_before_y, n=40, eps=0.02):
    iteration_count = 0
    bound_left, bound_medium, bound_right = bounds
    old_solution = []

    while True:
        n += 2
        coefficients = list(sympy.var(f'y:{n}'))

        h = (bound_medium - bound_left) / n

        x_points = numpy.linspace(bound_left + h, bound_medium - h, n - 2).tolist()

        system = create_system_of_equations(bounds, n, coefficients_before_y, coefficients, h)
        system = replace_x(system, n, x_points)
        system = add_boundary_values(system, coefficients, h)

        solution = TDMAsolver(system)

        if iteration_count != 0:
            solution_0, solution_m1 = solution[0], solution[-1]
            old_solution_0, old_solution_m1 = old_solution[0], old_solution[-1]
            bounds_values = solution_0, solution_m1, old_solution_0, old_solution_m1
            del solution[0], solution[-1], old_solution[0], old_solution[-1]

        if iteration_count != 0 and check_convergence_points(old_solution, solution, eps):
            old_solution, solution = set_boundary_values(old_solution, solution, bounds_values)
            break

        if iteration_count != 0:
            old_solution, solution = set_boundary_values(old_solution, solution, bounds_values)

        iteration_count += 1
        old_solution = solution

        n -= 2
        n *= 2

    x_points.insert(0, bound_left)
    x_points.append(bound_medium)

    data_type = namedtuple('data',
                           ('points', 'solution'))

    return data_type(x_points, solution)


def main():
    eps = 0.05

    n = 40

    left_lim = 0  # a
    medium_lim = 1.7  # c
    right_lim = 0.925  # b
    limits = (left_lim, medium_lim, right_lim)

    k_x = [1.8, 0.4]
    q_x = [7.0, 12]
    coefficients_before_y = k_x, q_x

    data = method_of_difference_approximations(limits, coefficients_before_y, n, eps)
    x = data.points
    y = data.solution

    pretty_printer(y)

    plt.plot(x, y, color='orange')
    plt.grid()
    plt.show()


if __name__ == '__main__':
    main()
