import sympy
from sympy.solvers.solveset import linsolve

from collections import namedtuple
from numpy import linspace
import matplotlib.pyplot as plt

from tdma import TDMAsolver

x = sympy.Symbol('x')

# check for current precision. Note that func_in_points has to be sympy object
def check_convergence_points(prev_points, current_points, eps, h_rate=2):
    print(len(prev_points))
    for i in range(len(prev_points)):
        if abs(current_points[h_rate * i] - prev_points[i]) > eps:
            # uncomment to watch the real difference
            print(current_points[h_rate * i], prev_points[i], current_points[h_rate * i] - prev_points[i])
            print(abs(current_points[h_rate * i] - prev_points[i]))
            return False

    return True


def func_for_partition_3(yk_m1, yk, yk_p1, h):
    func = (yk_p1 - 2 * yk + yk_m1) / h ** 2
    func -= (yk * x)
    func -= (2 * x)
    return func


def b_equation_for_partition_3(yn, yn_m1, yn_m2, h):
    func = 3 * yn - 4 * yn_m1 + yn_m2 - 6 * h
    return func


# this is the case when we don't have proper value for limit points
def a_equation_for_partition_3(y0, y1, y2, h):
    func = y0 * (3 + h) - 4 * y1 + y2 - 4.5 * h
    return func



def differences_method_modifyed(start_variables_count,
                                a,
                                b,
                                func_for_partition,
                                first_eq,
                                last_eq,
                                eps=0.01,
                                verbose=True):
    if verbose:
        print('CALCULATIONS [BEGIN:]')

    iteration_count = 0

    prev_answer = []
    old_points = []
    while True:
        start_variables_count += 2
        symbols = [sympy.Symbol('y' + str(i)) for i in
                   range(start_variables_count)]
        # define our step
        h = (b - a) / start_variables_count
        # generate x points:
        points = linspace(a + h, b - h, start_variables_count - 2).tolist()
        # build system of equations
        lin_system = [0]
        for i in range(1, start_variables_count - 1):
            lin_system.append(func_for_partition(symbols[i - 1],
                                                 symbols[i],
                                                 symbols[i + 1],
                                                 h).evalf())

        for i in range(1, start_variables_count - 1):
            lin_system[i] = lin_system[i].subs(x, points[i - 1])

        lin_system[0] = first_eq(symbols[0], symbols[1], symbols[2], h)
        lin_system.append(last_eq(symbols[-1], symbols[-2], symbols[-3], h))

        # solving our system and converting FiniteSet to simple list
        answer = TDMAsolver(lin_system)

        # showing the verbose difference between steps
        if iteration_count != 0 and verbose:
            print('ITERATION [%d]' % iteration_count)

            points.insert(0, a)
            points.append(b)
            old_points.insert(0, a)
            old_points.append(b)

            D, y = points, answer
            plt.figure()
            plt.plot(D, y, color='red', label='current')

            D, y = old_points, prev_answer
            plt.plot(D, y, label='prev')

            # this is the ticks for better visualisation
            plt.yticks(linspace(float(min(answer) - abs(min(answer) / 3)),
                                float(max(answer) + abs(max(answer))), 11))
            plt.title('Iteration_difference: ')
            plt.grid(True)
            plt.show()

            del points[0], points[-1], old_points[0], old_points[-1]

        if iteration_count != 0:
            a_a, a_b, p_a, p_b = answer[0], answer[-1], prev_answer[0], \
                                 prev_answer[-1]
            del answer[0], answer[-1], prev_answer[0], prev_answer[-1]
        # if there is no function to be compated with or we are already get our
        # precision we will break our loop
        if iteration_count != 0 and check_convergence_points(prev_answer,
                                                             answer,
                                                             eps):
            answer.insert(0, a_a)
            prev_answer.insert(0, p_a)
            answer.append(a_b)
            prev_answer.append(p_b)
            break

        if iteration_count != 0:
            answer.insert(0, a_a)
            prev_answer.insert(0, p_a)
            answer.append(a_b)
            prev_answer.append(p_b)

        iteration_count += 1
        prev_answer = answer
        old_points = points

        # this is done for border points
        start_variables_count -= 2
        start_variables_count *= 2

    points.insert(0, a)
    points.append(b)

    data_type = namedtuple('data',
                           ('points', 'answer', 'step', 'iterations_count'))

    if verbose:
        print('CALCULATIONS [END:]')

    return data_type(points, answer, (b - a) / start_variables_count,
                     iteration_count)


eps = 0.03

a = 1.5
b = 3.5
A = 0
B = 4
VARIABLES = 40

data = differences_method_modifyed(VARIABLES, a, b, func_for_partition_3,
                                   a_equation_for_partition_3,
                                   b_equation_for_partition_3,
                                   eps)

D, y = data.points, data.answer


plt.figure()
plt.plot(D, y, color='red')
plt.title('differences method task_3: ')
plt.grid(True)
plt.show()
print(y)
print(D)