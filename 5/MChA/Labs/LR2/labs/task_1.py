""" Лабораторная работа 2
y(-1) = y(1) = 0

Задание:
Составить разностную схему и решить уравнение вида: ay" + (1 + bx^2)*y = -1
с точностью 0.001, где a = sin(k), b = cos(k)

Тестовый пример: y" + (1 + x^2)*y = -1
Ответ: y(-1) = 0, y(-0.5) = 0.721, y(0) = 0.967, y(0.5) = 0.721, y(1) = 0
"""

import sympy
from math import sin, cos, pi
from labs import length
from helper import (get_break_points, get_length_of_interval,
                    pretty_printer, get_number_of_intervals)


def find_integration_step(initial_integration_step, limits, bounds, arg_trig_sin, eps):
    initial_n = get_number_of_intervals(limits, initial_integration_step) + 1
    solutions_1 = tuple(method_of_difference_approximations(initial_n, limits, bounds, arg_trig_sin).values())
    len_1 = length(solutions_1)
    h = initial_integration_step / 2
    n = get_number_of_intervals(limits, h)
    solutions = method_of_difference_approximations(n, limits, bounds, arg_trig_sin)
    solutions_2 = tuple(solutions.values())
    len_2 = length(solutions_2)
    for i, j in zip(range(len_1), range(0, len_2, 2)):
        if abs(solutions_1[i]) - abs(solutions_2[j + 1]) > eps:
            find_integration_step(initial_integration_step, limits, bounds, arg_trig_sin, eps)
    return solutions, n


def method_of_difference_approximations(n, limits, bounds, arg_trig_sin, arg_trig_cos=None, break_points=None):
    """ Returns the required coefficients of the approximate solution. """
    left_lim, right_lim = limits

    if arg_trig_cos is None:
        arg_trig_cos = arg_trig_sin

    a = sin(arg_trig_sin)
    b = cos(arg_trig_cos)

    coefficients = list(sympy.var(f'y:{n}'))

    bound_left, bound_right = bounds
    coefficients[0] = bound_left  # boundary condition y(-1) = 0
    coefficients[n-1] = bound_right  # boundary condition y(1) = 0

    if not break_points:
        break_points = get_break_points(right_lim, left_lim, n)

    h = get_length_of_interval(right_lim, left_lim, n)

    system = []
    for i in range(1, n-1):
        equation = (a * (coefficients[i + 1] - 2 * coefficients[i] + coefficients[i - 1]) /
                    h ** 2 + (1 + b * break_points[i] ** 2) * (coefficients[i]) + 1)
        system.append(equation)

    return sympy.solve(system, coefficients)


def main():
    left_lim = -1
    right_lim = 1
    limits = (left_lim, right_lim)

    bound_left = bound_right = 0
    bounds = (bound_left, bound_right)

    eps = 0.01
    initial_integration_step = 0.02

    arg_trig_sin = int(input('Enter the argument of the trigonometric function: '))

    solutions, n = find_integration_step(initial_integration_step, limits, bounds, arg_trig_sin, eps)

    pretty_printer(solutions, n)


def test_method_of_difference_approximations():
    left_lim = -1
    right_lim = 1
    limits = (left_lim, right_lim)

    arg_trig_sin = pi / 2
    arg_trig_cos = 0

    bound_left = bound_right = 0
    bounds = (bound_left, bound_right)

    break_points = [-1, -0.5, 0, 0.5, 1]

    h = 0.5
    n = int((right_lim - left_lim) / h) + 1

    solutions = method_of_difference_approximations(n, limits, bounds, arg_trig_sin, arg_trig_cos, break_points)

    pretty_printer(solutions, n)


if __name__ == '__main__':
    # main()
    test_method_of_difference_approximations()
