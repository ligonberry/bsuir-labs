import sympy
from sympy.solvers.solveset import linsolve

from collections import namedtuple
from numpy import linspace
import matplotlib.pyplot as plt

from tdma import TDMAsolver

x = sympy.Symbol('x')


# check for current precision. Note that func_in_points has to be sympy object
def check_convergence_points(prev_points, current_points, eps, h_rate=2):
    print(len(prev_points))
    for i in range(len(prev_points)):
        if abs(current_points[h_rate * i] - prev_points[i]) > eps:
            # uncomment to watch the real difference
            print(current_points[h_rate * i], prev_points[i], current_points[h_rate * i] - prev_points[i])
            print(abs(current_points[h_rate * i] - prev_points[i]))
            return False

    return True





def func_for_partition_4(yk_m1, yk, yk_p1, h, k, q):
    func = - k * (yk_p1 - 2 * yk + yk_m1) / h ** 2
    func += (q * yk)
    func -= (7 * (x + 1 / (x + 0.5)))
    return func


def b_equation_for_partition_4(yn, yn_m1, yn_m2, h):
    func = 0.4 * (3 * yn - 4 * yn_m1 + yn_m2) / (2 * h) + 0.5 * yn;
    return func


def a_equation_for_partition_4(y0, y1, y2, h):
    func = -1.2 * (-y2 + 4 * y1 - 3 * y0) / (2 * h) + 0.5 * y0;
    return func


def differences_method_modifyed_points(start_variables_count,
                                       a,
                                       b,
                                       c,
                                       k,
                                       q,
                                       func_for_partition,
                                       first_eq,
                                       last_eq,
                                       eps=0.1,
                                       verbose=True):
    if verbose:
        print('CALCULATIONS [BEGIN:]')

    iteration_count = 0

    prev_answer = []
    old_points = []
    while True:
        # generate variables for linsolve
        start_variables_count += 2
        symbols = [sympy.Symbol('y' + str(i)) for i in
                   range(start_variables_count)]

        # define our step
        h = (b - a) / start_variables_count

        # generate x points:
        points = linspace(a + h, b - h, start_variables_count - 2).tolist()

        # build system of equations
        lin_system = [0]
        for i in range(1, start_variables_count - 1):
            #print(a + i * h)
            if a + i * h < c:
                k_, q_ = k[0], q[0]
            else:
                k_, q_ = k[1], q[1]
            lin_system.append(func_for_partition(symbols[i - 1],
                                                 symbols[i],
                                                 symbols[i + 1],
                                                 h, k_, q_).evalf())

        for i in range(1, start_variables_count - 1):
            lin_system[i] = lin_system[i].subs(x, points[i - 1])

        lin_system[0] = first_eq(symbols[0], symbols[1], symbols[2], h)
        lin_system.append(last_eq(symbols[-1], symbols[-2], symbols[-3], h))

        # solving our system and converting FiniteSet to simple list
        answer = TDMAsolver(lin_system)
        # answer = list(list(linsolve(lin_system, *symbols))[0])

        # showing the verbose difference between steps
        if iteration_count != 0 and verbose:
            print('ITERATION [%d]' % iteration_count)

            points.insert(0, a)
            points.append(b)
            old_points.insert(0, a)
            old_points.append(b)

            D, y = points, answer
            plt.figure()
            plt.plot(D, y, color='red', label='current')

            D, y = old_points, prev_answer
            plt.plot(D, y, label='prev')

            # this is the ticks for better visualisation
            plt.yticks(linspace(float(min(answer) - abs(min(answer) / 3)),
                                float(max(answer) + abs(max(answer))), 11))
            plt.title('Iteration_difference: ')
            plt.grid(True)
            plt.show()

            del points[0], points[-1], old_points[0], old_points[-1]

        if iteration_count != 0:
            a_a, a_b, p_a, p_b = answer[0], answer[-1], prev_answer[0], \
                                 prev_answer[-1]
            del answer[0], answer[-1], prev_answer[0], prev_answer[-1]
        # if there is no function to be compated with or we are already get our
        # precision we will break our loop
        if iteration_count != 0 and check_convergence_points(prev_answer,
                                                             answer,
                                                             eps):
            answer.insert(0, a_a)
            prev_answer.insert(0, p_a)
            answer.append(a_b)
            prev_answer.append(p_b)
            break

        if iteration_count != 0:
            answer.insert(0, a_a)
            prev_answer.insert(0, p_a)
            answer.append(a_b)
            prev_answer.append(p_b)

        iteration_count += 1
        prev_answer = answer
        old_points = points

        # this is done for border points
        start_variables_count -= 2
        start_variables_count *= 2

    points.insert(0, a)
    points.append(b)

    data_type = namedtuple('data',
                           ('points', 'answer', 'step', 'iterations_count'))

    if verbose:
        print('CALCULATIONS [END:]')

    return data_type(points, answer, (b - a) / start_variables_count,
                     iteration_count)

VARIANT = 24
VARIABLES = 40
COUNT = 10

eps = 0.05
a = 0
b = 1.5
c = 0.925
k = [1.2, 0.4]
q = [8.3, 12]
A = 0
B = 4


data = differences_method_modifyed_points(VARIABLES, a, b, c, k, q,
                                          func_for_partition_4,
                                          a_equation_for_partition_4,
                                          b_equation_for_partition_4,
                                          eps)

D, y = data.points, data.answer
print(D)
func_y = []

plt.plot(D, y, color='red')
# plt.plot(D, func_y, color='blue')
#plt.yticks(linspace(-0.5, 0.5, 7))
plt.title('differences method task_4: ')
plt.grid()
plt.show()
