""" Лабораторная работа 2.
Задание:
Методом конечных разностей найти приближенное решение указанной в индивидуальном варианте
краевой задачи с точностью eps и посторить его график.
Решение системы разностных уравнений найти, используя метод прогонки.

Исходные данные:
u" + u' + 2*x*u = x^2 + 1
u(0.3) + 0.5*u'(0.3) = 3
u(2.7) = 1

eps = 0.04

Тестовый пример: y" + (1 + x^2)*y = -1
y(-1) = y(1) = 0
Ответ: y(-2) = 0, y(-1) = 0.721, y(0) = 0.967, y(1) = 0.721, y(2) = 0
"""

from collections import namedtuple

import matplotlib.pyplot as plt
import numpy
import sympy

from helper import check_convergence_points, replace_x, set_boundary_values
from tdma import TDMAsolver

x = sympy.Symbol('x')


def left_border_equation(y0, y1, y2, h):
    """ Simplify equation (y_0)' = (-y_2 + 4*y_1 - 3*y_0) / 2*h (1)
    u(0.3) + 0.5*u'(0.3) = 3 => u(0.3) = y_0
    y_0 + 0.5*(y_0)' = 3 =>
    (y_0)' = 2*(3 - y_0) =>
    Insert into (1)
    """
    func = y0 * (3 - 4 * h) - 4 * y1 + y2 + 12 * h
    return func


def test_left_border_equation(y1, y2):
    func = y2 - 4*y1
    return func


def right_border_equation(yn, yn_m1, yn_m2, h):
    """ Simplify equation (yn)' = (3yn - 4*yn_m1 + yn_m2) / 2*h (1)
    u(2.7) = 1 => yn = 1
    (yn)' = 0
    Insert into (1)
    """
    func = 3 * yn - 4 * yn_m1 + yn_m2
    return func


def test_right_border_equation(yn_m1, yn_m2):
    func = yn_m2 - 4*yn_m1
    return func


def create_system_of_equations(start_y_number, coefficients_before_y, coefficients, h):
    system = [0]
    for i in range(1, start_y_number - 1):
        y = coefficients[i + 1], coefficients[i], coefficients[i - 1]
        system.append(create_boundary_equation(y, coefficients_before_y, h).evalf())
    return system


def create_boundary_equation(y, coefficients_before_y, h):
    yk_p1, yk, yk_m1 = y
    p_x, q_x, f_x = coefficients_before_y
    func = (yk_p1 - 2 * yk + yk_m1) / h ** 2
    func += p_x(x) * (yk_p1 - yk) / h
    func += q_x(x) * yk
    func -= f_x(x)
    return func


def draw_exact_solution():
    y = lambda x: 0.0218667 * x ** 4 - 0.989067 * x ** 2 + 0.9672
    x_values = numpy.arange(-1.0, 1.1, 0.1)
    y_values = [y(x) for x in x_values]
    plt.plot(x_values, y_values, color='g')


def test_draw_plot(x, y):
    plt.plot(x, y, color='orange')
    draw_exact_solution()
    plt.grid()
    plt.show()


def method_of_difference_approximations(limits, coefficients_before_y, n=40, eps=0.1, test=False):
    iteration_count = 0
    left_lim, right_lim = limits

    old_solution = []

    while True:
        n += 2
        coefficients = list(sympy.var(f'y:{n}'))

        h = (right_lim - left_lim) / n

        x_points = numpy.linspace(left_lim + h, right_lim - h, n - 2).tolist()

        system = create_system_of_equations(n, coefficients_before_y, coefficients, h)
        system = replace_x(system, n, x_points)

        if test:
            system[0] = test_left_border_equation(coefficients[1], coefficients[2])
            system.append(test_right_border_equation(coefficients[-2], coefficients[-3]))
        else:
            system[0] = left_border_equation(coefficients[0], coefficients[1], coefficients[2], h)
            system.append(right_border_equation(coefficients[-1], coefficients[-2], coefficients[-3], h))

        solution = TDMAsolver(system)

        if iteration_count != 0:
            solution_0, solution_m1 = solution[0], solution[-1]
            old_solution_0, old_solution_m1 = old_solution[0], old_solution[-1]
            bounds_values = solution_0, solution_m1, old_solution_0, old_solution_m1
            del solution[0], solution[-1], old_solution[0], old_solution[-1]

        if iteration_count != 0 and check_convergence_points(old_solution, solution, eps):
            old_solution, solution = set_boundary_values(old_solution, solution, bounds_values)
            break

        if iteration_count != 0:
            old_solution, solution = set_boundary_values(old_solution, solution, bounds_values)

        iteration_count += 1
        old_solution = solution

        n -= 2
        n *= 2

    x_points.insert(0, left_lim)
    x_points.append(right_lim)

    data_type = namedtuple('data',
                           ('x_points', 'solution'))

    return data_type(x_points, solution)


def main():
    p_x = lambda x: 1
    q_x = lambda x: 2 * x
    f_x = lambda x: x ** 2 + 1

    coefficients_before_y = (p_x, q_x, f_x)

    eps = 0.04

    start_y_number = 40

    left_lim = 0.3  # a
    right_lim = 2.7  # b
    limits = (left_lim, right_lim)

    data = method_of_difference_approximations(limits, coefficients_before_y, start_y_number, eps)
    x = data.x_points
    y = data.solution

    plt.plot(x, y, color='orange')
    plt.grid()
    plt.show()


def test_method_of_difference_approximations():
    left_lim = -1  # a
    right_lim = 1  # b
    limits = (left_lim, right_lim)

    p_x = lambda x: 0
    q_x = lambda x: (1 + x ** 2)
    f_x = lambda x: -1
    coefficients_before_y = (p_x, q_x, f_x)

    data = method_of_difference_approximations(limits, coefficients_before_y, test=True)
    x = data.x_points
    y = data.solution

    print(x)
    print(y)
    test_draw_plot(x, y)


if __name__ == '__main__':
    main()
    # test_method_of_difference_approximations()
