""" Лабораторная работа 2. Вариант 2.1.11
Задание:
Найти приближенное решение краевой задачи методом конечных разностей.
u" + p(x)*u' + q(x)*u = f(x), a < x < b
u(a) = UA, u(b) = UB
с заданной точностью eps и построить его график.

Исходные данные:
p(x) = 0.25*(1 - x^2)
q(x) = 5*(1 + (cos(x))^2)
f(x) = 15*cos(x)
a = 0, b = 2
UA = 0, UB = 4
eps = 0.04

Тестовый пример: y" + (1 + x^2)*y = -1
y(-1) = y(1) = 0
Ответ: y(-2) = 0, y(-1) = 0.721, y(0) = 0.967, y(1) = 0.721, y(2) = 0
"""

import sympy
import matplotlib.pyplot as plt
from math import cos
import numpy as np
from labs import length
from helper import (get_break_points, get_length_of_interval,
                    get_number_of_intervals, pretty_printer)


def find_integration_step(initial_integration_step, limits, bounds, coefficients_before_y, eps):
    initial_n = get_number_of_intervals(limits, initial_integration_step) + 1
    solutions_1 = tuple(method_of_difference_approximations(initial_n, limits,
                                                            bounds, coefficients_before_y).values())
    len_1 = length(solutions_1)
    h = initial_integration_step / 2
    n = get_number_of_intervals(limits, h)
    solutions = method_of_difference_approximations(n, limits,
                                                    bounds, coefficients_before_y)
    solutions_2 = tuple(solutions.values())
    len_2 = length(solutions_2)
    for i, j in zip(range(len_1), range(0, len_2, 2)):
        if abs((solutions_1[i]) - solutions_2[j + 1]) > eps:
            find_integration_step(h, limits, bounds, coefficients_before_y, eps)
    return solutions, h


def method_of_difference_approximations(n, limits, bounds, coefficients_before_y, break_points=None):
    """" Returns the required coefficients of the approximate solution. """
    p_x, q_x, f_x = coefficients_before_y
    left_lim, right_lim = limits

    coefficients = list(sympy.var(f'y:{n}'))

    bound_left, bound_right = bounds
    coefficients[0] = bound_left  # boundary condition y(-1) = a = 0
    coefficients[n - 1] = bound_right  # boundary condition y(1) = b = 2

    if not break_points:
        break_points = get_break_points(right_lim, left_lim, n)

    h = get_length_of_interval(right_lim, left_lim, n)

    system = []
    for i in range(1, n - 1):
        equation = ((coefficients[i + 1] - 2 * coefficients[i] + coefficients[i - 1]) / h ** 2 +
                    p_x(break_points[i]) * (coefficients[i + 1] - coefficients[i]) / h +
                    q_x(break_points[i]) * (coefficients[i]) -
                    f_x(break_points[i]))
        system.append(equation)

    return sympy.solve(system, coefficients)


def draw_exact_solution():
    y = lambda x: 0.0218667 * x ** 4 - 0.989067 * x ** 2 + 0.9672
    x_values = np.arange(-1.0, 1.1, 0.1)
    y_values = [y(x) for x in x_values]
    plt.plot(x_values, y_values, color='g')


def draw_approximate_solution(solutions, right_lim, left_lim, n):
    x = get_break_points(right_lim, left_lim, n)
    y = [0] * (n + 1)
    y[1:n] = list(solutions.values())
    plt.plot(x, y, color='orange')


def test_draw_plot(solutions, right_lim, left_lim, n):
    draw_exact_solution()
    draw_approximate_solution(solutions, right_lim, left_lim, n)
    plt.grid()
    plt.show()


def draw_plot(solutions, right_lim, left_lim, n):
    draw_approximate_solution(solutions, right_lim, left_lim, n)
    plt.grid()
    plt.show()


def main():
    left_lim = 0  # a
    right_lim = 2  # b
    limits = (left_lim, right_lim)

    bound_left = 0  # UA
    bound_right = 4  # UB
    bounds = (bound_left, bound_right)

    p_x = lambda x: 0.25 * (1 - x ** 2)
    q_x = lambda x: 5 * (1 + (cos(x)) ** 2)
    f_x = lambda x: 15 * cos(x)
    coefficients_before_y = (p_x, q_x, f_x)

    eps = 0.02

    initial_integration_step = 0.025

    solutions, h = find_integration_step(initial_integration_step, limits, bounds, coefficients_before_y, eps)
    print("Integration step ", h)

    n = get_number_of_intervals(limits, h)

    pretty_printer(solutions, n)
    draw_plot(solutions, right_lim, left_lim, n)


def test_method_of_difference_approximations():
    left_lim = -1  # a
    right_lim = 1  # b
    limits = (left_lim, right_lim)

    bound_left = bound_right = 0  # UA, UB
    bounds = (bound_left, bound_right)

    p_x = lambda x: 0
    q_x = lambda x: (1 + x ** 2)
    f_x = lambda x: -1
    coefficients_before_y = (p_x, q_x, f_x)

    break_points = [-1, -0.5, 0, 0.5, 1]

    h = 0.5
    print("Integration step ", h)
    n = int((right_lim - left_lim) / h) + 1

    solutions = method_of_difference_approximations(n, limits, bounds, coefficients_before_y, break_points)
    pretty_printer(solutions, n)
    test_draw_plot(solutions, right_lim, left_lim, n)


if __name__ == '__main__':
    main()
    # test_method_of_difference_approximations()
