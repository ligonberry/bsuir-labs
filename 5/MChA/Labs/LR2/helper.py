import sympy
x = sympy.Symbol('x')


def get_length_of_interval(right_lim, left_lim, n):
    """ The length of each of the n parts of the original interval. """
    return (right_lim - left_lim) / (n - 1)


def get_number_of_intervals(limits, h):
    left_lim, right_lim = limits
    return int((right_lim - left_lim) / h)


def get_break_points(right_lim, left_lim, n):
    h = get_length_of_interval(right_lim, left_lim, n)
    break_points = []
    for i in range(n):
        break_points.append(left_lim + h * i)
    return break_points


def pretty_printer(solutions, n):
    print("The solution of this system will be")
    print(f"y{n-1} = 0")
    for key, value in solutions.items():
        print(f'{key} = {value}')
    print("y0 = 0")


def check_convergence_points(old_solution, solution, eps_, h_rate=2):
    """Checks if all modules of the solution difference are less,
    than the specified error. """
    len_old = len(old_solution)
    for i in range(len_old):
        if abs(solution[h_rate * i] - old_solution[i]) > eps_:
            print("Error at this step",
                  abs(old_solution[h_rate * i] - solution[i]))
            return False
    return True


def generate_basic_system(n):
    """Generates a system of basis functions for solving our equation. """
    sequence = []
    sequence.extend([(x ** i) * (1 - x ** 2) for i in range(n)])
    return sequence


def build_function_from_basis(basis):
    result = 0
    n = len(basis)
    coefficients = list(sympy.var(f'a:{n}'))
    for a_i, i in zip(coefficients, range(n)):
        result += a_i * basis[i]
    return result


def replace_x(system, start_y_number, x_points):
    """ Replace x with a numerical value. """
    for i in range(1, start_y_number - 1):
        system[i] = system[i].subs(x, x_points[i - 1])
    return system


def set_boundary_values(old_solution, solution, bounds_values):
    solution_0, solution_m1, old_solution_0, old_solution_m1 = bounds_values
    solution.insert(0, solution_0)
    old_solution.insert(0, old_solution_0)
    solution.append(solution_m1)
    old_solution.append(old_solution_m1)
    return old_solution, solution