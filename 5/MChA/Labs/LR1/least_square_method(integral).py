""" Лабораторная работа 1
y(-1) = y(1) = 0

Задание:
Решить методом коллокаций уравнение вида: ay" + (1 + bx^2)*y = -1,
где a = sin(k), b = cos(k)

Тестовый пример: y" + (1 + x^2)*y = -1
Ответ: y(x) = 0.985*(1 - x^2) - 0.078*(x^2 - x^4)

"""

import math
import sympy
from sympy.abc import x

import helper


def integral_lsm(coefficients, number_of_basis, residual, left_boundary_value, right_boundary_value):
    """ Return values of coefficients calculated by the integral lsm method. """
    system = []
    for ind in range(1, number_of_basis):
        system.append(2. * (residual * residual.diff(coefficients[ind]))
                      .integrate((x, left_boundary_value, right_boundary_value)))
    return sympy.solve(system, coefficients[1:])


def main():

    left_boundary_value = -1
    right_boundary_value = 1

    number_of_basis = int(input('Enter the number of function bases, i: ')) + 1
    arg_trig = int(input('Enter the argument of the trigonometric function, k: '))

    coefficients = helper.get_coefficients(number_of_basis)
    points = helper.get_points(number_of_basis)

    residual = helper.get_discrepancy(coefficients, number_of_basis, arg_trig)
    solution = integral_lsm(coefficients, number_of_basis, residual, left_boundary_value, right_boundary_value)

    helper.pretty_printer(solution, number_of_basis, points)


def test_integral_lsm():
    left_boundary_value = -1
    right_boundary_value = 1

    number_of_basis = 3
    arg_trig_sin = math.pi/2
    arg_trig_cos = 0

    coefficients = helper.get_coefficients(number_of_basis)
    points = [-0.5, 0, 0.5]

    residual = helper.get_discrepancy(coefficients, number_of_basis, arg_trig_sin, arg_trig_cos)
    solution = integral_lsm(coefficients, number_of_basis, residual, left_boundary_value, right_boundary_value)

    helper.pretty_printer(solution, number_of_basis, points)


if __name__ == '__main__':
    # main()
    test_integral_lsm()
