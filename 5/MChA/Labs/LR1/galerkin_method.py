""" Лабораторная работа 1
Задание:
Решить методом коллокаций уравнение вида: ay" + (1 + bx^2)*y = -1,
где a = sin(k), b = cos(k)
y(-1) = y(1) = 0

"""

import sympy
from sympy.abc import x
import helper


def galerkin_method(residual, coefficients, i, left_lim, right_lim):
    system = []
    for ind in range(1, i):
        system.append((residual * x ** ind * (1 - x)).integrate((x, left_lim, right_lim)))
    return sympy.solve(system, coefficients[1:])


def main():
    left_boundary_value = -1
    right_boundary_value = 1

    number_of_basis = int(input('Enter the number of function bases, i: ')) + 1
    arg_trig = int(input('Enter the argument of the trigonometric function, k: '))

    coefficients = helper.get_coefficients(number_of_basis)
    points = helper.get_points(number_of_basis)

    residual = helper.get_discrepancy(coefficients, number_of_basis, arg_trig)  # Calculating discrepancy.
    solution = galerkin_method(residual, coefficients, number_of_basis, left_boundary_value, right_boundary_value)

    helper.pretty_printer(solution, number_of_basis, points)


if __name__ == '__main__':
    main()
