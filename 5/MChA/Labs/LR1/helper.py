import sympy
from math import sin, cos
from sympy.abc import x


def get_discrepancy(coefficients, number_of_basis, arg_trig_sin, arg_trig_cos=None):
    if arg_trig_cos is None:
        arg_trig_cos = arg_trig_sin
    a = sin(arg_trig_sin)
    b = cos(arg_trig_cos)
    return _get_discrepancy(coefficients, number_of_basis, a, b)


def _get_discrepancy(coefficients, number_of_basis, a, b):
    """ Return function R(x) (discrepancy), where R(x) = L(x) - f(x). """
    y = 0
    for index in range(1, number_of_basis):
        if index == 1:
            y += coefficients[index] * index * (1 - x ** 2)
        else:
            y += coefficients[index] * x ** index * (1 - x ** 2)
    q = (1 + b * x ** 2) / a
    answer_digit = 1 / a
    y_diff = y.diff(x).diff(x)
    return (y_diff + y * q + answer_digit).expand()


def get_coefficients(number_of_basis):
    """ Return a string in the format 'a0, a1, a2, a3, ....' for further parsing. """
    return sympy.var(f'a:{number_of_basis}')


def get_points(number_of_basis):
    """ Return collocation points. """
    left_boundary_value = -1
    points = []
    average_point = 2 / number_of_basis + 0.01
    current_point = 1 - average_point
    while current_point > left_boundary_value:  # Calculating collocation points.
        points.append(current_point)
        current_point -= average_point
    return points


def pretty_printer(solution, number_of_basis, points, test=None):
    print(f"{'-'*number_of_basis*25}")
    print(f"     Points: {points}")
    print(f"{'-'*number_of_basis*25}")

    print(f"    Solution: {solution}")

    coeff = list(solution.values())
    if test:
        coeff = coeff[::-1]
    str_ = ''
    for i in range(number_of_basis - 1):
        str_ = str_ + f"{coeff[i]}*x^{2*(i+1)-2}*(1-x^2) + "
    print('\n y = ' + str_.rstrip(' +'))
