""" Лабораторная работа 1
y(-1) = y(1) = 0

Задание:
Решить методом коллокаций уравнение вида: ay" + (1 + bx^2)*y = -1,
где a = sin(k), b = cos(k)

Тестовый пример: y" + (1 + x^2)*y = -1
Ответ: y(x) = 0.927*(1 - x^2) - 0.022*(x^2 - x^4)
"""

import math
import sympy
from sympy.abc import x

import helper


def collocation_method(residual, coefficients, points):
    """ Return values of coefficients """
    system = []
    for point in points:
        system.append(residual.subs(x, point))
    return sympy.solve(system, coefficients[1:])


def main():
    number_of_basis = int(input('Enter the number of function bases, i: ')) + 1
    arg_trig = int(input('Enter the argument of the trigonometric function, k: '))

    coefficients = helper.get_coefficients(number_of_basis)
    points = helper.get_points(number_of_basis)

    residual = helper.get_discrepancy(coefficients, number_of_basis, arg_trig)  # Calculating discrepancy.
    solution = collocation_method(residual, coefficients, points)

    helper.pretty_printer(solution, number_of_basis, points)


def test_collocation_method():
    number_of_basis = 3
    arg_trig_sin = math.pi/2
    arg_trig_cos = 0
    coefficients = helper.get_coefficients(number_of_basis)

    points = [-0.5, 0, 0.5]

    residual = helper.get_discrepancy(coefficients, number_of_basis, arg_trig_sin, arg_trig_cos)
    solution = collocation_method(residual, coefficients, points)

    helper.pretty_printer(solution, number_of_basis, points, test=1)


if __name__ == '__main__':
    # main()
    test_collocation_method()
