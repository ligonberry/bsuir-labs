""" Лабораторная работа 1
y(-1) = y(1) = 0

Задание:
Решить методом коллокаций уравнение вида: ay" + (1 + bx^2)*y = -1,
где a = sin(k), b = cos(k)

Тестовый пример: y" + (1 + x^2)*y = -1
Ответ: y(x) = 0.985*(1 - x^2) - 0.078*(x^2 - x^4)
"""

import math
import sympy
from sympy.abc import x

import helper


def discrete_lsm(residual, coefficients, points, number_of_basis):
    """ Return values of coefficients calculated by the discrete lsm method. """
    sum_residual = 0
    for point in points:
        sum_residual = sum_residual + (residual ** 2).subs(x, point)
    system = []
    for ind in range(1, number_of_basis):
        system.append(sum_residual.diff(coefficients[ind]))
    return sympy.solve(system, coefficients[1:])


def get_points(number_of_basis):
    """ Return collocation points. """
    left_boundary_value = -1
    points = []
    avg_point = 2 / (number_of_basis + 2)
    current_point = 1 - avg_point
    while current_point > left_boundary_value:
        points.append(current_point)
        current_point -= avg_point
    return points


def main():
    number_of_basis = int(input('Enter the number of function bases, i: ')) + 1
    arg_trig = int(input('Enter the argument of the trigonometric function, k: '))

    coefficients = helper.get_coefficients(number_of_basis)
    points = get_points(number_of_basis)

    # вычисляем невязку
    R_x = helper.get_discrepancy(coefficients, number_of_basis, arg_trig)
    solution = discrete_lsm(R_x, coefficients, points, number_of_basis)

    helper.pretty_printer(solution, number_of_basis, points)


def test_discrete_lsm():
    number_of_basis = 3
    arg_trig_sin = math.pi/2
    arg_trig_cos = 0

    coefficients = helper.get_coefficients(number_of_basis)
    points = [-0.5, 0, 0.5]

    # вычисляем невязку
    R_x = helper.get_discrepancy(coefficients, number_of_basis, arg_trig_sin, arg_trig_cos)
    solution = discrete_lsm(R_x, coefficients, points, number_of_basis)

    helper.pretty_printer(solution, number_of_basis, points)


if __name__ == '__main__':
    main()
    # test_discrete_lsm()
