""" Лабораторная работа 6. Вариант 6.6
Колебания тонкой пластины без учута потерь на трение описываютмя нормированным
волновым уравнением вида:

 d^2 u      / d^2u       d^2 u  \
-------- - | -----   +  ------  | = 0, где u(x, y, t) - деформация пластины;
 dt^2       \ dx^2       dy^2  /           x, y - координаты;
                                           t - время.

Рассчитать колебания пластины при a = b = 2,
Г1 = u = 0,
      du
Г2 = ---- = 0,
      dn
Г3 = u = 0,
      du
Г4 = ---- = 0,
      dn

и начальных u(x, y, 0) = 2 * cos(PI * x / a) и

du(x, y, 0)
------------ = tg [ sin(2 * PI * x / a) ] * sin (PI * y / b)  условиях.
    dt
"""
import matplotlib
import numpy as np
from sympy import tan, cos, sin, pi
from sympy.abc import x, y

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from helpers import (get_time_steps_count, get_nodes_count, get_row_count, get_spatial_steps_count,
                     get_start_coordinate, set_boundary_conditions, set_initial_condition, set_x_boundary_conditions,
                     set_y_boundary_conditions)


def generate_mat(a, b, h, count_a, count_b):
    mat = []

    start_x, start_y = get_start_coordinate(a, b)

    for i in range(count_b):
        row = []
        for j in range(count_a):
            row.append((start_x + j * h, start_y + i * h))
        mat.append(row)

    return mat


def generate_solution_zero_layer(mat, init_condition):
    solution = []
    solution = set_initial_condition(solution, mat, init_condition)
    solution = set_boundary_conditions(solution, mat)
    return solution


def get_first_layer_node(u_0, i, j, init_conditions, steps):
    init_condition, derivative_init_condition = init_conditions
    tau, h = steps
    rho = 2 * tau ** 2 / h ** 2
    node = init_condition + tau * derivative_init_condition
    node += rho * ((u_0[i - 1][j] - 2 * u_0[i][j] + u_0[i + 1][j]) + (
            u_0[i][j - 1] - 2 * u_0[i][j] + u_0[i][j + 1]))
    return node


def generate_solution_first_layer(mat, u_0, init_conditions, steps):
    row_count = get_row_count(mat)
    nodes_count = get_nodes_count(mat)

    solution = [[0] * nodes_count]

    for i in range(1, row_count - 1):
        row = [0]
        for j in range(1, nodes_count - 1):
            row.append(get_first_layer_node(u_0, i, j, init_conditions, steps)
                       .subs([(x, mat[i][j][0]), (y, mat[i][j][1])]))
        row.append(0)
        solution.append(row)

    solution.append([0] * nodes_count)
    return solution


def get_layer_node(solution, indexes, steps):
    t, i, j = indexes
    tau, h = steps
    rho = tau ** 2 / h ** 2
    node = rho * (solution[t - 1][i - 1][j] - 2 * solution[t - 1][i][j] + solution[t - 1][i + 1][j])
    node += rho * (solution[t - 1][i][j - 1] - 2 * solution[t - 1][i][j] + solution[t - 1][i][j + 1])
    node -= (solution[t - 2][i][j] - 2 * solution[t - 1][i][j])
    return node


def solve_spatial_wave_equation(mat, layers, T, tau, h):
    u_0, u_1 = layers

    row_count = get_row_count(mat)
    nodes_count = get_nodes_count(mat)
    t_count = get_time_steps_count(T, tau)

    solution = np.zeros((t_count, row_count, nodes_count))

    solution[0] = u_0
    solution[1] = u_1

    for t in range(t_count):
        set_x_boundary_conditions(solution[t], mat)
        set_y_boundary_conditions(solution[t], mat)

    steps = tau, h
    for t in range(2, t_count):
        for i in range(1, row_count - 1):
            for j in range(1, nodes_count - 1):
                indexes = t, i, j
                solution[t][i][j] = get_layer_node(solution, indexes, steps)

    return solution


def main():
    a = 2
    b = 2
    T = 2

    h = 0.1
    tau = 0.25 * h ** 2

    init_condition = 2 * cos(pi * x / a)
    derivative_init_condition = tan(sin(2 * pi * x / a)) * sin(pi * y / b)

    count_x = get_spatial_steps_count(a, h)
    count_y = get_spatial_steps_count(b, h)
    count_t = get_time_steps_count(T, tau)

    mat = generate_mat(a, b, h, count_x, count_y)

    u_0 = generate_solution_zero_layer(mat, init_condition)

    init_conditions = init_condition, derivative_init_condition
    steps = tau, h
    u_1 = generate_solution_first_layer(mat, u_0, init_conditions, steps)

    layers = u_0, u_1
    solution = solve_spatial_wave_equation(mat, layers, T, tau, h)
    print(solution)

    x_ = np.linspace(-a / 2, a / 2, count_x)
    y_ = np.linspace(-b / 2, b / 2, count_y)
    X, Y = np.meshgrid(x_, y_)

    for t in range(0, count_t, 55):
        fig = plt.figure()
        ax = Axes3D(fig)
        Z = solution[t]
        ax.plot_wireframe(X, Y, Z)
        plt.legend([f'Solution of wave equation'])
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title(f'Time layer t = {t}')
        plt.show()


if __name__ == '__main__':
    main()
