from sympy.abc import x, y


def get_start_coordinate(a, b):
    start_x = - a / 2
    start_y = - b / 2
    return start_x, start_y


def get_spatial_steps_count(l, h):
    return int(l / h)


def get_time_steps_count(T, tau):
    return int(T / tau)


def get_row_count(mat):
    return len(mat)


def get_nodes_count(mat):
    return len(mat[0])


def set_initial_condition(solution, mat, init_condition):
    row_count = get_row_count(mat)
    nodes_count = get_nodes_count(mat)

    for i in range(row_count):
        row = []
        for j in range(nodes_count):
            row.append(init_condition.subs([(x, mat[i][j][0]), (y, mat[i][j][1])]))
        solution.append(row)
    return solution


def set_zero_layer_conditions(solution, mat):
    """ Make solution like [[0, 0, 0, 0, 0], [....], ..., [0, 0, 0, 0, 0]]. """
    row_count = get_row_count(mat)

    for i in range(row_count):
        solution[i][0] = 0
        solution[i][-1] = 0

    return solution


def set_x_boundary_conditions(solution, mat):
    """ Make solution like [[0, ..., 0], [0, ..., 0], [0, ..., 0], ... ]"""
    nodes_count = get_nodes_count(mat)

    for j in range(nodes_count):
        solution[0][j] = 0
        solution[-1][j] = 0
    return solution


def set_boundary_conditions(solution, mat):
    solution = set_zero_layer_conditions(solution, mat)
    solution = set_x_boundary_conditions(solution, mat)
    return solution


def set_y_boundary_conditions(solution, mat):
    nodes_count = get_nodes_count(mat)

    for i in range(nodes_count):
        solution[i][0] = 0
        solution[i][-1] = 0
    return solution
