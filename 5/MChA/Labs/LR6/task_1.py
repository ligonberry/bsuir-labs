""" Лабораторная работа 6. Вариант 6.4
Продолбные колебания u(x, t) тяги описываются уравнением

 / d^2 u          p  d^2 u  \
| -----------  -  - ------- | = 0,   0 < x < L
\  dt^2           E  dx^2  /


u (0, t) = 0
u (L, t) = 0,  0 <= t <= T,

где E - модуль упругости
    p - плотность материала стежня.
Тяга имеет длинну L и закреплена на концах. Захватив тягу в центре,
ее деформируют так, что продольное перемещение становится равным delta_u.
Затем тяга освобождается.
Рассчитать колебания u(x, t).

Исходные данные:
L = 0.15 (м)
delta_u = 0.001 (м)
E = 86 * 10^9 (Н/м^2)
p = 8,5 * 10^3 (кг/м^3)
"""

import math
import numpy
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt


def set_initial_condition(y_1, x, delta_u, N, L):
    for i in range(0, N + 1):
        if x[i] <= L / 2:
            y_1[i] = 2 * delta_u * x[i] / L
        else:
            y_1[i] = 2 * delta_u * (1 - x[i] / L)


def set_boundary_conditions(y, N):
    y[0] = 0
    y[N] = 0


def change_variables(y, y_1, y_2):
    """ Change variables before moving to the next time layer. """
    y_2[:] = y_1
    y_1[:] = y


# noinspection PyShadowingNames
def solve_wave_equation(L, T, tau, delta_u, a, gamma, show_plot=None):
    K = int(T / tau)
    t = numpy.linspace(0, K * tau, K + 1)  # Time grid

    h = tau * a / float(gamma)
    N = int(L / h)
    x = numpy.linspace(0, L, N + 1)  # Spatial grid

    gamma2 = gamma ** 2  # Auxiliary variable

    solution = []
    y = numpy.zeros(N + 1)  # Solution on n + 1 layer
    y_1 = numpy.zeros(N + 1)  # Solution on n layer
    y_2 = numpy.zeros(N + 1)  # Solution on n - 1 layer

    set_initial_condition(y_1, x, delta_u, N, L)

    show_plot(y_1, x, t, 0)

    # Use a special formula for the calculation in the first time step,
    # taking into account du / dt = 0
    for i in range(1, N):
        y[i] = y_1[i] + (gamma2 / 2) * (y_1[i - 1] - 2 * y_1[i] + y_1[i + 1])

    set_boundary_conditions(y, N)

    show_plot(y, x, t, 1)

    change_variables(y, y_1, y_2)

    for n in range(1, K):
        # Recalculate the values in the internal nodes of the grid on the layer n + 1
        for i in range(1, N):
            y[i] = - y_2[i] + 2 * y_1[i] + gamma2 * (y_1[i - 1] - 2 * y_1[i] + y_1[i + 1])

        set_boundary_conditions(y, N)

        show_plot(y, x, t, n + 1)

        change_variables(y, y_1, y_2)

    return solution, x


def plot_solution(L, T, tau, delta_u, a, gamma):
    """Run solve_wave_equation and render u(x, t) on each time layer. """

    class ShowPlot:
        def __call__(self, u, x, t, n):
            if n == 0:
                self.x_points = x
            plt.xlabel('x')
            plt.ylabel('u')
            plt.legend([f't = {t[n]}'], loc='lower left')
            plt.axis([0, L, -0.012, 0.012])
            plt.plot(self.x_points, u, 'r-')
            plt.show()

    show_plot = ShowPlot()
    solve_wave_equation(L, T, tau, delta_u, a, gamma, show_plot)


def main():
    delta_u = 0.01
    L = 0.15
    E = 86 * 10 ** 9
    p = 8.5 * 10 ** 3
    a = math.sqrt(E / p)

    T = 0.1
    tau = L / 50. / a

    gamma = 0.85
    plot_solution(L, T, tau, delta_u, a, gamma)


if __name__ == '__main__':
    main()
