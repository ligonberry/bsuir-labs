""" Лабораторная работа 5. Вариант 5.9
Пластина прямоугольной формы с вырезом на одной из сторон жестко закреплена
по краям и равномерно нагружена по площади. Прогиб пластины определяется из уравнения
Пуассона. Рассчитать прогию W(x, y) по данным, приведенным в таблице.

A = 100  - ширина пластины
B = 100  - длинна пластины
h = 35   - толщина плстины
R = 5    - радиус выреза
P = 110  - нагрузка
E = 120  - модуль упругости
v = 0.3  - коэффициент Пуассона

Граничное условие W = 0

 / d^2 w(x, y)     d^2 w(x, y) \    P
| -----------  +  ------------ | = ---
\  dx^2              dy^2     /     D

где D = Eh^3 / [12 * (1 - v^2)] - изгибная жесткость,
E =  - модуль упругости
h =  - толщина пластины
"""
import time
import numpy as np
np.set_printoptions(threshold=np.inf)
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


# noinspection PyShadowingNames
def poisson(A, B, P, D, R, h):
    matrix = np.zeros((A + 1, B + 1))
    current_delta = 1
    eps = 0.3

    cut_x = A / 2
    cut_y = 0

    circle_equation = lambda i, j: (cut_x - i) ** 2 + (cut_y - j) ** 2
    is_outside_cut = lambda i, j: R**2 <= circle_equation(i, j)
    is_inside_cut = lambda i, j: circle_equation(i, j) <= R**2

    while current_delta > eps:
        current_delta = 0
        for i in range(1, A):
            for j in range(1, B):
                if is_inside_cut(i, j):
                    continue
                if is_inside_cut(i + 1, j) and is_outside_cut(i, j):
                    p1 = (cut_x - np.sqrt(R ** 2 - (cut_y - j) ** 2)) - i
                    matrix[i][j] = p1 / (p1 + h) * matrix[i - 1, j]
                    continue
                if is_inside_cut(i, j) and is_outside_cut(i, j):
                    p1 = j - (cut_y - np.sqrt(R ** 2 - (cut_x - i) ** 2))
                    matrix[i][j] = p1 / (p1 + h) * matrix[i, j + 1]
                    continue
                if is_inside_cut(i - 1, j) and is_outside_cut(i, j):
                    p1 = i - (cut_x - np.sqrt(R ** 2 - (cut_y - j) ** 2))
                    matrix[i][j] = p1 / (p1 + h) * matrix[i + 1, j]
                    continue
                temp = matrix[i][j]
                matrix[i][j] = 0.25 * (
                        matrix[i - 1][j] + matrix[i + 1][j] + matrix[i][j - 1] + matrix[i][j + 1] - h ** 2 * P / D)
                delta = abs(temp - matrix[i][j])
                if delta > current_delta:
                    current_delta = delta
    return matrix


def main():
    A = 100
    B = 100
    R = 35
    h = 5
    P = 110
    E = 120
    v = 0.3
    D = E * h ** 3 / (12 - (1 - v ** 2))
    t1 = time.time()
    result = poisson(A, B, P, D, R, h)
    t2 = time.time()
    print(result)
    print("Time that the algorithm worked ", t2 - t1)
    fig = plt.figure()
    ax = Axes3D(fig)
    u_x = np.linspace(0, 1, B + 1)
    u_y = np.linspace(0, 1, A + 1)
    X, Y = np.meshgrid(u_x, u_y)
    Z = result
    ax.plot_wireframe(X, Y, Z)
    plt.show()


if __name__ == '__main__':
    main()
