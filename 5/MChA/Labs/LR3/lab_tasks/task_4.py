""" Лабораторная работа 3. Вариант 3.4.6

Задание:
--------
1. Взять входные данные Ua, Ub из задачи 3.
2. Использовать явную разностную схему.
3. Взять h = (b - a) / 10; шаг tau выбрать из условия устойчивости: tau <= 0.5 * (h^2 / k)
4. Изобразить графики зависимости приближенного решения от х при t = 0, 2*tau, 4*tau, ..., T.

du      d^2 (u)
-- = k --------- + f(x, t), a < x < b, 0 < t < T,
dt       dx^2

u(a, t) = g1(t),
u(b, t) = g2(t), 0 < t < T;
U(x, 0) = ф(x)  a <= x <= b;


Исходные данные:
---------------
a = -1, Ua = 2
b = 1, Ub = 1
k(x, t) = 2
T = 0.1
g1(0, t) = 1
g2(l, t) = 1
ф(x, 0) = x^2
f(x) = 0
"""


import numpy as np
from sympy.abc import x

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from helpers import calculate_maximum_deviation


def solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions, y_boundary_conditions, init_condition, n):
    f, k = coefficients_before_y

    a, b = boundary_conditions
    h = (b - a) / n
    tau = 0.5 * (h ** 2 / k)

    matrix = np.zeros((n + 1, n + 1))

    g_1, g_2 = y_boundary_conditions

    for i in range(n + 1):  # On the zero layer for t = 0.
        x_value = a + h * i
        if x_value <= b:
            matrix[0, i] = init_condition.subs(x, x_value)

    for i in range(1, n + 1):
        matrix[i, 0] = g_1  # the leftmost
        matrix[i, n] = g_2  # the rightmost

    for i in range(1, n+1):  # The calculation by the "explicit scheme".
        for j in range(1, n):
            matrix[i, j] = (
                    tau / h ** 2 * matrix[i - 1, j - 1] +
                    matrix[i - 1, j] -
                    2 * tau / h ** 2 * matrix[i - 1, j] +
                    tau / h ** 2 * matrix[i - 1, j + 1] +
                    tau * f)
    return matrix


def main():
    a = 1
    b = 1.5
    boundary_conditions = a, b

    g_1 = 1
    g_2 = 2.25
    y_boundary_conditions = g_1, g_2

    n = 10

    f = 0
    k = 2
    coefficients_before_y = f, k
    init_condition = x**2

    n_old = int(n/2)
    res_matrix_old = solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions, y_boundary_conditions,
                                                         init_condition, n_old)
    res_matrix = solve_explicit_approximation_scheme(coefficients_before_y, boundary_conditions, y_boundary_conditions, init_condition, n)
    print(res_matrix)
    calculate_maximum_deviation(res_matrix_old, res_matrix)

    x_points = np.linspace(a, b, n + 1)
    for i in range(0, n+1, 2):
        temperatures = res_matrix[i]
        plt.plot(x_points, temperatures)
        plt.title(f't = {i}')
        plt.grid()
        plt.show()

    temp_0 = res_matrix[0]
    temp_10 = res_matrix[10]
    plt.plot(x_points, temp_0, 'g-', label='t = 0')
    plt.plot(x_points, temp_10, 'r--', label='t = 50')
    plt.legend()
    plt.grid()
    plt.show()


if __name__ == '__main__':
    main()
