""" Лабораторная работа 3. Вариант 3.3.30

Промоделировать стационарные процессы теплопроводности стержня
в зависимости от входных данных задачи.

   d  /     du  \
- -- | K(x) --  | = f
  dx \      dx  /


Порядок решения задачи:
------------------------
1. Представить коэффициент теплопроводности K(x) в виде функции двух переменных x и c:
K(x) = K(x, c), где c - параметр.
2. Найти аналитическое решение.
3. Изменяя значение параметра c в коэффициенте теплопроводности,
   найти решение задачи для наборов 1-3.
4. На одном чертеже построить графики найденных решений.
   Сравнить полученные результаты.
5. Найти решение для четвертого набора параметров. На одном чертеже построить графики
   решений для наборов один и четыре. Сравнить полученные результаты.
6. Изменяя граничные условия Ua, Ub, построить решения для наборов параметров 5-7.

Исходные данные:
---------------
k(x) = exp(-x)
f(x) = 3 + exp(3*x)
a = 0.3, Ua = 3
b = 2.3, Ub = 1
"""
import sympy
from numpy import linspace
from sympy.solvers import solve

from sympy.abc import x

import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt


def get_exact_solution(f_x, k_x, c):
    c1, c2 = sympy.Symbol('c1'), sympy.Symbol('c2')
    #              1     /
    # u'(x) = - -------  | f(x) dx + c1
    #           k(x, c)  /
    first_integration = ((-sympy.integrate(f_x(x), x) + c1) / k_x(x, c)).expand()
    #         /
    # u(x) =  | u'(x) dx + c2
    #         /
    second_integration = sympy.integrate(first_integration, x) + c2
    return sympy.simplify(second_integration)


def get_boundary_problem_solution(exact_solution, boundary_conditions, y_boundary_conditions):
    c1, c2 = sympy.Symbol('c1'), sympy.Symbol('c2')

    a, b = boundary_conditions
    Ua, Ub = y_boundary_conditions

    # The solution must fit the right boundary condition.
    c2_value = solve(exact_solution.subs(x, b) - Ub, c2)[0]
    solution = exact_solution.subs(c2, c2_value)

    # The solution must fit the left boundary condition.
    c1_value = solve(solution.subs(x, a) - Ua, c1)[0]
    solution = solution.subs(c1, c1_value)
    return solution


def solve_equation(f_x, k_x, boundary_conditions, y_boundary_conditions, c):
    exact_solution = get_exact_solution(f_x, k_x, c)
    solution = get_boundary_problem_solution(exact_solution, boundary_conditions, y_boundary_conditions)
    return solution


def plot_three_solutions(solution_1, solution_2, solution_3, boundary_conditions, pretty_number=0):
    a, b = boundary_conditions
    x_ = linspace(a, b, 100)

    y1, y2, y3 = [], [], []
    for i in range(len(x_)):
        y1.append(solution_1.subs('x', x_[i]))
        y2.append(solution_2.subs('x', x_[i]))
        y3.append(solution_3.subs('x', x_[i]))

    fig, _ = plt.subplots()
    plt.title(f"Plots for solutions {pretty_number} - {pretty_number+2}")

    plt.plot(x_, y1, c='red', label=f'solution_{pretty_number}')
    plt.plot(x_, y2, c='green', label=f'solution_{pretty_number+1}')
    plt.plot(x_, y3, c='blue', label=f'solution_{pretty_number+2}')

    plt.legend()
    plt.show()


def plot_two_solutions(solution_1, solution_2, boundary_conditions):
    a, b = boundary_conditions
    x_ = linspace(a, b, 100)

    y1, y4 = [], []
    for i in range(len(x_)):
        y1.append(solution_1.subs('x', x_[i]))
        y4.append(solution_2.subs('x', x_[i]))

    fig, _ = plt.subplots()
    plt.title("Plots for solutions 1 and 4")

    plt.plot(x_, y1, c='red', label='solution_1')
    plt.plot(x_, y4, c='green', label='solution_4')

    plt.legend()
    plt.show()


def main():
    a = 0.3
    b = 2.3
    boundary_conditions = a, b

    Ua = 3
    Ub = 1
    y_boundary_conditions = Ua, Ub

    f_x = lambda x_: 3 + sympy.exp(3 * x_)
    k_x = lambda x_, _: sympy.exp(-x_)

    # Изменяя значение параметра c в коэффициенте теплопроводности,
    # найти решение задачи для наборов 1-3
    c_1 = 1
    solution_1 = solve_equation(f_x, k_x, boundary_conditions, y_boundary_conditions, c_1)
    c_2 = 2
    k_x_ = lambda x_, c_: c_ * k_x(x_, c_)
    solution_2 = solve_equation(f_x, k_x_, boundary_conditions, y_boundary_conditions, c_2)
    c_3 = 0.1
    solution_3 = solve_equation(f_x, k_x_, boundary_conditions, y_boundary_conditions, c_3)

    plot_three_solutions(solution_1, solution_2, solution_3, boundary_conditions, pretty_number=1)

    # Найти решение для четвертого набора параметров. На одном чертеже построить графики
    # решений для наборов один и четыре.
    c_4 = 1
    k_x_ = lambda x_, c_: 1 / k_x(x_, c_)
    solution_4 = solve_equation(f_x, k_x_, boundary_conditions, y_boundary_conditions, c_4)
    plot_two_solutions(solution_1, solution_4, boundary_conditions)

    # Изменяя граничные условия Ua, Ub, построить решения для наборов параметров 5-7.
    c = 1
    y_boundary_conditions_ = -Ua, Ub
    solution_5 = solve_equation(f_x, k_x, boundary_conditions, y_boundary_conditions_, c)
    y_boundary_conditions_ = Ua, -Ub
    solution_6 = solve_equation(f_x, k_x, boundary_conditions, y_boundary_conditions_, c)
    y_boundary_conditions_ = -Ua, -Ub
    solution_7 = solve_equation(f_x, k_x, boundary_conditions, y_boundary_conditions_, c)

    plot_three_solutions(solution_5, solution_6, solution_7, boundary_conditions, pretty_number=5)


if __name__ == '__main__':
    main()
