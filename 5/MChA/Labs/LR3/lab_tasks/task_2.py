""" Лабораторная работа 3. Вариант 3.3.30

Промоделировать стационарные процессы теплопроводности стержня
в зависимости от входных данных задачи - переменного коэффициента теплопроводности k(x) и
плотности источника тепла f(x).

   d  /     du  \
- -- | k(x) --  | = f
  dx \      dx  /

u(a) = Ua, u(b) = Ub


Порядок решения задачи:
------------------------
Составить разностную схему второго порядка точности для решения указанной задачи.
1. Взять исходные данные из первого набора параметров для первой задачи.
2. Шаг сетки положить равным h = (b - a) / 150.
3. Промоделировать процесс в зависимости от коэффициента теплопроводности k(x).
3.a. Полагать, что стержень состоит из двух материалов с различными коэффициентами
     теплопроводности k(x):

            k1, a <= x <= 0.5 * (b + a),
    k(x) =
            k2, 0.5 * (b + a) < x <= b,

    При 1. k1 << k2
        2. k2 << k1

3.b. Пусть стержень состоит из трех материалов с различными свойствами:

             k1, a <= x <= a + (b-a)/3

     k(x) =  k2,  a + (b-a)/3 <= x < a + 2 * (b-a)/3

             k3, a + 2 * (b-a)/3 <= x <= b

     При 1. k1 < k2 < k3
         2. k1 > k2 > k3
         3. k1 = k, k2 = 2k, k3 = k
         4. k1 = 20k, k2 = k, k3 = k

4. Промоделировать процесс теплопроводности в зависимости от правой части -
   ф-ции f(x), полагая, что f(x) - точечный истояник тепла. Задать точечный источник
   тепла можно:

    f(x) = c * delta * (x - x0), где c = const ( мощность источника)
                                     delta(x) - дельта-функция
                                     x0 - точка из отрезка [a, b], в которой располагается источник.

    Рассмотреть следующие варианты расположения источника:
        1. точечный источник поставлен на середину отрезка [a, b]
        2. два одинаковых по мощности источника поставлены в разные точки отрезка,
           симметричные относительно середины отрезка
        3. два различных по мощности источника поставлены симметрично
        4. предложить свой вариант расположения источников


Исходные данные:
---------------
    k(x) = exp(-x)
    f(x) = 3 + exp(3*x)
    a = 0.3, Ua = 3
    b = 2.3, Ub = 1
"""
import heapq
from collections import namedtuple
import sympy
from numpy import linspace
from sympy.abc import x

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from tdma import TDMAsolver
from helpers import calculate_error


def create_boundary_equation(y, coefficients_before_y, h):
    yk_p1, yk, yk_m1 = y
    k_x, f_x = coefficients_before_y
    func = -k_x * (yk_p1 - 2 * yk + yk_m1) / h ** 2
    func -= f_x(x)
    return func


def replace_x(system, start_y_number, points):
    """ Replace x with a numerical value. """
    for i in range(start_y_number - 2):
        system[i] = system[i].subs(x, points[i])
    return system


def substitute_boundary_values(system, coefficients, y_boundary_conditions):
    Ua, Ub = y_boundary_conditions

    system[0] = system[0].subs(coefficients[0], Ua)
    system[-1] = system[-1].subs(coefficients[-1], Ub)

    return system


def add_boundary_values(solution, points, boundary_conditions, y_boundary_conditions):
    a, b = boundary_conditions
    points.insert(0, a)
    points.append(b)

    Ua, Ub = y_boundary_conditions
    solution.insert(0, Ua)
    solution.append(Ub)

    return solution, points


def method_of_difference_approximations(boundary_conditions, y_boundary_conditions, coefficients_before_y,
                                        start_y_number=75):
    a, b = boundary_conditions
    k_x, f_x = coefficients_before_y

    start_y_number += 2
    coefficients = list(sympy.var(f'y:{start_y_number}'))

    h = (b - a) / start_y_number
    points = list(linspace(a + h, b - h, start_y_number - 2))

    system = []
    selected_k = 0
    for i in range(1, start_y_number - 1):
        if points[i - 1] > k_x[selected_k][0]:
            selected_k += 1

        y = coefficients[i - 1], coefficients[i], coefficients[i + 1]
        coefficients_before_y = k_x[selected_k][1], f_x
        func = create_boundary_equation(y, coefficients_before_y, h)
        system.append(func)

    system = replace_x(system, start_y_number, points)
    system = substitute_boundary_values(system, coefficients, y_boundary_conditions)

    del coefficients[0], coefficients[-1]

    solution = TDMAsolver(system)

    solution, points = add_boundary_values(solution, points, boundary_conditions, y_boundary_conditions)

    data_type = namedtuple('data',
                           ('points', 'solution'))

    return data_type(points, solution)


def plot_solutions_third_task(data_1, data_2):
    x_1 = data_1.points
    y_1 = data_1.solution

    x_2 = data_2.points
    y_2 = data_2.solution

    fig, _ = plt.subplots()
    plt.title("Plots for task 2.3")

    plt.plot(x_1, y_1, c='red', label='k1 >> k2')
    plt.plot(x_2, y_2, c='green', label='k2 >> k1')

    plt.legend()
    plt.show()


def plot_solutions_fourth_task(data_1, data_2, data_3, data_4):
    x_1 = data_1.points
    y_1 = data_1.solution

    x_2 = data_2.points
    y_2 = data_2.solution

    x_3 = data_3.points
    y_3 = data_3.solution

    x_4 = data_4.points
    y_4 = data_4.solution

    fig, _ = plt.subplots()
    plt.title("Plots for task 2.4")

    plt.plot(x_1, y_1, c='red', label='k1 < k2 < k3')
    plt.plot(x_2, y_2, c='green', label='k1 > k2 > k3')
    plt.plot(x_3, y_3, c='blue', label='k1 = k, k2 = 2k, k3 = k')
    plt.plot(x_4, y_4, c='orange', label='k1 = 20k, k2 = k, k3 = k')

    plt.legend()
    plt.show()


def plot_solutions_five_task(data_1, data_2, data_3, data_4):
    x_1 = data_1.points
    y_1 = data_1.solution

    x_2 = data_2.points
    y_2 = data_2.solution

    x_3 = data_3.points
    y_3 = data_3.solution

    x_4 = data_4.points
    y_4 = data_4.solution

    fig, _ = plt.subplots()
    plt.title("Plots for task 2.5")

    plt.plot(x_1, y_1, c='red', label='identical, middle of the [a, b]')
    plt.plot(x_2, y_2, c='green', label='identical, symmetrical of the [a, b]')
    plt.plot(x_3, y_3, c='blue', label='different, symmetrical of the [a, b]')
    plt.plot(x_4, y_4, c='orange', label='different, placed by my own')

    plt.legend()
    plt.show()


def main():
    a = 0.3
    b = 2.3
    boundary_conditions = a, b

    Ua = 3
    Ub = 1
    y_boundary_conditions = Ua, Ub

    f_x = lambda x_: 3 + sympy.exp(3 * x_)

    start_y_number = 150

    # region TASK 3
    # Промоделировать процесс в зависимости от коэффициента теплопроводности k(x)
    # Полагать, что стержень состоит из двух материалов с различными коэффициентами
    # теплопроводности k(x):
    # 1. k1 >> k2
    k_x_1 = 10
    k_x_2 = 0.1
    k_x = [(0.5 * (b + a), k_x_1), (b, k_x_2)]
    coefficients_before_y = k_x, f_x

    data_old = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                   coefficients_before_y, start_y_number=start_y_number)

    data_31 = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                  coefficients_before_y, start_y_number=start_y_number * 2)

    print("Calculate maximum derivation, when k1 >> k2: ")
    calculate_error(data_old, data_31)

    # 2. k2 >> k1
    k_x_1 = 0.01
    k_x_2 = 100
    k_x = [(0.5 * (b + a), k_x_1), (b, k_x_2)]
    coefficients_before_y = k_x, f_x

    data_old = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                   coefficients_before_y, start_y_number=start_y_number)

    data_32 = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                  coefficients_before_y, start_y_number=start_y_number * 2)

    print("Calculate maximum derivation, when k2 >> k1: ")
    calculate_error(data_old, data_32)

    # largest_error = heapq.nlargest(1, (((abs(a - b)), a, b) for a, b in zip(data_31.solution, data_32.solution)))
    # print(largest_error)

    plot_solutions_third_task(data_31, data_32)
    # endregion

    # region TASK 4
    # Пусть стержень состоит из трех материалов с различными свойствами:
    # 1. k1 < k2 < k3
    k_x_1 = 1
    k_x_2 = 2
    k_x_3 = 3
    k_x = [(a + 1 / 3 * (b - a), k_x_1), (a + 2 / 3 * (b - a), k_x_2), (b, k_x_3)]
    coefficients_before_y = k_x, f_x
    data_old = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                   coefficients_before_y, start_y_number=start_y_number)

    data_41 = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                  coefficients_before_y, start_y_number=start_y_number * 2)

    print("Calculate maximum derivation, when k1 < k2 < k3: ")
    calculate_error(data_old, data_41)

    # 2. k1 > k2 > k3
    k_x_1 = 3
    k_x_2 = 2
    k_x_3 = 1
    k_x = [(a + 1 / 3 * (b - a), k_x_1), (a + 2 / 3 * (b - a), k_x_2), (b, k_x_3)]
    coefficients_before_y = k_x, f_x
    data_old = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                   coefficients_before_y, start_y_number=start_y_number)

    data_42 = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                  coefficients_before_y, start_y_number=start_y_number * 2)

    print("Calculate maximum derivation, when k1 > k2 > k3: ")
    calculate_error(data_old, data_42)

    # 3. k1 = k, k2 = 2k, k3 = k
    k_x = 1
    k_x_1 = k_x
    k_x_2 = 2 * k_x
    k_x_3 = k_x
    k_x = [(a + 1 / 3 * (b - a), k_x_1), (a + 2 / 3 * (b - a), k_x_2), (b, k_x_3)]
    coefficients_before_y = k_x, f_x
    data_old = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                   coefficients_before_y, start_y_number=start_y_number)

    data_43 = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                  coefficients_before_y, start_y_number=start_y_number * 2)

    print("Calculate maximum derivation, when k1 = k, k2 = 2k, k3 = k: ")
    calculate_error(data_old, data_43)

    # 4. k1 = 20k, k2 = k, k3 = k
    k_x = 1
    k_x_1 = 20 * k_x
    k_x_2 = k_x
    k_x_3 = k_x
    k_x = [(a + 1 / 3 * (b - a), k_x_1), (a + 2 / 3 * (b - a), k_x_2), (b, k_x_3)]
    coefficients_before_y = k_x, f_x
    data_old = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                   coefficients_before_y, start_y_number=start_y_number)

    data_44 = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                  coefficients_before_y, start_y_number=start_y_number * 2)

    print("Calculate maximum derivation, when k1 = 20k, k2 = k, k3 = k: ")
    calculate_error(data_old, data_44)

    plot_solutions_fourth_task(data_41, data_42, data_43, data_44)
    # endregion

    # region TASK 5
    # Промоделировать процесс теплопроводности в зависимости от правой части -
    # ф-ции f(x), полагая, что f(x) - точечный истояник тепла.
    c = lambda x_: sympy.sin(x_)  # source power
    delta = lambda x_: sympy.cos(x_)  # delta function
    k_x = [(b, 1)]

    x_0 = a + (b - a) / 2
    # 1. точечный источник поставлен на середину отрезка [a, b]
    f_x = lambda x_: c(x_) * delta(x_) * (x_ - x_0)
    coefficients_before_y = k_x, f_x
    data_old = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                   coefficients_before_y, start_y_number=start_y_number)

    data_51 = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                  coefficients_before_y, start_y_number=start_y_number)

    print("Calculate maximum derivation, when two identical point source placed in the middle of the [a, b]: ")
    calculate_error(data_old, data_51)

    # 2. два одинаковых по мощности источника поставлены в разные точки отрезка,
    #    симметричные относительно середины отрезка
    f_x = lambda x_: c(x_) * delta(x_) * (x_ - x_0) * 0.3 * c(x_) * delta(x_) * (x_ - x_0) * 0.7
    coefficients_before_y = k_x, f_x
    data_old = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                   coefficients_before_y, start_y_number=start_y_number)

    data_52 = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                  coefficients_before_y, start_y_number=start_y_number)

    print("Calculate maximum derivation, when two identical sources of power are placed "
          "at different points of the segment, symmetrical about the middle of the segment: ")
    calculate_error(data_old, data_52)

    # 3. два различных по мощности источника расставлены симметрично
    f_x = lambda x_: c(x_) * delta(x_) * (x_ - x_0) * 0.3 * 100 * c(x_) * delta(x_) * (x_ - x_0) * 0.7
    coefficients_before_y = k_x, f_x
    data_old = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                   coefficients_before_y, start_y_number=start_y_number)

    data_53 = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                  coefficients_before_y, start_y_number=start_y_number)

    print("Calculate maximum derivation, when two different power sources are arranged symmetrically: ")
    calculate_error(data_old, data_53)

    # 4. предложить собственный вариант размещения источников:
    # первый расположен на 0.1 от начала, второй - 0.9 от начала.
    # мощности источников различны
    f_x = lambda x_: 100 * c(x_) * delta(x_) * (x_ - x_0) * 0.1 * c(x_) * delta(x_) * (x_ - x_0) * 0.9
    coefficients_before_y = k_x, f_x
    data_old = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                   coefficients_before_y, start_y_number=start_y_number)

    data_54 = method_of_difference_approximations(boundary_conditions, y_boundary_conditions,
                                                  coefficients_before_y, start_y_number=start_y_number)

    print("Calculate maximum derivation, when two different power sources are placed by my own: ")
    calculate_error(data_old, data_54)
    plot_solutions_five_task(data_51, data_52, data_53, data_54)

    # largest_error = heapq.nlargest(1, (((abs(a - b)), a, b) for a, b in zip(data_51.solution, data_53.solution)))
    # print(largest_error)
    # endregion


if __name__ == '__main__':
    main()
