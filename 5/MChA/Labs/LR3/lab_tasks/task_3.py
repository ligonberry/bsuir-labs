""" Лабораторная работа 3. Вариант 3.3.11

Задание:
--------
1. Найти приближенное решение краевой задачи с шагами tau = 0.05 и h = 0.01,
используя явную разностную схему. Построить графики pешений при значениях
t0 = 0, t1 = 5*tau, t2 = 20*tau, t3 = 50*tau.
2. Визуально определить момент времени t, при котором происходит установление процесса.
3. Произвести анимацию процесса установления.
4. Исследовать, как влияет начальная температура на процесс установления, взяв другие
функции ф(x).

du    d   /     du  \
-- =  -- | k(x) --  | + f(x)*(1 - exp(-t)),
dt    dx \      dx  /

u(0, t) = Ua,   0 < x < l,  0 < t < T;
u(l, t) = Ub,   a <= t <= T;
U(x, 0) = ф(x)  0 <= x <= l;


Исходные данные:
---------------
k(x) = cos(x)
f(x) = 10*cos(x)
a = 1, Ua = 2
b = 1.5, Ub = 1
"""

import matplotlib
import numpy as np
import sympy

matplotlib.use('TkAgg')
from matplotlib import animation
import matplotlib.pyplot as plt
from math import e
from mpl_toolkits.mplot3d import Axes3D
from sympy.abc import x, t

from helpers import calculate_maximum_deviation, get_chain


def plot_solution(x_points, y_points, layer):
    fig, _ = plt.subplots()
    plt.title(f"Temperatures on layer {layer}")
    plt.plot(x_points, y_points)
    plt.grid()
    plt.show()


def plot_bulk_solution(points, res_matrix):
    fig = plt.figure()
    ax = Axes3D(fig)
    X, Y = np.meshgrid(points, points)
    Z = res_matrix
    ax.plot_wireframe(X, Y, Z)
    plt.show()


def animate_solution(matrix, boundary_condition, numb_cells):
    # set up the figure, the axis, and the plot element we want to animate
    a, b = boundary_condition
    y_max = get_max_value(matrix)
    y_min = get_min_value(matrix)
    fig = plt.figure()
    ax = plt.axes(xlim=(a, b), ylim=(y_min, y_max))
    line, = ax.plot([], [], lw=2)

    def init():
        line.set_data([], [])
        return line,

    def update(i):
        x = np.linspace(a, b, numb_cells + 1)
        y = matrix[i]
        line.set_data(x, y)
        return line,

    animation_ = animation.FuncAnimation(fig, update, init_func=init, frames=(numb_cells + 1),
                                         interval=1, blit=True)

    plt.show()


def get_max_value(matrix):
    chain = get_chain(matrix)
    return max(chain)


def get_min_value(matrix):
    chain = get_chain(matrix)
    return min(chain)


def solve_explicit_approximation_scheme(f, k, boundary_conditions, y_boundary_conditions, init_condit, n, h, tau):
    matrix = np.zeros((n + 1, n + 1))

    a, b = boundary_conditions
    Ua, Ub = y_boundary_conditions

    for i in range(n + 1):  # On the zero layer for t = 0.
        x_value = a + h * i
        if x_value <= b:
            matrix[0, i] = init_condit.subs(x, x_value)

    for i in range(1, n + 1):
        matrix[i, 0] = Ua  # the leftmost
        matrix[i, n] = Ub  # the rightmost

    for i in range(1, n+1):  # The calculation by the "explicit scheme".
        for j in range(1, n):
            k_x = k.subs(x, j)
            f_x = f.subs([(t, i - 1), (x, j)])
            matrix[i, j] = (k_x * (
                    tau / h ** 2 * matrix[i - 1, j - 1] +
                    matrix[i - 1, j] -
                    2 * tau / h ** 2 * matrix[i - 1, j] +
                    tau / h ** 2 * matrix[i - 1, j + 1] ) +
                    tau * f_x)

    return matrix


def main():
    a = 1
    b = 1.5
    boundary_conditions = a, b

    Ua = 2
    Ub = 1
    y_boundary_conditions = Ua, Ub

    h = 0.01
    tau = 0.05
    numb_cells = int((b - a) / h)

    f = 10 * sympy.cos(x) * (1 - e ** -t)
    k = sympy.cos(x)
    init_condition = -2 * x + 4

    # 1. Построить графики pешений при значениях
    #    t0 = 0, t1 = 5*tau, t2 = 20*tau, t3 = 50*tau
    layers = [0, 5, 20, 50]

    h_old = h * 4
    tau_old = tau * 2
    numb_cells_old = int((b - a) / h_old)
    res_matrix_old = solve_explicit_approximation_scheme(f, k, boundary_conditions, y_boundary_conditions, init_condition, numb_cells_old,
                                                         h_old, tau_old)
    res_matrix = solve_explicit_approximation_scheme(f, k, boundary_conditions, y_boundary_conditions, init_condition,
                                                     numb_cells, h, tau)
    calculate_maximum_deviation(res_matrix_old, res_matrix)

    for layer in layers:
        print(f'v{layer} - {res_matrix[layer]}')
        x_points = np.linspace(a, b, numb_cells + 1)
        y_points = res_matrix[layer]
        plot_solution(x_points, y_points, layer)

    points = np.linspace(a, b, numb_cells + 1)
    plot_bulk_solution(points, res_matrix)

    # 3. Произвести анимацию процесса установления
    animate_solution(res_matrix, boundary_conditions, numb_cells)


def test_init_condition():
    a = 1
    b = 1.5
    boundary_conditions = a, b

    Ua = 2
    Ub = 1
    y_boundary_conditions = Ua, Ub

    h = 0.01
    tau = 0.05
    numb_cells = int((b - a) / h)

    f = 10 * sympy.cos(x) * (1 - e ** -t)
    k = sympy.cos(x)
    # 4. Исследовать, как влияет начальная температура на процесс установления, взяв другие
    #    функции ф(x).
    init_condition_1 = -2 * x + 4
    # Quadratic regression
    init_condition_2 = 0.9999982 + 3.000003*x - 2.000001*x**2
    # Exponential approximation: a + b * exp(-c*x)
    init_condition_3 = -12362.25 + 12366.25*sympy.exp(-0.0001617478*x)
    #                                  /  (x - b)^2  \
    # Gaussian approximation: a * exp | - ---------  |
    #                                  \   2*c^2     /
    init_condition_4 = 2.071776*sympy.exp(-(x - 0.8589636)**2/(2*0.5311056**2))

    layers = [0, 5, 20, 50]

    res_matrix_1 = solve_explicit_approximation_scheme(f, k, boundary_conditions, y_boundary_conditions,
                                                       init_condition_1, numb_cells, h, tau)
    res_matrix_2 = solve_explicit_approximation_scheme(f, k, boundary_conditions, y_boundary_conditions,
                                                       init_condition_2, numb_cells, h, tau)
    res_matrix_3 = solve_explicit_approximation_scheme(f, k, boundary_conditions, y_boundary_conditions,
                                                       init_condition_3, numb_cells, h, tau)
    res_matrix_4 = solve_explicit_approximation_scheme(f, k, boundary_conditions, y_boundary_conditions,
                                                       init_condition_4, numb_cells, h, tau)

    for layer in layers:
        x_points = np.linspace(a, b, numb_cells + 1)

        y_points_1 = res_matrix_1[layer]
        y_points_2 = res_matrix_2[layer]
        y_points_3 = res_matrix_3[layer]
        y_points_4 = res_matrix_4[layer]
        plt.title(f"Temperatures on layer {layer}")
        plt.plot(x_points, y_points_1, 'g-', label='linear')
        plt.plot(x_points, y_points_2, 'b-', label='quadratic')
        plt.plot(x_points, y_points_3, 'r--', label='exponential')
        plt.plot(x_points, y_points_4, 'y--', label='gaussian')
        plt.legend()
        plt.grid()
        plt.show()


if __name__ == '__main__':
    main()
    #test_init_condition()