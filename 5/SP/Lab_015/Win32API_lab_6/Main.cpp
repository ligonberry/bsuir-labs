#include <windows.h>
#include <tchar.h>
#include <vector>
#include <regex>

using namespace std;

#define WND_CLASS_NAME _T("WindowClass")
#define CURSOR_PATH _T("cursors/cursor.cur")
#define ICON_PATH _T("icons/icon.ico")

#define ID_LIST_ONE		120
#define BTN_START		121
#define BTN_STOP		122
#define STC_INFO		123


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

void searchInKey(HKEY, wstring);
wstring getKeyPath(HKEY);
DWORD WINAPI search(LPVOID);
bool checkKey(HKEY, DWORD);
bool regexFunc(wstring);

HINSTANCE hInst;
HANDLE thread;
HWND ghWnd;
wstring rgx = L"^[A-Z]:\\[,]*";
bool threads_exist = false;
bool stop_flag = true;
int MatchedCounter = 0;
int CheckedCounter = 0;

static  HWND static_label;



INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int ncmdshow)
{
	MSG msg = { 0 };
	WNDCLASS wc = { 0 };

	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.lpszMenuName = nullptr;
	wc.hbrBackground = HBRUSH(CreateSolidBrush(RGB(50, 50, 50)));
	wc.hCursor = LoadCursorFromFile(CURSOR_PATH);
	wc.hInstance = hInstance;
	wc.lpszClassName = WND_CLASS_NAME;
	wc.lpfnWndProc = WndProc;
	wc.hIcon = HICON(LoadImage(nullptr, ICON_PATH, IMAGE_ICON, 0, 0,
		LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_SHARED));

	if (!RegisterClassW(&wc))
	{
		return -1;
	}

	ghWnd = CreateWindow(WND_CLASS_NAME, _T("WINDOW"), WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, 1280, 720,
		NULL, NULL, NULL, NULL);

	CreateWindow(L"listbox", L"Results", WS_CHILD | WS_VISIBLE | LBS_EXTENDEDSEL | WS_BORDER | WS_VSCROLL | WS_HSCROLL,
		117, 25, 1000, 500, ghWnd, (HMENU)ID_LIST_ONE, hInst, NULL);
	CreateWindow(L"Button", L"Start", WS_CHILD | WS_VISIBLE, 555, 600, 75, 75, ghWnd, (HMENU)BTN_START, hInst, NULL);
	CreateWindow(L"Button", L"Stop", WS_CHILD | WS_VISIBLE, 650, 600, 75, 75, ghWnd, (HMENU)BTN_STOP, hInst, NULL);
	static_label = CreateWindow(L"static", L"/", WS_CHILD | WS_VISIBLE | SS_CENTER , 575, 540, 130, 40, ghWnd, (HMENU)STC_INFO, hInst, NULL);

	while (GetMessage(&msg, nullptr, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message,
	WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case BTN_START:
			if (stop_flag)
			{
				if (threads_exist)
				{
					ResumeThread(thread);
				}
				else
				{
					SendDlgItemMessage(hWnd, ID_LIST_ONE, LB_RESETCONTENT, NULL, NULL);
					thread = CreateThread(nullptr, NULL, LPTHREAD_START_ROUTINE(search), nullptr, NULL, nullptr);
					threads_exist = true;
				}
				stop_flag = false;
			}
			break;
		case BTN_STOP:
			if (!stop_flag)
			{
				SuspendThread(thread);
				stop_flag = TRUE;
			}
			break;
		default:
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}

DWORD WINAPI search(LPVOID lparam) {
	searchInKey(HKEY_LOCAL_MACHINE, L"");
	searchInKey(HKEY_CURRENT_CONFIG, L"");
	searchInKey(HKEY_CURRENT_USER, L"");
	searchInKey(HKEY_CLASSES_ROOT, L"");
	searchInKey(HKEY_USERS, L"");
	return 0;
}

void searchInKey(HKEY hRootKey, wstring path) {
	TCHAR keyName[4096];
	DWORD subkeysCount = 0;
	DWORD countValues = 0;
	HKEY hKey;
	FILETIME ftLastWriteTime;
	int retCode;

	if (RegOpenKeyEx(hRootKey, path.c_str(), 0, KEY_READ, &hKey) != ERROR_SUCCESS) {
		return;
	};
	RegQueryInfoKey(hKey, NULL, NULL,
		nullptr, &subkeysCount, NULL, NULL, &countValues, NULL, NULL, NULL, NULL);
	if (checkKey(hKey, countValues))
	{
		wstring keyFullPath = getKeyPath(hRootKey) + path;
		SendDlgItemMessage(ghWnd, ID_LIST_ONE, LB_ADDSTRING, NULL, LPARAM(keyFullPath.c_str()));
	}
	for (int i = 0; i < subkeysCount; i++) {
		DWORD keySize = sizeof(keyName);
		retCode = RegEnumKeyEx(hKey, i, keyName, &keySize, nullptr, nullptr, nullptr, &ftLastWriteTime);

		if (retCode == ERROR_SUCCESS) {
			wstring relativePath = path + keyName + L"\\";
			searchInKey(hRootKey, relativePath);
		}
	}
	TCHAR text[100];
	_stprintf_s(text, _T("%d / %d"), MatchedCounter, CheckedCounter);
	SetWindowText(static_label, text);
}

bool checkKey(HKEY hKey, DWORD values)
{
	CheckedCounter++;
	wstring clearStr;
	TCHAR keyName[4096];
	TCHAR valueName[4096];
	DWORD buffsize = 4096;
	DWORD Type = 0;
	wstring filnalKey;
	int retCode;
	for (int i = 0; i < values; i++)
	{
		retCode = RegEnumValue(hKey, i, keyName, &buffsize, nullptr, &Type, LPBYTE(valueName), &buffsize);
		if (retCode != ERROR_SUCCESS)
		{
			return false;
		}
		RegQueryValueEx(hKey, keyName, nullptr, &Type, LPBYTE(valueName), &buffsize);
		filnalKey = clearStr + valueName;
		if (regexFunc(filnalKey))
		{
			SendDlgItemMessage(ghWnd, ID_LIST_ONE, LB_ADDSTRING, NULL, LPARAM(valueName));
			MatchedCounter++;
			return true;
		}
	}
	return false;
}

bool regexFunc(wstring path)
{
	string s = string(path.begin(), path.end());
	string a = string(rgx.begin(), rgx.end());
	regex rx(a);
	if (regex_search(s.begin(), s.end(), rx))
	{
		if (GetFileAttributes(path.c_str()) != DWORD(-1))
		{
			return false;
		}
		return true;
	}
	return false;
}


wstring getKeyPath(HKEY hRootKey)
{
	if (hRootKey == HKEY_CLASSES_ROOT) {
		return L"HKCR\\";
	}
	if (hRootKey == HKEY_CURRENT_USER) {
		return L"HKCU\\";
	}
	if (hRootKey == HKEY_LOCAL_MACHINE) {
		return L"HKLM\\";
	}
	if (hRootKey == HKEY_USERS) {
		return L"HKU\\";
	}
	if (hRootKey == HKEY_CURRENT_CONFIG) {
		return L"HKCC\\";
	}
	return L"";
}