#include <windows.h>
#include <tchar.h>
#include <ctime>DrawRectangle

#define WND_CLASS_NAME _T("WindowClass")
#define CURSOR_PATH _T("cursors/cursor.cur")
#define ICON_PATH _T("icons/icon.ico")

#define BTN_START			120
#define BTN_STOP			121


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD WINAPI  RectDrawLoop(LPVOID);
void DrawRectangle(HWND, int, int, HBRUSH);
void ClearRectangle(HWND, int, int);

HINSTANCE hInst;
HANDLE threads[4];
HWND hWnd;
bool threadsLaunched = false;
bool threadsStopped = true;
struct rect {
	double id = 0;
	HBRUSH color;
}first, second, third, fourth;




INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int ncmdshow)
{
	srand(time(nullptr));
	MSG msg = { 0 };
	WNDCLASSW wc = { 0 };
	wc.hbrBackground = HBRUSH(CreateSolidBrush(RGB(50, 50, 50)));
	wc.hCursor = LoadCursorFromFile(CURSOR_PATH);
	wc.hInstance = hInstance;
	wc.lpszClassName = WND_CLASS_NAME;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.hIcon = HICON(LoadImage(nullptr, ICON_PATH, IMAGE_ICON, 0, 0,
		LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_SHARED));

	if (!RegisterClassW(&wc))
	{
		return -1;
	}

	HWND hWnd = CreateWindow(WND_CLASS_NAME, _T("WINDOW"), WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, 1280, 720,
		NULL, NULL, NULL, NULL);

	CreateWindow(L"Button", L"Start", WS_CHILD | WS_VISIBLE, 600, 620, 80, 25, hWnd, HMENU(BTN_START), hInst, NULL);
	CreateWindow(L"Button", L"Stop", WS_CHILD | WS_VISIBLE, 600, 650, 80, 25, hWnd, HMENU(BTN_STOP), hInst, NULL);

	while (GetMessage(&msg, nullptr, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HBRUSH orange = CreateSolidBrush(RGB(255, 125, 0));
	HBRUSH white = CreateSolidBrush(RGB(230, 230, 230));
	HBRUSH light = CreateSolidBrush(RGB(128, 128, 128));
	HBRUSH dark = CreateSolidBrush(RGB(16, 16, 16));

	rect *firstRef, *secondRef, *thirdRef, *fourthRef;

	firstRef = &first;
	secondRef = &second;
	thirdRef = &third;
	fourthRef = &fourth;

	firstRef->color = orange;
	secondRef->color = light;
	thirdRef->color = white;
	fourthRef->color = dark;

	firstRef->id = 1;
	secondRef->id = 2;
	thirdRef->id = 3;
	fourthRef->id = 4;

	switch (message) {
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case BTN_START:
			if (threadsStopped)
			{
				if (threadsLaunched)
				{
					ResumeThread(threads[0]);
					ResumeThread(threads[1]);
					ResumeThread(threads[2]);
					ResumeThread(threads[3]);
				}
				else
				{
					threads[0] = CreateThread(nullptr, NULL, LPTHREAD_START_ROUTINE(RectDrawLoop), firstRef, NULL, nullptr);
					Sleep(50);
					threads[1] = CreateThread(nullptr, NULL, LPTHREAD_START_ROUTINE(RectDrawLoop), secondRef, NULL, nullptr);
					Sleep(50);
					threads[2] = CreateThread(nullptr, NULL, LPTHREAD_START_ROUTINE(RectDrawLoop), thirdRef, NULL, nullptr);
					Sleep(50);
					threads[3] = CreateThread(nullptr, NULL, LPTHREAD_START_ROUTINE(RectDrawLoop), fourthRef, NULL, nullptr);
					threadsLaunched = true;
				}
				threadsStopped = false;
			}
			break;
		case BTN_STOP:
			if (!threadsStopped)
			{
				SuspendThread(threads[0]);
				SuspendThread(threads[1]);
				SuspendThread(threads[2]);
				SuspendThread(threads[3]);
				threadsStopped = TRUE;
			}
			break;
		default:
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}
DWORD WINAPI  RectDrawLoop(LPVOID lparam)
{
	rect *up = static_cast<rect*>(lparam);
	int x = 100, y = 50;
	while (true)
	{
		for (int i = 0; i < up->id; i++)
		{
			x = rand() % 1050 + 100;
			y = rand() % 500 + 50;
		}
		DrawRectangle(hWnd, x, y, up->color);
		Sleep(500);
		ClearRectangle(hWnd, x, y);
		Sleep(500);
	}
}
void DrawRectangle(HWND hWnd, int x, int y, HBRUSH color)
{
	PAINTSTRUCT ps;
	BeginPaint(hWnd, &ps);
	HDC hdc = GetDC(hWnd);

	SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(0, 0, 0)));
	SelectObject(hdc, color);

	Rectangle(hdc, x - 40, y - 30, x + 40, y + 30);

	EndPaint(hWnd, &ps);
	DeleteDC(hdc);
}

void ClearRectangle(HWND hWnd, int x, int y)
{
	PAINTSTRUCT ps;
	BeginPaint(hWnd, &ps);
	HDC hdc = GetDC(hWnd);

	SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(50, 50, 50)));
	SelectObject(hdc, CreateSolidBrush(RGB(50, 50, 50)));

	Rectangle(hdc, x - 22, y - 12, x + 22, y + 12);

	EndPaint(hWnd, &ps);
	DeleteDC(hdc);
}