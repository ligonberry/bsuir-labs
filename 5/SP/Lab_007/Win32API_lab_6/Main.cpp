#include <windows.h>
#include <tchar.h>

#define WND_CLASS_NAME _T("WindowClass")
#define CURSOR_PATH _T("cursors/cursor.cur")
#define ICON_PATH _T("icons/icon.ico")


#define LISTBOX_LEFT 121
#define LISTBOX_RIGHT 122

#define EDIT_NEW 123
#define BUTTON_ADD 124

#define BUTTON_MOVE 125

#define BUTTON_DELETE_LEFT 126
#define BUTTON_DELETE_RIGHT 127

#define BUTTON_CLEAR 128



LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

void DeleteSelectedItemFromListBox(HWND hWnd, UINT listId);
BOOL PresentInListBox(HWND hWnd, UINT listId, LPWSTR str);


const int BUFFER_SIZE = 1024;
TCHAR buffer[BUFFER_SIZE];


INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int ncmdshow)
{
	HWND hWnd;
	MSG msg = { 0 };
	WNDCLASSW wc = { 0 };
	wc.hbrBackground = HBRUSH(CreateSolidBrush(RGB(50, 50, 50)));
	wc.hCursor = LoadCursorFromFile(CURSOR_PATH);
	wc.hInstance = hInstance;
	wc.lpszClassName = WND_CLASS_NAME;
	wc.lpfnWndProc = WndProc;
	wc.hIcon = HICON(LoadImage(NULL, ICON_PATH, IMAGE_ICON, 0, 0,
		LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_SHARED));

	if (!RegisterClassW(&wc))
	{
		return -1;
	}

	hWnd = CreateWindow(WND_CLASS_NAME, _T("WINDOW"), WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, 900, 600,
		NULL, NULL, NULL, NULL);

	CreateWindow(L"listbox", L"listbox_l", WS_CHILD | WS_VISIBLE | LBS_EXTENDEDSEL | WS_BORDER | WS_VSCROLL,
		200, 100, 200, 290, hWnd, HMENU(LISTBOX_LEFT), hInstance, NULL);
	CreateWindow(L"listbox", L"listbox_r", WS_CHILD | WS_VISIBLE | LBS_EXTENDEDSEL | WS_BORDER | WS_VSCROLL,
		500, 100, 200, 290, hWnd, HMENU(LISTBOX_RIGHT), hInstance, NULL);
	CreateWindow(L"button", L">", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		400, 100, 100, 289, hWnd, HMENU(BUTTON_MOVE), hInstance, NULL);

	CreateWindow(L"edit", L"", WS_BORDER | WS_VISIBLE | WS_CHILD | ES_LEFT | ES_MULTILINE,
		100, 100, 100, 30, hWnd, HMENU(EDIT_NEW), hInstance, NULL);
	CreateWindow(L"button", L"Add", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		100, 130, 100, 30, hWnd, HMENU(BUTTON_ADD), hInstance, NULL);

	CreateWindow(L"button", L"Delete", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		200, 390, 200, 60, hWnd, HMENU(BUTTON_DELETE_LEFT), hInstance, NULL);
	CreateWindow(L"button", L"Delete", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		500, 390, 200, 60, hWnd, HMENU(BUTTON_DELETE_RIGHT), hInstance, NULL);

	CreateWindow(L"button", L"Clear", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		400, 390, 100, 60, hWnd, HMENU(BUTTON_CLEAR), hInstance, NULL);



	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_LBUTTONDOWN:
	{
		break;
	}
	case WM_COMMAND:
		switch (LOWORD(wParam)) {

		case BUTTON_ADD:
			GetDlgItemText(hWnd, EDIT_NEW, buffer, BUFFER_SIZE);
			if (!PresentInListBox(hWnd, LISTBOX_LEFT, buffer))
			{
				SendDlgItemMessage(hWnd, LISTBOX_LEFT, LB_ADDSTRING, 0, LPARAM(buffer));
				SetDlgItemText(hWnd, EDIT_NEW, _T(""));
			}
			break;

		case BUTTON_MOVE:
		{
			int selectedItems[BUFFER_SIZE];

			int selectedCount = SendDlgItemMessage(hWnd, LISTBOX_LEFT, LB_GETSELCOUNT, 0, 0);
			SendDlgItemMessage(hWnd, LISTBOX_LEFT, LB_GETSELITEMS, BUFFER_SIZE, LPARAM(selectedItems));

			for (int i = selectedCount - 1; i >= 0; i--)
			{
				SendDlgItemMessage(hWnd, LISTBOX_LEFT, LB_GETTEXT, selectedItems[i], LPARAM(buffer));
				if (!PresentInListBox(hWnd, LISTBOX_RIGHT, buffer))
				{
					SendDlgItemMessage(hWnd, LISTBOX_RIGHT, LB_ADDSTRING, NULL, LPARAM(buffer));
				}
			}
			break;
		}

		case BUTTON_DELETE_LEFT:
			DeleteSelectedItemFromListBox(hWnd, LISTBOX_LEFT);
			break;

		case BUTTON_DELETE_RIGHT:
			DeleteSelectedItemFromListBox(hWnd, LISTBOX_RIGHT);
			break;

		case BUTTON_CLEAR:
			SendDlgItemMessage(hWnd, LISTBOX_LEFT, LB_RESETCONTENT, NULL, NULL);
			SendDlgItemMessage(hWnd, LISTBOX_RIGHT, LB_RESETCONTENT, NULL, NULL);
			break;

		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;

}


void DeleteSelectedItemFromListBox(HWND hWnd, UINT listId)
{
	int selectedItems[BUFFER_SIZE];

	int selectedCount = SendDlgItemMessage(hWnd, listId, LB_GETSELCOUNT, 0, 0);
	SendDlgItemMessage(hWnd, listId, LB_GETSELITEMS, BUFFER_SIZE, LPARAM(selectedItems));
	for (int i = selectedCount - 1; i >= 0; i--)
	{
		SendDlgItemMessage(hWnd, listId, LB_DELETESTRING, selectedItems[i], NULL);
	}
}

BOOL PresentInListBox(HWND hWnd, UINT listId, LPWSTR str)
{
	TCHAR itemText[BUFFER_SIZE];
	bool presentFlag = FALSE;

	int count = SendDlgItemMessage(hWnd, listId, LB_GETCOUNT, NULL, NULL);

	for (int i = 0; i < count; i++)
	{
		SendDlgItemMessage(hWnd, listId, LB_GETTEXT, i, LPARAM(itemText));
		if (_tcscmp(itemText, str) == 0)
		{
			presentFlag = TRUE;
		}
	}
	return presentFlag;
}