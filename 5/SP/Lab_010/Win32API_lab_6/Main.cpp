#include <windows.h>
#include <tchar.h>

#define WND_CLASS_NAME _T("WindowClass")
#define CURSOR_PATH _T("cursors/cursor.cur")
#define ICON_PATH _T("icons/icon.ico")

#define CLR_RED			120
#define CLR_GREEN		121
#define CLR_BLUE		122

#define FGR_RHOMBUS		130
#define FGR_SQUARE		131
#define FGR_CIRCLE		132
#define FGR_STAR		133
#define WND_DRAWCHECK	140

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HINSTANCE hInst;
HWND hRadioRed, hRadioBlue, hRadioGreen;
HWND hRadioSquare, hRadioRhombus, hRadioStar, hRadioCircle;
HWND hDrawCheck;
UINT colorMsg, figureMsg, drawMsg;
bool checker = false;


INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int ncmdshow)
{
	MSG msg = { 0 };
	WNDCLASSW wc = { 0 };
	wc.hbrBackground = HBRUSH(CreateSolidBrush(RGB(50, 50, 50)));
	wc.hCursor = LoadCursorFromFile(CURSOR_PATH);
	wc.hInstance = hInstance;
	wc.lpszClassName = WND_CLASS_NAME;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.hIcon = HICON(LoadImage(nullptr, ICON_PATH, IMAGE_ICON, 0, 0,
		LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_SHARED));

	if (!RegisterClassW(&wc))
	{
		return -1;
	}

	CreateWindow(WND_CLASS_NAME, _T("Controller"), WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, 375, 275,
		NULL, NULL, NULL, NULL);

	colorMsg = RegisterWindowMessage(L"MessageColor");
	figureMsg = RegisterWindowMessage(L"MessageFigure");
	drawMsg = RegisterWindowMessage(L"MessageDraw");


	while (GetMessage(&msg, nullptr, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmInfoId;
	int index = 0;
	switch (message) {
	case WM_COMMAND:
		wmInfoId = LOWORD(wParam);
		if (wmInfoId == CLR_RED || wmInfoId == CLR_BLUE || wmInfoId == CLR_GREEN)
		{
			SendMessage(hRadioRed, BM_SETCHECK, false, NULL);
			SendMessage(hRadioBlue, BM_SETCHECK, false, NULL);
			SendMessage(hRadioGreen, BM_SETCHECK, false, NULL);
			if (wmInfoId == CLR_RED)
			{
				SendMessage(hRadioRed, BM_SETCHECK, true, NULL);
				index = 1;
			}
			else if (wmInfoId == CLR_GREEN)
			{
				SendMessage(hRadioGreen, BM_SETCHECK, true, NULL);
				index = 2;
			}
			else if (wmInfoId == CLR_BLUE)
			{
				SendMessage(hRadioBlue, BM_SETCHECK, true, NULL);
				index = 3;
			}

			PostMessage(HWND_BROADCAST, colorMsg, NULL, index);
		}
		else
		{
			if (wmInfoId == FGR_SQUARE || wmInfoId == FGR_RHOMBUS || wmInfoId == FGR_STAR || wmInfoId == FGR_CIRCLE)
			{
				SendMessage(hRadioSquare, BM_SETCHECK, false, NULL);
				SendMessage(hRadioRhombus, BM_SETCHECK, false, NULL);
				SendMessage(hRadioStar, BM_SETCHECK, false, NULL);
				SendMessage(hRadioCircle, BM_SETCHECK, false, NULL);
				if (wmInfoId == FGR_RHOMBUS)
				{
					SendMessage(hRadioRhombus, BM_SETCHECK, true, NULL);
					index = 1;
				}
				
				else if (wmInfoId == FGR_SQUARE)
				{
					SendMessage(hRadioSquare, BM_SETCHECK, true, NULL);
					index = 2;
				}
				else if (wmInfoId == FGR_CIRCLE)
				{
					SendMessage(hRadioCircle, BM_SETCHECK, true, NULL);
					index = 3;
				}
				else if (wmInfoId == FGR_STAR)
				{
					SendMessage(hRadioStar, BM_SETCHECK, true, NULL);
					index = 4;
				}

				PostMessage(HWND_BROADCAST, figureMsg, NULL, index);
			}
			else
			{
				if (wmInfoId == WND_DRAWCHECK)
				{
					checker = !checker;
					SendMessage(hDrawCheck, BM_SETCHECK, checker, NULL);
					PostMessage(HWND_BROADCAST, drawMsg, NULL, checker);
				}
			}
		}
		break;
	case WM_CREATE:
		hRadioRed = CreateWindow(L"Button", L"Red", WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
			50, 50, 100, 28, hWnd, HMENU(CLR_RED), hInst, NULL);
		hRadioGreen = CreateWindow(L"Button", L"Green", WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
			50, 80, 100, 29, hWnd, HMENU(CLR_GREEN), hInst, NULL);
		hRadioBlue = CreateWindow(L"Button", L"Blue", WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
			50, 110, 100, 28, hWnd, HMENU(CLR_BLUE), hInst, NULL);

		hRadioRhombus = CreateWindow(L"Button", L"Rhombus", WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
			200, 50, 100, 20, hWnd, HMENU(FGR_RHOMBUS), hInst, NULL);
		hRadioSquare = CreateWindow(L"Button", L"Square", WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
			200, 73, 100, 20, hWnd, HMENU(FGR_SQUARE), hInst, NULL);
		hRadioCircle = CreateWindow(L"Button", L"Circle", WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
			200, 96, 100, 21, hWnd, HMENU(FGR_CIRCLE), hInst, NULL);
		hRadioStar = CreateWindow(L"Button", L"Star", WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
			200, 119, 100, 20, hWnd, HMENU(FGR_STAR), hInst, NULL);

		hDrawCheck = CreateWindow(L"Button", L"Draw", WS_CHILD | WS_VISIBLE | BS_CHECKBOX | BS_CENTER,
			50, 145, 250, 25, hWnd, HMENU(WND_DRAWCHECK), hInst, NULL);

		SendMessage(hRadioRed, BM_SETCHECK, true, NULL);
		SendMessage(hRadioRhombus, BM_SETCHECK, true, NULL);
		SendMessage(hDrawCheck, BM_SETCHECK, false, NULL);

		PostMessage(HWND_BROADCAST, colorMsg, NULL, 1);
		PostMessage(HWND_BROADCAST, figureMsg, NULL, 1);
		PostMessage(HWND_BROADCAST, drawMsg, NULL, false);

		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}
