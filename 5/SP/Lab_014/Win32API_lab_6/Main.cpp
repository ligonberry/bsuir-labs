#include <windows.h>
#include <tchar.h>
#include <vector>
#include <TlHelp32.h>
using namespace std;

#define WND_CLASS_NAME _T("WindowClass")
#define CURSOR_PATH _T("cursors/cursor.cur")
#define ICON_PATH _T("icons/icon.ico")

#define LIST_ONE		121
#define LIST_TWO		122

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

void LoadProcesses(HWND);
void LoadModules(HWND);
void FillListboxWithModules(HWND, DWORD);
void SetPriority(HWND, DWORD);
void GetPriority(DWORD);
void Menu(HWND, LPARAM);
WCHAR* str;
HMENU hMenu;
int index;
HINSTANCE hInst;
HANDLE hProcess;
vector<PROCESSENTRY32> vProc;
int value = -1;



INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int ncmdshow)
{
	MSG msg = { 0 };
	WNDCLASS wc = { 0 };

	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.lpszMenuName = nullptr;
	wc.hbrBackground = HBRUSH(CreateSolidBrush(RGB(50, 50, 50)));
	wc.hCursor = LoadCursorFromFile(CURSOR_PATH);
	wc.hInstance = hInstance;
	wc.lpszClassName = WND_CLASS_NAME;
	wc.lpfnWndProc = WndProc;
	wc.hIcon = HICON(LoadImage(nullptr, ICON_PATH, IMAGE_ICON, 0, 0,
		LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_SHARED));

	if (!RegisterClassW(&wc))
	{
		return -1;
	}

	CreateWindow(WND_CLASS_NAME, _T("WINDOW"), WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, 610, 500,
		NULL, NULL, NULL, NULL);

	while (GetMessage(&msg, nullptr, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message,
	WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_CREATE:
		CreateWindow(L"listbox", L"list_one", WS_CHILD | WS_VISIBLE | LBS_EXTENDEDSEL | WS_VSCROLL | WS_HSCROLL | WS_BORDER | LBS_NOTIFY,
			50, 20, 500, 250, hWnd, HMENU(LIST_ONE), hInst, NULL);
		CreateWindow(L"listbox", L"list_two", WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | WS_BORDER | LBS_NOTIFY,
			50, 270, 500, 150, hWnd, HMENU(LIST_TWO), hInst, NULL);
		SendDlgItemMessage(hWnd, LIST_ONE, LB_RESETCONTENT, NULL, NULL);
		LoadProcesses(hWnd);
		break;
	case WM_CONTEXTMENU:
		Menu(hWnd, lParam);
		break;
	case WM_COMMAND:
	{
		SendDlgItemMessage(hWnd, LIST_TWO, LB_RESETCONTENT, NULL, NULL);
		LoadModules(hWnd);
		if (DWORD(wParam) == IDLE_PRIORITY_CLASS || DWORD(wParam) == NORMAL_PRIORITY_CLASS ||
			DWORD(wParam) == HIGH_PRIORITY_CLASS || DWORD(wParam) == REALTIME_PRIORITY_CLASS ||
			DWORD(wParam) == ABOVE_NORMAL_PRIORITY_CLASS || DWORD(wParam) == BELOW_NORMAL_PRIORITY_CLASS)
		{
			SetPriority(hWnd, LOWORD(wParam));
			LoadProcesses(hWnd);
		}
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void LoadProcesses(HWND hWnd)
{
	PROCESSENTRY32 proc;
	TCHAR szBuff[1024];
	vProc.clear();
	HANDLE CONST hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == hSnapshot) {
		return;
	}
	proc.dwSize = sizeof(PROCESSENTRY32);
	Process32First(hSnapshot, &proc);
	do {
		vProc.push_back(proc);
		GetPriority(proc.pcPriClassBase);
		wsprintf(szBuff, L"%s    |  Priority = %s\r\n", proc.szExeFile, str);
		SendDlgItemMessage(hWnd, LIST_ONE, LB_ADDSTRING, 0, LPARAM(szBuff));
	} while (Process32Next(hSnapshot, &proc));

	CloseHandle(hSnapshot);
}

void LoadModules(HWND hWnd)
{
	index = SendDlgItemMessage(hWnd, LIST_ONE, LB_GETCURSEL, 0, 0);
	if (index != LB_ERR)
	{
		hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, vProc[index].th32ProcessID);
		FillListboxWithModules(hWnd, vProc[index].th32ProcessID);
	}
}


void FillListboxWithModules(HWND hWnd, DWORD id)
{
	HANDLE CONST hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, id);
	MODULEENTRY32 module;

	if (hSnapshot == INVALID_HANDLE_VALUE) return;

	module.dwSize = sizeof(MODULEENTRY32);
	Module32First(hSnapshot, &module);
	do
	{
		SendDlgItemMessage(hWnd, LIST_TWO, LB_ADDSTRING, 0, LPARAM(module.szModule));
	} while (Module32Next(hSnapshot, &module));

	CloseHandle(hSnapshot);
}

void GetPriority(DWORD id)
{
	switch (id)
	{
	case 4:
		str = L"Low";
		break;
	case 6:
		str = L"Below normal";
		break;
	case 8:
		str = L"Normal";
		break;
	case 9:
		str = L"Normal";
		break;
	case 10:
		str = L"Above normal";
		break;
	case 11:
		str = L"Above normal";
		break;
	case 13:
		str = L"High";
		break;
	case 24:
		str = L"Realtime";
		break;
	default:
		str = L"N/A";
		break;
	}
}

void SetPriority(HWND hWnd, DWORD priority)
{
	SetPriorityClass(hProcess, priority);
	SendDlgItemMessage(hWnd, LIST_ONE, LB_RESETCONTENT, NULL, NULL);

	SendDlgItemMessage(hWnd, LIST_TWO, LB_RESETCONTENT, NULL, NULL);
}

void Menu(HWND hWnd, LPARAM lParam)
{
	index = SendDlgItemMessage(hWnd, LIST_ONE, LB_GETCURSEL, 0, 0);
	if (index != LB_ERR)
	{
		hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, vProc[index].th32ProcessID);
		value = GetPriorityClass(hProcess);
	}
	hMenu = CreatePopupMenu();
	AppendMenu(hMenu, value == BELOW_NORMAL_PRIORITY_CLASS ? MF_DISABLED : 0, BELOW_NORMAL_PRIORITY_CLASS, L"Below normal");
	AppendMenu(hMenu, value == NORMAL_PRIORITY_CLASS ? MF_DISABLED : 0, NORMAL_PRIORITY_CLASS, L"Normal");
	AppendMenu(hMenu, value == ABOVE_NORMAL_PRIORITY_CLASS ? MF_DISABLED : 0, ABOVE_NORMAL_PRIORITY_CLASS, L"Above normal");
	AppendMenu(hMenu, value == HIGH_PRIORITY_CLASS ? MF_DISABLED : 0, HIGH_PRIORITY_CLASS, L"High");

	TrackPopupMenu(hMenu, TPM_LEFTALIGN, LOWORD(lParam), HIWORD(lParam), 0, hWnd, nullptr);
	value = -1;
}
