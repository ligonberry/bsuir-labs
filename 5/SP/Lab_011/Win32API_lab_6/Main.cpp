#include <windows.h>
#include <tchar.h>

#define WND_CLASS_NAME _T("WindowClass")
#define CURSOR_PATH _T("cursors/cursor.cur")
#define ICON_PATH _T("icons/icon.ico")

#define CLR_RED			120
#define CLR_GREEN		121
#define CLR_BLUE		122

#define FGR_RHOMBUS		130
#define FGR_SQUARE		131
#define FGR_CIRCLE		132
#define FGR_STAR		133

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void DrawPicture(HWND, int, int, int, int);

HINSTANCE hInst;
UINT colorMsg, figureMsg, drawMsg;
bool allowedToDraw = false;
int xPos = 0;
int yPos = 0;


INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int ncmdshow)
{
	MSG msg = { 0 };
	WNDCLASSW wc = { 0 };
	wc.hbrBackground = HBRUSH(CreateSolidBrush(RGB(50, 50, 50)));
	wc.hCursor = LoadCursorFromFile(CURSOR_PATH);
	wc.hInstance = hInstance;
	wc.lpszClassName = WND_CLASS_NAME;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.hIcon = HICON(LoadImage(nullptr, ICON_PATH, IMAGE_ICON, 0, 0,
		LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_SHARED));

	if (!RegisterClassW(&wc))
	{
		return -1;
	}

	CreateWindow(WND_CLASS_NAME, _T("BOARD"), WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, 1280, 720,
		NULL, NULL, NULL, NULL);

	colorMsg = RegisterWindowMessage(L"MessageColor");
	figureMsg = RegisterWindowMessage(L"MessageFigure");
	drawMsg = RegisterWindowMessage(L"MessageDraw");

	while (GetMessage(&msg, nullptr, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int color = CLR_RED;
	static int figure = FGR_SQUARE;
	if (message == colorMsg)
	{
		if (lParam == 1)
		{
			color = CLR_RED;
		}
		else if (lParam == 2)
		{
			color = CLR_GREEN;
		}
		else if (lParam == 3)
		{
			color = CLR_BLUE;
		}
	}
	else if (message == figureMsg)
	{
		if (lParam == 1)
		{
			figure = FGR_RHOMBUS;
		}
		else if (lParam == 2)
		{
			figure = FGR_SQUARE;
		}
		else if (lParam == 3)
		{
			figure = FGR_CIRCLE;
		}
		else  if (lParam == 4)
		{
			figure = FGR_STAR;
		}
	}
	else if (message == drawMsg)
		{
			if (lParam)
			{
				allowedToDraw = true;
			}
			else
			{
				allowedToDraw = false;
			}
		}
	
	switch (message) {
	case WM_LBUTTONDOWN:
		xPos = LOWORD(lParam);
		yPos = HIWORD(lParam);
		InvalidateRect(hWnd, nullptr, FALSE);
		break;
	case WM_PAINT:
		DrawPicture(hWnd, xPos, yPos, color, figure);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}

void DrawPicture(HWND hWnd, int x, int y, int color, int figureId)
{
	PAINTSTRUCT ps;
	HBRUSH hBrush = CreateSolidBrush(RGB(255, 0, 0));
	HDC hdc = BeginPaint(hWnd, &ps);
	CreateCompatibleDC(hdc);

	HBRUSH red = CreateSolidBrush(RGB(255, 0, 0));
	HBRUSH green = CreateSolidBrush(RGB(0, 255, 0));
	HBRUSH blue = CreateSolidBrush(RGB(0, 0, 255));

	if (allowedToDraw)
	{
		if (color == CLR_RED)
		{
			hBrush = red;
		}
		else if (color == CLR_GREEN)
			{
				hBrush = green;
			}
		else if (color == CLR_BLUE)
			{
				hBrush = blue;
			}
					
		SelectObject(hdc, CreatePen(PS_SOLID, 1, RGB(0, 0, 0)));
		SelectObject(hdc, hBrush);

		if (figureId == FGR_RHOMBUS)
		{
			POINT rhombus[4];
			rhombus[0].x = x;
			rhombus[0].y = y - 25;
			rhombus[1].x = x - 20;
			rhombus[1].y = y;
			rhombus[2].x = x;
			rhombus[2].y = y + 25;
			rhombus[3].x = x + 20;
			rhombus[3].y = y;
			Polygon(hdc, rhombus, 4);
		}
		
		else if (figureId == FGR_SQUARE)
		{
			POINT square[4];

			square[0].x = x - 20;
			square[0].y = y - 20;
			square[1].x = x + 20;
			square[1].y = y - 20;
			square[2].x = x + 20;
			square[2].y = y + 20;
			square[3].x = x - 20;
			square[3].y = y + 20;
			Polygon(hdc, square, 4);
		}
		else if (figureId == FGR_CIRCLE)
		{
			Ellipse(hdc, x - 20, y - 20, x + 20, y + 20);
		}
		else if (figureId == FGR_STAR)
		{
			POINT star[10];
			star[0].x = x;
			star[0].y = y - 20;
			star[1].x = x - 6;
			star[1].y = y - 5;
			star[2].x = x - 20;
			star[2].y = y - 5;
			star[3].x = x - 10;
			star[3].y = y + 5;
			star[4].x = x - 12;
			star[4].y = y + 20;
			star[5].x = x;
			star[5].y = y + 10;
			star[6].x = x + 12;
			star[6].y = y + 20;
			star[7].x = x + 10;
			star[7].y = y + 5;
			star[8].x = x + 20;
			star[8].y = y - 5;
			star[9].x = x + 6;
			star[9].y = y - 5;
			Polygon(hdc, star, 10);
		}
	}
}