#include <windows.h>
#include <tchar.h>

#define WND_CLASS_NAME _T("WindowClass")
#define CURSOR_PATH _T("cursors/cursor.cur")
#define ICON_PATH _T("icons/icon.ico")

#define LISTBOX 121


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD WINAPI  AddTextFirst(LPVOID);
DWORD WINAPI  AddTextSecond(LPVOID);

HWND ghWnd;
HANDLE g_hEventInitComplete = nullptr;
TCHAR text_one[6] = L"first";
TCHAR text_two[7] = L"second";


INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int ncmdshow)
{
	MSG msg = { 0 };
	WNDCLASS wc = { 0 };

	wc.hbrBackground = HBRUSH(CreateSolidBrush(RGB(50, 50, 50)));
	wc.hCursor = LoadCursorFromFile(CURSOR_PATH);
	wc.hInstance = hInstance;
	wc.lpszClassName = WND_CLASS_NAME;
	wc.lpfnWndProc = WndProc;
	wc.hIcon = HICON(LoadImage(nullptr, ICON_PATH, IMAGE_ICON, 0, 0,
		LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_SHARED));

	if (!RegisterClassW(&wc))
	{
		return -1;
	}

	ghWnd = CreateWindow(WND_CLASS_NAME, _T("WINDOW"), WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, 400, 600,
		NULL, NULL, NULL, NULL);


	CreateWindow(L"listbox", L"listbox_l", WS_CHILD | WS_VISIBLE | LBS_EXTENDEDSEL | WS_BORDER | WS_VSCROLL,
		100, 100, 200, 290, ghWnd, HMENU(LISTBOX), hInstance, NULL);


	while (GetMessage(&msg, nullptr, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message,
	WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_CREATE:
		g_hEventInitComplete = CreateEvent(NULL, FALSE, FALSE, NULL);
		CreateThread(nullptr, NULL, LPTHREAD_START_ROUTINE(AddTextFirst), NULL, NULL, NULL);
		CreateThread(nullptr, NULL, LPTHREAD_START_ROUTINE(AddTextSecond), NULL, NULL, NULL);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}

DWORD WINAPI  AddTextFirst(LPVOID lparam)
{
	while (true)
	{
		WaitForSingleObject(g_hEventInitComplete, INFINITE);
		SendDlgItemMessage(ghWnd, LISTBOX, LB_ADDSTRING, 0, LPARAM(text_one));
		Sleep(1000);
		SetEvent(g_hEventInitComplete);
	}
}

DWORD WINAPI  AddTextSecond(LPVOID lparam)
{
	while (true)
	{
		SendDlgItemMessage(ghWnd, LISTBOX, LB_ADDSTRING, 0, LPARAM(text_two));
		Sleep(1000);
		SetEvent(g_hEventInitComplete);
		WaitForSingleObject(g_hEventInitComplete, INFINITE);
	}
}
