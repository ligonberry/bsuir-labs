#include <windows.h>
#include <tchar.h>

#define WND_CLASS_NAME _T("WindowClass")
#define CURSOR_PATH _T("cursors/cursor.cur")
#define ICON_PATH _T("icons/icon.ico")

#define TIMER 20
#define FILE_START 59
#define FILE_STOP 60

LPWSTR text = _T("<>");
POINT drawPoint = { 20, 20 };
INT timerStatus = 0;
INT speed = 20;
INT directionX = 1;
INT directionY = 1;
SIZE size;
HMENU hMenu;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void AddMenus(HWND);


INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int ncmdshow)
{
	MSG msg = { 0 };
	WNDCLASSW wc = { 0 };
	wc.hbrBackground = (HBRUSH)CreateSolidBrush(RGB(50, 50, 50));
	wc.hCursor = LoadCursorFromFile(CURSOR_PATH);
	wc.hInstance = hInstance;
	wc.lpszClassName = WND_CLASS_NAME;
	wc.lpfnWndProc = WndProc;
	wc.hIcon = (HICON)LoadImage(NULL, ICON_PATH, IMAGE_ICON, 0, 0,
		LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_SHARED);

	if (!RegisterClassW(&wc))
	{
		return -1;
	}

		CreateWindow(WND_CLASS_NAME, _T("WINDOW"), WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, 1280, 720,
		NULL, NULL, NULL, NULL);

	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		AddMenus(hWnd);
		break;
	case WM_LBUTTONDOWN:
	{
		if (timerStatus == 0)
		{
			timerStatus = 1;
			SetTimer(hWnd, TIMER, 20, nullptr);
		}
		else
		{
			timerStatus = 0;
			KillTimer(hWnd, TIMER);
		}
		break;
	}
	case WM_COMMAND:
		switch (wParam)
		{
		case FILE_START:
			timerStatus = 1;
			SetTimer(hWnd, TIMER, 20, nullptr);
			break;
		case FILE_STOP:
			timerStatus = 0;
			KillTimer(hWnd, TIMER);
			break;
		}
		break;
	case WM_TIMER:
	{
		RECT windowRect;
		GetWindowRect(hWnd, &windowRect);
		int width = windowRect.right - windowRect.left;
		if (drawPoint.x < 10 || drawPoint.x > width - 70) {
			directionX *= -1;
		}
		drawPoint.x += directionX * speed;

		int height = windowRect.bottom - windowRect.top;
		if (drawPoint.y < 10 || drawPoint.y > height - 115) {
			directionY *= -1;
		}
		drawPoint.y += directionY * speed;


		InvalidateRect(hWnd, NULL, TRUE);
		break;
	}
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);

		HFONT font = CreateFont(48, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));
		SelectObject(hdc, font);
		SetTextColor(hdc, RGB(200, 200, 200));
		SetBkMode(hdc, TRANSPARENT);
		TextOut(hdc, drawPoint.x, drawPoint.y, text, _tcsclen(text));
		DeleteObject(font);
		ReleaseDC(hWnd, hdc);
		EndPaint(hWnd, &ps);
		break;
	}
	case WM_DESTROY:
		KillTimer(hWnd, TIMER);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;

}

void AddMenus(HWND hWnd)
{
	hMenu = CreateMenu();

	HMENU hFileMenu = CreateMenu();
	AppendMenu(hFileMenu, MF_STRING, FILE_START, _T("Start"));
	AppendMenu(hFileMenu, MF_STRING, FILE_STOP, _T("Stop"));

	AppendMenu(hMenu, MF_POPUP, UINT_PTR(hFileMenu), _T("File"));

	SetMenu(hWnd, hMenu);
}
