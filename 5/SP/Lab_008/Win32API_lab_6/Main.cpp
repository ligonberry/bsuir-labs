#include <windows.h>
#include <tchar.h>
#include "resource.h"

#define WND_CLASS_NAME _T("WindowClass")
#define CURSOR_PATH _T("cursors/cursor.cur")
#define ICON_PATH _T("icons/icon.ico")

#define BTN_DRAW_ID  40
#define BTN_CLEAR_ID 41


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

void AddControls(HWND);
VOID DrawBitmap(HDC, int, int, HBITMAP);
void DrawPicture(HWND &hWnd);
VOID DrawButton(LPDRAWITEMSTRUCT);

int OffsetX = 614;
int OffsetY = 350;
HINSTANCE hInst;




INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int ncmdshow)
{
	hInst = hInstance;
	MSG msg = { 0 };
	WNDCLASSW wc = { 0 };
	wc.hbrBackground = HBRUSH(CreateSolidBrush(RGB(50, 50, 50)));
	wc.hCursor = LoadCursorFromFile(CURSOR_PATH);
	wc.hInstance = hInstance;
	wc.lpszClassName = WND_CLASS_NAME;
	wc.lpfnWndProc = WndProc;
	wc.hIcon = HICON(LoadImage(NULL, ICON_PATH, IMAGE_ICON, 0, 0,
		LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_SHARED));

	if (!RegisterClassW(&wc))
	{
		return -1;
	}

	HWND hWnd = CreateWindow(WND_CLASS_NAME, _T("WINDOW"), WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, 1280, 720,
		NULL, NULL, NULL, NULL);


	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
	{
		AddControls(hWnd);
		break;
	}
	case WM_COMMAND:
		switch (wParam)
		{
		case BTN_DRAW_ID:
		{

			DrawPicture(hWnd);
			break;
		}

		case BTN_CLEAR_ID:
		{
			InvalidateRect(hWnd, 0, TRUE);
			UpdateWindow(hWnd);
			break;
		}
		}
		break;
	case WM_DRAWITEM:
		DrawButton(LPDRAWITEMSTRUCT(lParam));
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;

}

void DrawPicture(HWND &hWnd) {
	PAINTSTRUCT ps;

	HDC hdc = GetDC(hWnd);
	HPEN hPenOld;
	HPEN hLinePen;
	COLORREF qLineColor;
	POINT poly[8];



	qLineColor = RGB(20, 10, 0);
	hLinePen = CreatePen(PS_SOLID, 6, qLineColor);
	HPEN(SelectObject(hdc, hLinePen));
	SelectObject(hdc, GetStockObject(HOLLOW_BRUSH));


	// nose

	poly[0].x = OffsetX - 45;
	poly[0].y = OffsetY + 50;
	poly[1].x = OffsetX + 45;
	poly[1].y = OffsetY + 50;
	poly[2].x = OffsetX + 57;
	poly[2].y = OffsetY + 70;
	poly[3].x = OffsetX + 20;
	poly[3].y = OffsetY + 110;
	poly[4].x = OffsetX - 20;
	poly[4].y = OffsetY + 110;

	poly[5].x = OffsetX - 20;
	poly[5].y = OffsetY + 110;
	poly[6].x = OffsetX - 57;
	poly[6].y = OffsetY + 70;
	poly[7].x = OffsetX - 45;
	poly[7].y = OffsetY + 50;


	Polyline(hdc, poly, 8);


	Ellipse(hdc, OffsetX - 40, OffsetY + 100, OffsetX - 10, OffsetY + 70);
	Ellipse(hdc, OffsetX + 40, OffsetY + 100, OffsetX + 10, OffsetY + 70);

	MoveToEx(hdc, OffsetX - 160, OffsetY - 0, NULL);
	LineTo(hdc, OffsetX - 85, OffsetY + 60);

	MoveToEx(hdc, OffsetX + 160, OffsetY - 0, NULL);
	LineTo(hdc, OffsetX + 85, OffsetY + 60);

	// bridge


	poly[0].x = OffsetX - 60;
	poly[0].y = OffsetY + 20;
	poly[1].x = OffsetX - 30;
	poly[1].y = OffsetY - 0;
	poly[2].x = OffsetX + 30;
	poly[2].y = OffsetY - 0;
	poly[3].x = OffsetX + 60;
	poly[3].y = OffsetY + 20;


	Polyline(hdc, poly, 4);


	//chin
	Rectangle(hdc, OffsetX - 34, OffsetY + 145, OffsetX + 34, OffsetY + 146);

	//ears
	MoveToEx(hdc, OffsetX - 80, OffsetY - 145, NULL);
	LineTo(hdc, OffsetX - 140, OffsetY - 200);
	LineTo(hdc, OffsetX - 250, OffsetY - 250);
	LineTo(hdc, OffsetX - 180, OffsetY - 100);

	MoveToEx(hdc, OffsetX + 80, OffsetY - 145, NULL);
	LineTo(hdc, OffsetX + 140, OffsetY - 200);
	LineTo(hdc, OffsetX + 250, OffsetY - 250);
	LineTo(hdc, OffsetX + 180, OffsetY - 100);


	//eyes

	qLineColor = RGB(200, 120, 30);
	hLinePen = CreatePen(PS_SOLID, 5, qLineColor);
	HPEN(SelectObject(hdc, hLinePen));

	Ellipse(hdc, OffsetX - 140, OffsetY - 60, OffsetX - 60, OffsetY - 0);
	Ellipse(hdc, OffsetX + 140, OffsetY - 60, OffsetX + 60, OffsetY - 0);




	MoveToEx(hdc, OffsetX - 65, OffsetY + 130, NULL);
	LineTo(hdc, OffsetX - 35, OffsetY + 180);
	LineTo(hdc, OffsetX - 35, OffsetY + 145);

	MoveToEx(hdc, OffsetX + 65, OffsetY + 130, NULL);
	LineTo(hdc, OffsetX + 35, OffsetY + 180);
	LineTo(hdc, OffsetX + 35, OffsetY + 145);



	qLineColor = RGB(20, 10, 0);
	hLinePen = CreatePen(PS_SOLID, 15, qLineColor);
	hPenOld = HPEN(SelectObject(hdc, hLinePen));


	MoveToEx(hdc, OffsetX - 145, OffsetY - 75, NULL);
	LineTo(hdc, OffsetX - 35, OffsetY - 40);

	MoveToEx(hdc, OffsetX + 145, OffsetY - 75, NULL);
	LineTo(hdc, OffsetX + 35, OffsetY - 40);

	Arc(hdc, OffsetX + 80, OffsetY - 50, OffsetX + 110, OffsetY - 20, OffsetX + 35, OffsetY - 40, OffsetX + 35, OffsetY - 40);
	Arc(hdc, OffsetX - 80, OffsetY - 50, OffsetX - 110, OffsetY - 20, OffsetX - 35, OffsetY - 40, OffsetX - 35, OffsetY - 40);



	SelectObject(hdc, hPenOld);
	DeleteObject(hLinePen);

	EndPaint(hWnd, &ps);
}


void AddControls(HWND hWnd)
{
	HWND hbtn_draw = CreateWindow(L"button", L"Draw", WS_CHILD | WS_VISIBLE | BS_OWNERDRAW,
		540, 600, 64, 64, hWnd, HMENU(BTN_DRAW_ID), NULL, NULL);

	HWND hbtn_erase = CreateWindow(L"button", L"Erase", WS_CHILD | WS_VISIBLE | BS_OWNERDRAW,
		624, 600, 64, 64, hWnd, HMENU(BTN_CLEAR_ID), NULL, NULL);

}

VOID DrawBitmap(HDC hDC, int x, int y, HBITMAP hBitmap) {
	HDC hMemDC;
	BITMAP bm;
	POINT  ptSize, ptOrg;

	hMemDC = CreateCompatibleDC(hDC);
	SelectObject(hMemDC, hBitmap);
	SetMapMode(hMemDC, GetMapMode(hDC));

	GetObject(hBitmap, sizeof(BITMAP), LPSTR(&bm));

	ptSize.x = bm.bmWidth;
	ptSize.y = bm.bmHeight;

	DPtoLP(hDC, &ptSize, 1);

	ptOrg.x = 0;
	ptOrg.y = 0;

	DPtoLP(hMemDC, &ptOrg, 1);

	BitBlt(hDC, x, y, ptSize.x, ptSize.y,
		hMemDC, ptOrg.x, ptOrg.y, SRCCOPY);

	DeleteDC(hMemDC);
}

VOID DrawButton(LPDRAWITEMSTRUCT lpInfo)
{

	HBITMAP hBitmap;
	int ResourceID;

	if (lpInfo->CtlType != ODT_BUTTON)
		return;

	switch (lpInfo->CtlID) {
	case BTN_DRAW_ID:

		ResourceID = IDB_DRAW;

		break;
	case BTN_CLEAR_ID:

		ResourceID = IDB_ERASE;

		break;
	default:
		return;
	}

	hBitmap = LoadBitmap(hInst, MAKEINTRESOURCE(ResourceID));

	if (!hBitmap) {
		return;
	};


	DrawBitmap(lpInfo->hDC, (lpInfo->rcItem).left, (lpInfo->rcItem).top, hBitmap);


	DeleteObject(hBitmap);
}
