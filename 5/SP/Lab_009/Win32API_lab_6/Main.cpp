#include <windows.h>
#include <tchar.h>
#include "resource.h"

#define WND_CLASS_NAME _T("WindowClass")
#define CURSOR_PATH _T("cursors/cursor.cur")
#define ICON_PATH _T("icons/icon.ico")

#define MY_TIMER	120
const int offset = 64;


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void LoadBitmaps(HINSTANCE);
void PrintBitmap(HWND);

HINSTANCE hInst;
HBITMAP bitmapArray[6];
int bitmapIndex = 0;
int xPos = 100;
int yPos = 100;
int xMouse = xPos;
int yMouse = yPos;
bool redraw = false;
int flipped = 1;


INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int ncmdshow)
{
	hInst = hInstance;
	MSG msg = { 0 };
	WNDCLASSW wc = { 0 };
	wc.hbrBackground = HBRUSH(CreateSolidBrush(RGB(50, 50, 50)));
	wc.hCursor = LoadCursorFromFile(CURSOR_PATH);
	wc.hInstance = hInstance;
	wc.lpszClassName = WND_CLASS_NAME;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.hIcon = HICON(LoadImage(nullptr, ICON_PATH, IMAGE_ICON, 0, 0,
		LR_LOADFROMFILE | LR_DEFAULTSIZE | LR_SHARED));

	if (!RegisterClassW(&wc))
	{
		return -1;
	}

	CreateWindow(WND_CLASS_NAME, _T("WINDOW"), WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, 1280, 720,
		NULL, NULL, NULL, NULL);

	LoadBitmaps(hInstance);
	while (GetMessage(&msg, nullptr, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_LBUTTONDOWN:
		xMouse = LOWORD(lParam);
		yMouse = HIWORD(lParam);
		if (xMouse < xPos)
		{
			flipped = -1;
		}
		else
		{
			flipped = 1;
		}
		xMouse += 16;
		yMouse -= 90;
		xMouse = xMouse / 20 * 20;
		yMouse = yMouse / 20 * 20;
		SetTimer(hWnd, MY_TIMER, 50, nullptr);
		break;
	case WM_TIMER:
	{
		redraw = false;
		if (xPos != xMouse)
		{
			xPos -= (xPos - xMouse) / abs(xPos - xMouse) * 20;
			redraw = true;
		}
		if (yPos != yMouse)
		{
			yPos -= (yPos - yMouse) / abs(yPos - yMouse) * 20;
			redraw = true;
		}
		if (redraw)
		{
			InvalidateRect(hWnd, nullptr, TRUE);
		}
		else
		{
			KillTimer(hWnd, MY_TIMER);
		}
		break;
	}
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	case WM_PAINT:
		PrintBitmap(hWnd);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}


void LoadBitmaps(HINSTANCE) {
	bitmapArray[0] = HBITMAP(LoadBitmap(hInst, MAKEINTRESOURCE(IDB_ICON0)));
	bitmapArray[1] = HBITMAP(LoadBitmap(hInst, MAKEINTRESOURCE(IDB_ICON1)));
	bitmapArray[2] = HBITMAP(LoadBitmap(hInst, MAKEINTRESOURCE(IDB_ICON2)));
	bitmapArray[3] = HBITMAP(LoadBitmap(hInst, MAKEINTRESOURCE(IDB_ICON3)));
	bitmapArray[4] = HBITMAP(LoadBitmap(hInst, MAKEINTRESOURCE(IDB_ICON4)));
	bitmapArray[5] = HBITMAP(LoadBitmap(hInst, MAKEINTRESOURCE(IDB_ICON5)));
}


void PrintBitmap(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	if (bitmapArray[bitmapIndex])
	{
		HDC hMemDC = CreateCompatibleDC(hdc);
		HBITMAP(SelectObject(hMemDC, bitmapArray[bitmapIndex]));
		StretchBlt(hdc, xPos - flipped*offset, yPos, flipped * 128, 128, hMemDC, 0, 0, 250, 250, SRCCOPY);
		DeleteDC(hMemDC);
	}
	bitmapIndex = (bitmapIndex + 1) % 6;
	EndPaint(hWnd, &ps);
}
