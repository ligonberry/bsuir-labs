import heapq
import numpy as np
from enum import Enum
from collections import deque
from lcg import LCG
from matplotlib import pyplot as plt


class EventType(Enum):
    NEW_REQUEST = 0
    HANDLE_REQUEST = 1


def int_sequence():
    num = 1
    while True:
        yield num
        num += 1


class Request:
    def __init__(self, request_number, start_time):
        self.request_number = request_number
        self.start_time = start_time
        self.system_end_time = start_time
        self.queue_end_time = start_time


class QueueingSystem:
    def __init__(self, n, m, lm, mu, p):
        self.n = n
        self.m = m
        self.lm = lm
        self.mu = mu
        self.p = p
        self.mu_p = self.mu * self.p
        self.ro = self.lm / self.mu_p

        self.free_channels = self.n
        self.events = []
        self.time = 0

        self.queue = deque(maxlen=self.m)
        self.number_generator = int_sequence()

        self.uniform_generator = LCG()

        self.request_count = 0
        self.rejected_count = 0
        self.accepted_count = 0
        self.busy_channels_density = 0
        self.queue_density = 0
        self.requests = []
        self.state_freq = [0] * (self.n + self.m + 1)
        self.states = []
        self.time_seq = []

    def generate_request(self):
        request_time = self.time + np.random.exponential(1 / self.lm)
        request_number = next(self.number_generator)
        request = Request(request_number, request_time)
        heapq.heappush(self.events, (request_time, EventType.NEW_REQUEST, request))

    def handle_request(self, request):
        self.free_channels -= 1
        print(
            f"  Начало обслуживания заявки #{request.request_number} "
            f"({self.n - self.free_channels}/{self.n} каналов занято)"
        )
        handle_time = self.time + np.random.exponential(1 / self.mu)
        heapq.heappush(self.events, (handle_time, EventType.HANDLE_REQUEST, request))

    def add_to_queue(self, request):
        print(f"  Попытка добавить заявку #{request.request_number} в очередь...")
        q_max = self.queue.maxlen
        q_len = len(self.queue)
        if q_len >= q_max:
            self.rejected_count += 1
            print(
                f"  Очередь занята ({q_len}/{q_max}). "
                f"Заявка #{request.request_number} покидает систему"
            )
        else:
            self.queue.append(request)
            print(
                f"  Заявка #{request.request_number} "
                f"добавлена в очередь ({len(self.queue)}/{q_max})"
            )

    def process_request(self, request):
        if self.free_channels > 0:
            self.handle_request(request)
        else:
            self.add_to_queue(request)

    def run(self, end_time=1000):
        self.generate_request()

        while True:
            prev_time = self.time
            self.time, event_type, request = heapq.heappop(self.events)
            delta_time = self.time - prev_time

            self.busy_channels_density += (self.n - self.free_channels) * delta_time
            self.queue_density += len(self.queue) * delta_time
            state = self.n - self.free_channels + len(self.queue)
            self.state_freq[state] += delta_time
            self.states.append(state)
            self.time_seq.append(self.time)

            if self.time > end_time:
                return

            print(
                f"Время: {self.time} -- Свободные каналы: {self.free_channels} -- "
                f"Мест в очереди: {self.queue.maxlen - len(self.queue)}"
            )

            if event_type == EventType.NEW_REQUEST:
                self.request_count += 1
                self.requests.append(request)
                print(f"  Получена новая заявка #{request.request_number}")
                self.process_request(request)
                self.generate_request()
            elif event_type == EventType.HANDLE_REQUEST:
                request.system_end_time = self.time
                self.free_channels += 1
                if next(self.uniform_generator) < self.p:
                    self.accepted_count += 1
                    print(
                        f"  Заявка #{request.request_number} обслужена "
                        f"({self.n - self.free_channels}/{self.n} каналов занято)"
                    )
                    if len(self.queue) > 0:
                        request = self.queue.pop()
                        request.queue_end_time = self.time
                        print(
                            f"  Заявка #{request.request_number} "
                            f"извлечена из очереди({len(self.queue)}/{self.queue.maxlen})"
                        )
                        self.handle_request(request)
                else:
                    print(
                        f"  Заявка #{request.request_number} не обслужена "
                        f"({self.n - self.free_channels}/{self.n} каналов занято)"
                    )
                    self.process_request(request)

            print("*" * 100)

    def p_0(self):
        first_sum = sum(
            (self.ro ** i) / np.math.factorial(i) for i in range(self.n + 1)
        )
        second_sum = sum(
            (self.ro ** (self.n + i)) / (np.math.factorial(self.n) * self.n ** i)
            for i in range(1, self.m + 1)
        )
        return 1 / (first_sum + second_sum)

    def p_i(self, i):
        if i > self.n + self.m:
            raise Exception("Invalid index")
        if i >= self.n:
            m = i - self.n
            n = self.n
        else:
            m = 0
            n = i
        return (self.p_0() * self.ro ** (n + m)) / ((n ** m) * np.math.factorial(n))

    def P_reject(self):
        return self.p_i(self.n + self.m)

    def P_reject_emp(self):
        return self.rejected_count / self.request_count

    def Q(self):
        return 1 - self.P_reject()

    def Q_emp(self):
        return 1 - self.P_reject_emp()

    def A(self):
        return self.lm * self.Q()

    def A_emp(self):
        return self.accepted_count / self.time

    def k(self):
        return self.A() / self.mu_p

    def k_emp(self):
        return self.busy_channels_density / self.time

    def L_q(self):
        return (
            self.p_0()
            * self.ro ** self.n
            * (
                self.ro / self.n
                - (self.m + 1) * (self.ro / self.n) ** (self.m + 1)
                + self.m * (self.ro / self.n) ** (self.m + 2)
            )
            / (np.math.factorial(self.n) * (1 - self.ro / self.n) ** 2)
        )

    def L_q_emp(self):
        return self.queue_density / self.time

    def L_s(self):
        return self.L_q() + self.k()

    def L_s_emp(self):
        return self.L_q_emp() + self.k_emp()

    def T_q(self):
        return self.L_q() / self.lm

    def T_q_emp(self):
        deltas = [r.queue_end_time - r.start_time for r in self.requests]
        return sum(deltas) / len(deltas)

    def T_s(self):
        return self.L_s() / self.lm

    def T_s_emp(self):
        deltas = [r.system_end_time - r.start_time for r in self.requests]
        return sum(deltas) / len(deltas)

    def show_all_chars(self):
        p_all = [self.p_i(i) for i in range(self.n + self.m + 1)]
        print(f"Предельные вероятности: {p_all}")
        print(
            f"Вероятность отказа: {self.P_reject()}, "
            f"эмпирическая: {self.P_reject_emp()}"
        )
        print(
            f"Относительная пропускная способность: {self.Q()}, "
            f"эмпирическая: {self.Q_emp()}"
        )
        print(
            f"Абсолютная пропускная способность: {self.A()}, "
            f"эмпирическая: {self.A_emp()}"
        )
        print(
            f"Среднее число занятых каналов: {self.k()}, "
            f"эмпирическое: {self.k_emp()}"
        )
        print(
            f"Среднее число заявок в очереди: {self.L_q()}, "
            f"эмпирическое: {self.L_q_emp()}"
        )
        print(
            f"Среднее число заявок в системе: {self.L_s()}, "
            f"эмпирическое: {self.L_s_emp()}"
        )
        print(
            f"Среднее время заявки в очереди: {self.T_q()}, "
            f"эмпирическое: {self.T_q_emp()}"
        )
        print(
            f"Среднее время заявки в системе: {self.T_s()}, "
            f"эмпирическое: {self.T_s_emp()}"
        )

    def freq_plot(self):
        plt.title("Вероятности/частоты состояний")
        state_count = self.n + self.m + 1
        plt.plot(
            [i for i in range(state_count)],
            [self.p_i(i) for i in range(state_count)],
            label="вероятность",
        )
        plt.plot(
            [i for i in range(state_count)],
            [f / sum(self.state_freq) for f in self.state_freq],
            label="частота",
        )
        plt.legend()
        axes = plt.gca()
        axes.set_xlim([None, None])
        axes.set_ylim([0, 1])
        plt.xticks(range(state_count))
        plt.show()

    def states_plot(self):
        plt.title("Состояния")
        plt.plot(self.time_seq, self.states)
        plt.show()

    def show_all_plots(self):
        self.freq_plot()
        self.states_plot()


def test_1():
    system = QueueingSystem(2, 3, 0.4, 0.5, 0.9)
    system.run()
    system.show_all_chars()
    system.show_all_plots()


def test_2():
    system = QueueingSystem(2, 1000, 0.4, 0.5, 0.9)
    system.run()
    system.show_all_chars()
    system.show_all_plots()


def test_3():
    system = QueueingSystem(20, 3, 0.4, 0.5, 0.9)
    system.run()
    system.show_all_chars()
    system.show_all_plots()


def test_4():
    system = QueueingSystem(2, 3, 0.5, 0.4, 0.9)
    system.run()
    system.show_all_chars()
    system.show_all_plots()


def test_5():
    system = QueueingSystem(2, 3, 0.4, 0.5, 0.1)
    system.run()
    system.show_all_chars()
    system.show_all_plots()


if __name__ == "__main__":
    test_1()
