import math
import random

import numpy as np
import scipy.fftpack
from loguru import logger
from matplotlib import pyplot as plt
from sympy import exp, integrate, limit
from sympy.abc import t, w

from util import StatisticUtils


NUM_INF = 10 ** 10000
LIMIT = 50


ACCURACY = 7


class ProcessGenerator:
    def __init__(self, D, alpha, tau, noise_sd, process_size):
        self.D = D
        self.alpha = alpha
        self.tau = tau

        self.noise_sd = noise_sd

        self.K_tay = self.D * (1 + self.alpha * abs(t)) * exp(-self.alpha * abs(t))

        self.time_line = np.arange(0, process_size, self.tau)
        self.size = len(self.time_line)

        self.theoretical_M = 0
        self.theoretical_D = self._round(self.correlation(0))

        self.x = []
        self.y = []

    @staticmethod
    def _round(param):
        return round(param, ACCURACY)

    def correlation(self, tau):
        return self.D * (1 + self.alpha * abs(tau)) * exp(-self.alpha * abs(tau))

    @staticmethod
    def is_ergodic(K_tay):
        # достаточное (но не необходимое) условие
        if limit(K_tay, t, np.inf) == 0:
            logger.debug("Процесс эргодичен")
            return True

    # noinspection PyPep8Naming
    @staticmethod
    def is_ergodic_by_Slutsky(K_tay):
        # условие Слуцкого (более слабое)
        T = np.inf
        integral = integrate(K_tay, (t, 0, NUM_INF))
        if limit(integral / T, t, T) == 0:
            logger.debug("Процесс эргодичен по отношению к среднему по времени")
            return True

    def S_y(self):
        # s = w / (2 * math.pi)
        # fourier_transform(self.K_tay, t, s) считает численно, поэтому не получается
        # красивая формула (только приблеженная)
        return (4 * (self.alpha ** 3) * self.D) / (self.alpha ** 2 + w ** 2) ** 2

    def f_y(self):
        # ψ(iw) - комплексно сопряженная функция для S_y
        return (self.alpha * (4 * self.alpha * self.D) ** 0.5) / (
            (self.alpha + 1j * w) ** 2
        )

    def S_x(self):
        # спектральная характеристика белого шума
        return self.noise_sd ** 2

    def f_x(self):
        # φ(iw) - комплексно сопряженная функция для S_x
        return self.noise_sd

    def W(self):
        # частотная характеристика фильтра
        # решив систему вида:
        #  | S_y(w) = |W(iw)|^2 * S_x(w)
        # <  S_x(w) = φ(iw) * φ(-iw)
        #  | S_y(w) = ψ(-iw) * ψ(iw)
        # получим W(iw) = ψ(iw) / φ(iw)
        return self.f_y() / self.f_x()

    def h(self):
        # импульсная характеристика
        # учитывая, что W(iw) - отображение функции h(t):
        # https://www.webmath.ru/poleznoe/table_preobrazovanie_laplasa.php
        return t * exp(-self.alpha * t) * self.W() * (1j * w + self.alpha) ** 2

    @staticmethod
    def get_h(tau):
        return 8.0 * tau * math.exp(-2 * tau)

    def generate_x_signal(self):
        self.x = [random.normalvariate(0, 1) for _ in range(self.size)]

    def generate_y_signal(self):
        # моделирование случайного процесса с заданной функцией коррелции
        # в аналоговом виде
        for j, x_j in enumerate(self.x):
            y_j = 0

            for i in range(0, j):
                y_j += self.x[j - i] * self.get_h(i * self.tau)

            self.y.append(self.tau * y_j)

    # noinspection PyPep8Naming
    def check_params(self):
        M = StatisticUtils.get_M(self.y)
        logger.debug(f"Оценка математического ожидания = {M}")
        logger.debug(
            f"Теоритическое значение математического ожидания = {self.theoretical_M}."
        )

        D = StatisticUtils.get_D(self.y)
        logger.debug(f"Смещенная оценка дисперсии = {D}")
        corrected_D = StatisticUtils.get_corrected_D(self.y)
        logger.debug(f"Несмещенная оценка дисперсии по выборке = " f"{corrected_D}")
        logger.debug(f"Теоритическое значение дисперсии = {self.theoretical_D}.")

        t_M_low, t_M_high = StatisticUtils.get_student_M_interval(self.y)
        logger.debug(
            f"{t_M_low} <= M <= {t_M_high} "
            f"с помощью параметра распределения Стьюдента (g = 0.95)"
        )
        D_low, D_high = StatisticUtils.get_D_interval(self.y)
        logger.debug(f"{D_low} <= D <= {D_high} с помощью моментов 4-ого порядка")

    def show_x_signal(self):
        plt.title("Входной сигнал: X")
        plt.plot(self.time_line, self.x)
        plt.show()

    def show_y_signal(self):
        plt.title("Выходной сигнал: Y")
        plt.plot(self.time_line, self.y)
        plt.show()

    def show_correlation_comparison(self):
        plt.title("Коррелограммы")
        t_corr = self.theoretical_correlation()
        e_corr = self.empirical_correlation()
        plt.plot(self.time_line[:LIMIT], t_corr[:LIMIT], label="теоретическая")
        plt.plot(self.time_line[:LIMIT], e_corr[:LIMIT], label="эмпирическая")
        plt.legend()
        plt.show()

    # noinspection PyPep8Naming
    def theoretical_correlation(self):
        r = np.array([self.correlation(tau) for tau in self.time_line])
        theoretical_D = self._round(self.correlation(0))
        r /= theoretical_D
        return r

    # noinspection PyPep8Naming
    def empirical_correlation(self):
        y = [y_i - self.theoretical_M for y_i in self.y]
        r = np.correlate(y, y, mode="full")[self.size - 1:]
        r /= (self.theoretical_D * self.size)
        return r

    def show_S_comparison(self):
        plt.title("Сравнение спектров")
        t_corr = self.theoretical_correlation()
        t_spectrum = self.get_S(t_corr[:LIMIT])
        e_corr = self.empirical_correlation()
        e_spectrum = self.get_S(e_corr[:LIMIT])
        plt.plot(self.time_line[:LIMIT], t_spectrum, label="теоретический")
        plt.plot(self.time_line[:LIMIT], e_spectrum, label="эмпирический")
        plt.legend()
        plt.show()

    @staticmethod
    def get_S(correlation):
        # Return discrete Fourier transform of real or complex sequence.
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.fftpack.fft.html
        spectrum = scipy.fftpack.fft(correlation)
        return np.abs(spectrum)

    def test_stationary(self):
        from statsmodels.tsa.stattools import adfuller

        # https://machinelearningmastery.com/time-series-data-stationary-python/
        # https://en.wikipedia.org/wiki/Dickey%E2%80%93Fuller_test
        # проверка на стационарность в узком смысле
        result = adfuller(self.y)
        logger.debug(f"Статистика теста ADF: {result[0]}")
        logger.debug(f"p-value: {result[1]}")
        logger.debug(f"Критические значения:")
        for key, value in result[4].items():
            logger.debug(f"{key} = {value}")

        if result[1] <= 0.05:
            logger.debug(f"Нулева гипотеза отклоняется. Процесс является стационарным.")
            return True
        logger.debug(
            f"Нулевая гипотеза не отклоняется. Процесс не является стационарным."
        )

    def solve(self):
        self.is_ergodic_by_Slutsky(self.K_tay)
        self.is_ergodic(self.K_tay)

        self.generate_x_signal()
        self.generate_y_signal()

        self.W()
        self.h()
        self.check_params()

        self.show_x_signal()
        self.show_y_signal()
        self.show_correlation_comparison()
        self.show_S_comparison()
        self.test_stationary()
        plt.show()


if __name__ == "__main__":
    D = 0.06
    alpha = 2
    noise_sd = 1
    process_size = 350
    tau = 0.03
    p = ProcessGenerator(D, alpha, tau, noise_sd, process_size)
    p.solve()
