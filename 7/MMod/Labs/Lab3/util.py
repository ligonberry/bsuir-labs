from scipy.stats import t, norm

ACCURACY = 7
EPS = 1e-16
SHAPE = (4, 4)


# noinspection PyPep8Naming
class StatisticUtils:
    def __init__(self):
        pass

    @staticmethod
    def _round(param):
        return round(param, ACCURACY)

    @staticmethod
    def get_M(values):
        return StatisticUtils._round(sum(values) / len(values))

    @staticmethod
    def get_D(values):
        M_value = StatisticUtils.get_M(values)
        D = []
        for value in values:
            D.append((value - M_value) ** 2)
        return StatisticUtils._round(sum(D) / len(values))

    @staticmethod
    def get_corrected_D(values):
        M_value = StatisticUtils.get_M(values)
        D = []
        for value in values:
            D.append((value - M_value) ** 2)
        return StatisticUtils._round(sum(D) / (len(values) - 1))

    @staticmethod
    def _get_eps(coefficient, D, values):
        return coefficient * (D / len(values)) ** 0.5

    @staticmethod
    def _get_t(values, alpha):
        return t.ppf(1 - alpha / 2, len(values) - 1)

    @staticmethod
    def get_student_M_interval(values, alpha=0.05):
        t_coefficient = StatisticUtils._get_t(values, alpha)

        M_value = StatisticUtils.get_M(values)
        D = StatisticUtils.get_corrected_D(values)

        eps = StatisticUtils._get_eps(t_coefficient, D, values)
        M_high = StatisticUtils._round(M_value + eps)
        M_low = StatisticUtils._round(M_value - eps)
        return M_low, M_high

    @staticmethod
    def get_D_interval(values, alpha=0.05):
        n = len(values)
        M_value = StatisticUtils.get_M(values)
        corrected_D = StatisticUtils.get_corrected_D(values)

        y_4 = sum([(value - M_value) ** 4 for value in values]) / (
            n * (corrected_D ** 2)
        )
        under_root = (y_4 - (n - 3) / (n - 1)) / n
        z = norm.ppf(1 - alpha / 2)
        D_low = StatisticUtils._round(corrected_D / (1 + z * under_root ** 0.5))
        D_high = StatisticUtils._round(corrected_D / (1 - z * under_root ** 0.5))
        return D_low, D_high
