from time import time


class Generator:
    """ В данном классе реализован линейный конгруэнтный метод
    для получения псевдослучайных чисел. """

    a = 6364136223846793005
    c = 1442695040888963407
    m = 2 ** 64

    def __init__(self, x=None):
        current_milli_time = int(time() * 1000)
        self.x = x if x else current_milli_time

    def __iter__(self):
        return self

    def __next__(self, a=0, b=1):
        self.x = (self.a * self.x + self.c) % self.m
        value = self.x / self.m
        return (b - a) * value + a
