import random

import numpy as np
from scipy.stats import t, chi2, norm

ACCURACY = 7


# noinspection PyPep8Naming
class StatisticUtils:
    def __init__(self):
        pass

    @staticmethod
    def _round(param):
        return round(param, ACCURACY)

    @staticmethod
    def get_M(values):
        return StatisticUtils._round(sum(values) / len(values))

    @staticmethod
    def get_D(values):
        M_value = StatisticUtils.get_M(values)
        D = []
        for value in values:
            D.append((value - M_value) ** 2)
        return StatisticUtils._round(sum(D) / len(values))

    @staticmethod
    def get_corrected_D(values):
        M_value = StatisticUtils.get_M(values)
        D = []
        for value in values:
            D.append((value - M_value) ** 2)
        return StatisticUtils._round(sum(D) / (len(values) - 1))

    @staticmethod
    def _get_eps(u, D, values):
        return u * (D / len(values)) ** 0.5

    @staticmethod
    def get_laplace_M_interval(values):
        u = 1.96  # g = 0.95

        M_value = StatisticUtils.get_M(values)
        D = StatisticUtils.get_corrected_D(values)

        eps = StatisticUtils._get_eps(u, D, values)
        M_high = StatisticUtils._round(M_value + eps)
        M_low = StatisticUtils._round(M_value - eps)
        return M_low, M_high

    @staticmethod
    def _get_t(values, alpha):
        return t.ppf(1 - alpha / 2, len(values) - 1)

    @staticmethod
    def get_student_M_interval(values, alpha=0.05):
        t_coefficient = StatisticUtils._get_t(values, alpha)

        M_value = StatisticUtils.get_M(values)
        D = StatisticUtils.get_corrected_D(values)

        eps = StatisticUtils._get_eps(t_coefficient, D, values)
        M_high = StatisticUtils._round(M_value + eps)
        M_low = StatisticUtils._round(M_value - eps)
        return M_low, M_high

    @staticmethod
    def get_D_interval(values, alpha=0.05):
        n = len(values)
        M_value = StatisticUtils.get_M(values)
        corrected_D = StatisticUtils.get_corrected_D(values)

        y_4 = sum([(value - M_value) ** 4 for value in values]) / (
            n * (corrected_D ** 2)
        )
        under_root = (y_4 - (n - 3) / (n - 1)) / n
        z = norm.ppf(1 - alpha / 2)
        D_low = StatisticUtils._round(corrected_D / (1 + z * under_root ** 0.5))
        D_high = StatisticUtils._round(corrected_D / (1 - z * under_root ** 0.5))
        return D_low, D_high

    @staticmethod
    def check_convergence_by_chi2(values, n, p, alpha=0.05):
        min_value = min(values)
        max_value = max(values)

        g = max_value - min_value + 1
        frequencies = np.zeros(g)
        for value in values:
            frequencies[value - min_value] += 1

        exp_frequencies = []
        for k in range(min_value, max_value + 1):
            exp_frequencies.append(p(k) * n)

        chi_2 = 0
        for i in range(g):
            chi_2 += (frequencies[i] - exp_frequencies[i]) ** 2 / exp_frequencies[i]

        interval = chi2.interval(1 - alpha, g - 1)
        return StatisticUtils._round(chi_2), interval

    @staticmethod
    def check_convergence_by_kolmogorov(values, n, F):
        k = 1.36  # g = 0.95

        sorted_values = np.sort(values)

        K_j_plus = []
        K_j_minus = []
        for j in range(n):
            K_j_plus.append(j / n - F(sorted_values[j]))
            K_j_minus.append(F(sorted_values[j]) - (j - 1) / n)

        K_n_plus = StatisticUtils._round(n ** 0.5 * max(K_j_plus))
        K_n_minus = StatisticUtils._round(n ** 0.5 * max(K_j_minus))

        return K_n_minus, K_n_plus, k

    @staticmethod
    def check_correlation(values):
        n = len(values)
        first = random.sample(values, int(n / 2))
        second = np.setdiff1d(values, first)

        cov_matrix = np.cov(np.array([first, second]))
        correlation = cov_matrix[0][1] / (cov_matrix[0][0] * cov_matrix[1][1]) ** 0.5
        return StatisticUtils._round(correlation)
