from enum import Enum
from math import log

import matplotlib.pyplot as plt
import numpy as np
from loguru import logger

from generator import Generator
from util import StatisticUtils

FIG_SIZE = (15, 5)
INTERVAL = np.arange(0, 1.1, 0.1)

ACCURACY = 7


class DRV:
    def __init__(self, m, D, F, p, values_count=10000):

        self.a = 0
        self.b = 20

        self.m = m
        self.D = D
        self.F = F
        self.p = p

        self.generator = Generator()

        self.values_count = values_count

        self.values = self.get_values(values_count)

        self.counts = [
            self.Test.MEDIUM.value,
            self.Test.BIG.value,
            self.Test.HUGE.value,
        ]

    class Test(Enum):
        SMALL = 1000
        MEDIUM = 10000
        BIG = 100000
        HUGE = 1000000

    @staticmethod
    def _round(param):
        return round(param, ACCURACY)

    def get_values(self, count=10000):
        sample = []
        x_array = range(count)

        for random_val in self.generator:
            if len(sample) >= count:
                return sample
            for i in x_array:
                random_val -= self.p(i)
                if random_val < 0:
                    sample.append(i)
                    break

    def task_1_1(self):
        for count in self.counts:
            self.show_hist(count)

    def show_hist(self, count):
        if count > 100:
            M = int(3 * log(count))
        else:
            M = int(count ** 0.5)

        plt.figure(figsize=FIG_SIZE)
        values = self.get_values(count)
        logger.debug(f"Гистограмма для выборки размером {count}")
        plt.hist(values, bins=M, density=True)
        plt.show()

    def task_1_2(self):
        M = StatisticUtils.get_M(self.values)
        theoretical_M = self._round(self.m())
        logger.debug(f"Оценка математического ожидания = {M}")
        logger.debug(
            f"Теоритическое значение математического ожидания = {theoretical_M}."
        )
        logger.debug(
            f"Отклонение: абсолютная величина = {self._round(abs(theoretical_M - M))}"
        )
        logger.debug(
            f"Отклонение: относительная величина = "
            f"{self._round(abs(100 - M / theoretical_M * 100))}%"
        )

        D = StatisticUtils.get_D(self.values)
        theoretical_D = self._round(self.D())
        logger.debug(f"Смещенная оценка дисперсии = {D}")
        logger.debug(f"Теоритическое значение дисперсии = {theoretical_D}.")
        logger.debug(
            f"Отклонение: абсолютная величина = {self._round(abs(theoretical_D - D))}"
        )
        logger.debug(
            f"Отклонение: относительная величина = "
            f"{self._round(abs(100 - D / theoretical_D * 100))}%"
        )

        corrected_D = StatisticUtils.get_corrected_D(self.values)
        logger.debug(f"Несмещенная оценка дисперсии по выборке = " f"{corrected_D}")
        logger.debug(
            f"Отклонение: абсолютная величина = "
            f"{self._round(abs(theoretical_D - corrected_D))}"
        )
        logger.debug(
            f"Отклонение: относительная величина = "
            f"{self._round(abs(100 - corrected_D / theoretical_D * 100))}%"
        )

    def task_1_3(self):
        t_M_low, t_M_high = StatisticUtils.get_student_M_interval(self.values)
        logger.debug(
            f"{t_M_low} <= M <= {t_M_high} "
            f"с помощью параметра распределения Стьюдента (g = 0.95)"
        )
        u_M_low, u_M_high = StatisticUtils.get_laplace_M_interval(self.values)
        logger.debug(
            f"{u_M_low} <= M <= {u_M_high} "
            f"с помощью параметра распределения Лапласа (g = 0.95)"
        )
        D_low, D_high = StatisticUtils.get_D_interval(self.values)
        logger.debug(f"{D_low} <= D <= {D_high} с помощью моментов 4-ого порядка")

    def task_2_1(self):
        logger.info(
            f"2.1.1 Проверка гипотезы о соответствии закона распределения "
            f"с помощью критерия хи квадрат"
        )
        count = self.Test.BIG.value
        values = self.get_values(count)

        chi_2, interval = StatisticUtils.check_convergence_by_chi2(
            values, count, self.p
        )
        logger.debug(f"Коэффициент хи 2 = {chi_2}. Интервал = {interval}")

        if interval[0] < chi_2 < interval[1]:
            logger.debug(
                "Критерий Пирсона не отвергает гипотезу о равномерности заданного "
                "распределения."
            )
            return
        logger.debug(
            "Критерий Пирсона отвергает гипотезу о равномерности заданного "
            "распределения."
        )

    def solve(self):
        logger.info("1. Выполнение статистического исследования.")
        logger.info("1.1 Построение гистограмм распределения.")
        self.task_1_1()

        logger.info("1.2 Точечные оценки")
        self.task_1_2()

        logger.info("1.3 Интервальные оценки")
        self.task_1_3()

        logger.info(
            "2. Проверить гипотезы о соответствии закона распределения полученной "
            "случайной величины требуемому"
        )
        self.task_2_1()


if __name__ == "__main__":
    # Распределение Пуассона
    import mpmath

    l = 10

    def p(k):
        return (l ** k) * np.exp(-l) / np.math.factorial(k)

    def m():
        return l

    def D():
        return l

    def F(k):
        return mpmath.gammainc(k + 1, l) / np.math.factorial(k)

    d = DRV(m, D, F, p)
    d.solve()
