import math
from enum import Enum

import matplotlib.pyplot as plt
import numpy as np
from loguru import logger
from scipy.stats import norm

from generator import Generator
from util import StatisticUtils

FIG_SIZE = (15, 5)

ACCURACY = 7
EPS = 1e-16


class NormalDistribution:
    def __init__(self, m, sigma, values_count):
        self.m = m
        self.sigma = sigma

        self.generator = Generator()
        self.values = []
        self.values_count = values_count

        self.counts = [
            self.Test.TINY.value,
            self.Test.SMALL.value,
            self.Test.MEDIUM.value,
            self.Test.BIG.value,
        ]

    class Test(Enum):
        TINY = 100
        SMALL = 1000
        MEDIUM = 10000
        BIG = 100000

    @staticmethod
    def _round(param):
        return round(param, ACCURACY)

    def F(self, x):
        return 0.5 * (1 + math.erf((x - self.m) / ((2 * self.sigma ** 2) ** 0.5)))

    def get_values(self, count=100000):
        # Box-Muller algorithm
        # https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
        values = []
        for u1, u2 in zip(self.generator, self.generator):
            if len(values) >= count:
                return values
            z = np.sqrt(-2.0 * np.log(u1)) * np.cos(2 * np.pi * u2)
            values.append(self.m + z * self.sigma)
        return values

    @staticmethod
    def show_hist(count, values):
        if count > 100:
            M = int(2 * math.log(count))
        else:
            M = int(count ** 0.5)
        logger.debug(f"Гистограмма для выборки размером {count}")
        plt.figure(figsize=FIG_SIZE)

        min_x = min(values)
        max_x = max(values)

        x = np.linspace(min_x, max_x, count)
        plt.plot(x, norm.pdf(x))
        plt.hist(
            values, bins=M, color="blue", density=True, histtype="stepfilled", alpha=0.2
        )
        plt.show()

    def task_1_1(self):
        for count in self.counts:
            values = self.get_values(count)
            self.show_hist(count, values)

    def task_1_2(self):
        M = StatisticUtils.get_M(self.values)
        theoretical_M = self.m
        logger.debug(f"Оценка математического ожидания = {M}")
        logger.debug(
            f"Теоритическое значение математического ожидания = {theoretical_M}."
        )
        logger.debug(
            f"Отклонение: абсолютная величина = {self._round(abs(theoretical_M - M))}"
        )

        D = StatisticUtils.get_D(self.values)
        theoretical_D = self._round(self.sigma ** 2)
        logger.debug(f"Смещенная оценка дисперсии = {D}")
        logger.debug(f"Теоритическое значение дисперсии = {theoretical_D}.")
        logger.debug(
            f"Отклонение: абсолютная величина = {self._round(abs(theoretical_D - D))}"
        )
        logger.debug(
            f"Отклонение: относительная величина = "
            f"{self._round(abs(100 - D / theoretical_D * 100))}%"
        )

        corrected_D = StatisticUtils.get_corrected_D(self.values)
        logger.debug(f"Несмещенная оценка дисперсии по выборке = " f"{corrected_D}")
        logger.debug(
            f"Отклонение: абсолютная величина = "
            f"{self._round(abs(theoretical_D - corrected_D))}"
        )
        logger.debug(
            f"Отклонение: относительная величина = "
            f"{self._round(abs(100 - corrected_D / theoretical_D * 100))}%"
        )

    def task_1_3(self):
        t_M_low, t_M_high = StatisticUtils.get_student_M_interval(self.values)
        logger.debug(
            f"{t_M_low} <= M <= {t_M_high} "
            f"с помощью параметра распределения Стьюдента (g = 0.95)"
        )
        u_M_low, u_M_high = StatisticUtils.get_laplace_M_interval(self.values)
        logger.debug(
            f"{u_M_low} <= M <= {u_M_high} "
            f"с помощью параметра распределения Лапласа (g = 0.95)"
        )
        D_low, D_high = StatisticUtils.get_D_interval(self.values)
        logger.debug(f"{D_low} <= D <= {D_high} с помощью моментов 4-ого порядка")

    def task_2_1(self, values):
        logger.info(
            f"2.1.1 Проверка гипотезы о соответствии закона распределения "
            f"с помощью критерия Колмогорова-Смирнова"
        )
        n = len(values)
        K_n_minus, K_n_plus, k = StatisticUtils.check_convergence_by_kolmogorov(
            values, n, self.F
        )

        logger.debug(f"Коэффициент k = {k}. K+ = {K_n_plus}, K- = {K_n_minus}")
        if K_n_minus > k or K_n_plus > k:
            logger.debug(
                "Критерий Колмогорова-Смирнова отвергает гипотезу о нормальности "
                "заданного распределения."
            )
            return
        logger.debug(
            "Критерий Колмогорова-Смирнова не отвергает гипотезу о нормальности "
            "заданного распределения."
        )

    def solve(self):
        logger.info(
            "Написать программу реализующую метод формирования непрерывной "
            "случайной величины с определенным распределением "
            "(Нормальное распределение) с использованием "
            "реализованного датчика БСВ"
        )
        logger.info("1 Выполнить статистическое исследование")
        logger.info("1.1 Построение гистограммы")

        self.values = self.get_values(count=self.values_count)
        self.task_1_1()

        logger.info("1.2 Точечные оценки")
        self.task_1_2()

        logger.info("1.3 Интервальные оценки")
        self.task_1_3()

        logger.info(
            "2. Проверить гипотезы о соответствии закона распределения полученной "
            "случайной величины требуемому"
        )

        values = self.get_values()
        self.task_2_1(values)


if __name__ == "__main__":
    logger.warning("Нормальное распределение")
    n = NormalDistribution(m=0, sigma=1, values_count=1000)
    n.solve()
