from enum import Enum
from math import log

import matplotlib.pyplot as plt
from loguru import logger

from generator import Generator
from util import StatisticUtils

FIG_SIZE = (15, 5)

ACCURACY = 7


class Task1:
    def __init__(self, values_count):
        self.generator = Generator()

        self.values = []
        self.values_count = values_count

        self.counts = [
            self.Test.TINY.value,
            self.Test.SMALL.value,
            self.Test.MEDIUM.value,
            self.Test.BIG.value,
        ]

    class Test(Enum):
        TINY = 100
        SMALL = 1000
        MEDIUM = 10000
        BIG = 100000

    @staticmethod
    def F(x, a=0, b=1):
        return (x - a) / (b - a)

    @staticmethod
    def _round(param):
        return round(param, ACCURACY)

    def get_values(self, count=100000):
        values = []
        for x in self.generator:
            if len(values) >= count:
                return values
            values.append(x)

    def show_hist(self, count):
        if count > 100:
            M = int(3 * log(count))
        else:
            M = int(count ** 0.5)
        plt.figure(figsize=FIG_SIZE)
        values = self.get_values(count)
        plt.hist(values, bins=M, density=True)
        plt.show()

    def task_1_1(self):
        for count in self.counts:
            self.show_hist(count)

    def task_1_2(self):
        M = StatisticUtils.get_M(self.values)
        theoretical_M = 0.5
        logger.debug(f"Оценка математического ожидания = {M}")
        logger.debug(
            f"Теоритическое значение математического ожидания = {theoretical_M}."
        )
        logger.debug(
            f"Отклонение: абсолютная величина = {self._round(abs(theoretical_M - M))}"
        )
        logger.debug(
            f"Отклонение: относительная величина = "
            f"{self._round(abs(100 - M / theoretical_M * 100))}%"
        )

        D = StatisticUtils.get_D(self.values)
        theoretical_D = self._round(1 / 12)
        logger.debug(f"Смещенная оценка дисперсии = {D}")
        logger.debug(f"Теоритическое значение дисперсии = {theoretical_D}.")
        logger.debug(
            f"Отклонение: абсолютная величина = {self._round(abs(theoretical_D - D))}"
        )
        logger.debug(
            f"Отклонение: относительная величина = "
            f"{self._round(abs(100 - D / theoretical_D * 100))}%"
        )

        corrected_D = StatisticUtils.get_corrected_D(self.values)
        logger.debug(f"Несмещенная оценка дисперсии по выборке = " f"{corrected_D}")
        logger.debug(
            f"Отклонение: абсолютная величина = "
            f"{self._round(abs(theoretical_D - corrected_D))}"
        )
        logger.debug(
            f"Отклонение: относительная величина = "
            f"{self._round(abs(100 - corrected_D / theoretical_D * 100))}%"
        )

    def task_1_3(self):
        t_M_low, t_M_high = StatisticUtils.get_student_M_interval(self.values)
        logger.debug(
            f"{t_M_low} <= M <= {t_M_high} "
            f"с помощью параметра распределения Стьюдента (g = 0.95)"
        )
        u_M_low, u_M_high = StatisticUtils.get_laplace_M_interval(self.values)
        logger.debug(
            f"{u_M_low} <= M <= {u_M_high} "
            f"с помощью параметра распределения Лапласа (g = 0.95)"
        )
        D_low, D_high = StatisticUtils.get_D_interval(self.values)
        logger.debug(f"{D_low} <= D <= {D_high} с помощью моментов 4-ого порядка")

    def task_2_1(self):
        logger.info(
            f"2.1.1 Проверка гипотезы о соответствии закона распределения "
            f"с помощью критерия Колмогорова-Смирнова"
        )
        n = self.Test.MEDIUM.value
        values = self.get_values(n)
        K_n_minus, K_n_plus, k = StatisticUtils.check_convergence_by_kolmogorov(
            values, n, self.F
        )

        logger.debug(f"Коэффициент k = {k}. K+ = {K_n_plus}, K- = {K_n_minus}")

        if K_n_minus > k or K_n_plus > k:
            logger.debug(
                "Критерий Колмогорова-Смирнова отвергает гипотезу о равномерности "
                "заданного распределения."
            )
            return
        logger.debug(
            "Критерий Колмогорова-Смирнова не отвергает гипотезу о равномерности "
            "заданного распределения."
        )

    def task_3_1(self):
        for count in self.counts:
            values = self.get_values(count)
            correlation = StatisticUtils.check_correlation(values)
            logger.debug(
                f"Коэффициент корреляции для выборки размером "
                f"{count} = {correlation}"
            )

    def solve(self):
        logger.info("1. Выполнение статистического исследования.")
        logger.info("1.1 Построение гистограмм распределения.")
        self.task_1_1()
        self.values = self.get_values(self.values_count)

        logger.info("1.2 Точечные оценки")
        self.task_1_2()

        logger.info("1.3 Интервальные оценки")
        self.task_1_3()

        logger.info(
            "2. Проверить гипотезы о соответствии закона распределения полученной "
            "случайной величины требуемому"
        )
        self.task_2_1()

        logger.info(
            "3. Проверить статистическую независимость чисел генерируеммых полученным "
            "вами генератором"
        )
        self.task_3_1()


if __name__ == "__main__":
    t = Task1(values_count=100000)
    t.solve()
