import numpy as np
from loguru import logger

import matplotlib.pyplot as plt

from sympy import integrate, exp
from sympy.abc import x, y

from generator import Generator
from util import StatisticUtils

FIG_SIZE = (15, 5)

ACCURACY = 7


class TwoDimensionalCRV:
    # https://en.wikipedia.org/wiki/Exponential_distribution
    # где lambda = 1

    @staticmethod
    def f(x):
        return np.exp(-x)

    @staticmethod
    def inverse_function(y):
        # Метод обратной функции
        # при данной постановке задачи условие нормировки не выполняется =>
        # нужно удалить 2 из условия
        # y = exp(-x) => ln y = -x => x = -ln y
        return -np.log(y)

    def __init__(self, values_count):
        self.generator = Generator()

        self.values_count = values_count
        self.x, self.y = self.get_values(self.values_count)

    def get_values(self, count):
        values_x = []
        values_y = []
        for x, y in zip(self.generator, self.generator):
            if len(values_x) >= count:
                return values_x, values_y

            # f(x, y) = exp(-x-y)
            # f(x) = int exp(-x-y) dy 0 .. inf
            # f(x) = exp(-x)
            f_1 = self.inverse_function(x)
            values_x.append(f_1)

            # f(x, y) = exp(-x-y)
            # f(x) = exp(-x)
            # f(y | x) = f(x, y) / f(x) = exp(-y)
            # f_2 = - ln x_2
            f_2 = self.inverse_function(y)
            values_y.append(f_2)

    @staticmethod
    def _create_hist(count, values, val_name):
        if count > 100:
            M = int(2 * np.log(count))
        else:
            M = int(count ** 0.5)
        logger.debug(f"Гистограмма для СВ {val_name}. Выборка размером {count}")
        plt.figure(figsize=FIG_SIZE)

        # строим график истинной функции
        min_x = min(values)
        max_x = max(values)

        x = np.linspace(min_x, max_x, count)
        y = list(map(TwoDimensionalCRV.f, x))
        plt.title(f"СВ {val_name}. Выборка размером {count}")
        plt.plot(x, y)

        # строим гистограмму
        plt.hist(
            values, bins=M, color="blue", density=True, histtype="stepfilled", alpha=0.2
        )
        plt.show()

    def get_M_intervals(self):
        t_M_x_low, t_M_x_high = StatisticUtils.get_student_M_interval(self.x)
        logger.debug(
            f"{t_M_x_low} <= M(x) <= {t_M_x_high} "
            f"с помощью параметра распределения Стьюдента (g = 0.95)"
        )

        t_M_y_low, t_M_y_high = StatisticUtils.get_student_M_interval(self.y)
        logger.debug(
            f"{t_M_y_low} <= M(y) <= {t_M_y_high} "
            f"с помощью параметра распределения Стьюдента (g = 0.95)"
        )

    def get_D_intervals(self):
        D_x_low, D_x_high = StatisticUtils.get_D_interval(self.x)
        logger.debug(
            f"{D_x_low} <= D(x) <= {D_x_high} с помощью моментов 4-ого порядка"
        )

        D_y_low, D_y_high = StatisticUtils.get_D_interval(self.y)
        logger.debug(
            f"{D_y_low} <= D(y) <= {D_y_high} с помощью моментов 4-ого порядка"
        )

    def get_correlation(self):
        corr = StatisticUtils.check_correlation(self.x, self.y)
        if 0 <= abs(corr) <= 0.2:
            logger.debug(
                f"Наблюдается очень слабая корреляция величин x и y. "
                f"Коэффициент корреляции = {corr}"
            )
        elif 0.2 <= abs(corr) <= 0.5:
            logger.debug(
                f"Наблюдается слабая корреляция величин x и y. "
                f"Коэффициент корреляции = {corr}"
            )
        elif 0.5 <= abs(corr) <= 0.7:
            logger.debug(
                f"Наблюдается средняя корреляция величин x и y. "
                f"Коэффициент корреляции = {corr}"
            )
        elif 0.7 <= abs(corr) <= 0.9:
            logger.debug(
                f"Наблюдается сильная корреляция величин x и y. "
                f"Коэффициент корреляции = {corr}"
            )
        elif 0.9 <= abs(corr) <= 1:
            logger.debug(
                f"Наблюдается функциональная зависимость величин x и y. "
                f"Коэффициент корреляции = {corr}"
            )

    def task_1(self):
        logger.info(f"1. Выполнить статистическое исследование.")

        logger.info(f"1.2 Гистограммы составляющих вектора")
        logger.debug(f"X вектор: {self.x}")
        logger.debug(f"Y вектор: {self.y}")
        self._create_hist(self.values_count, self.x, "X")
        self._create_hist(self.values_count, self.y, "Y")

        logger.info(f"1.3 Точечные оценки математического ожидания и дисперсии")
        M_x = StatisticUtils.get_M(self.x)
        M_y = StatisticUtils.get_M(self.y)
        logger.debug(f"Оценки математического ожидания: M(x) = {M_x}, M(y) = {M_y}")

        D_x = StatisticUtils.get_D(self.x)
        D_y = StatisticUtils.get_D(self.y)
        logger.debug(f"Оценки дисперсии: D(x) = {D_x}, D(y) = {D_y}")

        corrected_D_x = StatisticUtils.get_corrected_D(self.x)
        corrected_D_y = StatisticUtils.get_corrected_D(self.y)
        logger.info(
            f"Несмещенные оценки дисперсии по выборке: D(x) = {corrected_D_x}, "
            f"D(y) = {corrected_D_y}"
        )

        logger.info(f"1.4 Интервальные оценки математического ожидания и дисперсии")
        self.get_M_intervals()
        self.get_D_intervals()

        logger.info(f"1.5 Коэффициент корреляции")
        self.get_correlation()

    def task_2(self):
        # https://forecasting.svetunkov.ru/forecasting_toolbox/statistics-and-hypothesis/
        # http://www.ipiran.ru/frenkel/hypothesis_testing.pdf
        logger.info(
            f"2.1 Проверить гипотезы о соответствии полученных оценок "
            f"характеристик (математическое ожидание, дисперсия, "
            f"корреляция) случайной величины теоретическим."
        )

        M = x * exp(-x)
        M_x = integrate(M, (x, 0, np.inf))

        M = y * exp(-y)
        M_y = integrate(M, (y, 0, np.inf))
        logger.debug(f"Математическое ожидание: M(x) = {M_x}, M(y) = {M_y}")

        D = ((x - M_x) ** 2) * exp(-x)
        D_x = integrate(D, (x, 0, np.inf))

        D = ((y - M_y) ** 2) * exp(-y)
        D_y = integrate(D, (y, 0, np.inf))

        logger.debug(f"Дисперсия: D(x) = {D_x}, D(y) = {D_y}")

        logger.info(
            f"1. Проверка гипотезы о соответствии оценки математического "
            f"ожидания случайной величины теореической"
        )
        # можно сделать и с помощью t-статистики,
        # для больших выборок Z и t-тесты обеспечивают почти идентичные
        # результаты
        z_x, z_max_x = StatisticUtils.M_test(self.x, self.values_count, M_x, D_x)
        self.is_correct_M(z_x, z_max_x, "X")

        z_y, z_max_y = StatisticUtils.M_test(self.y, self.values_count, M_y, D_y)
        self.is_correct_M(z_y, z_max_y, "Y")

        logger.info(
            f"2. Проверка гипотезы о соответствии оценки дисперсии "
            f"случайной величины теореической"
        )
        chi2_x, interval_x = StatisticUtils.D_test(self.x, self.values_count, D_x)
        self.is_correct_D(chi2_x, interval_x, "X")

        chi2_y, interval_y = StatisticUtils.D_test(self.y, self.values_count, D_y)
        self.is_correct_D(chi2_y, interval_y, "Y")

        logger.info(
            f"3. Проверка гипотезы о соответствии оценки корреляции "
            f"случайной величины теореической"
        )
        corr = StatisticUtils.check_correlation(self.x, self.y)
        t, t_critical = StatisticUtils.correlation_test(corr, self.values_count)
        self.is_correct_correlation(t, t_critical)

    @staticmethod
    def is_correct_M(z, z_max, v_name):
        if -z_max <= z <= z_max:
            logger.debug(
                f"Z тест для математического ожидания случайной величины {v_name}: "
                f"получено = {-z_max} < {z} < {z_max}"
            )
            logger.debug(
                f"Гипотеза о соответствии оценки математического ожидания "
                f"случайной величины {v_name} теоретической не отклоняется"
            )
            return
        logger.debug(
            f"Z тест для математического ожидания случайной величины {v_name}: "
            f"получено = {z} > {z_max} или {-z_max} > {z_max}"
        )
        logger.debug(
            f"Гипотеза о соответствии оценки математического ожидания "
            f"случайной величины {v_name} теоретической отклоняется"
        )

    @staticmethod
    def is_correct_D(chi_2, interval, v_name):
        if interval[0] < chi_2 < interval[1]:
            logger.debug(
                f"Хи^2 тест для дисперсии случайной величины {v_name}: "
                f"получено = {chi_2} ∈ {interval}"
            )
            logger.debug(
                f"Гипотеза о соответствии оценки математического ожидания "
                f"случайной величины {v_name} теоретической не отклоняется"
            )
            return
        logger.debug(
            f"Хи^2 тест для дисперсии случайной величины {v_name}: "
            f"получено = {chi_2} ∉ {interval}"
        )
        logger.debug(
            f"Гипотеза о соответствии оценки математического ожидания "
            f"случайной величины {v_name} теоретической отклоняется"
        )

    @staticmethod
    def is_correct_correlation(t_empirical, t_critical):
        if t_empirical > t_critical:
            logger.debug(
                f"t тест для коэффициента корреляции случайных величин: "
                f"получено = {t_empirical} > {t_critical}"
            )
            logger.debug(
                f"Гипотеза о соответствии оценки коэффициента корреляции "
                f"теоретическому значению не отклоняется"
            )
            return
        logger.debug(
            f"t тест для коэффициента корреляции случайных величин: "
            f"получено = {t_empirical} < {t_critical}"
        )
        logger.debug(
            f"Гипотеза о соответствии оценки коэффициента корреляции "
            f"теоретическому значению отклоняется"
        )

    def solve(self):
        self.task_1()
        self.task_2()


if __name__ == "__main__":
    drv = TwoDimensionalCRV(1000)
    drv.solve()
