from typing import NamedTuple

import matplotlib.pyplot as plt
import numpy as np
from loguru import logger

from generator import Generator
from util import StatisticUtils

ACCURACY = 7


# noinspection PyPep8Naming
class TwoDimensionalDRV:
    class DRV(NamedTuple):
        x: int
        y: int

    def set_values(self, values_count):
        for _ in range(values_count):
            random_val = next(self.generator)
            f = np.searchsorted(self.F, random_val)
            x, y = f // self.n, f % self.n
            self.values.append(self.DRV(x, y))

    def __init__(self, values_count, distribution_table):
        self.generator = Generator()
        self.F = np.cumsum(distribution_table)
        self.distribution_table = self._round_array(distribution_table)

        self.m, self.n = self.distribution_table.shape

        self.values = []
        self.values_count = values_count
        self.set_values(values_count)

        self.x = [value.x for value in self.values]
        self.y = [value.y for value in self.values]

    @staticmethod
    def _round(param):
        return round(param, ACCURACY)

    @staticmethod
    def _round_array(array):
        return np.around(array, decimals=ACCURACY)

    def _create_hist(self, x, y, val_name):
        # Столбчатая диаграмма или гистограмма — диаграмма,
        # представленная прямоугольными зонами (столбцами),
        # высоты или длины которых пропорциональны величинам,
        # которые они отображают.
        # Прямоугольные зоны могут быть расположены вертикально или горизонтально.
        logger.debug(
            f"Гистограмма для СВ {val_name}. Выборка размером {self.values_count}"
        )
        plt.title(f"Гистограмма для составляющей вектора: {val_name}.")
        plt.bar(range(x), y, color="royalblue", alpha=0.7)
        plt.grid(color="#95a5a6", linestyle="--", linewidth=2, axis="y", alpha=0.7)
        plt.show()

    def task_1(self):
        logger.info(f"1. Выполнить статистическое исследование.")

        logger.info(f"1.1 Построение эмпирической матрицы распределения")
        self.get_empirical_distribution_matrix()
        x_vector = self.distribution_table.sum(axis=1)
        y_vector = self.distribution_table.sum(axis=0)

        logger.info(f"1.2 Гистограммы составляющих вектора")
        logger.debug(f"X вектор: {x_vector}, Y вектор: {y_vector}")
        self._create_hist(self.m, x_vector, "X вектор")
        self._create_hist(self.n, y_vector, "Y вектор")

        logger.info(f"1.3 Точечные оценки математического ожидания и дисперсии")
        M_x = StatisticUtils.get_M(self.x)
        M_y = StatisticUtils.get_M(self.y)

        logger.debug(f"Оценки математического ожидания: M(x) = {M_x}, M(y) = {M_y}")
        D_x = StatisticUtils.get_D(self.x)
        D_y = StatisticUtils.get_D(self.y)

        logger.debug(f"Оценки дисперсии: D(x) = {D_x}, D(y) = {D_y}")
        corrected_D_x = StatisticUtils.get_corrected_D(self.x)
        corrected_D_y = StatisticUtils.get_corrected_D(self.y)

        logger.info(
            f"Несмещенные оценки дисперсии по выборке: D(x) = {corrected_D_x}, "
            f"D(y) = {corrected_D_y}"
        )

        logger.info(f"1.4 Интервальные оценки математического ожидания и дисперсии")
        self.get_M_intervals()
        self.get_D_intervals()

        logger.info(f"1.5 Коэффициент корреляции")
        self.get_correlation()

    def get_empirical_distribution_matrix(self):
        matrix = np.zeros(shape=(self.m, self.n))
        for x, y in self.values:
            matrix[x][y] += 1
        empirical_matrix = matrix / self.values_count
        logger.debug(f"Эмпирическая матрица распределения:\n {empirical_matrix}")
        return empirical_matrix

    def get_M_intervals(self):
        M_x_low, M_x_high = StatisticUtils.get_student_M_interval(self.x)
        logger.debug(
            f"{M_x_low} <= M(x) <= {M_x_high} "
            f"с помощью параметра распределения Стьюдента (g = 0.95)"
        )

        M_y_low, M_y_high = StatisticUtils.get_student_M_interval(self.y)
        logger.debug(
            f"{M_y_low} <= M(y) <= {M_y_high} "
            f"с помощью параметра распределения Стьюдента (g = 0.95)"
        )

    def get_D_intervals(self):
        D_x_low, D_x_high = StatisticUtils.get_D_interval(self.x)
        logger.debug(
            f"{D_x_low} <= D(x) <= {D_x_high} с помощью моментов 4-ого порядка"
        )

        D_y_low, D_y_high = StatisticUtils.get_D_interval(self.y)
        logger.debug(
            f"{D_y_low} <= D(y) <= {D_y_high} с помощью моментов 4-ого порядка"
        )

    def get_correlation(self):
        # http://www.datuapstrade.lv/rus/spss/section_11/4/
        corr = StatisticUtils.check_correlation(self.x, self.y)
        if 0 <= abs(corr) <= 0.2:
            logger.debug(
                f"Наблюдается очень слабая корреляция величин x и y. "
                f"Коэффициент корреляции = {corr}"
            )
        elif 0.2 <= abs(corr) <= 0.5:
            logger.debug(
                f"Наблюдается слабая корреляция величин x и y. "
                f"Коэффициент корреляции = {corr}"
            )
        elif 0.5 <= abs(corr) <= 0.7:
            logger.debug(
                f"Наблюдается средняя корреляция величин x и y. "
                f"Коэффициент корреляции = {corr}"
            )
        elif 0.7 <= abs(corr) <= 0.9:
            logger.debug(
                f"Наблюдается средняя корреляция величин x и y. "
                f"Коэффициент корреляции = {corr}"
            )
        elif 0.9 <= abs(corr) <= 1:
            logger.debug(
                f"Наблюдается функциональная зависимость величин x и y. "
                f"Коэффициент корреляции = {corr}"
            )

    def task_2(self):
        logger.info(
            f"2.1 Проверить гипотезы о соответствии закона распределения "
            f"полученной случайной величины требуемому."
        )
        logger.info(
            f"Проверка гипотезы о соответствии закона распределения "
            f"с помощью критерия хи квадрат"
        )
        chi_2, interval = StatisticUtils.check_convergence_by_chi2(
            self.values, self.values_count, self.distribution_table
        )
        logger.debug(f"Коэффициент хи^2 = {chi_2}. Интервал = {interval}")

        if interval[0] < chi_2 < interval[1]:
            logger.debug(
                "Критерий Пирсона не отвергает гипотезу о виде заданного "
                "распределения."
            )
            return
        logger.debug(
            "Критерий Пирсона отвергает гипотезу о виде заданного распределения."
        )

    def task_3(self):
        # https://forecasting.svetunkov.ru/forecasting_toolbox/statistics-and-hypothesis/
        # http://www.ipiran.ru/frenkel/hypothesis_testing.pdf
        # https://medstatistic.ru/theory/pirson.html
        logger.info(
            f"3.1 Проверить гипотезы о соответствии полученных оценок "
            f"характеристик (математическое ожидание, дисперсия, "
            f"корреляция) случайной величины теоретическим."
        )

        x_vector_prob = self.distribution_table.sum(axis=1)
        y_vector_prob = self.distribution_table.sum(axis=0)

        M_x = self._round((np.array(range(self.n)) * x_vector_prob).sum())
        M_y = self._round((np.array(range(self.m)) * y_vector_prob).sum())
        logger.debug(
            f"Теоритическое математическое ожидание: " f"M(x) = {M_x}, M(y) = {M_y}"
        )

        D_x = (
            ((np.array(range(self.n)) - np.full(self.n, M_x)) ** 2) * x_vector_prob
        ).sum()
        D_y = (
            ((np.array(range(self.m)) - np.full(self.m, M_y)) ** 2) * y_vector_prob
        ).sum()
        logger.debug(f"Теоретическая дисперсия: D(x) = {D_x}, D(y) = {D_y}")

        logger.info(
            f"1. Проверка гипотезы о соответствии оценки математического "
            f"ожидания случайной величины теоретической"
        )
        # можно сделать и с помощью t-статистики,
        # для больших выборок Z и t-тесты обеспечивают почти идентичные
        # результаты
        z_x, z_max_x = StatisticUtils.M_test(self.x, self.n, M_x, D_x)
        self.is_correct_M(z_x, z_max_x, "X")

        z_y, z_max_y = StatisticUtils.M_test(self.y, self.m, M_y, D_y)
        self.is_correct_M(z_y, z_max_y, "Y")

        logger.info(
            f"2. Проверка гипотезы о соответствии оценки дисперсии "
            f"случайной величины теоретической"
        )
        chi2_x, interval_x = StatisticUtils.D_test(self.x, self.n, D_x)
        self.is_correct_D(chi2_x, interval_x, "X")

        chi2_y, interval_y = StatisticUtils.D_test(self.y, self.m, D_y)
        self.is_correct_D(chi2_y, interval_y, "Y")

        logger.info(
            f"3. Проверка гипотезы о соответствии оценки корреляции "
            f"случайной величины теоретической"
        )
        corr = StatisticUtils.check_correlation(self.x, self.y)
        t, t_critical = StatisticUtils.correlation_test(corr, self.values_count)
        self.is_correct_correlation(t, t_critical)

    @staticmethod
    def is_correct_M(z, z_max, v_name):
        if -z_max < z < z_max:
            logger.debug(
                f"Z тест для математического ожидания случайной величины {v_name}: "
                f"получено = {-z_max} < {z} < {z_max}"
            )
            logger.debug(
                f"Гипотеза о соответствии оценки математического ожидания "
                f"случайной величины {v_name} теоретической не отклоняется"
            )
            return
        logger.debug(
            f"Z тест для математического ожидания случайной величины {v_name}: "
            f"получено = {z} > {z_max} или {-z_max} > {z_max}"
        )
        logger.debug(
            f"Гипотеза о соответствии оценки математического ожидания "
            f"случайной величины {v_name} теоретической отклоняется"
        )

    @staticmethod
    def is_correct_D(chi_2, interval, v_name):
        if interval[0] < chi_2 < interval[1]:
            logger.debug(
                f"Хи^2 тест для дисперсии случайной величины {v_name}: "
                f"получено = {chi_2} ∈ {interval}"
            )
            logger.debug(
                f"Гипотеза о соответствии оценки математического ожидания "
                f"случайной величины {v_name} теоретической не отклоняется"
            )
            return
        logger.debug(
            f"Хи^2 тест для дисперсии случайной величины {v_name}: "
            f"получено = {chi_2} ∉ {interval}"
        )
        logger.debug(
            f"Гипотеза о соответствии оценки математического ожидания "
            f"случайной величины {v_name} теоретической отклоняется"
        )

    @staticmethod
    def is_correct_correlation(t_empirical, t_critical):
        if t_empirical > t_critical:
            logger.debug(
                f"t тест для коэффициента корреляции случайных величин: "
                f"получено = {t_empirical} > {t_critical}"
            )
            logger.debug(
                f"Гипотеза о соответствии оценки коэффициента корреляции "
                f"теоретическому значению не отклоняется"
            )
            return
        logger.debug(
            f"t тест для коэффициента корреляции случайных величин: "
            f"получено = {t_empirical} < {t_critical}"
        )
        logger.debug(
            f"Гипотеза о соответствии оценки коэффициента корреляции "
            f"теоретическому значению отклоняется"
        )

    def solve(self):
        self.task_1()
        self.task_2()
        self.task_3()


if __name__ == "__main__":
    '''distribution_table = np.array(
        [[0, 0.1, 0, 0.1],
         [0, 0, 0.2, 0],
         [0.3, 0, 0, 0.15],
         [0, 0, 0.15, 0]
         ]
    )'''
    distribution_table = np.array(
        [[0, 0, 0, 0.729],
         [0, 0, 0.243, 0],
         [0, 0.027, 0, 0],
         [0.001, 0, 0, 0]]
    )
    drv = TwoDimensionalDRV(10000, distribution_table)
    drv.solve()
