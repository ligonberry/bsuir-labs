import math

import numpy as np
from scipy.stats import t, chi2, norm

ACCURACY = 7
EPS = 1e-16
SHAPE = (4, 4)


# noinspection PyPep8Naming
class StatisticUtils:
    def __init__(self):
        pass

    @staticmethod
    def _round(param):
        return round(param, ACCURACY)

    @staticmethod
    def get_M(values):
        return StatisticUtils._round(sum(values) / len(values))

    @staticmethod
    def get_D(values):
        M_value = StatisticUtils.get_M(values)
        D = []
        for value in values:
            D.append((value - M_value) ** 2)
        return StatisticUtils._round(sum(D) / len(values))

    @staticmethod
    def get_corrected_D(values):
        M_value = StatisticUtils.get_M(values)
        D = []
        for value in values:
            D.append((value - M_value) ** 2)
        return StatisticUtils._round(sum(D) / (len(values) - 1))

    @staticmethod
    def _get_eps(coefficient, D, values):
        return coefficient * (D / len(values)) ** 0.5

    @staticmethod
    def _get_t(values, alpha):
        return t.ppf(1 - alpha / 2, len(values) - 1)

    @staticmethod
    def get_student_M_interval(values, alpha=0.05):
        t_coefficient = StatisticUtils._get_t(values, alpha)

        M_value = StatisticUtils.get_M(values)
        D = StatisticUtils.get_corrected_D(values)

        eps = StatisticUtils._get_eps(t_coefficient, D, values)
        M_high = StatisticUtils._round(M_value + eps)
        M_low = StatisticUtils._round(M_value - eps)
        return M_low, M_high

    @staticmethod
    def get_D_interval(values, alpha=0.05):
        n = len(values)
        M_value = StatisticUtils.get_M(values)
        corrected_D = StatisticUtils.get_corrected_D(values)

        y_4 = sum([(value - M_value) ** 4 for value in values]) / (
            n * (corrected_D ** 2)
        )
        under_root = (y_4 - (n - 3) / (n - 1)) / n
        z = norm.ppf(1 - alpha / 2)
        D_low = StatisticUtils._round(corrected_D / (1 + z * under_root ** 0.5))
        D_high = StatisticUtils._round(corrected_D / (1 - z * under_root ** 0.5))
        return D_low, D_high

    @staticmethod
    def check_correlation(first, second):

        cov_matrix = np.cov(np.array([first, second]))

        D_first = cov_matrix[0][0]
        D_second = cov_matrix[1][1]
        k = cov_matrix[0][1]
        correlation = k / (D_first * D_second) ** 0.5
        return StatisticUtils._round(correlation)

    @staticmethod
    def check_convergence_by_chi2(values, n, distribution_table, alpha=0.05):
        shape = distribution_table.shape

        frequencies = np.ndarray(shape)
        for x, y in values:
            frequencies[x][y] += 1

        g = len(frequencies.flatten())

        chi_2 = 0
        frequencies = frequencies.flatten()
        exp_frequencies = (np.array(distribution_table) * n).flatten()
        for i in range(g):
            chi_2 += (frequencies[i] - exp_frequencies[i]) ** 2 / (
                exp_frequencies[i] + EPS
            )

        interval = chi2.interval(1 - alpha, shape[0] - 1)
        return StatisticUtils._round(chi_2), interval

    @staticmethod
    def M_test(values, n, M, D, alpha=0.05):
        # можем применять, так как оценки мат ожидания действительно
        # нормально распределены, в отличие от оценок дисперсии
        # https://ru.wikipedia.org/wiki/Z-test
        expected_M = StatisticUtils.get_M(values)
        z = (expected_M - M) / (((D + EPS) / n) ** 0.5)
        beta = 1 - alpha
        z_max = norm.ppf((1 + beta) / 2)
        return StatisticUtils._round(z), StatisticUtils._round(z_max)

    @staticmethod
    def D_test(values, n, D, alpha=0.05):
        corrected_D = StatisticUtils.get_corrected_D(values)
        chi_2 = corrected_D / (D + EPS) * (n - 1)
        interval = chi2.interval(1 - alpha, n - 1)
        return StatisticUtils._round(chi_2), interval

    @staticmethod
    def _t_student(alpha, n):
        return t.ppf(1 - alpha, n - 1)

    @staticmethod
    def correlation_test(correlation, n, alpha=0.05):
        # https://economy-ru.info/page/010046100047072163098147152158249185017234247076/
        # https://economy-ru.info/page/010046100047072163098147152158249185017234247076/
        # (см. пример)
        t = abs(correlation) * math.sqrt((n - 2) / (1 - (correlation + EPS) ** 2))
        t_max = StatisticUtils._t_student(alpha, n - 1)
        return StatisticUtils._round(t), StatisticUtils._round(t_max)
