from time import time


class Generator:
    a = 6364136223846793005
    c = 1442695040888963407
    m = 2 ** 64

    def __init__(self, first=0, last=1, x=None):
        self.first = first
        self.last = last
        self.x = x if x else int(time() * 1000)

    def __iter__(self):
        return self

    def __next__(self):
        self.x = (self.a * self.x + self.c) % self.m
        value = self.x / self.m
        return (self.last - self.first) * value + self.first
