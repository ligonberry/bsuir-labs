from dataclasses import dataclass
from enum import Enum

import numpy as np

from loguru import logger


# 7. Задачи о нахождении пути максимальной длины из узла s в узел t


# noinspection PyPep8Naming
class TaggingMethodSolver:

    def __init__(self, D, n, s, t):
        self.s = s
        self.t = t

        self.D = D  # матрица смежности

        self.n = n
        self.B = [np.inf] * self.n
        self.B[self.s] = 0

        self.f = {self.s: 0}

        self.I_star = [self.s]  # множество узлов, для которых известна функция Беллмана

    def _get_neighboring_nodes(self):
        # Пусть t ∉ I_*, тогда построим множество узлов w(I_*) = {j ∈ I: (i, j) ∈ U,
        # j ∉ I_*, i ∈ I_*}, соседних с I_* (куда мы можем добраться).
        w = set()
        for i in self.I_star:
            for distance in self.D[i]:
                if 0 < distance < np.inf:
                    j = np.where(self.D[i] == distance)[0][0]
                    if j in self.I_star:
                        break
                    w.add(j)
        logger.debug(f"W(I_*) = {w}")
        return w

    def _existing_destinations_indexes(self, destinations):
        index = []
        for i in range(self.n):
            if 0 < destinations[i] < np.inf:
                index.append(i)
        return index

    class InIStar(Enum):
        NO = False
        YES = True

    def _get_marks(self, j):
        destinations = []
        for row in self.D:
            destinations.append(row[j])
        indexes = self._existing_destinations_indexes(destinations)
        logger.debug(f"[i1, i2, ..., in] ∈ I_*- = {indexes}")

        marks = []
        for i in indexes:
            if i not in self.I_star:
                marks.append(self.InIStar.NO.value)
                break
            else:
                marks.append(self.InIStar.YES.value)
        return marks

    def _get_j_star(self):
        neighbors = self._get_neighboring_nodes()
        for j in neighbors:
            marks = self._get_marks(j)
            logger.debug(f"Marks: {marks}")
            if all(marks):
                return j
        return

    @dataclass
    class Node:
        j: int
        value: float

        def __repr__(self):
            return f"Узел {self.j}: {self.value}"

    def _get_path(self, i):
        path = []
        for j in range(len(self.B)):
            if self.B[j] != np.inf and self.D[j][i] != np.inf:
                path.append(self.Node(j, self.D[j][i]))
        return path

    def _get_max_path(self, path):
        # Ищем максимальную сумму B_i + c_ij
        B_i = -1
        c_ij = -1
        for node in path:
            if self.B[node.j] > B_i:
                B_i = self.B[node.j]
                c_ij = node.value
        return B_i, c_ij

    def solve(self):
        for i in range(self.n):
            logger.info(f"Итерация №{i}")
            # В силу того, что в сети S нет контуров, среди узлов j ∈ w(I_*)
            # обязательно найдется узел j_*, для которого I_j*- ⊂ I*
            j_star = self._get_j_star()

            # Если t в множестве I_* => задача решена
            if self.t in self.I_star:
                logger.success(f"t = {self.t} в множестве I_* = {self.I_star}."
                               f"Задача решена: B[{self.t}] = {self.B[self.t]}")
                return self

            self.I_star.append(j_star)

            path = self._get_path(j_star)
            logger.debug(f"Путь: {path}")
            B_i, c_ij = self._get_max_path(path)
            logger.debug(f"B_{j_star} = B_i:{B_i} + c_ij:{c_ij}")

            self.B[j_star] = B_i + c_ij

            i = self.B.index(B_i)
            self.f[i] = j_star
            logger.debug(f"Максимальный путь на данной итерации - {self.f}")
