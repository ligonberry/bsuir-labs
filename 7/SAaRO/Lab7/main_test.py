import numpy as np

from main import TaggingMethodSolver


def test_example_1():
    D = np.array([[0, 2, np.inf, np.inf, np.inf, 1],
                  [np.inf, 0, 2, np.inf, 7, np.inf],
                  [np.inf, np.inf, 0, 8, np.inf, np.inf],
                  [np.inf, np.inf, np.inf, 0, np.inf, np.inf],
                  [np.inf, np.inf, 1, 1, 0, np.inf],
                  [np.inf, 4, 4, np.inf, 1, 0]])

    n = 6
    s = 0
    t = 3
    t = TaggingMethodSolver(D, n, s, t)
    t.solve()
    assert t.B[3] == 21.0


if __name__ == "__main__":
    test_example_1()
