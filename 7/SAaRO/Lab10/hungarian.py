import copy
import sys
from enum import Enum


class Mark(Enum):
    NONE = 0
    STAR = 1
    PRIME = 2


class Hungarian:

    def __init__(self):
        self.C = None
        self.row_covered = []
        self.col_covered = []
        self.size = 0
        self.M = None

    def solve(self, matrix):
        self.C = copy.deepcopy(matrix)
        self.size = len(self.C)
        self.row_covered = [False for _ in range(self.size)]
        self.col_covered = [False for _ in range(self.size)]
        self.M = [[Mark.NONE for _ in range(self.size)] for _ in range(self.size)]

        self._step_1()

    def _step_1(self):
        for row in self.C:
            minimal = min(row)
            for j in range(self.size):
                row[j] -= minimal
        self._step_2()

    def _step_2(self):
        for i in range(self.size):
            for j in range(self.size):
                if self.C[i][j] == 0 and not self.row_covered[i] and not self.col_covered[j]:
                    self.M[i][j] = Mark.STAR
                    self.row_covered[i] = True
                    self.col_covered[j] = True

        self._cover_clear()
        self._step_3()

    def _step_3(self):
        col_count = 0
        for i in range(self.size):
            for j in range(self.size):
                if self.M[i][j] == Mark.STAR and not self.col_covered[j]:
                    self.col_covered[j] = True
                    col_count += 1
        if col_count >= self.size:
            self.step_7()
        else:
            self._step_4()

    def _step_4(self):
        done = False
        while not done:
            (row, col) = self._find_uncovered_zero()
            if row == -1:
                done = True
                self._step_6()
            else:
                self.M[row][col] = Mark.PRIME
                starred_zero_column = self._find_starred_zero_in_row(row)
                if starred_zero_column == -1:
                    done = True
                    self._step_5((row, col))
                else:
                    self.row_covered[row] = True
                    self.col_covered[starred_zero_column] = False

    def _step_5(self, path_z0):
        count = 0
        done = False
        path = [[-1 for _ in range(2)] for _ in range(self.size * 2 + 1)]
        (path[0][0], path[0][1]) = path_z0
        while not done:
            row = self._find_starred_zero_in_col(path[count][1])
            if row == -1:
                done = True
            else:
                count += 1
                path[count][0] = row
                path[count][1] = path[count - 1][1]

            if not done:
                col = self._find_primed_zero_in_row(path[count][0])
                count += 1
                path[count][0] = path[count - 1][0]
                path[count][1] = col

        self._augment_path(path, count)
        self._cover_clear()
        self._clear_primes()
        self._step_3()

    def _step_6(self):
        min_value = self._find_min_uncovered_value()
        for i in range(self.size):
            for j in range(self.size):
                if self.row_covered[i]:
                    self.C[i][j] += min_value
                if not self.col_covered[j]:
                    self.C[i][j] -= min_value
        self._step_4()

    def step_7(self):
        results = []
        for i in range(self.size):
            for j in range(self.size):
                if self.M[i][j] == Mark.STAR:
                    results += [(i + 1, j + 1)]
        print(results)

    def _cover_clear(self):
        for i in range(self.size):
            self.row_covered[i] = False
            self.col_covered[i] = False

    def _find_uncovered_zero(self):
        row = -1
        col = -1
        i = 0
        done = False

        while not done:
            j = 0
            while True:
                if self.C[i][j] == 0 and not self.row_covered[i] and not self.col_covered[j]:
                    row = i
                    col = j
                    done = True
                j += 1
                if j >= self.size or done:
                    break
            i += 1
            if i >= self.size:
                done = True

        return row, col

    def _find_starred_zero_in_row(self, row):
        col = -1
        for j in range(self.size):
            if self.M[row][j] == Mark.STAR:
                col = j
                break

        return col

    def _find_starred_zero_in_col(self, col):
        row = -1
        for i in range(self.size):
            if self.M[i][col] == Mark.STAR:
                row = i
                break

        return row

    def _find_primed_zero_in_row(self, row):
        col = -1
        for j in range(self.size):
            if self.M[row][j] == Mark.PRIME:
                col = j
                break

        return col

    def _augment_path(self, path, count):
        for i in range(count + 1):
            row, column = path[i][0], path[i][1]
            if self.M[row][column] == Mark.STAR:
                self.M[row][column] = Mark.NONE
            else:
                self.M[row][column] = Mark.STAR

    def _clear_primes(self):
        for i in range(self.size):
            for j in range(self.size):
                if self.M[i][j] == Mark.PRIME:
                    self.M[i][j] = Mark.NONE

    def _find_min_uncovered_value(self):
        min_value = sys.maxsize
        for i in range(self.size):
            for j in range(self.size):
                if not self.row_covered[i] and not self.col_covered[j]:
                    if min_value > self.C[i][j]:
                        min_value = self.C[i][j]
        return min_value


matrix = [[2, 10, 9, 7],
          [15, 4, 14, 8],
          [13, 14, 16, 11],
          [4, 15, 13, 19]]
h = Hungarian()
h.solve(matrix)
