import time

import numpy as np
from loguru import logger
# from memory_profiler import profile


def time_round(time, accuracy=4):
    return round(time, accuracy)


def benchmark_time(func):
    def wrapper(*args, **kwargs):
        t1 = time.time()
        result = func(*args, **kwargs)
        t2 = time.time()
        logger.info(f"Функция {func.__name__} выполняется {time_round(t2 - t1)} секунд")
        return result

    return wrapper


class ResourceAllocationSolver:
    def __init__(self, f):
        self.f = f

        # Количество технологий и сырья соответственно
        self.n, self.c = f.shape

        # Оптимальное значение целевой функции - функция Беллмана
        self.B = np.zeros([self.n, self.c])

        # Начальное условие для уравнения Беллмана
        self.B[0] = self.f[0]

        # х°k(y) - значение, на котором достигает максимума правая часть уравнения
        # B_k(y) = max (f_k(х°k(y)) + B_k-1(y - х°k(y)), k ∈ {1, n}, 0 <= y <= c)
        self.x_k_max = np.zeros([self.n, self.c])
        self.x_k_max[0] = range(self.c)

        self.x = []

    def return_stroke(self, n, c):
        # Обратный ход решения уравнения Беллмана.
        # Положим в k = n, у = с => найдем число х°n(с), которое,
        # по определению, равно оптимальному количеству сырья, выделяемому на
        # процесс n, если объем сырья на все n процессов равен с. Таким образом,
        # компонента х_n оптимального плана х° = (х°1, х°2, ..., х°n) исходной задачи
        # определена: х°n =х°n(с).
        # Если n-му процессу выделили х°n единиц сырья, то на остальные n-1
        # процессов осталось с — х°n единиц.
        # Тогда х°n-1 = х°n (с - х°n)
        if n > 0:
            self.return_stroke(n - 1, int(c - self.x_k_max[n][c]))

        self.x.append(self.x_k_max[n][c])
        logger.debug(
            f"Обратный ход решения уравнения Беллмана: "
            f"n = {n}, c = {c} x = {self.x}"
        )

    def get_optimal_strategy(self, k, y):
        maximum = -np.inf
        for z in range(y + 1):
            curr = self.f[k][z] + self.B[k - 1][y - z]
            if curr > maximum:
                maximum = curr
                self.x_k_max[k][y] = z
        return maximum

    # @profile
    @benchmark_time
    def create_bellman_function(self):
        # Выделим k-ому процессу сырье в количестве z (0 <= z <= y)
        # При этом прибыль от k-ого процесса = f_k (z).
        # B_k = max(f_k (z) + B_k-1 (y - z))
        for k in range(1, self.n):
            for y in range(self.c):
                self.B[k][y] = self.get_optimal_strategy(k, y)
        logger.debug(f"Матрица значений функции Беллмана B = \n{self.B}")

    def solve(self):
        self.create_bellman_function()
        self.return_stroke(self.n - 1, self.c - 1)
        logger.success(f"Оптимальный план = {self.x}")
        return self
