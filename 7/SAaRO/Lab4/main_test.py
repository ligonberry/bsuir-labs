import numpy as np

from main import ResourceAllocationSolver


def example_test_1():
    f = np.array(
        [
            np.array([0, 1, 2, 3, 4, 5]),
            np.array([0, 0, 1, 2, 4, 7]),
            np.array([0, 2, 2, 3, 3, 5]),
        ]
    )
    result = ResourceAllocationSolver(f).solve()
    assert result.x == [0, 5, 0]


def example_test_2():
    f = np.array(
        [
            np.array([0, 3, 4, 5, 8, 9, 10]),
            np.array([0, 2, 3, 7, 9, 12, 13]),
            np.array([0, 1, 2, 6, 11, 11, 13]),
        ]
    )
    result = ResourceAllocationSolver(f).solve()
    assert result.x == [1, 1, 4]


def test_1():
    f = np.array(
        [
            np.array([0, 1, 2, 2, 4, 5, 6]),
            np.array([0, 2, 3, 5, 7, 7, 8]),
            np.array([0, 2, 4, 5, 6, 7, 7]),
        ]
    )
    result = ResourceAllocationSolver(f).solve()
    assert result.x == [0, 4, 2]


def test_7():
    f = np.array(
        [
            np.array([0, 4, 4, 6, 9, 12, 12, 15, 16, 19, 19, 19]),
            np.array([0, 1, 1, 1, 4, 7, 8, 8, 13, 13, 19, 20]),
            np.array([0, 2, 5, 6, 7, 8, 9, 11, 11, 13, 13, 18]),
            np.array([0, 1, 2, 4, 5, 7, 8, 8, 9, 9, 15, 19]),
            np.array([0, 2, 5, 7, 8, 9, 10, 10, 11, 14, 17, 21]),
        ]
    )
    result = ResourceAllocationSolver(f).solve()
    assert result.x == [7, 0, 2, 0, 2]


if __name__ == "__main__":
    example_test_1()
    example_test_2()
    test_1()
    test_7()
