from loguru import logger

from datetime import timedelta
from timeit import default_timer as timer

from max_flow.edmonds_karp import EdmondsKarpSolver
from max_flow.ford_falkerson import FordFalkersonSolver


def solve_ford_falkerson(d, n, s, t):
    logger.warning("Алгоритм Форда-Фалкерсона")
    start = timer()
    solver = FordFalkersonSolver(d, n, s, t).solve()
    end = timer()
    logger.info(
        f"Воемя выполнения для алгоритма Форда-Фалкерсона = "
        f"{timedelta(seconds=end - start)}"
    )
    return solver.flow


def solve_edmonds_karp(d, n, s, t):
    logger.warning(f"Алгоритм Эдмондса-Карпа")
    start = timer()
    solver = EdmondsKarpSolver(d, n, s, t).solve()
    end = timer()
    logger.info(
        f"Время выполнения для алгоритма Эдмондса-Карпа = "
        f"{timedelta(seconds=end - start)}"
    )
    return solver.flow


def test_example_1():
    d = [
        [0, 4, 0, 9, 0, 0, 0],
        [0, 0, 0, 2, 4, 0, 0],
        [0, 0, 0, 0, 1, 10, 0],
        [0, 0, 1, 0, 0, 6, 0],
        [0, 0, 0, 0, 0, 1, 2],
        [0, 0, 0, 0, 0, 0, 9],
        [0, 0, 0, 0, 0, 0, 0],
    ]
    n = 7
    s = 0
    t = 6
    f = solve_ford_falkerson(d, n, s, t)
    e = solve_edmonds_karp(d, n, s, t)
    assert f == e == 10


def test_example_2():
    d = [
        [0, 1000000, 1000000, 0],
        [0, 0, 1, 1000000],
        [0, 0, 0, 1000000],
        [0, 0, 0, 0]
    ]
    n = 4
    s = 0
    t = 3
    f = solve_ford_falkerson(d, n, s, t)
    e = solve_edmonds_karp(d, n, s, t)
    assert f == e == 2000000.0


def test_1():
    d = [
        [0, 3, 2, 1, 0, 6, 0, 0],
        [0, 0, 0, 1, 2, 0, 0, 0],
        [0, 0, 0, 1, 2, 0, 0, 0],
        [0, 0, 0, 0, 7, 5, 4, 1],
        [0, 0, 0, 0, 0, 0, 3, 2],
        [0, 0, 0, 0, 0, 0, 0, 4],
        [0, 0, 0, 0, 0, 3, 0, 5]
    ]
    n = 8
    s = 0
    t = 7
    f = solve_ford_falkerson(d, n, s, t)
    e = solve_edmonds_karp(d, n, s, t)
    assert f == e == 10


def test_6():
    d = [
        [0, 0, 0, 3, 6, 0, 0, 0],
        [1, 0, 0, 4, 7, 0, 1, 0],
        [5, 5, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 2, 2, 1, 3],
        [0, 0, 0, 0, 0, 4, 3, 0],
        [0, 0, 5, 0, 0, 0, 0, 2],
        [0, 0, 0, 0, 0, 7, 0, 0]
    ]
    n = 8
    s = 0
    t = 7
    f = solve_ford_falkerson(d, n, s, t)
    e = solve_edmonds_karp(d, n, s, t)
    assert f == e == 5


def test_7():
    d = [
        [0, 5, 0, 9],
        [0, 0, 2, 1],
        [3, 0, 0, 0],
        [0, 0, 6, 0]
    ]
    n = 4
    s = 0
    t = 3
    f = solve_ford_falkerson(d, n, s, t)
    e = solve_edmonds_karp(d, n, s, t)
    assert f == e == 10


if __name__ == "__main__":
    logger.warning("TEST EXAMPLE 1")
    test_example_1()
    logger.warning("TEST EXAMPLE 2")
    test_example_2()
    logger.warning("TEST 1")
    test_1()
    logger.warning("TEST 6")
    test_6()
    logger.warning("TEST 7 (полный граф)")
    test_7()
