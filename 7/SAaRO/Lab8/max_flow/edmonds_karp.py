from loguru import logger
from numpy import inf, zeros

#  8. Задачи о нахождении максимального потока из узла s в узел t


# noinspection PyUnresolvedReferences
class EdmondsKarpSolver:
    def _set_neighbours(self):
        for j, row in enumerate(self.d):
            for i, elem in enumerate(row):
                if elem != 0:
                    self.neighbours[i].add(j)
                    self.neighbours[j].add(i)
        logger.debug(f"Множество соседних вершин: {self.neighbours}")

    def __init__(self, d, n, s, t):
        self.n = n

        self.d = d
        self.x = zeros(shape=(n, n))

        self.neighbours = [set() for _ in range(n)]
        self._set_neighbours()

        self.s = s
        self.t = t
        self.flow = 0

        self.c_min = [0] * self.n
        self.c_min[self.s] = inf

        self.path = None  # множество помеченных узлов

    def get_c_min(self):
        self.path = {self.s: -inf}
        self._set_is_visited()
        return self._bfs()

    def _set_is_visited(self):
        self.is_visited = [False] * self.n
        self.is_visited[0] = True

    def _bfs(self):
        bfs_q = [self.s]
        while bfs_q:
            u = bfs_q.pop(0)
            for v in self.neighbours[u]:
                v: int = v
                delta = self.d[u][v] - self.x[u][v]
                if delta > 0 and not self.is_visited[v]:
                    self.is_visited[v] = True
                    self.path[v] = u
                    self.c_min[v] = min(self.c_min[u], delta)
                    if v != self.t:
                        bfs_q.append(v)
                        logger.debug(
                            f"Вершина {v} посещена и добавлена в увеличивающий  путь "
                            f"{self.path}"
                        )
                    else:
                        return self.c_min[self.t]
        return 0

    def set_nodes(self, c_min):
        # Для каждого ребра на найденном пути увеличиваем поток на c_min,
        # а в противоположном ему — уменьшаем на c_min.
        v = self.t
        while v != self.s:
            u = self.path[v]
            self.x[u][v] += c_min
            self.x[v][u] -= c_min
            v = u

    def solve(self):
        iteration_count = 1
        while True:
            logger.info(f"Итерация №{iteration_count}")

            c_min = self.get_c_min()
            logger.debug(f"Приращение с: {c_min}")

            if c_min == 0:
                break

            self.flow += c_min
            self.set_nodes(c_min)

            iteration_count += 1

        logger.success(f"Максимальный поток на сети: {self.flow}")
        return self
