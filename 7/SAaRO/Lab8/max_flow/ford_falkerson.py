from loguru import logger
from numpy import inf, zeros

#  8. Задачи о нахождении максимального потока из узла s в узел t


class FordFalkersonSolver:
    # Важно, что алгоритм не конкретизирует, какой именно путь мы ищем на ШАГе 2
    # или как мы это делаем. По этой причине алгоритм гарантированно сходится
    # только для целых пропускных способностей, но даже для них при больших значениях
    # пропускных способностей он может работать очень долго.
    # Если пропускные способности вещественны,
    # алгоритм может работать бесконечно долго, не сходясь к оптимальному решению
    # Сложность - O(Ef), где E - кол-во ребер, f - максимальный поток

    def _set_neighbours(self):
        for j, row in enumerate(self.d):
            for i, elem in enumerate(row):
                if elem != 0:
                    self.neighbours[i].add(j)
                    self.neighbours[j].add(i)
        logger.debug(f"Множество соседних вершин: {self.neighbours}")

    def __init__(self, d, n, s, t):
        self.n = n

        self.d = d
        self.x = zeros(shape=(n, n))

        self.neighbours = [set() for _ in range(n)]
        self._set_neighbours()

        self.s = s
        self.t = t
        self.flow = 0

        self.c_min = [0] * self.n
        self.c_min[self.s] = inf

        self.path = None

    def _set_is_visited(self):
        self.is_visited = [False] * self.n
        self.is_visited[0] = True

    def _is_t_visited(self):
        return self.is_visited[self.t]

    def find_path(self, u):
        for v in self.neighbours[u]:
            v: int = v

            # проверка нужна из-за рекурсивности алгоритма
            if self._is_t_visited():
                return self.c_min[self.t]

            # ШАГ 2: рассмотрим непомеченный узел j, для которого существует
            #        дуга (i, j) ∈ U и x_ij <= d_ij
            delta = self.d[u][v] - self.x[u][v]
            if delta > 0 and not self.is_visited[v]:
                # Помечаем узел и добавляем его в множество помеченных
                # узлов L
                self.is_visited[v] = True
                self.path[v] = u
                self.c_min[v] = min(self.c_min[u], delta)
                logger.debug(
                    f"Вершина {v} посещена и добавлена в увеличивающий  путь {self.path}"
                )

                self.find_path(v)

                # ШАГ 4: Если узел t помечен => STOP: увеличивающий путь найден
                #        Начинаем алгоритм восстановления пути
                if self._is_t_visited():
                    return self.c_min[self.t]

    def get_c_min(self):
        self._set_is_visited()
        self.path = [inf] * self.n
        return self.find_path(0)

    def set_nodes(self, c_min):
        # Для каждого ребра на найденном пути увеличиваем поток на c_min,
        # а в противоположном ему — уменьшаем на c_min.
        v = self.t
        while v != self.s:
            u = self.path[v]
            self.x[u][v] += c_min
            self.x[v][u] -= c_min
            v = u

    def solve(self):
        iteration_count = 1
        while True:
            logger.info(f"Итерация №{iteration_count}")

            c_min = self.get_c_min()
            logger.debug(f"Приращение с_min: {c_min}")

            # Если уже больше никак не возможно построить поток лучше
            # => мы нашли максимальный поток.
            if not self._is_t_visited():
                logger.success(f"Максимальный поток на сети: {self.flow}")
                return self

            if c_min < 0:
                return

            logger.warning(self.path)
            self.flow += c_min
            self.set_nodes(c_min)

            iteration_count += 1
