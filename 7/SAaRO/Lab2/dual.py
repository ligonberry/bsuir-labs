import random
from typing import List

import numpy as np
from dataclasses import dataclass


@dataclass
class Solution:
    x_0: List[float]
    Jb: List[int]
    Jn: List[int]

    def __repr__(self):
        return f"(x = {self.x_0}, Jb = {self.Jb}, Jn = {self.Jn})"

    def __iter__(self):
        yield from (self.x_0, self.Jb, self.Jn)


# noinspection PyPep8Naming
class DualSimplexMethodSolver:
    def __init__(self, A, b, c, Jb, d_min, d_max):
        self.A = A
        self.b = b
        self.c = c

        self.m, self.n = A.shape

        self.J = range(self.n)
        self.Jb = Jb

        self.d_min = d_min
        self.d_max = d_max

    def _get_Jn(self):
        return list(set(range(self.n)) - set(self.Jb))

    def _get_deltas(self, y):
        def delta(J):
            return y @ self.A[:, J] - self.c[J]

        return list(map(delta, list(self.J)))

    def is_optimum(self, pseudo_plan):
        deltas = []
        for i, x_i in enumerate(pseudo_plan):
            deltas.append(
                self.d_min[self.Jb[i]] - x_i <= 0 <= self.d_max[self.Jb[i]] - x_i
            )
        return all(deltas)

    def _find_k(self, pseudo_plan):
        result = []
        for i, x_i in enumerate(pseudo_plan):
            min_optimum = self.d_min[self.Jb[i]] - x_i
            max_optimum = self.d_max[self.Jb[i]] - x_i
            if not (min_optimum <= 0 <= max_optimum):
                result.append(i)
        return random.choice(result)

    @staticmethod
    def _get_mu(mu, j):
        for i, mu_i in mu:
            if i == j:
                return mu_i

    def solve(self):
        Jn = self._get_Jn()

        B = np.linalg.inv(self.A[:, self.Jb])

        # ШАГ 1: найдем m-вектор
        y = self.c[self.Jb] @ B

        # и оценки Δj = y'A_j - c_j
        deltas = self._get_deltas(y)

        # сформируем множества Jn+ и Jn-
        Jn_minus = []
        Jn_plus = []

        for j in Jn:
            if deltas[j] < 0:
                Jn_minus.append(j)
            else:
                Jn_plus.append(j)

        iter_count = 1
        while True:
            iter_count += 1

            # ШАГ 2: Построим вектор x по следующим правилам:
            b = np.copy(self.b)
            for j in Jn:
                if j in Jn_plus:
                    ksi = self.d_min[j]
                else:
                    ksi = self.d_max[j]
                b += -self.A[:, j] * ksi

            pseudo_plan = B @ b

            # ШАГ 3: Проверим критерий оптимальности: если выполняется соотношение:
            # d_*j <= x_j <= d_j* --- вектор x_0 - оптимальный план задачи.
            if self.is_optimum(pseudo_plan):
                x_0 = np.zeros(self.n)

                # заполняем для базисных индексов
                for i, x_i in enumerate(pseudo_plan):
                    x_0[self.Jb[i]] = x_i

                for j in Jn:
                    if j in Jn_minus:
                        x_0[j] = self.d_max[j]
                    else:
                        x_0[j] = self.d_min[j]
                # noinspection PyTypeChecker
                return Solution(x_0, self.Jb, Jn)

            # Шаг 4: Найдем такой индекс j_k ∈ J_Б, что x_jk ∉ [d_*jk, d_jk*]
            k = self._find_k(pseudo_plan)

            # ШАГ 5: Положим, что
            #        μ_jk = 1, если x_jk < d_*jk
            #        μ_jk = -1, если x_jk > d_jk*
            j_k = self.Jb[k]

            if pseudo_plan[k] < self.d_min[j_k]:
                mu_jk = 1
            else:
                mu_jk = -1

            # Подсчитаем m-вектор
            e_k = np.eye(1, self.m, k)
            delta_y = mu_jk * e_k @ B

            # и числа:
            mu = []
            for j in self.J:
                mu.append((j, float(delta_y @ self.A[:, j])))

            # ШАГ 6: Найдем шаги σ, j ∈ Jn по правилу:
            #        σ_j = -Δj / μ_j, если j ∈ Jn+ и μ_j < 0
            #                         либо j ∈ Jn- и μ_j > 0
            #        σ_j = ∞, в противном случае
            sigma = []
            for mu_j in mu:
                if (
                        mu_j[0] in Jn_plus and mu_j[1] < 0 or
                        mu_j[0] in Jn_minus and mu_j[1] > 0
                ):
                    sigma.append(-deltas[mu_j[0]] / mu_j[1])
                else:
                    sigma.append(np.inf)
            sigma_0 = min(sigma)

            # ШАГ 7: Если σ_0 = ∞, то прекращаем решение задачи, т.к она не имеет
            # допустимых планов
            if np.isinf(sigma_0):
                return

            # ШАГ 8: Построим новый коплан Δ' = (Δ'j, j ∈ J) по правилу:
            #        Δ'j = Δj + σ_0 * μ_j, j ∈ Jn ∪ j_k
            #        Δ'j = 0, j ∈ JБ \ j_k
            for j in self.J:
                if j in self.Jb and j != j_k:
                    deltas[j] = 0.0
                else:
                    deltas[j] = deltas[j] + sigma_0 * self._get_mu(mu, j)

            # ШАГ 9: Построим новый базис J'Б = (JБ \ j_k) ∪ j*, соотв. ему базисную
            # матрицу A'Б = (Aj, j ∈ JБ) и обратную матрицу B'
            j_0 = sigma.index(sigma_0)
            self.Jb[k] = j_0

            B = np.linalg.inv(self.A[:, self.Jb])

            # ШАГ 10: Построим новые множества J'n, J'n-, J'n+
            # j* = j_0
            Jn = self._get_Jn()
            if mu_jk == 1:
                # варианты 1 и 3
                if j_0 in Jn_plus:
                    Jn_plus.remove(j_0)
                Jn_plus.append(j_k)
            else:
                # вариант 2
                if j_0 in Jn_plus:
                    Jn_plus.remove(j_0)

            Jn_minus = list(set(Jn) - set(Jn_plus))
