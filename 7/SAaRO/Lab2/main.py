from math import floor
from typing import List

import numpy as np
from dataclasses import dataclass
from loguru import logger

from dual import DualSimplexMethodSolver

ACCURACY = 4
EPS = 0.0001


class BranchAndBoundMethodSolver:

    @dataclass
    class Solution:
        x: List
        cost: float

        def __str__(self):
            return f"(x = {self.x}, cost = {self.cost})"

        def __gt__(self, other):
            return self.cost > other.cost

    def __init__(self, A, b, c, Jb, d_min, d_max):
        self.A = A
        self.b = b
        self.c = c

        self.m, self.n = A.shape

        self.Jb = Jb

        self.d_min = d_min
        self.d_max = d_max

        # При μ_0 = 1 вектор μ принимаем за решение задачи.
        # При μ_0 = 0 в задаче нет допустимых планов.
        self.mu_0 = 0

        # Предполагаем самый худший случай (нет дополнительной информации)
        self.r_0 = -np.inf

        self.solution = None

    @staticmethod
    def is_integer(val):
        return abs(round(val) - val) < EPS

    def is_integer_array(self, x):
        return all([self.is_integer(val) for val in x])

    def get_float_elem_index(self, x):
        for i, val in enumerate(x):
            if not self.is_integer(val):
                return i

    @staticmethod
    def _round(array):
        result = []
        for number in array:
            result.append(round(number, ACCURACY))
        return result

    def recursion(self, d_min, d_max):
        # ШАГ 1-2: Выбираем задачу линейного программирования и решаем ее

        solution = DualSimplexMethodSolver(
            self.A, self.b, self.c, self.Jb, d_min, d_max
        ).solve()

        logger.info(f"Новая итерация")
        logger.debug(f"Решение задачи линейного программирования: {solution}")

        # Если эта задача не имеет решения
        if solution is None:
            logger.debug(
                "Вычеркиваем данную задачу из списка и возвращаемся к началу "
                "новой итерации: идем на ШАГ 1, заменив t на t + 1"
            )
            return

        x, _, _ = solution

        # или имеет решение, для которого выполняется:
        # c'x <= r_0
        cost = self.c @ x
        if cost <= self.r_0:
            logger.debug(
                "Вычеркиваем данную задачу из списка и возвращаемся к началу "
                "новой итерации: идем на ШАГ 1, заменив t на t + 1"
            )
            return

        # ШАГ 3: Если на решении x выполняется условие целочисленности,
        # то фиксируем это решение: полагаем μ = x, μ_0 = 1.
        # Изменяем оценку r_o = c'x. Вычеркиваем рассмотренную задачу из списка и
        # переходим к новой итерации: идем на ШАГ 1, заменив t на t + 1
        if self.is_integer_array(x):
            self.mu_0 = 1
            self.r_0 = cost
            self.solution = self.Solution(x, cost)
            logger.success(f"Решение μ = {self._round(x)} является целочисленным. "
                           f"Значение целевой функции c'x = {cost}")
            return
        else:
            # Если на решении x задачи условие целочисленности не выполняется,
            # то идем на ШАГ 4.
            logger.debug(f"Условие целочисленности не выполняется: x = {x}")

            # ШАГ 4: Выберем любую переменную x_j0, которая не удовлетворяет
            # условию целочисленности.
            j0 = self.get_float_elem_index(x)
            # Обозначим через l_0 целую часть числа x_j0.
            l_0 = floor(x[j0])
            logger.debug(
                f"Целая чать числа, которое не удовлетворяет условию "
                f"целочисленности [x_{j0}] = {l_0}"
            )

            # Удалим старую рассматриваемую задачу из списка, а вместо нее добавим
            # две новые задачи. Эти задачи отличаются от задачи, выбранной на ШАГе 1,
            # и от друг друга только прямыми ограничениями на переменную x_j0.
            # В первой задача эти ограничения имеют вид:
            #      d_min_jo <= x_j0 <= l_0
            new_d_max = np.copy(d_max)
            new_d_max[j0] = l_0
            self.recursion(d_min, new_d_max)

            # Во второй задаче эти ограничения имеют вид:
            #      l_0 + 1 <= x_j0 <= d_max_j0
            new_d_min = np.copy(d_min)
            new_d_min[j0] = l_0 + 1
            self.recursion(new_d_min, d_max)

    def solve(self):
        self.recursion(self.d_min, self.d_max)
        # ШАГ 5: Останавливаем алгоритм.
        # При μ_0 = 1 вектор μ принимаем за решение задачи.
        if self.mu_0:
            return self.solution
        else:
            logger.critical("Задача не имеет допустимых планов")
