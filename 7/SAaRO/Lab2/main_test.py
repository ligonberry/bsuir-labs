import numpy as np

from main import BranchAndBoundMethodSolver
from loguru import logger


# noinspection PyPep8Naming
def setup_test_0():
    A = np.array([[1, -5, 3, 1, 0, 0],
                  [4, -1, 1, 0, 1, 0],
                  [2, 4, 2, 0, 0, 1]])
    b = np.array([-8, 22, 30])
    c = np.array([7, -2, 6, 0, 5, 2])
    d_low = np.array([2, 1, 0, 0, 1, 1])
    d_high = np.array([6, 6, 5, 2, 4, 6])
    Jb = [3, 4, 5]
    return BranchAndBoundMethodSolver(A, b, c, Jb, d_low, d_high)


def test_0():
    x0 = setup_test_0()
    solution = x0.solve()
    assert (solution.x == [6, 3, 0, 1, 1, 6]).all()
    assert solution.cost == 53


# noinspection PyPep8Naming
def setup_1():
    A = np.array([
        [1, 0, 0, 3, 1, -3, 4, -1],
        [0, 1, 0, 4, -3, 3, 5, 3],
        [0, 0, 1, 1, 0, 2, -2, 1]
    ])
    b = np.array([30, 78, 5])
    c = np.array([2, 1, -2, -1, 4, -5, 5, 5])
    d_low = np.array([0, 0, 0, 0, 0, 0, 0, 0])
    d_high = np.array([5, 5, 3, 4, 5, 6, 6, 8])
    Jb = [3, 4, 5]
    return BranchAndBoundMethodSolver(A, b, c, Jb, d_low, d_high)


def test_1():
    x0 = setup_1()
    solution = x0.solve()
    assert (solution.x == [5, 5, 3, 4, 0, 1, 6, 8]).all()
    assert solution.cost == 70.0


# noinspection PyPep8Naming
def setup_2():
    A = np.array([
        [1, -3, 2, 0, 1, -1, 4, -1, 0],
        [1, -1, 6, 1, 0, -2, 2, 2, 0],
        [2, 2, -1, 1, 0, -3, 2, -1, 1],
        [4, 1, 0, 0, 1, -1, 0, -1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1]
    ])
    b = np.array([18, 18, 30, 15, 18])
    c = np.array([7, 5, -2, 4, 3, 1, 2, 8, 3])
    d_low = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])
    d_high = np.array([8, 8, 8, 8, 8, 8, 8, 8, 8])
    Jb = [1, 2, 3, 4, 5]
    return BranchAndBoundMethodSolver(A, b, c, Jb, d_low, d_high)


def test_2():
    x0 = setup_2()
    solution = x0.solve()
    assert round(solution.cost) == 78


def setup_9():
    A = np.array([
        [2, 0, 1, 0, 0, 3, 5],
        [0, 2, 2.1, 0, 0, 3.5, 5],
        [0, 0, 3, 2, 0, 2, 1.1],
        [0, 0, 3, 0, 2, 2, -2.5]
    ])
    b = np.array([58, 66.3, 36.7, 13.5])
    c = np.array([1, 2, 3, 1, 2, 3, 4])
    d_low = np.array([1, 1, 1, 1, 1, 1, 1])
    d_high = np.array([2, 3, 4, 5, 8, 7, 7])
    Jb = [1, 2, 3, 4]
    return BranchAndBoundMethodSolver(A, b, c, Jb, d_low, d_high)


def test_9():
    x0 = setup_9()
    solution = x0.solve()
    assert round(solution.cost) == 74


if __name__ == "__main__":
    logger.warning("\nTEST EXAMPLE")
    test_0()
    logger.warning("\nTEST 1")
    test_1()
    logger.warning("\nTEST 2")
    test_2()
    logger.warning("\nTEST 9")
    test_9()
