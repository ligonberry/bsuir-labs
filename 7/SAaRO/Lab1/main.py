import random
from typing import NamedTuple

import numpy as np
from loguru import logger


# noinspection PyPep8Naming
class DualSimplexMethodSolver:
    def __init__(self, A, b, c, Jb, d_min, d_max):
        self.A = A
        self.b = b
        self.c = c

        self.m, self.n = A.shape

        self.J = range(self.n)
        self.Jb = Jb

        self.d_min = d_min
        self.d_max = d_max

    def _get_Jn(self):
        return set(range(self.n)) - set(self.Jb)

    def _get_deltas(self, y):
        def delta(j):
            return y @ self.A[:, j] - self.c[j]

        return list(map(delta, list(self.J)))

    def is_optimum(self, pseudo_plan):
        deltas = []
        for i, x_i in enumerate(pseudo_plan):
            deltas.append(
                self.d_min[self.Jb[i]] - x_i <= 0 <= self.d_max[self.Jb[i]] - x_i
            )
        logger.debug(f"Δ: {deltas}")
        return all(deltas)

    def _find_k(self, pseudo_plan):
        result = []
        for i, x_i in enumerate(pseudo_plan):
            d_min_i, d_max_i = self.d_min[self.Jb[i]], self.d_max[self.Jb[i]]
            is_optimum = d_min_i <= x_i <= d_max_i
            if not is_optimum:
                logger.debug(
                    f"Найден индекс k={i}, что x_j{i}={x_i} ∉ [{d_min_i}, {d_max_i}]"
                )
                result.append(i)
        return random.choice(result)

    class Mu(NamedTuple):
        j: int
        value: float

    def solve(self):
        Jn = self._get_Jn()

        B = np.linalg.inv(self.A[:, self.Jb])

        # ШАГ 1: найдем m-вектор y
        y = self.c[self.Jb] @ B

        # и оценки Δj = y'A_j - c_j
        deltas = self._get_deltas(y)
        logger.debug(f"Вектор y = {y} и оценки {deltas}")

        # сформируем множества Jn+ и Jn-
        Jn_minus = []
        Jn_plus = []

        for j in Jn:
            if deltas[j] < 0:
                Jn_minus.append(j)
            else:
                Jn_plus.append(j)

        logger.debug(f"Индексы Jn+ = {Jn_plus}, Jn- = {Jn_minus}")

        iter_count = 1
        while True:
            logger.info(f"Итерация №{iter_count}")
            iter_count += 1

            logger.debug(f"Базисные индексы итерации {self.Jb}")

            # ШАГ 2: Построим вектор x по следующим правилам:
            b = np.copy(self.b)
            for j in Jn:
                if j in Jn_plus:
                    ksi = self.d_min[j]
                else:
                    ksi = self.d_max[j]
                b -= self.A[:, j] * ksi

            pseudo_plan = B @ b

            logger.debug(f"X_Б = {pseudo_plan}")

            # ШАГ 3: Проверим критерий оптимальности: если выполняется соотношение:
            #        d_*j <= x_j <= d_j* --- вектор x_0 - оптимальный план задачи.
            if self.is_optimum(pseudo_plan):
                x_0 = np.zeros(self.n)

                # находим план прямой задачи по ее двойственному плану:
                for i, x_i in enumerate(pseudo_plan):
                    x_0[self.Jb[i]] = x_i

                for j in Jn:
                    if j in Jn_minus:
                        x_0[j] = self.d_max[j]
                    else:
                        x_0[j] = self.d_min[j]
                logger.success(f"Решение найдено: x_0 = {x_0}")
                return x_0

            # Шаг 4: Найдем такой индекс j_k ∈ J_Б, что x_jk ∉ [d_*jk, d_jk*]
            k = self._find_k(pseudo_plan)

            # ШАГ 5: Положим, что
            #        μ_jk = 1, если x_jk < d_*jk
            #        μ_jk = -1, если x_jk > d_jk*
            j_k = self.Jb[k]

            if pseudo_plan[k] < self.d_min[j_k]:
                mu_jk = 1
            else:
                mu_jk = -1

            # Подсчитаем m-вектор
            e_k = np.eye(1, self.m, k)
            delta_y = mu_jk * e_k @ B
            logger.debug(f"jk = j{k} = {j_k}, μ_jk = {mu_jk}, Δy = {delta_y}")

            # и числа:
            mu = []
            for j in self.J:
                mu.append(self.Mu(j, delta_y @ self.A[:, j]))
            logger.debug(f"μ_j = {mu}")

            # ШАГ 6: Найдем шаги σ, j ∈ Jn по правилу:
            #        σ_j = -Δj / μ_j, если j ∈ Jn+ и μ_j < 0
            #                         либо j ∈ Jn- и μ_j > 0
            #        σ_j = ∞, в противном случае
            sigma = []
            for mu_j in mu:
                if (
                        mu_j.j in Jn_plus and mu_j.value < 0 or
                        mu_j.j in Jn_minus and mu_j.value > 0
                ):
                    sigma.append(-deltas[mu_j.j] / mu_j.value)
                else:
                    sigma.append(np.inf)
            sigma_0 = min(sigma)
            logger.debug(f"Вектор σ = {sigma}, минимальная σ_0 = {sigma_0}")

            # ШАГ 7: Если σ_0 = ∞, то прекращаем решение задачи, т.к она не имеет
            #        допустимых планов
            if np.isinf(sigma_0):
                logger.critical("Задача не имеет допустимых планов!")
                return

            # ШАГ 8: Построим новый коплан Δ' = (Δ'j, j ∈ J) по правилу:
            #        Δ'j = Δj + σ_0 * μ_j, j ∈ Jn ∪ j_k
            #        Δ'j = 0, j ∈ JБ \ j_k
            for j in self.J:
                if j in self.Jb and j != j_k:
                    deltas[j] = 0.0
                else:
                    deltas[j] = deltas[j] + sigma_0 * mu[j].value
            logger.debug(f"Новый коплан Δ' = {deltas}")

            # ШАГ 9: Построим новый базис J'Б = (JБ \ j_k) ∪ j*, соотв. ему базисную
            #        матрицу A'Б = (Aj, j ∈ JБ) и обратную матрицу B'
            j_star = sigma.index(sigma_0)
            self.Jb[k] = j_star
            logger.debug(f"Новый базис J'Б = {self.Jb}")

            B = np.linalg.inv(self.A[:, self.Jb])

            # ШАГ 10: Построим новые множества J'n, J'n-, J'n+
            Jn = self._get_Jn()
            if mu_jk == 1:
                # варианты 1 и 3
                if j_star in Jn_plus:
                    Jn_plus.remove(j_star)
                Jn_plus.append(j_k)
            else:
                # вариант 2
                if j_star in Jn_plus:
                    Jn_plus.remove(j_star)

            Jn_minus = list(Jn - set(Jn_plus))
            logger.debug(f"Индексы J'n = {Jn}, J'n- = {Jn_minus}, J'n+ = {Jn_plus}")
