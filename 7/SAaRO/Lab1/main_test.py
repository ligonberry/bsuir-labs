import numpy as np
from loguru import logger

from main import DualSimplexMethodSolver


# noinspection PyPep8Naming
def setup_example():
    A = np.array([[2, 1, -1, 0, 0, 1],
                  [1, 0, 1, 1, 0, 0],
                  [0, 1, 0, 0, 1, 0]])
    b = np.array([2, 5, 0])
    c = np.array([3, 2, 0, 3, -2, -4])
    d_min = [0, -1, 2, 1, -1, 0]
    d_max = [2, 4, 4, 3, 3, 5]
    J = np.array([3, 4, 5])
    return DualSimplexMethodSolver(A, b, c, J, d_min, d_max)


def test_solve_example():
    d = setup_example()
    assert all([a == b for a, b in zip(d.solve(), [1.5, 1, 2, 1.5, -1, 0])])


# noinspection PyPep8Naming
def setup_1():
    A = np.array([[1, -5, 3, 1, 0, 0],
                  [4, -1, 1, 0, 1, 0],
                  [2, 4, 2, 0, 0, 1]])
    b = np.array([-7, 22, 30])
    c = np.array([7, -2, 6, 0, 5, 2])
    d_min = [2, 1, 0, 0, 1, 1]
    d_max = [6, 6, 5, 2, 4, 6]
    J = np.array([3, 4, 5])
    return DualSimplexMethodSolver(A, b, c, J, d_min, d_max)


def test_solve_1():
    d = setup_1()
    assert all([round(a) == b for a, b in zip(d.solve(), [5., 3., 1., 0., 4., 6.])])


# noinspection PyPep8Naming
def setup_2():
    A = np.array([[1, 0, 2, 2, -3, 3],
                  [0, 1, 0, -1, 0, 1],
                  [1, 0, 1, 3, 2, 1]])
    b = np.array([15, 0, 13])
    c = np.array([3, 0.5, 4, 4, 1, 5])
    d_min = [0, 0, 0, 0, 0, 0]
    d_max = [3, 5, 4, 3, 3, 4]
    J = np.array([3, 4, 5])
    return DualSimplexMethodSolver(A, b, c, J, d_min, d_max)


def test_solve_2():
    d = setup_2()
    assert all([round(a, 4) == b for a, b in zip(d.solve(),
                                                 [3.0, 0.0, 4.0, 1.1818, 0.6364, 1.1818])
                ])


# noinspection PyPep8Naming
def setup_4():
    A = np.array([[1, 0, 0, 12, 1, -3, 4, -1],
                  [0, 1, 0, 11, 12, 3, 5, 3],
                  [0, 0, 1, 1, 0, 22, -2, 1]])
    b = np.array([40, 107, 61])
    c = np.array([2, 1, -2, -1, 4, -5, 5, 5])
    d_min = [0, 0, 0, 0, 0, 0, 0, 0]
    d_max = [3, 5, 5, 3, 4, 5, 6, 3]
    J = np.array([0, 1, 2])
    return DualSimplexMethodSolver(A, b, c, J, d_min, d_max)


def test_solve_4():
    d = setup_4()
    assert all([round(a, 4) == b for a, b in zip(d.solve(),
                                                 [3, 5, 0, 1.8779, 2.7545, 3.0965, 6, 3]
                                                 )])


# noinspection PyPep8Naming
def setup_5():
    A = np.array(
        [[1, 7, 2, 0, 1, -1, 4], [0, 5, 6, 1, 0, -3, 2], [3, 2, 2, 1, 1, 1, 5]]
    )
    b = np.array([1, 4, 7])
    c = np.array([1, 2, 1, -3, 3, 1, 0])
    d_min = [-1, 1, -2, 0, 1, 2, 4]
    d_max = [3, 2, 2, 5, 3, 4, 5]
    J = np.array([3, 4, 5])
    return DualSimplexMethodSolver(A, b, c, J, d_min, d_max)


def test_solve_5():
    d = setup_5()
    assert d.solve() is None


if __name__ == "__main__":
    logger.debug("\nTEST EXAMPLE")
    test_solve_example()
    logger.debug("\nTEST 1")
    test_solve_1()
    logger.debug("\nTEST 2")
    test_solve_2()
    logger.debug("\nTEST 4")
    test_solve_4()
    logger.debug("\nTEST 5")
    test_solve_5()
