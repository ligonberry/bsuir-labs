from loguru import logger
from numpy import inf, array


class FloydMethodSolver:
    def _generate_R(self):
        return [list(range(self.n)) for _ in range(self.n)]

    def __init__(self, D):
        self.D = D  # матрица длин кратчайших путей: содержит текущие оценки для
        # кратчайших путей.
        self.n = len(D[0])
        self.R = self._generate_R()  # матрица маршрутов: нахождение промежуточных
        # узлов кратчайших путей

    def show_results(self):
        logger.debug("Матрица кратчайших путей: ")
        logger.success(f"\n{array(self.D)}\n")

        logger.debug("Матрица маршрутов: ")
        logger.success(f"\n{array(self.R)}")

    def solve(self):
        # ШАГ 1: вычеркнем элемент j-ого столбца и j-ой строки матрицы D(j-1)
        for j in range(self.n):
            for i in range(self.n):
                for k in range(self.n):
                    # Правила, упрощаюшие пересчет матрицы D.
                    # Если d_jk и d_ij == inf => элемент не пересчитывается.
                    if self.D[i][j] != inf and self.D[j][k] != inf:

                        # ШАГ 2: для каждого элемента (i, k), (i != j, k != j)
                        if i != j and k != j:
                            # сравним число d_ik и сумму соответсвенных эл
                            # базового столбца и строки:
                            if self.D[i][k] > self.D[i][j] + self.D[j][k]:
                                self.D[i][k] = self.D[i][j] + self.D[j][k]
                                # паралельно изменяем эл. матрицы R
                                self.R[i][k] = self.R[i][j]

        return self
