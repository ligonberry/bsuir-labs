from datetime import timedelta
from timeit import default_timer as timer

from loguru import logger
from numpy import array_equal, inf
from scipy.sparse import csgraph

from main import FloydMethodSolver


# https://gist.github.com/mosco/11178777


def solve(D):
    start = timer()
    res = FloydMethodSolver(D).solve()
    end = timer()
    logger.info(
        f"Время выполнения для FloydMethodSolver = {timedelta(seconds=end - start)}"
    )
    res.show_results()
    return res


def scipy_solve(D):
    start = timer()
    scipy_D = csgraph.floyd_warshall(csgraph=D, directed=True)
    end = timer()
    logger.info(
        f"Время выполнения для floyd_warshall = {timedelta(seconds=end - start)}"
    )
    return scipy_D


def test_example_1():
    D = [
        [0, 9, inf, 3, inf, inf, inf, inf],
        [9, 0, 2, inf, 7, inf, inf, inf],
        [inf, 2, 0, 2, 4, 8, 6, inf],
        [3, inf, 2, 0, inf, inf, 5, inf],
        [inf, 7, 4, inf, 0, 10, inf, inf],
        [inf, inf, 8, inf, 10, 0, 7, inf],
        [inf, inf, 6, 5, inf, 7, 0, inf],
        [inf, inf, inf, inf, 9, 12, 10, 0],
    ]

    logger.warning(f"TEST EXAMPLE 1")
    res = solve(D)
    scipy_D = scipy_solve(D)

    assert array_equal(res.D, scipy_D)


def test_2():
    D = [
        [0, 3, 2, 6, inf, inf, inf, inf, inf],
        [inf, 0, inf, 2, inf, inf, inf, inf, inf],
        [inf, inf, 0, inf, inf, 4, inf, inf, inf],
        [inf, inf, 3, 0, 1, inf, 6, inf, inf],
        [inf, inf, inf, inf, 0, inf, 7, 5, inf],
        [inf, inf, inf, inf, 5, 0, inf, 4, inf],
        [inf, inf, inf, inf, inf, inf, 0, 2, 4],
        [inf, inf, inf, inf, inf, inf, inf, 0, 4],
        [inf, inf, inf, inf, inf, inf, inf, inf, 0],
    ]

    logger.warning(f"TEST 2")
    res = solve(D)
    scipy_D = scipy_solve(D)

    assert array_equal(res.D, scipy_D)


def test_3():
    D = [
        [0, 3, 2, 6, inf, inf, inf, inf, inf],
        [inf, 0, inf, 2, inf, inf, inf, inf, inf],
        [inf, inf, 0, inf, inf, 4, inf, inf, inf],
        [inf, inf, 3, 0, 1, inf, 6, inf, inf],
        [inf, inf, inf, inf, 0, inf, 7, 5, inf],
        [inf, inf, inf, inf, 5, 0, inf, 4, inf],
        [inf, inf, inf, inf, inf, inf, 0, 2, 4],
        [inf, inf, inf, inf, inf, inf, inf, 0, 15],
        [inf, inf, inf, inf, inf, inf, inf, inf, 0],
    ]

    logger.warning(f"TEST 3")
    res = solve(D)
    scipy_D = scipy_solve(D)

    assert array_equal(res.D, scipy_D)


def test_4():
    D = [
        [0, 3, 4, inf, 5, inf, inf, inf],
        [inf, 0, 2, 1, inf, inf, 4, inf],
        [inf, inf, 0, 3, 2, inf, inf, inf],
        [inf, inf, inf, 0, inf, inf, 3, inf],
        [inf, inf, inf, 4, 0, 8, inf, 3],
        [inf, inf, inf, 5, inf, 0, inf, 2],
        [inf, inf, inf, inf, inf, 2, 0, 1],
        [inf, inf, inf, inf, inf, inf, inf, 0],
    ]

    logger.warning(f"TEST 4")
    res = solve(D)
    scipy_D = scipy_solve(D)

    assert array_equal(res.D, scipy_D)


if __name__ == "__main__":
    test_example_1()
    test_2()
    test_3()
    test_4()
