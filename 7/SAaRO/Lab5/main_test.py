from typing import NamedTuple

from main import PotentialProblemSolver


class Node(NamedTuple):
    source: int
    destination: int
    cost: int
    x: int
    is_basic: int


def test_example_1():
    node_count = 6
    S = [
        Node(0, 1, 1, 1, 1),
        Node(1, 5, 3, 0, 0),
        Node(5, 0, -2, 0, 0),
        Node(2, 1, 3, 3, 1),
        Node(5, 2, 3, 9, 1),
        Node(5, 4, 4, 0, 0),
        Node(4, 2, 4, 0, 0),
        Node(4, 3, 1, 5, 1),
        Node(2, 3, 5, 1, 1),
    ]

    p = PotentialProblemSolver(node_count, S).solve()

    assert p.x.sort() == [4, 3, 5, 1, 6, 0, 0, 0].sort()
    assert p.f == 23


def test_1():
    node_count = 9
    S = [
        Node(0, 1, 9, 2, 1),
        Node(0, 7, 5, 7, 1),
        Node(1, 2, 1, 4, 1),
        Node(1, 6, 5, 3, 1),
        Node(1, 5, 3, 0, 0),
        Node(7, 6, 2, 0, 0),
        Node(7, 8, 2, 0, 0),
        Node(6, 8, 1, 0, 0),
        Node(6, 2, -1, 0, 0),
        Node(6, 3, 4, 0, 0),
        Node(6, 4, 7, 5, 1),
        Node(2, 8, -2, 0, 0),
        Node(3, 2, -3, 0, 0),
        Node(4, 3, 6, 3, 1),
        Node(8, 5, 6, 2, 1),
        Node(5, 4, 8, 4, 1),
    ]

    p = PotentialProblemSolver(node_count, S).solve()
    assert p.f == 127


def test_7():
    node_count = 7
    S = [
        Node(0, 1, 7, 2, 1),
        Node(0, 2, 6, 3, 1),
        Node(2, 3, 6, 4, 1),
        Node(2, 4, 5, 4, 1),
        Node(5, 6, 4, 2, 1),
        Node(6, 4, 7, 5, 1),
        Node(1, 2, 4, 0, 0),
        Node(1, 5, 3, 0, 0),
        Node(3, 5, 1, 0, 0),
        Node(4, 3, 4, 0, 0),
        Node(4, 5, -1, 0, 0),
        Node(0, 4, 3, 0, 0),
        Node(6, 0, 2, 0, 0),
    ]

    p = PotentialProblemSolver(node_count, S).solve()
    assert p.f == 85


if __name__ == "__main__":
    test_example_1()
    test_1()
    test_7()
