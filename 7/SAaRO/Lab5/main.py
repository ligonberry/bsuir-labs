from enum import Enum
from dataclasses import dataclass

import numpy as np

from loguru import logger


class PotentialProblemSolver:
    class Arc:
        def __init__(self, S):
            self.source = S.source
            self.destination = S.destination
            self.cost = S.cost

            self.x = S.x
            self.is_basic = S.is_basic

    def _get_arcs(self):
        arcs = []
        for i in self.S:
            arcs.append(self.Arc(i))
        return arcs

    def __init__(self, n, S):
        self.S = S

        self.arcs = self._get_arcs()

        self.n = n
        self.m = len(self.arcs)

        # Множество базисных позиций
        self.Ub = None

        self.u = [np.inf] * self.n
        self.u[0] = 0

        self.cycle_start_point = 0
        self.cycle_end_point = np.inf

        self.U_plus = []
        self.U_minus = []

        self.f = 0
        self.x = []

    def set_Ub(self):
        self.Ub = {i: [] for i in range(self.n)}
        for vertex in range(self.n):
            for arc in range(self.m):
                if self.arcs[arc].is_basic:
                    source = self.arcs[arc].source
                    destination = self.arcs[arc].destination
                    if vertex == source:
                        self.Ub[source].append(destination)
                        self.Ub[destination].append(source)

    def count_potentials(self):
        is_visited = [False] * self.n

        def get_u(start, is_visited):
            is_visited[start] = True

            for vertex in self.Ub[start]:
                if not is_visited[vertex]:

                    for temp_arc in self.arcs:
                        if start == temp_arc.source and vertex == temp_arc.destination:
                            self.u[vertex] = self.u[start] - temp_arc.cost
                        elif (
                            start == temp_arc.destination and vertex == temp_arc.source
                        ):
                            self.u[vertex] = self.u[start] + temp_arc.cost

                    get_u(vertex, is_visited)

        get_u(0, is_visited)

    @dataclass
    class Path:
        source: int
        destination: int

        def __repr__(self):
            return f"({self.source}, {self.destination})"

    @dataclass
    class Delta:
        path: dataclass
        delta: float

        def __iter__(self):
            yield from (self.path, self.delta)

        def __repr__(self):
            return f"Δ_{self.path} = {self.delta}"

    def get_deltas(self):
        deltas = []
        for arc in self.arcs:
            if not arc.is_basic:
                path = self.Path(source=arc.source, destination=arc.destination)
                delta = self.u[arc.source] - self.u[arc.destination] - arc.cost
                deltas.append(self.Delta(path=path, delta=delta))
        return deltas

    @staticmethod
    def get_max_delta(deltas):
        values = [value for _, value in deltas]
        max_value = max(values)
        max_delta_index = np.argmax(values)
        max_delta_path = deltas[max_delta_index].path
        return max_delta_path, max_value, max_delta_index

    def show_x(self):
        return [
            f"x ({arc.source + 1}->{arc.destination + 1}) = {arc.x}"
            for arc in self.arcs
        ]

    def mark_node_as_basic(self, max_delta_path):
        for arc in self.arcs:
            if (
                arc.source == max_delta_path.source
                and arc.destination == max_delta_path.destination
            ):
                arc.is_basic = True

    class Visited(Enum):
        YES = 1
        NO = 0

    def _walk_to_closest(self, vertex, is_visited, previous):
        is_visited[vertex] = self.Visited.YES.value

        for v_neighbor in self.Ub[vertex]:

            if is_visited[v_neighbor] == self.Visited.NO.value:
                previous[v_neighbor] = vertex
                if self._walk_to_closest(v_neighbor, is_visited, previous):
                    return True
            elif (
                is_visited[v_neighbor] == self.Visited.YES.value
                and previous[vertex] != v_neighbor
            ):
                self.cycle_end_point = vertex
                self.cycle_start_point = v_neighbor
                return True

        return False

    def _set_cycle(self, cycle, previous):
        vertex = self.cycle_end_point
        while vertex != self.cycle_start_point:
            cycle.append(vertex)
            vertex = previous[vertex]

        cycle.append(self.cycle_start_point)
        return cycle

    def get_cycle(self, source):
        self.set_Ub()

        is_visited = [0] * self.n
        previous = [None] * self.n

        cycle = []
        if self._walk_to_closest(source, is_visited, previous):
            cycle = self._set_cycle(cycle, previous)

        return reversed(cycle)

    def create_U_plus_minus(self, U, max_delta_path):
        for vertex in range(len(U) - 1):
            for node in self.arcs:
                if U[vertex] == node.source and U[vertex + 1] == node.destination:
                    self.U_plus.append(self.Path(U[vertex], U[vertex + 1]))
                elif U[vertex] == node.destination and U[vertex + 1] == node.source:
                    self.U_minus.append(self.Path(U[vertex + 1], U[vertex]))

        if max_delta_path not in self.U_plus:
            self.U_plus, self.U_minus = self.U_minus, self.U_plus

    @dataclass
    class Teta:
        path: dataclass
        value: float

        def __repr__(self):
            return f"Θ_{self.path} = {self.value}"

        def __iter__(self):
            yield from (self.path, self.value)

    def get_tetas(self):
        tetas = []
        for node in self.arcs:
            path = self.Path(node.source, node.destination)
            if path in self.U_minus:
                tetas.append(self.Teta(path, node.x))
        return tetas

    @staticmethod
    def get_min_teta(tetas):
        values = [value for _, value in tetas]
        min_value = min(values)
        min_teta = np.argmin(values)
        min_path = tetas[min_teta].path
        return min_value, min_path

    def get_index(self, source, destination):
        for arc in self.arcs:
            if source == arc.source and destination == arc.destination:
                return self.arcs.index(arc)

    def solve(self):
        iteration_count = 1
        while True:
            logger.info(f"Итерация №{iteration_count}")
            iteration_count += 1
            # Создать базисное множество в формате
            # [{номер_вершины} : [ее вершины-соседи], ...]
            self.set_Ub()
            logger.debug(f"Базисное множество дуг: {self.Ub}")

            # Используя базисное множество дуг Ub, подсчитаем потенциалы
            # u_i i ∈ I узлов по правилу:
            # u_i - u_j = c_ij (i, j) ∈ Ub
            # u_0 = 0
            self.count_potentials()
            logger.debug(f"Значения потенциалов u: {self.u}")

            # Зная потенциалы, подсчитаем оценки
            # Δ_ij = u_i - u_j - c_ij, (i, j) ∈ Uн = U \ Ub
            deltas = self.get_deltas()
            logger.debug(f"Значения Δ_ij: {deltas}")

            # Найдем максимальную оценку
            max_delta_path, max_delta_value, max_delta_index = self.get_max_delta(
                deltas
            )
            logger.debug(
                f"Максимальная оценка {max_delta_index} со значением "
                f"{max_delta_value} и дугой {max_delta_path}"
            )

            # Проверим выполнения условия
            # Δ_ij <= 0, (i, j) ∈ Uн = U \ Ub
            if max_delta_value <= 0:
                for arc in self.arcs:
                    self.f += arc.x * arc.cost
                    self.x.append(arc.x)
                logger.success(f"Значение f = {self.f}")
                logger.success(self.show_x())
                return self

            # Добавим узел (i_0, j_0) с максимальной оценкой во множество базисных дуг
            self.mark_node_as_basic(max_delta_path)

            # Найдем цикл (с помощью обхода в ширину и
            # последовательного добавления соседей)
            cycle = list(self.get_cycle(max_delta_path.source))
            cycle.append(cycle[0])
            logger.debug(f"Цикл {cycle}")

            # Выделим в этом цикле множество прямых U+_цикл и обратных U-_цикл дуг
            self.create_U_plus_minus(cycle, max_delta_path)
            logger.debug(f"U+ = {self.U_plus}, U- = {self.U_minus}")

            tetas = self.get_tetas()
            min_teta_value, min_teta_path = self.get_min_teta(tetas)
            logger.debug(
                f"Θ = {tetas}. Минимальная Θ_0 = {min_teta_value}, "
                f"дуга, которая ей соответсвует {min_teta_path}"
            )

            # Построим новый поток x' = (x_ij, (i, j) ∈ U по правилу:
            # x'_ij = x_ij, (i, j) ∈ U \ U_цикл
            # x'_ij = x_ij + Q_0, (i, j) ∈ U+_цикл
            for vertex in self.U_plus:
                i = self.get_index(vertex.source, vertex.destination)
                self.arcs[i].x += min_teta_value

            # x'_ij = x_ij - Q_0, (i, j) ∈ U-_цикл
            for vertex in self.U_minus:
                i = self.get_index(vertex.source, vertex.destination)
                self.arcs[i].x -= min_teta_value

            # Для нового потока построим новое базисное множество дуг:
            # U'и = (Ub \ (i*, j*)) ∪ (i_0, j_0)
            i = self.get_index(min_teta_path.source, min_teta_path.destination)
            self.arcs[i].is_basic = False

            self.U_plus = []
            self.U_minus = []
