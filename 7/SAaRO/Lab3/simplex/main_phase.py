from dataclasses import dataclass
from math import inf
from typing import List

import numpy as np


@dataclass
class Solution:
    B: List[List[float]]
    x: List[float]
    Jb: List[int]
    Jn: List[int]


# noinspection PyPep8Naming
def inverse_matrix(Ar, x, i):
    l = Ar @ x
    if l[i] == 0:
        raise ValueError("Matrix is irreversible")
    else:
        l1 = l.copy()
        l1[i] = -1
        l2 = (-1.0 / l[i]) * l1
        Q = np.eye(len(x))
        Q[:, i] = l2
        return Q @ Ar


def is_optimum(non_basis_deltas):
    for _, delta in non_basis_deltas:
        if delta < 0:
            return False
    return True


def get_j_0(non_basis_deltas):
    for index, delta in non_basis_deltas:
        if delta < 0:
            return index


def is_not_limited(z):
    for z_i in z:
        if z_i >= 0:
            return False
    return True


# noinspection PyPep8Naming
class MainPhaseSimplex:
    def __init__(self, A, c, initial_plan, J_b):
        self.A = A
        self.c = c
        self.initial_plan = initial_plan

        self.x = initial_plan
        self.Jb = J_b

        self.Jn = None
        self.B = None

        self.m, self.n = A.shape

    def _get_non_basis_deltas(self, deltas):
        non_basis_deltas = []
        for index, delta in enumerate(deltas):
            if index not in self.Jb:
                non_basis_deltas.append((index, delta))
        return non_basis_deltas

    def _get_tetas(self, z):
        tetas = []
        for index, value in zip(self.Jb, z):
            if value > 0:
                tetas.append(self.x[index] / value)
            else:
                tetas.append(inf)
        return tetas

    def solve(self):
        # print(self.A)
        A_b = self.A[:, self.Jb]
        self.B = np.linalg.inv(A_b)
        # print(self.B)

        while True:
            c_b = self.c[self.Jb]

            # STEP 1
            u = c_b @ self.B
            deltas = u @ self.A - self.c
            non_basis_deltas = self._get_non_basis_deltas(deltas)

            # STEP 2
            if is_optimum(non_basis_deltas):
                self.Jn = list(set(range(self.n)) - set(self.Jb))
                return Solution(
                    self.B, self.x, self.Jb, self.Jn
                )

            # STEP 3
            else:
                j_0 = get_j_0(non_basis_deltas)
                z = self.B @ self.A[:, j_0]

                if is_not_limited(z):
                    print("Cost function feasible plans aren't limited at the top")
                    break

                # STEP 4
                else:
                    tetas = self._get_tetas(z)
                    teta_0 = min(tetas)
                    s = tetas.index(teta_0)

                    # STEP 5
                    self.Jb[s] = j_0
                    new_plan = []

                    for index, value in enumerate(self.x):
                        if index in self.Jb:
                            if index == j_0:
                                new_plan.append(teta_0)
                            else:
                                new_plan.append(
                                    value - teta_0 * z[self.Jb.index(index)]
                                )
                        else:
                            new_plan.append(0)
                    self.x = new_plan

                    # STEP 6
                    self.B = inverse_matrix(self.B, self.A[:, j_0], tetas.index(teta_0))
