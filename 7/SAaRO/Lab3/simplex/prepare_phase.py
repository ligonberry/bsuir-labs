from random import choice

import numpy as np

from simplex.main_phase import MainPhaseSimplex


# noinspection PyPep8Naming
class PreparePhaseSimplex:
    def __init__(self, A, b):
        self.A = A
        self.m, self.n = A.shape

        self.b = b
        self.c = np.zeros(self.n)

        self.initial_plan = np.zeros(self.n)

        self.J = []
        self.J_u = []
        self.J_b = []

    # noinspection PyPep8Naming
    def _get_extended_A(self, A):
        """ Получим расширеннную матрицу (исходная А + Е). """
        return np.concatenate((self.A, A), axis=1)

    def _get_main_phase_obj(self, A, c, initial_plan, J):
        A = self._get_extended_A(A)
        c = np.concatenate((self.c, c))
        x = np.concatenate((self.initial_plan, initial_plan))
        return MainPhaseSimplex(A=A, c=c, initial_plan=x, J_b=list(J))

    def get_auxiliary_task_solution(self):
        main_phase_obj = self._get_main_phase_obj(
            A=np.eye(self.m), c=-np.ones(self.m), initial_plan=self.b, J=self.J_u
        )
        return main_phase_obj.solve()

    def case_a(self, basic_plan, basic_index):
        if np.any(basic_plan[self.n:] == 0):
            print(
                f"""Заданные ограничения несовместны => 
                      нет допустимых базисных 
                      планов ({basic_plan}, {basic_index})"""
            )
            return True

    def create_e_vector(self, J_b_asterisk, j_k):
        """e_i — единичный m-вектор с единицей на i-ом месте."""
        e = np.zeros(self.m)
        i = J_b_asterisk.index(j_k)
        e[i] = 1
        return e

    # noinspection PyPep8Naming
    def _get_inverse_A_b_asterisk(self, J_b_asterisk):
        """ Получим матрицу ((A_b)*)-1 """
        # print(self.m, len(self.A))
        extended_A = self._get_extended_A(np.eye(len(self.A)))
        # print(self.m, len(self.A), len(extended_A))
        A_b_asterisk = extended_A[self.J_b, :][:, J_b_asterisk]
        return np.linalg.inv(A_b_asterisk)

    # noinspection PyPep8Naming
    def _get_A_j(self, j_0):
        """ Получим матрицу A_j """
        extended_A = self._get_extended_A(np.eye(self.m))
        return extended_A[self.J_b, :][:, j_0].T

    def get_alpha(self, e, J_b_asterisk, j_0):
        inverse_A_b_asterisk = self._get_inverse_A_b_asterisk(J_b_asterisk)
        A_j = self._get_A_j(j_0)
        return e @ inverse_A_b_asterisk @ A_j

    @staticmethod
    def case_c1(J_b_asterisk, j_0, j_k):
        """Оптимальному плану  задачи припишем новый базис
           J_b = (J_b - j_k) + j_0 """
        k = J_b_asterisk.index(j_k)
        J_b_asterisk[k] = j_0

    def case_c2(self, J_b_asterisk, j_k):
        linearly_dependent_index = j_k - self.n
        self.J_b.remove(linearly_dependent_index)
        J_b_asterisk.remove(j_k)

    def solve(self):
        self.J = range(self.n + self.m)
        self.J_u = range(self.n, self.n + self.m)
        self.J_b = list(range(self.m))

        solution = self.get_auxiliary_task_solution()

        basic_plan, J_b_asterisk = solution.x, solution.Jb

        if self.case_a(basic_plan, J_b_asterisk):
            return

        else:
            J_intersection = set(J_b_asterisk) & set(self.J_u)

            while J_intersection:
                j_k = choice(list(J_intersection))
                j_0 = choice(list(set(self.J) - set(J_b_asterisk)))

                e = self.create_e_vector(J_b_asterisk, j_k)
                alpha = self.get_alpha(e, J_b_asterisk, j_0)

                if alpha != 0:
                    self.case_c1(J_b_asterisk, j_0, j_k)
                else:
                    self.case_c2(J_b_asterisk, j_k)
                    self.m = len(self.J_b)

                J_intersection = set(J_b_asterisk) & set(self.J_u)
            # case b =>
            return basic_plan[: self.n], J_b_asterisk
