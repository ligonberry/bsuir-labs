from math import floor

import numpy as np
from interruptingcow import timeout
from loguru import logger

from simplex.main_phase import MainPhaseSimplex
from simplex.prepare_phase import PreparePhaseSimplex

ACCURACY = 4
EPS = 0.0001

TIMEOUT = 30


def is_timeout(function):
    def wrapper(*args, **kwargs):
        try:
            with timeout(TIMEOUT, exception=RuntimeError):
                iteration_count = 1
                while True:
                    logger.warning(f"Итерация №{iteration_count}")
                    iteration_count += 1
                    result = function(*args, **kwargs)
                    if result:
                        return result
        except RuntimeError:
            logger.error(
                f"Функция {function.__name__} выполняется дольше " f"{TIMEOUT} секунд"
            )

    return wrapper


class GomoriMethodSolver:
    def __init__(self, A, b, c):
        self.A = A
        self.b = b
        self.c = c

        self.m, self.n = A.shape

        # Обратная матрица
        self.B = None

        # Базисные и небазисные индексы
        self.Jb = None
        self.Jn = None

        # Оптимальное целочисленное решение
        self.x = None
        self.cost = None

    @staticmethod
    def is_integer(val):
        return abs(round(val) - val) < EPS

    def is_integer_array(self, x):
        return all([self.is_integer(val) for val in x])

    def get_float_elem_index(self, x):
        for i, val in enumerate(x):
            if not self.is_integer(val):
                return i

    def update_properties(self, result):
        for key, _ in vars(self).items():
            value = result.__dict__.get(key)
            # logger.warning(f"Было {key} = {_}")
            if value is not None:
                # logger.info(f"Установим {key} = {value}")
                setattr(self, key, value)

    @staticmethod
    def _round(number):
        return round(number, ACCURACY)

    def _round_array(self, array):
        result = []
        for number in array:
            result.append(self._round(number))
        return result

    def get_y(self):
        # Выберем искусственную переменную (это обязательно
        # базисная переменная) x_i0, i0 ∈ Jb - оптимальный базис
        # текущей задачи линейного программирования.
        i0 = self.get_float_elem_index(self.x)
        j_i0 = self.Jb.index(i0)

        e_i0 = np.eye(1, self.m, j_i0)[0]
        return e_i0 @ self.B

    # noinspection PyPep8Naming
    def change_A(self, y):
        a = y @ self.A

        f_j = np.array([el - floor(el) for el in a])
        self.A = np.vstack([self.A, f_j])
        logger.debug(f"Новое ограничение (строка матрица А) = {f_j}")

        # Добавляем новую колонку в матрицу, потому что
        # с новым ограничением добавлятся новая переменная
        column = -np.eye(1, self.m + 1, self.m)[0]
        self.A = np.hstack([self.A, column.reshape(self.m + 1, 1)])
        logger.debug(
            f"Новое ограничение (столбец матрицы А) = {self._round_array(column)}"
        )

    def change_b(self, y):
        b = y @ self.b
        f = b - floor(b)
        self.b = np.append(self.b, [f])
        logger.debug(f"Новое ограничение (вектор b) = {self._round_array(self.b)}")

    def change_c(self):
        self.c = np.append(self.c, [0])
        logger.debug(f"Новое ограничение (вектор с) = {self._round_array(self.c)}")

    def change_shapes(self):
        self.m += 1
        self.n += 1

    def change_restrictions(self, y):
        self.change_A(y)
        self.change_b(y)
        self.change_c()
        self.change_shapes()

    def create_cutting_plane(self):
        y = self.get_y()
        logger.debug(f"Сформируем отсекающую плоскость (ограничение): y = {y}")

        self.change_restrictions(y)

    def show(self):
        logger.success(
            f"Решение задачи целочисленного "
            f"программирования = {self._round_array(self.x)}"
        )
        logger.success(f"Значение целевой функции = {self._round(self.cost)}")

    # noinspection PyPep8Naming
    @is_timeout
    def solve(self):
        # ШАГ 1: Найти оптимальное решение
        # задачи линейного программирования.
        x, J = PreparePhaseSimplex(A=self.A, b=self.b).solve()
        solution = MainPhaseSimplex(A=self.A, c=self.c, initial_plan=x, J_b=J).solve()
        logger.debug(
            f"Решение задачи линейного "
            f"программирования: {self._round_array(solution.x)}"
        )

        self.update_properties(solution)

        # ШАГ 3: Если все неискусственные переменные задачи
        # линейного программирования целые => STOP
        if self.is_integer_array(self.x):
            self.cost = self.x @ self.c
            self.show()
            return self

        # ШАГ 4: Сформируем отсекающую плоскость (ограничения)
        self.create_cutting_plane()
