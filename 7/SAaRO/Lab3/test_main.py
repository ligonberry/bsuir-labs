import numpy as np
from loguru import logger

from main import GomoriMethodSolver


# noinspection PyPep8Naming
def setup_example_0():
    A = np.array([[7, 4, 1]])
    b = np.array([13])
    c = np.array([21, 11, 0])
    return GomoriMethodSolver(A, b, c)


def test_example_0():
    x0 = setup_example_0()
    solution = x0.solve()
    assert int(solution.cost) == 33


# noinspection PyPep8Naming
def setup_example_1():
    A = np.array([
        [5, -1, 1, 0, 0],
        [-1, 2, 0, 1, 0],
        [-7, 2, 0, 0, 1]
    ])
    b = np.array([15, 6, 0])
    c = np.array([-3.5, 1, 0, 0, 0])
    return GomoriMethodSolver(A, b, c)


def test_example_1():
    x0 = setup_example_1()
    solution = x0.solve()
    assert int(solution.cost) == 0


# noinspection PyPep8Naming
def setup_1():
    A = np.array([
        [1, -5, 3, 1, 0, 0],
        [4, -1, 1, 0, 1, 0],
        [2, 4, 2, 0, 0, 1]
    ])
    b = np.array([-8, 22, 30])
    c = np.array([7, -2, 6, 0, 5, 2])
    return GomoriMethodSolver(A, b, c)


def test_1():
    x0 = setup_1()
    solution = x0.solve()
    assert int(solution.cost) == 160


if __name__ == "__main__":
    logger.warning("\nTEST EXAMPLE 0")
    test_example_0()
    logger.warning("\nTEST EXAMPLE 1")
    test_example_1()
    logger.warning("\nTEST 1")
    test_1()
