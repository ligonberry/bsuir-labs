from typing import NamedTuple

from loguru import logger

from datetime import timedelta
from timeit import default_timer as timer

from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import dijkstra

from dijkstra.array import DijkstraArrayMethodSolver
from dijkstra.heap import DijkstraHeapMethodSolver


def solve_dijkstra_array(S, n, s):
    logger.warning(f"Реализация алгоритма Дейкстры на массивах")
    start = timer()
    solver = DijkstraArrayMethodSolver(S, n, s).solve()
    end = timer()
    logger.info(
        f"Воемя выполнения для алгоритма Дейкстры на массивах = {timedelta(seconds=end - start)}"
    )
    return solver.shortest_tree


def solve_dijkstra_heap(S, n, s):
    logger.warning(f"Реализация алгоритма Дейкстры на куче")
    start = timer()
    solver = DijkstraHeapMethodSolver(S, n, s).solve()
    end = timer()
    logger.info(
        f"Время выполнения для алгоритма Дейкстры на куче = {timedelta(seconds=end - start)}"
    )
    return solver.shortest_tree


def solve_dijkstra_scipy(D):
    logger.warning(f"Реализация стандартного алгоритма Дейкстры")
    start = timer()
    solver = dijkstra(D, directed=True, return_predecessors=True)
    end = timer()
    logger.info(
        f"Время выполнения стандартного алгоритма Дейкстры = {timedelta(seconds=end - start)}"
    )
    return solver


class Arc(NamedTuple):
    s: int
    t: int
    distance: int


def test_example_1():
    logger.critical("TEST EXAMPLE 1")
    n = 7
    s = 0

    # density = 2E / V*(V-1) = 0.57
    S = [
        Arc(0, 1, 12),
        Arc(0, 5, 1),
        Arc(5, 1, 10),
        Arc(1, 2, 2),
        Arc(6, 1, 2),
        Arc(5, 6, 8),
        Arc(5, 4, 5),
        Arc(4, 6, 2),
        Arc(6, 2, 6),
        Arc(2, 3, 1),
        Arc(4, 3, 15),
        Arc(2, 4, 5)
    ]

    array_res = solve_dijkstra_array(S, n, s)
    heap_res = solve_dijkstra_heap(S, n, s)

    D = [
        [0, 12, 0, 0, 0, 1, 0],
        [0, 0, 2, 0, 0, 0, 0],
        [0, 0, 1, 5, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 15, 0, 0, 0, 2],
        [0, 10, 0, 5, 0, 0, 8],
        [0, 2, 6, 0, 0, 0, 0]
    ]

    graph = csr_matrix(D)
    solve_dijkstra_scipy(graph)

    result = [0, 10, 12, 13, 6, 1, 8]
    for i in range(len(result)):
        assert result[i] == array_res.get(i)
        assert result[i] == heap_res.get(i)


def test_5():
    logger.critical("TEST 5")
    n = 7
    s = 0

    # density = 2E / V*(V-1) = 0.76
    S = [
        Arc(0, 1, 4),
        Arc(0, 5, 6),
        Arc(1, 5, 3),
        Arc(1, 3, 1),
        Arc(1, 2, 7),
        Arc(2, 3, 2),
        Arc(2, 4, 5),
        Arc(3, 0, 2),
        Arc(3, 5, 4),
        Arc(3, 4, 6),
        Arc(4, 6, 1),
        Arc(5, 6, 3),
        Arc(5, 4, 2),
        Arc(6, 1, 7),
        Arc(6, 3, 3),
        Arc(6, 2, 1),
    ]

    array_res = solve_dijkstra_array(S, n, s)
    heap_res = solve_dijkstra_heap(S, n, s)

    result = [0, 4, 10, 5, 8, 6, 9]
    for i in range(len(result)):
        assert result[i] == array_res.get(i)
        assert result[i] == heap_res.get(i)


if __name__ == "__main__":
    test_example_1()
    test_5()
