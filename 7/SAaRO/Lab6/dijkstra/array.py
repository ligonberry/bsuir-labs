from collections import defaultdict

from loguru import logger


# noinspection PyTypeChecker,DuplicatedCode
class DijkstraArrayMethodSolver:
    class Graph:
        def __init__(self, n):
            self.vertex = set(range(n))
            self.nodes = defaultdict(list)
            self.distances = {}

        def add_nodes(self, s, t, distance):
            self.nodes[s].append(t)
            self.distances[(s, t)] = distance

    def initialize_graph_arcs(self, S):
        for arc in S:
            self.graph.add_nodes(arc.s, arc.t, arc.distance)
        logger.info(f"Полученный граф - {dict(self.graph.nodes)}")

    def __init__(self, S, n, start_node):

        self.start_node = start_node

        self.graph = self.Graph(n)
        self.initialize_graph_arcs(S)

        self.shortest_tree = {start_node: 0}

    def _get_min_vertex(self):
        min_vertex = None
        for vertex in self.graph.vertex:
            if vertex in self.shortest_tree:
                if min_vertex is None:
                    min_vertex = vertex
                elif self.shortest_tree[vertex] < self.shortest_tree[min_vertex]:
                    min_vertex = vertex
        return min_vertex

    def solve(self):
        iteration_count = 1
        while self.graph.vertex:

            min_vertex = self._get_min_vertex()
            min_weight = self.shortest_tree[min_vertex]
            logger.debug(
                f"Узел с минимальной стоимостью - {min_vertex}. "
                f"Минимальная стоимость - {min_weight}"
            )

            self.graph.vertex.remove(min_vertex)

            for v in self.graph.nodes[min_vertex]:
                weight = min_weight + self.graph.distances[(min_vertex, v)]
                if v not in self.shortest_tree or weight < self.shortest_tree[v]:
                    self.shortest_tree[v] = weight

            logger.debug(
                f"Дерево кратчайших путей на итерации {iteration_count}: "
                f"{self.shortest_tree}"
            )
            iteration_count += 1

        logger.success(f"Дерево кратчайших путей - {self.shortest_tree}")

        return self
