import React from 'react';
import PropTypes from 'prop-types';
import { Button, Message } from 'semantic-ui-react';
import './index.css';
import { save } from 'save-file';
import dateFormat from 'dateformat';
import TransactionsStorage from '../../util/storage/transactions';

class DataExport extends React.Component {
  handleSaveFile = async () => {
    const transactions = await TransactionsStorage.getAll();
    const accounts = this.props.state.entities.accounts;

    let csv = 'date;account;category;total;currency;description;transfer\r\n';
    
    for (const transaction of transactions) {
      const date = dateFormat(transaction.date, 'dd.mm.yyyy');
      const account = accounts.byKey[transaction.accountId]
        ? accounts.byKey[transaction.accountId].name
        : transaction.accountId;
      const tags = (transaction.tags || []).join(',');
      const amount = `${transaction.amount / 100}`.replace(".", ",");
      const currency = transaction.currency;
      const note = transaction.note || '';
      const target =
        transaction.kind === 1
          ? accounts.byKey[transaction.linkedAccountId]
            ? accounts.byKey[transaction.linkedAccountId].name
            : transaction.linkedAccountId
          : '';

      csv += `${date};${account};${tags};${amount};${currency};${note};${target}\r\n`;
    }

    save(
      new Blob([csv], { type: 'text/csv;charset=utf-8' }),
      new Date(Date.now()).toISOString() + '_export.csv'
    );
  };

  render() {
    return (
      <div className="mt-dataExport">
        <p>Export transactions to a CSV file.</p>
        {this.props.error && (
          <Message
            error
            icon="warning circle"
            header="Failed to Export"
            content={this.props.error}
          />
        )}
        {!this.props.isFileSelected && (
          <React.Fragment>
            <Button
              content="Export CSV File"
              icon="file text"
              onClick={this.handleSaveFile}
            />
          </React.Fragment>
        )}
      </div>
    );
  }
}

DataExport.propTypes = {
  error: PropTypes.string
};

export default DataExport;
