import { createHashHistory, createBrowserHistory } from 'history';
import React from 'react';
import ReactDOM from 'react-dom';
import * as Sentry from '@sentry/browser';
import 'semantic-ui-css/semantic.min.css';
import Root from './containers/Root';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store/configureStore';
import { fetchExchangeRates } from 'util/currency';
import { CURRENCY } from 'entities/Currency';

Sentry.init({
  dsn: 'https://5ae855d4c1d840c1b06679123069574f@sentry.io/1335198'
});

export const currencyRate = value => {
  if (value) {
    return localStorage.setItem('CURRENCY_RATE', JSON.stringify(value));
  }
  
  const localItem = localStorage.getItem('CURRENCY_RATE');

  if (localItem) {
    return JSON.parse(localItem);
  }

  const result = {};
  for (const key of Object.keys(CURRENCY)) {
    result[key] = 1;
  }

  return result;
}

const store = configureStore();
const history =
  process.env.REACT_APP_IS_MOBILE === 'true'
    ? createHashHistory()
    : createBrowserHistory();

export const loadCurrencyRate = async (base, target) => {
  const value = await fetchExchangeRates(base, target);
  currencyRate(value);
};

const loadCurrencyTimer = setInterval(async () => {
  if (store.getState().settings.currency.base) {
    const base = store.getState().settings.currency.base;
    
    await loadCurrencyRate(base, Object.keys(CURRENCY));
    clearInterval(loadCurrencyTimer);
  }
}, 1000);

ReactDOM.render(
  <Root store={store} history={history} />,
  document.getElementById('root')
);

registerServiceWorker();

if (module.hot) {
  module.hot.accept();
}
