import { handleActions } from 'redux-actions';
import {
  fillInTransactionForm,
  openTransactionInModal,
  changeKind,
  changeAccount,
  changeAmount,
  changeCurrency,
  changeLinkedAccount,
  changeLinkedAmount,
  changeLinkedCurrency,
  changeTags,
  changeDate,
  changeNote
} from '../../../actions/ui/form/transaction';
import { defaultKind } from '../../../entities/Transaction';
import { currencyRate } from '../../../index';

const CHARS_AFTER_DOT = 2;

const initialState = {
  kind: defaultKind,
  isModalOpen: false
};

export default handleActions(
  {
    [fillInTransactionForm]: (state, { payload }) => payload,
    [openTransactionInModal]: (state, { payload }) => ({
      ...state,
      ...payload,
      isModalOpen: true
    }),
    [changeKind]: (state, { payload }) => ({ ...state, kind: payload }),
    [changeAccount]: (state, { payload }) => ({
      ...state,
      accountId: payload.accountId,
      currency: payload.currency.includes(state.currency)
        ? state.currency
        : payload.currency[0]
    }),
    [changeLinkedAccount]: (state, { payload }) => ({
      ...state,
      linkedAccountId: payload.accountId,
      linkedCurrency: payload.currency.includes(state.linkedCurrency)
        ? state.linkedCurrency
        : payload.currency[0]
    }),
    [changeAmount]: (state, { payload }) => {
      let amount = payload;
      let linkedAmount = "";

      const currency = currencyRate();

      if (amount !== '') {
        const linked = currency[state.linkedCurrency];
        const base = currency[state.currency];

        const value = amount * (linked / base);
        linkedAmount = Number.isInteger(value)
          ? `${value}`
          : value.toFixed(CHARS_AFTER_DOT);
      }

      return {
        ...state,
        amount: payload,
        linkedAmount:
          state.currency === state.linkedCurrency ? payload : linkedAmount
      };
    },
    [changeLinkedAmount]: (state, { payload }) => {
      let amount = payload;
      let currencyAmount = "";

      const currency = currencyRate();

      if (amount !== '') {
        const linked = currency[state.linkedCurrency];
        const base = currency[state.currency];

        const value = amount * (base / linked);
        currencyAmount = Number.isInteger(value)
          ? `${value}`
          : value.toFixed(CHARS_AFTER_DOT);
      }

      return {
        ...state,
        amount:
          state.currency === state.linkedCurrency ? payload : currencyAmount,
        linkedAmount: payload
      };
    },
    [changeCurrency]: (state, { payload }) => {
      let amount = state.amount;
      let linkedAmount = "";

      const currency = currencyRate();

      if (amount !== '') {
        const linked = currency[state.linkedCurrency];
        const base = currency[payload];

        const value = amount * (linked / base);
        linkedAmount = Number.isInteger(value)
          ? `${value}`
          : value.toFixed(CHARS_AFTER_DOT);
      }

      return {
        ...state,
        currency: payload,
        linkedAmount:
          state.linkedCurrency === payload ? state.amount : linkedAmount
      };
    },
    [changeLinkedCurrency]: (state, { payload }) => {
      let amount = state.amount;
      let linkedAmount = "";

      const currency = currencyRate();

      if (amount !== '') {
        const linked = currency[payload];
        const base = currency[state.linkedCurrency];

        const value = amount * (linked / base);
        linkedAmount = Number.isInteger(value)
          ? `${value}`
          : value.toFixed(CHARS_AFTER_DOT);
      }

      return {
        ...state,
        amount: state.amount,
        linkedCurrency: payload,
        linkedAmount:
          state.currency === payload ? state.amount : linkedAmount
      };
    },
    [changeTags]: (state, { payload }) => ({
      ...state,
      tags: { ...state.tags, [state.kind]: payload }
    }),
    [changeDate]: (state, { payload }) => ({ ...state, date: payload }),
    [changeNote]: (state, { payload }) => ({ ...state, note: payload })
  },
  initialState
);
